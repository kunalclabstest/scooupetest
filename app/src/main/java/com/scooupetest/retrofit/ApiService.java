package com.scooupetest.retrofit;


import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;


public interface ApiService {

    /**
     * @param accessToken
     * @param deviceToken
     * @param latitude
     * @param longitude
     * @param appVersion
     * @param deviceType
     * @param uniqueDeviceId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/start_app_using_access_token")
    public void accessTokenLogin(@Field("access_token") String accessToken, @Field("device_token") String deviceToken, @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("app_version") int appVersion, @Field("device_type") String deviceType, @Field("unique_device_id") String uniqueDeviceId, Callback<String> callback);


    /**
     * @param userFbId
     * @param userFbName
     * @param fbAccessToken
     * @param userName
     * @param fbMail
     * @param latitude
     * @param longitude
     * @param deviceToken
     * @param country
     * @param appVersion
     * @param osVersion
     * @param deviceName
     * @param deviceType
     * @param uniqueDeviceId
     * @param otp
     * @param phoneNumber
     //* @param password
     * @param heraAboutText
     * @param callback
     */
    @FormUrlEncoded
    @POST("/customer_fb_registeration_form")
    public void sendFacebookLoginValues(@Field("user_fb_id") String userFbId, @Field("user_fb_name") String userFbName, @Field("fb_access_token") String fbAccessToken,
                                        @Field("username") String userName, @Field("fb_mail") String fbMail, @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("device_token") String deviceToken,
                                        @Field("country") String country, @Field("app_version") int appVersion, @Field("os_version") String osVersion, @Field("device_name") String deviceName, @Field("device_type") String deviceType,
                                        @Field("unique_device_id") String uniqueDeviceId, @Field("otp") String otp, @Field("ph_no") String phoneNumber, /*@Field("password") String password,*/ @Field("how_hear_us") String heraAboutText, @Field("nounce") String nounce,
                                        @Field("payment_type_flag") int paymentTypeFlag, @Field("corporate_email") String corporateEmail, @Field("corporate_otp") String corporateCode, Callback<String> callback);

    /**
     * @param nounce
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/add_credit_card")
    public void addCard(@Field("nounce") String nounce, @Field("access_token") String accessToken, @Field("payment_type_flag") int paymentTypeFlag, Callback<String> callback);


    @FormUrlEncoded
    @POST("/add_corp_account_info")
    public void addCorporateAccount(@Field("access_token") String accessToken, @Field("corporate_email") String corporateEmail, @Field("corporate_otp") String corporateCode, Callback<String> callback);


    /**
     * @param callback
     */

    @POST("/get_braintree_token")
    public void getBrainTreeClientToken(Callback<String> callback);

    /**
     * @param accessToken
     * @param oldPassword
     * @param newPassword
     * @param callback
     */

    @FormUrlEncoded
    @POST("/reset_password")
    public void resetPassword(@Field("access_token") String accessToken, @Field("old_password") String oldPassword, @Field("new_password") String newPassword, Callback<String> callback);


    /**
     * @param accessToken
     * @param currentMode
     * @param callback
     */
    @FormUrlEncoded
    @POST("/booking_history")
    public void getRidesAsync(@Field("access_token") String accessToken, @Field("current_mode") String currentMode, Callback<String> callback);

    /**
     * @param userName
     * @param phoneNo
     * @param eMail
     * @param password
     * @param otp
     * @param deviceType
     * @param uniqueDeviceId
     * @param deviceToken
     * @param latitude
     * @param longitude
     * @param country
     * @param deviceName
     * @param appVersion
     * @param osVersion
     * @param hearAboutText
     * @param makeMeDriverFlag
     * @param imageFlag
     * @param nounce
     * @param image
     * @param callback
     */
    @FormUrlEncoded
    @POST("/customer_registeration")
    public void sendSignupValues(@Field("user_name") String userName, @Field("ph_no") String phoneNo, @Field("email") String eMail, @Field("password") String password,
                                 @Field("otp") String otp, @Field("device_type") String deviceType, @Field("unique_device_id") String uniqueDeviceId, @Field("device_token") String deviceToken,
                                 @Field("latitude") Double latitude, @Field("longitude") Double longitude, @Field("country") String country, @Field("device_name") String deviceName,
                                 @Field("app_version") int appVersion, @Field("os_version") String osVersion, @Field("how_hear_us") String hearAboutText, @Field("make_me_driver_flag") String makeMeDriverFlag,
                                 @Field("image_flag") String imageFlag, @Field("nounce") String nounce, @Field("image") String image,
                                 @Field("payment_type_flag") int paymentTypeFlag, @Field("corporate_email") String corporateEmail, @Field("corporate_otp") String corporateCode, Callback<String> callback);


    /**
     * @param email
     * @param callback
     */
    @FormUrlEncoded
    @POST("/forgot_password")
    public void forgotPasswordAsync(@Field("email") String email, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/show_pickup_schedules")
    public void getFutureSchedulesAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param pickupId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/remove_pickup_schedule")
    public void removeScheduledRideAPI(@Field("access_token") String accessToken, @Field("pickup_id") String pickupId, Callback<String> callback);

    /**
     * @param section
     * @param callback
     */
    @FormUrlEncoded
    @POST("/get_information")
    public void getHelpAsync(@Field("section") String section, Callback<String> callback);

    /**
     * @param accessToken
     * @param splitId
     * @param userResponseFlag
     * @param callback
     */
    @FormUrlEncoded
    @POST("/user_response_to_split_fare_request")
    public void acceptDeclineSplitFareRequest(@Field("access_token") String accessToken, @Field("split_id") String splitId, @Field("user_response_flag") int userResponseFlag, Callback<String> callback);

    /**
     * @param accessToken
     * @param sessionId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/cancel_the_request")
    public void cancelCustomerRequestAsync(@Field("access_token") String accessToken, @Field("session_id") String sessionId, Callback<String> callback);


    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/logout")
    public void logoutAsync(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param latitude
     * @param longitude
     * @param callback
     */
    @FormUrlEncoded
    @POST("/request_now")
    public void noDriverAsync(@Field("access_token") String accessToken, @Field("latitude") String latitude, @Field("longitude") String longitude, Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/skip_rating_by_customer")
    public void skipFeedbackForCustomerAsync(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, Callback<String> callback);


    /**
     * @param accessToken
     * @param givenRating
     * @param engagementId
     * @param driverId
     * @param feedback
     * @param callback
     */
    @FormUrlEncoded
    @POST("/rate_the_driver")
    public void submitFeedbackToDriverAsync(@Field("access_token") String accessToken, @Field("given_rating") String givenRating, @Field("engagement_id") String engagementId,
                                            @Field("driver_id") String driverId, @Field("feedback") String feedback, Callback<String> callback);

    /**
     * @param phoneNo
     * @param callback
     */
    @FormUrlEncoded
    @POST("/send_otp_via_call")
    public void initiateOTPCallAsync(@Field("phone_no") String phoneNo, Callback<String> callback);

    /**
     * @param accessToken
     * @param userName
     * @param backAccountToken
     * @param imageFlag
     * @param image
     * @param callback
     */
    @Multipart
    @POST("/edit_customer_profile")
    public void updateProfile(@Part("access_token") TypedString accessToken, @Part("user_name") TypedString userName, @Part("bank_account_stripe_token") TypedString backAccountToken,
                              @Part("image_flag") TypedString imageFlag, @Part("image") TypedFile image, Callback<String> callback);

    /**
     * @param cardId
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/delete_credit_card")
    public void deleteCard(@Field("card_id") String cardId, @Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/list_credit_cards")
    public void getAvailablePaymentMethods(@Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param cardId
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/change_default_credit_card")
    public void makeDefaultCard(@Field("card_id") String cardId, @Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param latitude
     * @param longitude
     * @param pickUpTime
     * @param carType
     * @param manualLatitude
     * @param manualLongitude
     * @param manualAddress
     * @param callback
     */
    @FormUrlEncoded
    @POST("/insert_pickup_schedule")
    public void insertScheduleRideAsync(@Field("access_token") String accessToken, @Field("latitude") String latitude, @Field("longitude") String longitude,
                                        @Field("pickup_time") String pickUpTime, @Field("car_type") int carType, @Field("manual_destination_latitude") Double manualLatitude,
                                        @Field("manual_destination_longitude") Double manualLongitude, @Field("manual_destination_address") String manualAddress, Callback<String> callback);

    /**
     * @param accessToken
     * @param pickupId
     * @param latitude
     * @param longitude
     * @param pickUpTime
     * @param manualLatitude
     * @param manualLongitude
     * @param manualAddress
     * @param callback
     */
    @FormUrlEncoded
    @POST("/modify_pickup_schedule")
    public void modifyScheduleRideAsync(@Field("access_token") String accessToken, @Field("pickup_id") String pickupId, @Field("latitude") Double latitude, @Field("longitude") Double longitude,
                                        @Field("pickup_time") String pickUpTime, @Field("manual_destination_latitude") Double manualLatitude,
                                        @Field("manual_destination_longitude") Double manualLongitude, @Field("manual_destination_address") String manualAddress, Callback<String> callback);

    /**
     * @param email
     * @param password
     * @param deviceType
     * @param uniqueDeviceId
     * @param latitude
     * @param longitude
     * @param country
     * @param deviceName
     * @param appVersion
     * @param osVersion
     * @param callback
     */
    @FormUrlEncoded
    @POST("/email_login")
    public void sendLoginValues(@Field("email") String email, @Field("password") String password, @Field("device_type") String deviceType, @Field("unique_device_id") String uniqueDeviceId,
                                @Field("device_token") String deviceToken, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("country") String country, @Field("device_name") String deviceName,
                                @Field("app_version") int appVersion, @Field("os_version") String osVersion, Callback<String> callback);

    /**
     * @param accessToken
     * @param engagementId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/split_fare_screen")
    public void getSplitFareInfo(@Field("access_token") String accessToken, @Field("engagement_id") String engagementId, Callback<String> callback);

    /**
     * @param splitId
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/resend_split_fare_request")
    public void reSendSplitFareRequest(@Field("split_id") String splitId, @Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param phoneNo
     * @param engagementId
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/send_split_fare_request")
    public void sendSplitFareRequest(@Field("phone_no") String phoneNo, @Field("engagement_id") String engagementId, @Field("access_token") String accessToken, Callback<String> callback);

    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/payment_on_ride_completion")
    public void payFare(@Field("access_token") String accessToken, @Field("tip") String tip, @Field("engagement_id") String engagementId, @Field("payment_method") String paymentMethod,
                        @Field("to_pay") String toPay, Callback<String> callback);

    @GET("/json")
    public void getDistanceMatrixData(@Query("origins") String origins, @Query("destinations") String destination, @Query("language") String language ,Callback<String> callback);

    @GET("/json")
    public String getDirectionUrl(@Query("origin") String origin, @Query("destination") String destination, @Query("sensor") String sensor);

    @GET("/json")
    public String getAutoCompleteSearchResults(@Query("input") String input, @Query("type") String type, @Query("location") String location, @Query("radius") String radius, @Query("key") String key);


    @GET("/json")
    public String getSearchResultsFromPlaceId(@Query("placeid") String placeId, @Query("key") String key);

    @GET("/json")
    public String getGeocodeApi(@Query("latlng") String latLng, @Query("sensor") String sensor);

    @FormUrlEncoded
    @POST("/find_a_driver")
    public void findNearbyDrivers(@Field("access_token") String accessToken, @Field("latitude") double latitude, @Field("longitude") double longitude, @Field("car_type") int carType,
                                  Callback<String> callback);
    /**
     * @param accessToken
     * @param engagementId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/split_fare_screen")
    public void getSplitFareDataFromServer(@Field("access_token")String accessToken,@Field("engagement_id")String engagementId,Callback<String> callback );
  /**
     * @param accessToken
     * @param promoCode
     * @param callback
     */
    @FormUrlEncoded
    @POST("/redeem_promotion")
    public void applyPromoCodeAPI(@Field("access_token") String accessToken, @Field("promo_code") String promoCode, Callback<String> callback);


    /**
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/get_all_promotion")
    public void getAvailablePromoCodes(@Field("access_token") String accessToken, Callback<String> callback);

    @FormUrlEncoded
    @POST("/set_default_promo")
    public void makeDefaultPromoCode(@Field("promo_id") String cardId, @Field("access_token") String accessToken, Callback<String> callback);

    @FormUrlEncoded
    @POST("/cancel_the_ride_by_customer")
    public void cancelRideAfterAccept(@Field("engagement_id") String engagementId, @Field("access_token") String accessToken, Callback<String> callback);

    @FormUrlEncoded
    @POST("/send_sos")
    public void sendSosSms( @Field("access_token") String accessToken,@Field("ids") String emergencyContactsId, Callback<String> callback);

    //EmergencyContacts API

    /**
     *
     * @param accessToken
     * @param callback
     */
    @FormUrlEncoded
    @POST("/list_sos")
    public void getEmergencyContacts(@Field("access_token") String accessToken,  Callback<String> callback);

    @FormUrlEncoded
    @POST("/add_sos")
    public void addEmergencyContact(@Field("access_token") String accessToken, @Field("sos_no") String number,@Field("sos_name") String name, Callback<String> callback);

    @FormUrlEncoded
    @POST("/remove_sos")
    public void deleteEmergencyContact(@Field("access_token") String accessToken, @Field("id") String id, Callback<String> callback);


    /**
     * @param accessToken
     */
    @FormUrlEncoded
    @POST("/get_current_user_status")
    public String getUserStatus(@Field("access_token")String accessToken);

    /**
     * @param accessToken
     * @param driverId
     * @param callback
     */
    @FormUrlEncoded
    @POST("/get_driver_current_location")
    public void getDriverLocation(@Field("access_token")String accessToken, @Field("driver_id")String driverId, Callback<String >callback);

    /**
     * @param accessToken
     * @param latitude
     * @param longitude
     * @param carType
     * @param currentLatitude
     * @param currentLongitude
     * @param duplicateFlag
     * @param locationAccuracy
     * @param manualDestinationAddress
     * @param manualDestinationLatitude
     * @param manualDestinationLongitude
     * @param timezoneOffset
     * @param callback
     */
    @FormUrlEncoded
    @POST("/request_ride")
    public void requestRide(@Field("access_token")String accessToken, @Field("latitude")String latitude,@Field("longitude")String longitude,
                            @Field("car_type")String carType,@Field("timezone_offset")String timezoneOffset,
                            @Field("manual_destination_latitude")String manualDestinationLatitude,@Field("manual_destination_longitude")String manualDestinationLongitude,
                            @Field("manual_destination_address")String manualDestinationAddress, @Field("current_latitude")String currentLatitude,
                            @Field("current_longitude")String currentLongitude, @Field("duplicate_flag")String duplicateFlag,
                            @Field("location_accuracy")String locationAccuracy, Callback<String> callback);

    @FormUrlEncoded
    @POST("/get_fare_by_region")
    public void getFareByRegion(@Field("latitude")String latitude,@Field("longitude")String longitude, Callback<String >callback);
}