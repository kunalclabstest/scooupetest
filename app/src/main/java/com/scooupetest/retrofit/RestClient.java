//package rmn.offlinemap;
//
//import retrofit.RestAdapter;
//
//public class RestClient {
//    public static final String BASE_URL = "http://api.deets.clicklabs.in:1441";
//    private  ApiService apiService;
//
//    public RestClient() {
////        Gson gson = new GsonBuilder()
////                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
////                .create();
//
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setEndpoint(BASE_URL)
//                .build();
//        apiService = restAdapter.create(ApiService.class);
//
//
//    }
//
//    public ApiService getApiService() {
//        return apiService;
//    }
//}


package com.scooupetest.retrofit;

import com.scooupetest.Data;

import retrofit.RestAdapter;

public class RestClient {
    // public static final String BASE_URL = "http://api.deets.clicklabs.in:1441";
    private static ApiService apiService = null;
    private static ApiService distanceMatrixApiService = null, autoCompleteSearchResult = null,
            searchResultsFromPalceIdService = null, directionApiService = null, geoCodeApiService = null;


    public static ApiService getApiService() {
        if (apiService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Data.SERVER_URL)
                    .setConverter(new StringConverter())
                    .build();
            apiService = restAdapter.create(ApiService.class);
        }
        return apiService;
    }

    public static ApiService getDistanceMatrixApiService() {


        if (distanceMatrixApiService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("https://maps.googleapis.com/maps/api/distancematrix")
                    .setConverter(new StringConverter())
                    .build();
            distanceMatrixApiService = restAdapter.create(ApiService.class);
        }


        return distanceMatrixApiService;
    }

    public static ApiService getAutoCompleteSearchResult() {

        if (autoCompleteSearchResult == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("https://maps.googleapis.com/maps/api/place/autocomplete")
                    .setConverter(new StringConverter())
                    .build();
            autoCompleteSearchResult = restAdapter.create(ApiService.class);
        }

        return autoCompleteSearchResult;
    }

    public static ApiService getSearchResultsFromPlaceId() {
        if (searchResultsFromPalceIdService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("https://maps.googleapis.com/maps/api/place/details")
                    .setConverter(new StringConverter())
                    .build();
            searchResultsFromPalceIdService = restAdapter.create(ApiService.class);
        }

        return searchResultsFromPalceIdService;
    }

    public static ApiService getDirectionsUrl() {


        if (directionApiService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://maps.googleapis.com/maps/api/directions")
                    .setConverter(new StringConverter())
                    .build();
            directionApiService = restAdapter.create(ApiService.class);
        }


        return directionApiService;
    }

    public static ApiService getGeocodeApi() {


        if (geoCodeApiService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://maps.googleapis.com/maps/api/geocode")
                    .setConverter(new StringConverter())
                    .build();
            geoCodeApiService = restAdapter.create(ApiService.class);
        }


        return geoCodeApiService;
    }
}
