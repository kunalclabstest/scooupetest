package com.scooupetest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.customlayouts.ProgressWheel;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.datastructure.EmergencyContactsData;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;


public class SendStatusActivity extends BaseActivity {



    RelativeLayout backBtn,relativeProgress;


    static final int PICK_CONTACT = 1;
    ListView listViewEmergencyContacts;
    EmergencyContactsListAdapter emergencyContactsListAdapter;
    ArrayList<EmergencyContactsData> emergencyContactsList = new ArrayList<EmergencyContactsData>();

    ListView listViewOtherContacts;
    OtherContactsListAdapter otherContactsListAdapter;
    ArrayList<EmergencyContactsData> otherContactsList = new ArrayList<EmergencyContactsData>();

    LinearLayout linearOtherContacts;

    Button sendToMoreBtn,sendBtn;
    Boolean isAlertSend=false;
    ProgressWheel progress;

    TextView tvNnoEmergencyContacts,tvSendStatusToOthers;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_status);

        LinearLayout relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(SendStatusActivity.this, relative, 1134, 720, false);

        //Top Layout
        backBtn = (RelativeLayout) findViewById(R.id.backBtn);
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        sendBtn = (Button) findViewById(R.id.btnSend);
        sendBtn.setTypeface(Data.getFont(getApplicationContext()));
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedContactsId="",otherContactsNumber="";
                for(int i=0;i<emergencyContactsList.size();i++)
                {
                    if(emergencyContactsList.get(i).isContactSelected)
                    {
                        selectedContactsId=emergencyContactsList.get(i).id+","+selectedContactsId;
                    }
                }
                for(int i=0;i<otherContactsList.size();i++)
                {
                    if(otherContactsList.get(i).isContactSelected)
                    {
                        otherContactsNumber=otherContactsList.get(i).phoneNo+","+otherContactsNumber;
                    }
                }
                if(!selectedContactsId.equalsIgnoreCase("") || !otherContactsNumber.equalsIgnoreCase(""))
                {
                    sendAlert(SendStatusActivity.this,selectedContactsId,otherContactsNumber);
                }
                else
                {
                    new DialogPopup().alertPopup(SendStatusActivity.this, "", getString(R.string.select_atleast_one_contact));
                }

            }
        });

        //Emergency Contacts Layout
        TextView tvEmergencyContacts = (TextView) findViewById(R.id.tvEmergencyContacts);
        tvEmergencyContacts.setTypeface(Data.getFont(getApplicationContext()));
         tvNnoEmergencyContacts = (TextView) findViewById(R.id.tvNnoEmergencyContacts);
        tvNnoEmergencyContacts.setTypeface(Data.getFont(getApplicationContext()));
        listViewEmergencyContacts = (com.scooupetest.utils.NonScrollListView) findViewById(R.id.listViewEmergencyContacts);


        emergencyContactsListAdapter = new EmergencyContactsListAdapter();
        listViewEmergencyContacts.setAdapter(emergencyContactsListAdapter);
        progress=(ProgressWheel)findViewById(R.id.progress_wheel);
        progress.spin();
        relativeProgress=(RelativeLayout)findViewById(R.id.relativeProgress);


        //OtherContacts Layout
        TextView tvOtherContacts = (TextView) findViewById(R.id.tvOtherContacts);
        tvOtherContacts.setTypeface(Data.getFont(getApplicationContext()));
        tvSendStatusToOthers = (TextView) findViewById(R.id.tvSendStatusToOthers);
        tvSendStatusToOthers.setTypeface(Data.getFont(getApplicationContext()));
        listViewOtherContacts = (com.scooupetest.utils.NonScrollListView) findViewById(R.id.listViewOtherContacts);

        otherContactsListAdapter = new OtherContactsListAdapter();
        listViewOtherContacts.setAdapter(otherContactsListAdapter);
        linearOtherContacts=(LinearLayout)findViewById(R.id.linearOtherContacts);

        sendToMoreBtn = (Button) findViewById(R.id.btnSendToMore);
        sendToMoreBtn.setTypeface(Data.getFont(getApplicationContext()));
        sendToMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                    startActivityForResult(intent, PICK_CONTACT);
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        getEmergencyContacts();

    }



    /**
     * ASync for sos to server
     */
    public void sendAlert(final Activity activity,String selectedContactsId,String otherContactsNumber) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));


            RestClient.getApiService().sendSosSms(Data.userData.accessToken,selectedContactsId + otherContactsNumber, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    Log.i("Server response SOs", "response = " + responseString);
                    try {
                        JSONObject jObj = new JSONObject(responseString);

                        if (!jObj.isNull("error")) {

                            String errorMessage = jObj.getString("error");

                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        }
                        else
                        {
                            int flag = jObj.getInt("flag");
                            if (ApiResponseFlags.ORDER_PLACED.getOrdinal() == flag) {

                                String message = jObj.getString("message");
                                isAlertSend=true;
                                new DialogPopup().alertPopup(activity, "", message);


                            } else {
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }
                        }

                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }
                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    DialogPopup.dismissLoadingDialog();
                }
            });

        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backBtn.performClick();
    }

    /**
     * API call to get Emergency Contacts from server
      */
    public void getEmergencyContacts() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

           progress.setVisibility(View.VISIBLE);
            tvNnoEmergencyContacts.setVisibility(View.GONE);
            listViewEmergencyContacts.setVisibility(View.GONE);
            Log.i("access_token", "=" + Data.userData.accessToken);

            RestClient.getApiService().getEmergencyContacts(Data.userData.accessToken, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {


                            String errorMessage = jObj
                                    .getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(SendStatusActivity.this);
                            } else {
                                tvNnoEmergencyContacts.setVisibility(View.VISIBLE);
                                tvNnoEmergencyContacts.setText(errorMessage+" Tap to retry.");
                                listViewEmergencyContacts.setVisibility(View.GONE);
                                progress.setVisibility(View.GONE);
//                                new DialogPopup().alertPopup(
//                                        SendStatusActivity.this, "",
//                                        errorMessage);
//
                            }
                        } else {
                            emergencyContactsList.clear();
                            JSONArray emergencyContactsArray = jObj.getJSONObject("data").getJSONArray("sos");

                            for (int i = 0; i < emergencyContactsArray.length(); i++) {

                                JSONObject emergencyContactsObject = emergencyContactsArray.getJSONObject(i);
                                String phoneNumber = emergencyContactsObject.getString("sos_phone_no");
                                emergencyContactsList.add(new EmergencyContactsData(emergencyContactsObject.getString("sos_name"), phoneNumber, emergencyContactsObject.getString("id"),true));
                            }

                            if (emergencyContactsList.size() > 0) {
                                tvNnoEmergencyContacts.setVisibility(View.GONE);
                                listViewEmergencyContacts.setVisibility(View.VISIBLE);
                                emergencyContactsListAdapter.notifyDataSetChanged();
                                relativeProgress.setVisibility(View.GONE);
//                                if(emergencyContactsList.size()>=5)
//                                {
//                                    listViewEmergencyContacts.getLayoutParams().height = (int) (500 * ASSL
//                                            .Yscale());
//
//                                }

                            } else {
                                relativeProgress.setVisibility(View.VISIBLE);
                                tvNnoEmergencyContacts.setVisibility(View.VISIBLE);
                                listViewEmergencyContacts.setVisibility(View.GONE);
                                tvNnoEmergencyContacts.setText(getString(R.string.no_contact_added));

                            }

                            Log.v("json response = ", jObj.toString()
                                    + "");


                        }

                        progress.setVisibility(View.GONE);

                    } catch (Exception exception) {
                        exception.printStackTrace();
                        relativeProgress.setVisibility(View.VISIBLE);
                        tvNnoEmergencyContacts.setVisibility(View.VISIBLE);
                        tvNnoEmergencyContacts.setText(Data.SERVER_ERROR_MSG +" "+getString(R.string.tap_to_retry));
                        listViewEmergencyContacts.setVisibility(View.GONE);
                        progress.setVisibility(View.GONE);
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());

//                    new DialogPopup().alertPopup(SendStatusActivity.this,
//                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                    relativeProgress.setVisibility(View.VISIBLE);
                    tvNnoEmergencyContacts.setVisibility(View.VISIBLE);
                    tvNnoEmergencyContacts.setText(Data.SERVER_NOT_RESOPNDING_MSG + " "+getString(R.string.tap_to_retry));
                    listViewEmergencyContacts.setVisibility(View.GONE);
                    progress.setVisibility(View.GONE);
                }
            });
        } else {
            relativeProgress.setVisibility(View.VISIBLE);
            tvNnoEmergencyContacts.setVisibility(View.VISIBLE);
            tvNnoEmergencyContacts.setText(getString(R.string.check_internet_message)+ " "+getString(R.string.tap_to_retry));
            listViewEmergencyContacts.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
        }

    }

    /**
     * To get selected contact Data from dialer app
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    if (data != null) {
                        Uri uri = data.getData();

                        if (uri != null) {
                            Cursor c = null;
                            try {
                                c = getContentResolver().query(uri, new String[]{
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                ContactsContract.CommonDataKinds.Phone.TYPE,ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                                        null, null, null);

                                if (c != null && c.moveToFirst()) {
                                    String number = c.getString(0);
                                    int type = c.getInt(1);
                                    number = number.replace(" ", "");
                                    number = number.replace("(", "");
                                    number = number.replace("/", "");
                                    number = number.replace(")", "");
                                    number = number.replace("N", "");
                                    number = number.replace(",", "");
                                    number = number.replace("*", "");
                                    number = number.replace(";", "");
                                    number = number.replace("#", "");
                                    number = number.replace("-", "");
                                    number = number.replace(".", "");
                                    Log.i("Type=" + type, "Number after trim=" + number);

                                    Boolean isRequsetAlreadySent = false;
                                   // otherContactsList.clear();

                                    for (int i = 0; i < emergencyContactsList.size(); i++) {
                                        if (emergencyContactsList.get(i).phoneNo.equalsIgnoreCase(number)) {
                                            isRequsetAlreadySent = true;
                                            break;
                                        }
                                    }

                                    for (int i = 0; i < otherContactsList.size(); i++) {
                                        if (otherContactsList.get(i).phoneNo.equalsIgnoreCase(number)) {
                                            isRequsetAlreadySent = true;
                                            break;
                                        }
                                    }

//                                    if(emergencyContactsList.size()>=4)
//                                    {
//                                        sendToMoreBtn.setVisibility(View.GONE);
//                                    }
                                    if (!isRequsetAlreadySent) {
                                       otherContactsList.add(new EmergencyContactsData(c.getString(2), number,"",true));
                                        otherContactsListAdapter.notifyDataSetChanged();
                                        tvSendStatusToOthers.setVisibility(View.GONE);
                                        listViewOtherContacts.setVisibility(View.VISIBLE);
                                    } else {
                                        new DialogPopup().alertPopup(
                                                SendStatusActivity.this, "",
                                                getString(R.string.contact_already_added));
                                    }
                                    Log.i("isRequsetAlreadySent=", "=" + isRequsetAlreadySent);
                                    Log.i("Type=" + type, "Number=" + number);
                                }
                            } finally {
                                if (c != null) {
                                    c.close();
                                }
                            }
                        }
                    }

                }
                break;

        }
    }


    //Adapter to set Emergency Contacts
    class ViewHolderHelp {
        TextView name, number;
        LinearLayout relative;
        ImageView imgDeleteContact,imgSelectUnselectContact;
        int id;
        RelativeLayout relativeSelectUnselectContact;

    }

    class EmergencyContactsListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderHelp holder;

        public EmergencyContactsListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return emergencyContactsList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderHelp();
                convertView = mInflater.inflate(R.layout.list_item_emergency_contacts, null);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.name.setTypeface(Data.getFont(getApplicationContext()));
                holder.number = (TextView) convertView.findViewById(R.id.number);
                holder.number.setTypeface(Data.getFont(getApplicationContext()));
                holder.imgDeleteContact = (ImageView) convertView.findViewById(R.id.imgDeleteContact);
                holder.imgSelectUnselectContact = (ImageView) convertView.findViewById(R.id.imgSelectUnselectContact);
                holder.relativeSelectUnselectContact = (RelativeLayout) convertView.findViewById(R.id.relativeselectUnselectContact);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, ViewGroup.LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderHelp) convertView.getTag();
            }

            holder.id = position;

            holder.name.setText(emergencyContactsList.get(position).userName);

            holder.number.setText(emergencyContactsList.get(position).phoneNo);
            holder.imgDeleteContact.setVisibility(View.GONE);
            holder.relativeSelectUnselectContact.setVisibility(View.VISIBLE);
            if(emergencyContactsList.get(position).isContactSelected)
                {
                    holder.imgSelectUnselectContact.setBackgroundResource(R.drawable.user_checked);
                } else {
                    holder.imgSelectUnselectContact.setBackgroundResource(R.drawable.user_unchecked);

                }

            holder.relativeSelectUnselectContact.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.i("position", "==" + position);
                    if (emergencyContactsList.get(position).isContactSelected) {
                        emergencyContactsList.get(position).isContactSelected=false;
                    } else {
                        emergencyContactsList.get(position).isContactSelected=true;
                    }
                    emergencyContactsListAdapter.notifyDataSetChanged();
                }
            });

            return convertView;
        }

    }



    //Adapter to set Other Contacts
    class ViewHolder {
        TextView name, number;
        LinearLayout relative;
        ImageView imgDeleteContact,imgSelectUnselectContact;
        int id;
        RelativeLayout relativeSelectUnselectContact;

    }

    class OtherContactsListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolder holder;

        public OtherContactsListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return otherContactsList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.list_item_emergency_contacts, null);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.name.setTypeface(Data.getFont(getApplicationContext()));
                holder.number = (TextView) convertView.findViewById(R.id.number);
                holder.number.setTypeface(Data.getFont(getApplicationContext()));
                holder.imgDeleteContact = (ImageView) convertView.findViewById(R.id.imgDeleteContact);
                holder.imgSelectUnselectContact = (ImageView) convertView.findViewById(R.id.imgSelectUnselectContact);
                holder.relativeSelectUnselectContact = (RelativeLayout) convertView.findViewById(R.id.relativeselectUnselectContact);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, ViewGroup.LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.id = position;

            holder.name.setText(otherContactsList.get(position).userName);

            holder.number.setText(otherContactsList.get(position).phoneNo);
            holder.imgDeleteContact.setVisibility(View.GONE);
            holder.relativeSelectUnselectContact.setVisibility(View.VISIBLE);
            if (otherContactsList.get(position).isContactSelected) {
                holder.imgSelectUnselectContact.setBackgroundResource(R.drawable.user_checked);

            } else {
                holder.imgSelectUnselectContact.setBackgroundResource(R.drawable.user_unchecked);

            }

            holder.relativeSelectUnselectContact.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.i("position", "==" + position);
                    if (otherContactsList.get(position).isContactSelected) {
                        otherContactsList.get(position).isContactSelected=false;
                    } else {
                        otherContactsList.get(position).isContactSelected=true;
                    }
                    otherContactsListAdapter.notifyDataSetChanged();
                }
            });

            return convertView;
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus && isAlertSend)
        {
            isAlertSend=false;
            backBtn.performClick();
        }
    }
}
