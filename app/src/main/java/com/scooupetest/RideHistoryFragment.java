package com.scooupetest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.datastructure.RideInfo;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.DateOperations;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class RideHistoryFragment extends Fragment {

    ProgressBar progressBar;
    TextView textViewInfoDisplay;
    ListView listView;

    RideHistoryListAdapter rideHistoryListAdapter;

    RelativeLayout main;


    ArrayList<RideInfo> rides = new ArrayList<RideInfo>();

    public RideHistoryFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rides.clear();
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        main = (RelativeLayout) rootView.findViewById(R.id.main);
        main.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ASSL.DoMagic(main);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        textViewInfoDisplay = (TextView) rootView.findViewById(R.id.textViewInfoDisplay);
        textViewInfoDisplay.setTypeface(Data.getFont(getActivity()));
        listView = (ListView) rootView.findViewById(R.id.listView);

        rideHistoryListAdapter = new RideHistoryListAdapter(getActivity());
        listView.setAdapter(rideHistoryListAdapter);

        progressBar.setVisibility(View.GONE);
        textViewInfoDisplay.setVisibility(View.GONE);

        textViewInfoDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRidesAsync(getActivity());
            }
        });

        getRidesAsync(getActivity());

        return rootView;
    }


    public void updateListData(String message, boolean errorOccurred) {
        if (errorOccurred) {
            textViewInfoDisplay.setText(message);
            textViewInfoDisplay.setVisibility(View.VISIBLE);

            rides.clear();
            rideHistoryListAdapter.notifyDataSetChanged();
        } else {
            if (rides.size() == 0) {
                textViewInfoDisplay.setText(message);
                textViewInfoDisplay.setVisibility(View.VISIBLE);
            } else {
                textViewInfoDisplay.setVisibility(View.GONE);
            }
            rideHistoryListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * ASync for get rides from server
     */
    public void getRidesAsync(final Activity activity) {

        if (AppStatus.getInstance(activity).isOnline(activity)) {
            progressBar.setVisibility(View.VISIBLE);
            textViewInfoDisplay.setVisibility(View.GONE);
            RestClient.getApiService().getRidesAsync(Data.userData.accessToken, "0", new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.d("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);
                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                updateListData("Some error occurred. Tap to retry", true);
                            }
                        } else {
                            JSONArray bookingData = jObj.getJSONArray("booking_data");
                            rides.clear();
                            if (bookingData.length() > 0) {
                                for (int i = 0; i < bookingData.length(); i++) {
                                    JSONObject booData = bookingData.getJSONObject(i);
                                    rides.add(new RideInfo(booData.getString("id"), booData.getString("from"),
                                            booData.getString("to"), booData.getString("fare"), booData.getString("tip"),booData.getString("discount"), booData.getString("distance"),
                                            booData.getString("time"), booData.getInt("coupon_used"), booData.getInt("ride_time"), booData.getInt("wait_time")));
                                }
                            }
                            updateListData("No rides currently", false);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        updateListData("Some error occurred. Tap to retry", true);
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    progressBar.setVisibility(View.GONE);
                    updateListData("Some error occurred. Tap to retry", true);
                }
            });
        } else {
            updateListData("No Internet connection. Tap to retry", true);
        }
    }

    class ViewHolderRideHistory {
        TextView fromText, fromValue, toText, toValue, distanceValue, timeValue, fareValue,
                rideTimeText, rideTimeValue, waitTimeText, waitTimeValue, actualFareText, actualFareValue, discountText, discountValue;
        ImageView couponImg;
        LinearLayout relative;
        int id;
    }

    class RideHistoryListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderRideHistory holder;
        Context context;

        public RideHistoryListAdapter(Context context) {
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
        }

        @Override
        public int getCount() {
            return rides.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderRideHistory();
                convertView = mInflater.inflate(R.layout.list_item_ride_history, null);

                holder.fromText = (TextView) convertView.findViewById(R.id.fromText);
                holder.fromText.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.fromValue = (TextView) convertView.findViewById(R.id.fromValue);
                holder.fromValue.setTypeface(Data.getFont(context));
                holder.toText = (TextView) convertView.findViewById(R.id.toText);
                holder.toText.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.toValue = (TextView) convertView.findViewById(R.id.toValue);
                holder.toValue.setTypeface(Data.getFont(context));
                holder.distanceValue = (TextView) convertView.findViewById(R.id.distanceValue);
                holder.distanceValue.setTypeface(Data.getFont(context));
                holder.timeValue = (TextView) convertView.findViewById(R.id.timeValue);
                holder.timeValue.setTypeface(Data.getFont(context));
                holder.fareValue = (TextView) convertView.findViewById(R.id.fareValue);
                holder.fareValue.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.couponImg = (ImageView) convertView.findViewById(R.id.couponImg);
                holder.rideTimeText = (TextView) convertView.findViewById(R.id.rideTimeText);
                holder.rideTimeText.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.rideTimeValue = (TextView) convertView.findViewById(R.id.rideTimeValue);
                holder.rideTimeValue.setTypeface(Data.getFont(context));
                holder.waitTimeText = (TextView) convertView.findViewById(R.id.waitTimeText);
                holder.waitTimeText.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.waitTimeValue = (TextView) convertView.findViewById(R.id.waitTimeValue);
                holder.waitTimeValue.setTypeface(Data.getFont(context));
                holder.actualFareText = (TextView) convertView.findViewById(R.id.actualFareText);
                holder.actualFareText.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.actualFareText.setText(getResources().getString(R.string.payable_amount)+ ": ");
                holder.actualFareValue = (TextView) convertView.findViewById(R.id.actualFareValue);
                holder.actualFareValue.setTypeface(Data.getFont(context));
                holder.discountText = (TextView) convertView.findViewById(R.id.discountText);
                holder.discountText.setTypeface(Data.getFont(context), Typeface.BOLD);
                holder.discountText.setText(getResources().getString(R.string.discount)+": ");
                holder.discountValue = (TextView) convertView.findViewById(R.id.discountValue);
                holder.discountValue.setTypeface(Data.getFont(context));


                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);

                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderRideHistory) convertView.getTag();
            }


            RideInfo booking = rides.get(position);

            holder.id = position;

            holder.fromValue.setText(booking.fromLocation);
            holder.toValue.setText(booking.toLocation);
            if (booking.distance.equalsIgnoreCase("1")) {
                holder.distanceValue.setText(booking.distance + " " + getString(R.string.distance_unit));
            } else {
                holder.distanceValue.setText(booking.distance + " " + getString(R.string.distance_unit) + "s");
            }

            holder.timeValue.setText(DateOperations.convertDate(DateOperations.utcToLocal(booking.time)));
            holder.fareValue.setText(getResources().getString(R.string.currency_symbol) + (Double.parseDouble(booking.fare) + Double.parseDouble(booking.tip) - Double.parseDouble(booking.discount)));
            holder.actualFareValue.setText(getResources().getString(R.string.currency_symbol) + (Double.parseDouble(booking.fare) + Double.parseDouble(booking.tip)));
            holder.discountValue.setText(getResources().getString(R.string.currency_symbol) + (Double.parseDouble(booking.discount)));

            if (booking.rideTime == 60) {
                holder.rideTimeValue.setText("1 Hr");
            } else if (booking.rideTime > 60 && booking.rideTime < 120) {
                holder.rideTimeValue.setText("" + (booking.rideTime / 60) + " Hr " + (booking.rideTime % 60) + " Min");
            } else if (booking.rideTime >= 120) {
                holder.rideTimeValue.setText("" + (booking.rideTime / 60) + " Hrs " + (booking.rideTime% 60) + " Min");
            } else {
                holder.rideTimeValue.setText("" + (booking.rideTime / 1) + " Min");
            }

            if (booking.waitTime == 60) {
                holder.waitTimeValue.setText("1 Hr");
            } else if (booking.waitTime > 60 && booking.waitTime < 120) {
                holder.waitTimeValue.setText("" + (booking.waitTime / 60) + " Hr " + (booking.waitTime % 60) + " Min");
            } else if (booking.waitTime>= 120) {
                holder.waitTimeValue.setText("" + (booking.waitTime / 60) + " Hrs " + (booking.waitTime% 60) + " Min");
            } else {
                holder.waitTimeValue.setText("" + (booking.waitTime / 1) + " Min");
            }

            if (1 == booking.couponUsed) {
                holder.couponImg.setVisibility(View.VISIBLE);
            } else {
                holder.couponImg.setVisibility(View.GONE);
            }


            return convertView;
        }

    }

}


