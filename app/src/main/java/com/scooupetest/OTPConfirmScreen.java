package com.scooupetest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.interfaces.SmsReceivedHandler;
import com.scooupetest.locationfiles.LocationFetcher;
import com.scooupetest.locationfiles.LocationUpdate;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.DeviceTokenGenerator;
import com.scooupetest.utils.FlurryEventLogger;
import com.scooupetest.utils.IDeviceTokenReceiver;
import com.scooupetest.utils.Log;
import com.flurry.android.FlurryAgent;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class OTPConfirmScreen extends BaseActivity implements LocationUpdate, SmsReceivedHandler {

    public static boolean intentFromRegister = true;
    public static EmailRegisterData emailRegisterData;
    public static FacebookRegisterData facebookRegisterData;
    public static SmsReceivedHandler smsReceivedHandler;
    Button backBtn;
    TextView title;
    TextView otpHelpText, pleaseWaitText, weWillCallText;
    EditText otpEt;
    Button confirmBtn, callMeBtn;
    LinearLayout relative;
    boolean loginDataFetched = false;
    String otpHelpStr = "Please enter the One Time Password you just received via SMS at ";
    String waitStr = "Please wait for ";
    Timer waitingTimer;
    TimerTask waitingTimerTask;
    int timerCount = 30;

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_confrim_screen);

        loginDataFetched = false;
        smsReceivedHandler = OTPConfirmScreen.this;

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(OTPConfirmScreen.this, relative, 1134, 720, false);

        backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setTypeface(Data.getFont(getApplicationContext()));
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));

        otpHelpText = (TextView) findViewById(R.id.otpHelpText);
        otpHelpText.setTypeface(Data.getFont(getApplicationContext()));
        pleaseWaitText = (TextView) findViewById(R.id.pleaseWaitText);
        pleaseWaitText.setTypeface(Data.getFont(getApplicationContext()));
        weWillCallText = (TextView) findViewById(R.id.weWillCallText);
        weWillCallText.setTypeface(Data.getFont(getApplicationContext()));

        otpEt = (EditText) findViewById(R.id.otpEt);
        otpEt.setTypeface(Data.getFont(getApplicationContext()));

        confirmBtn = (Button) findViewById(R.id.confirmBtn);
        confirmBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        callMeBtn = (Button) findViewById(R.id.callMeBtn);
        callMeBtn.setTypeface(Data.getFont(getApplicationContext()));


        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });

        otpEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                otpEt.setError(null);
            }
        });


        confirmBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String otpCode = otpEt.getText().toString().trim();
                if (otpCode.length() > 0) {
                    stopWaitingTimer();
                    callMeBtn.setBackgroundResource(R.drawable.main_interaction_btn_selector);
                    if (RegisterScreen.facebookLogin) {
                        sendFacebookSignupValues(OTPConfirmScreen.this, otpCode);
                        FlurryEventLogger.otpConfirmClick(otpCode);
                    } else {
                        sendSignupValues(OTPConfirmScreen.this, otpCode);
                        FlurryEventLogger.otpConfirmClick(otpCode);
                    }
                } else {
                    otpEt.requestFocus();
                    otpEt.setError("Field can't be empty");
                }

            }
        });

        otpEt.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        confirmBtn.performClick();
                        break;

                    case EditorInfo.IME_ACTION_NEXT:
                        break;

                    default:
                }
                return true;
            }
        });


        callMeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                stopWaitingTimer();
                if (RegisterScreen.facebookLogin) {
                    initiateOTPCallAsync(OTPConfirmScreen.this, facebookRegisterData.phoneNo);
                    FlurryEventLogger.otpThroughCall(facebookRegisterData.phoneNo);
                } else {
                    initiateOTPCallAsync(OTPConfirmScreen.this, emailRegisterData.phoneNo);
                    FlurryEventLogger.otpThroughCall(emailRegisterData.phoneNo);
                }
            }
        });


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        try {
            if (RegisterScreen.facebookLogin) {
                otpHelpText.setText(otpHelpStr + " " + facebookRegisterData.phoneNo);
            } else {
                otpHelpText.setText(otpHelpStr + " " + emailRegisterData.phoneNo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (intentFromRegister) {
            startWaitingTimer();
            callMeBtn.setBackgroundResource(R.drawable.second_interaction_btn_selector);
        } else {
            stopWaitingTimer();
            callMeBtn.setBackgroundResource(R.drawable.main_interaction_btn_selector);
        }


        new DeviceTokenGenerator(this).generateDeviceToken(this, new IDeviceTokenReceiver() {

            @Override
            public void deviceTokenReceived(final String regId) {
                Data.deviceToken = regId;
                Log.e("deviceToken in IDeviceTokenReceiver", Data.deviceToken + "..");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Data.locationFetcher == null) {
            Data.locationFetcher = new LocationFetcher(OTPConfirmScreen.this, 1000, 1);
        }
    }

    @Override
    protected void onPause() {
        try {
            if (Data.locationFetcher != null) {
                Data.locationFetcher.destroy();
                Data.locationFetcher = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    public void startWaitingTimer() {
        try {
            stopWaitingTimer();
            waitingTimer = new Timer();
            waitingTimerTask = new TimerTask() {

                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (timerCount > 1) {
                                pleaseWaitText.setText(waitStr + timerCount + " seconds");
                            } else {
                                pleaseWaitText.setText(waitStr + timerCount + " second");
                            }
                            if (timerCount <= 0) {
                                stopWaitingTimer();
                                callMeBtn.setBackgroundResource(R.drawable.main_interaction_btn_selector);
                            }
                            timerCount--;
                        }
                    });
                }
            };
//			pleaseWaitText.setVisibility(View.VISIBLE);
            timerCount = 30;
            waitingTimer.scheduleAtFixedRate(waitingTimerTask, 1000, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopWaitingTimer() {
        try {
            if (waitingTimerTask != null) {
                waitingTimerTask.cancel();
            }
            if (waitingTimer != null) {
                waitingTimer.cancel();
                waitingTimer.purge();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            waitingTimerTask = null;
            waitingTimer = null;
            timerCount = 30;
            pleaseWaitText.setVisibility(View.GONE);
        }
    }


    public void startPleaseWaitHandler() {
//		pleaseWaitText.setVisibility(View.VISIBLE);
        pleaseWaitText.setText("Please wait for some time...");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                pleaseWaitText.setVisibility(View.GONE);
            }
        }, 20 * 1000);
    }


    /**
     * ASync for confirming otp from server
     */
    public void sendSignupValues(final Activity activity, String otp) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("user_name", "=" + emailRegisterData.name);
            Log.i("ph_no", "=" + emailRegisterData.phoneNo);
            Log.i("email", "=" + emailRegisterData.emailId);
            Log.i("password", "=" + emailRegisterData.password);
            Log.i("otp", "=" + otp);
            Log.i("device_token", "=" + Data.deviceToken);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("country", "=" + Data.country);
            Log.i("device_name", "=" + Data.deviceName);
            Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("referral_code", "=" + emailRegisterData.referralCode);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

            if (Data.locationFetcher != null) {
                Data.latitude = Data.locationFetcher.getLatitude();
                Data.longitude = Data.locationFetcher.getLongitude();
            }
            RestClient.getApiService().sendSignupValues(emailRegisterData.name, emailRegisterData.phoneNo, emailRegisterData.emailId, emailRegisterData.password, otp, Data.DEVICE_TYPE, Data.uniqueDeviceId, Data.deviceToken,
                    Data.latitude, Data.longitude, Data.country, Data.deviceName, Data.appVersion, Data.osVersion, emailRegisterData.referralCode, "0", "0", Data.nounce, "",
                    Data.paymentTypeFlag, Data.corporateEmail, Data.corporteCode, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);

                                if (!newUpdate) {

                                    if (!jObj.isNull("error")) {
                                        DialogPopup.dismissLoadingDialog();
                                        String errorMessage = jObj.getString("error");
                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            HomeActivity.logoutUser(activity);
                                        } else {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                    } else {
                                        new JSONParser().parseLoginData(activity, response);

                                        Database.getInstance(OTPConfirmScreen.this).insertEmail(emailRegisterData.emailId);
                                        Database.getInstance(OTPConfirmScreen.this).close();

                                        loginDataFetched = true;

                                        DialogPopup.dismissLoadingDialog();

                                    }
                                } else {
                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "",getString(R.string.check_internet_message));
        }

    }


    /**
     * ASync for login from server
     */
    public void sendFacebookSignupValues(final Activity activity, String otp) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("user_fb_id", "=" + Data.fbId);
            Log.i("user_fb_name", "=" + Data.fbFirstName + " " + Data.fbLastName);
            Log.i("fb_access_token", "=" + Data.fbAccessToken);
            Log.i("username", "=" + Data.fbUserName);
            Log.i("fb_mail", "=" + Data.fbUserEmail);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("device_token", "=" + Data.deviceToken);
            Log.i("country", "=" + Data.country);
            Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("device_name", "=" + Data.deviceName);
            Log.i("device_type", "=" + Data.DEVICE_TYPE);
            Log.i("otp", "=" + otp);
            Log.i("ph_no", "=" + facebookRegisterData.phoneNo);
            //Log.i("password", "=" + facebookRegisterData.password);
            Log.i("referral_code", "=" + facebookRegisterData.referralCode);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

            if (Data.locationFetcher != null) {
                Data.latitude = Data.locationFetcher.getLatitude();
                Data.longitude = Data.locationFetcher.getLongitude();
            }

            RestClient.getApiService().sendFacebookLoginValues(Data.fbId, Data.fbFirstName + " " + Data.fbLastName, Data.fbAccessToken, Data.fbUserName, Data.fbUserEmail,
                    Data.latitude, Data.longitude, Data.deviceToken, Data.country, Data.appVersion, Data.osVersion, Data.deviceName, Data.DEVICE_TYPE, Data.uniqueDeviceId, otp,
                    facebookRegisterData.phoneNo, /*facebookRegisterData.password,*/ facebookRegisterData.referralCode, Data.nounce,
                    Data.paymentTypeFlag, Data.corporateEmail, Data.corporteCode, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.v("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);


                                boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);

                                if (!newUpdate) {

                                    if (!jObj.isNull("error")) {
                                        DialogPopup.dismissLoadingDialog();
                                        String errorMessage = jObj.getString("error");
                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            HomeActivity.logoutUser(activity);
                                        } else {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                    } else {
                                        new JSONParser().parseLoginData(activity, response);
                                        loginDataFetched = true;

                                        Database.getInstance(OTPConfirmScreen.this).insertEmail(Data.fbUserEmail);
                                        Database.getInstance(OTPConfirmScreen.this).close();

                                        DialogPopup.dismissLoadingDialog();
                                    }
                                } else {
                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                DialogPopup.dismissLoadingDialog();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    /**
     * ASync for initiating OTP Call from server
     */
    public void initiateOTPCallAsync(final Activity activity, String phoneNo) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("phone_no", ">" + phoneNo);

            RestClient.getApiService().initiateOTPCallAsync(phoneNo, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.i("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            int flag = jObj.getInt("flag");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                            DialogPopup.dismissLoadingDialog();
                        } else {
                            String message = jObj.getString("message");
                            int flag = jObj.getInt("flag");
                            if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                new DialogPopup().alertPopup(activity, "", message);
                            }
                            startPleaseWaitHandler();
                            DialogPopup.dismissLoadingDialog();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                        DialogPopup.dismissLoadingDialog();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus && loginDataFetched) {
            stopWaitingTimer();
            loginDataFetched = false;

            Data.tutorial= 1;
            startActivity(new Intent(OTPConfirmScreen.this, HomeActivity.class));
            smsReceivedHandler = null;
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    boolean isPhoneValid(CharSequence phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }


    @Override
    public void onBackPressed() {
        performBackPressed();
        super.onBackPressed();
    }

    @Override
    public void onReceiveOTP(String OTP) {
        otpEt.setText(OTP);
        confirmBtn.performClick();
    }


    public void performBackPressed() {
        Intent intent = new Intent(OTPConfirmScreen.this, SplashLogin.class);
        intent.putExtra("back_from_otp", true);
        startActivity(intent);
        smsReceivedHandler = null;
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    @Override
    protected void onDestroy() {
        try {
            if (Data.locationFetcher != null) {
                Data.locationFetcher.destroy();
                Data.locationFetcher = null;
            }
            smsReceivedHandler = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }


    @Override
    public void onLocationChanged(Location location, int priority) {

    }
}


class EmailRegisterData {
    String name, emailId, phoneNo, password, referralCode;

    public EmailRegisterData(String name, String emailId, String phoneNo, String password, String referralCode) {
        this.name = name;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.password = password;
        this.referralCode = referralCode;
    }
}

class FacebookRegisterData {
    String phoneNo, referralCode; //, password

    public FacebookRegisterData(String phoneNo, /* String password,*/ String referralCode) {
        this.phoneNo = phoneNo;
        //this.password = password;
        this.referralCode = referralCode;
    }


}
