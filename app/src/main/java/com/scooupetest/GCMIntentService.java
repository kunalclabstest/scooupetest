package com.scooupetest;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;

import com.scooupetest.datastructure.PassengerScreenMode;
import com.scooupetest.datastructure.PushFlags;
import com.scooupetest.utils.DateOperations;
import com.scooupetest.utils.FeaturesConfig;
import com.scooupetest.utils.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

public class GCMIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;
    public static boolean RIDE_REJECTED = false;

    public GCMIntentService() {
        super("GcmIntentService");
    }

    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }


    @SuppressWarnings("deprecation")
    private void notificationManager(Context context, String message, boolean ring) {

        try {
            long when = System.currentTimeMillis();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Log.v("message", "," + message);

            Intent notificationIntent = new Intent(context, SplashNewActivity.class);


            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setAutoCancel(true);
            builder.setContentTitle(context.getString(R.string.app_name));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            builder.setContentText(message);
            builder.setTicker(message);

            if (ring) {
                builder.setLights(Color.GREEN, 500, 500);
            } else {
                builder.setDefaults(Notification.DEFAULT_ALL);
            }

            builder.setWhen(when);
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
            builder.setSmallIcon(R.drawable.notif_icon);
            builder.setContentIntent(intent);


            Notification notification = builder.build();
            notificationManager.notify(NOTIFICATION_ID, notification);

            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
            wl.acquire(15000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    private void notificationManagerResume(Context context, String message, boolean ring) {

        try {
            long when = System.currentTimeMillis();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Log.v("message", "," + message);

            Intent notificationIntent = new Intent(context, HomeActivity.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setAutoCancel(true);
            builder.setContentTitle(context.getString(R.string.app_name));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            builder.setContentText(message);
            builder.setTicker(message);

            if (ring) {
                builder.setLights(Color.GREEN, 500, 500);
            } else {
                builder.setDefaults(Notification.DEFAULT_ALL);
            }

            builder.setWhen(when);
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
            builder.setSmallIcon(R.drawable.notif_icon);
            builder.setContentIntent(intent);


            Notification notification = builder.build();
            notificationManager.notify(NOTIFICATION_ID, notification);

            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
            wl.acquire(15000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onHandleIntent(Intent intent) {
        String currentTimeUTC = DateOperations.getCurrentTimeInUTC();
        String currentTime = DateOperations.getCurrentTime();

        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
                /*
	             * Filter messages based on message type. Since it is likely that GCM
	             * will be extended in the future with new message types, just ignore
	             * any message types you're not interested in, or that you don't
	             * recognize.
	             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//	                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
//	                sendNotification("Deleted messages on server: " +
//	                        extras.toString());
                // If it's a fontFamily GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.

                String SHARED_PREF_NAME1 = "myPref", SP_ACCESS_TOKEN_KEY = "access_token";

                SharedPreferences pref1 = getSharedPreferences(SHARED_PREF_NAME1, 0);
                final String accessToken = pref1.getString(SP_ACCESS_TOKEN_KEY, "");
                if (!"".equalsIgnoreCase(accessToken)) {

                    try {
                        Log.e("Recieved a gcm message arg1...", "," + intent.getExtras());

                        if (!"".equalsIgnoreCase(intent.getExtras().getString("message", ""))) {

                            String message = intent.getExtras().getString("message");

                            try {
                                JSONObject jObj = new JSONObject(message);

                                int flag = jObj.getInt("flag");

                                if (PushFlags.RIDE_ACCEPTED.getOrdinal() == flag) {
                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.rideRequestAcceptedInterrupt(jObj);
                                        notificationManagerResume(this, getString(R.string.ride_accept), false);
                                    } else {
                                        notificationManager(this, getString(R.string.ride_accept), false);
                                    }
                                } else if (PushFlags.DRIVER_ARRIVED.getOrdinal() == flag) {
                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.driverArrivedInterrupt();
                                        notificationManagerResume(this, getString(R.string.driver_arrived), false);
                                    } else {
                                        notificationManager(this, getString(R.string.driver_arrived), false);
                                    }
                                }  else if (PushFlags.RIDE_STARTED.getOrdinal() == flag) {

                                    if (HomeActivity.appInterruptHandler != null) {
                                        notificationManagerResume(this, getString(R.string.ride_start), false);
                                        HomeActivity.appInterruptHandler.startRideForCustomer(0);
                                    } else {
                                        String SHARED_PREF_NAME = "myPref",
                                                SP_CUSTOMER_SCREEN_MODE = "customer_screen_mode",
                                                P_IN_RIDE = "P_IN_RIDE";
                                        SharedPreferences pref = getSharedPreferences(SHARED_PREF_NAME, 0);
                                        Editor editor = pref.edit();
                                        editor.putString(SP_CUSTOMER_SCREEN_MODE, P_IN_RIDE);
                                        editor.commit();

                                        notificationManager(this, getString(R.string.ride_start), false);
                                    }

                                }
                                else if(PushFlags.FORCE_STATE_CHANGE.getOrdinal()==flag){

                                    if (HomeActivity.appInterruptHandler != null) {
                                        notificationManagerResume(this, getString(R.string.forcefully_end_message), false);
                                        HomeActivity.appInterruptHandler.forceStateChange();
                                    }
                                    else
                                    {
                                        notificationManager(this, getString(R.string.forcefully_end_message), false);
                                    }
                                }

                                else if (PushFlags.RIDE_ENDED.getOrdinal() == flag) {

                                    Data.totalDistance = jObj.getDouble("distance_travelled");
                                    Data.totalFare = jObj.getDouble("fare");
                                    Data.waitTime = jObj.getString("wait_time");
                                    Data.discount = jObj.getDouble("discount");
                                    Data.CO2Saved = jObj.getDouble("co2_saved");
                                    try {
                                        Data.rideTime = String.valueOf(Double.parseDouble(jObj.getString("ride_time")) + Double.parseDouble(jObj.getString("wait_time")));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Data.rideTime = "10";
                                    }


                                    if (HomeActivity.appInterruptHandler != null) {
                                        if (PassengerScreenMode.P_IN_RIDE == HomeActivity.passengerScreenMode) {
                                            notificationManagerResume(this, getString(R.string.ride_end), false);
                                            Data.isPaymentSuccessfull = jObj.getString("is_payment_successful");
                                            FeaturesConfig.isTipEnable = jObj.getInt("tip_enable")==1?true:false;
                                            FeaturesConfig.isAutomaticPaymentEnable = jObj.getInt("automatic_payment")==1?true:false;

                                            if (jObj.has("car_type")) {
                                                Data.defaultCarType = jObj.getInt("car_type");
                                            }
                                            HomeActivity.appInterruptHandler.customerEndRideInterrupt(jObj);
                                        }
                                    } else {
                                        notificationManager(this, getString(R.string.ride_end), false);
                                    }
                                    String SHARED_PREF_NAME = "myPref",
                                            SP_CUSTOMER_SCREEN_MODE = "customer_screen_mode",
                                            P_RIDE_END = "P_RIDE_END",
                                            SP_C_TOTAL_DISTANCE = "c_total_distance",
                                            SP_C_TOTAL_FARE = "c_total_fare",
                                            SP_C_WAIT_TIME = "c_wait_time",
                                            SP_C_RIDE_TIME = "c_ride_time";
                                    SharedPreferences pref = getSharedPreferences(SHARED_PREF_NAME, 0);
                                    Editor editor = pref.edit();
                                    editor.putString(SP_CUSTOMER_SCREEN_MODE, P_RIDE_END);
                                    editor.putString(SP_C_TOTAL_DISTANCE, "" + Data.totalDistance);
                                    editor.putString(SP_C_TOTAL_FARE, "" + Data.totalFare);
                                    editor.putString(SP_C_WAIT_TIME, Data.waitTime);
                                    editor.putString(SP_C_RIDE_TIME, Data.rideTime);
                                    editor.commit();
                                } else if (PushFlags.START_WAIT.getOrdinal() == flag) {

                                    if (HomeActivity.appInterruptHandler != null) {
                                        if (PassengerScreenMode.P_IN_RIDE == HomeActivity.passengerScreenMode) {
                                            Log.i("START_WAIT  Push", "in appInterruptHandler");
                                            HomeActivity.appInterruptHandler.onWaitStartedPushRecieved(jObj.getLong("wait_time"));
                                            notificationManagerResume(this, getString(R.string.wait_start), false);

                                        }
                                    } else {
                                        notificationManager(this, getString(R.string.wait_start), false);
                                    }
                                } else if (PushFlags.END_WAIT.getOrdinal() == flag) {

                                    if (HomeActivity.appInterruptHandler != null) {
                                        if (PassengerScreenMode.P_IN_RIDE == HomeActivity.passengerScreenMode) {
                                            Log.i("START_WAIT Push", "in appInterruptHandler");
                                            HomeActivity.appInterruptHandler.onWaitEndedPushRecieved(jObj.getLong("wait_time"));
                                            notificationManagerResume(this, getString(R.string.wait_stop), false);

                                        }
                                    } else {
                                        notificationManager(this, getString(R.string.wait_stop), false);
                                    }
                                }


                                //on recieve splitFare request
                                else if (PushFlags.SPLIT_FARE_REQUEST.getOrdinal() == flag) {

                                    if (HomeActivity.appInterruptHandler != null) {

                                        RIDE_REJECTED = true;
                                        if (PassengerScreenMode.P_INITIAL == HomeActivity.passengerScreenMode) {
                                            Log.i("SPLIT fare Push", "in appInterruptHandler");
                                            notificationManagerResume(this, getString(R.string.split_fare_request), false);
                                            HomeActivity.appInterruptHandler.onSplitFareRequestRecieved(jObj);
                                        }
                                    } else {
                                        notificationManager(this, getString(R.string.split_fare_request), false);
                                    }
                                } else if (PushFlags.RIDE_REJECTED_BY_DRIVER.getOrdinal() == flag) {

                                    RIDE_REJECTED = true;

                                    //String message1 = jObj.getString("message");
                                    String message1 = jObj.getString("message");
                                    if (HomeActivity.activity == null) {
                                        notificationManager(this, "" + message1, false);
                                    } else {
                                        notificationManagerResume(this, "" + message1, false);
                                    }

                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.startRideForCustomer(1);
                                    }

                                }  else if (PushFlags.NO_DRIVERS_AVAILABLE.getOrdinal() == flag) {
                                    String log = jObj.getString("log");
                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.onNoDriversAvailablePushRecieved(log);
                                    }
                                } else if (PushFlags.DEFAULTER.getOrdinal() == flag) {
                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.setDefaulterPushReceived();
                                    }
                                }
                                else if (PushFlags.DRIVER_HIT_COLLECT_CASH.getOrdinal() == flag) {
                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.collectCashPushRecieve();
                                    }
                                }
                                else if (PushFlags.CHANGE_STATE.getOrdinal() == flag) {
                                    String logMessage = jObj.getString("message");
                                    if (HomeActivity.appInterruptHandler != null) {
                                        HomeActivity.appInterruptHandler.onChangeStatePushReceived();
                                        notificationManagerResume(this, logMessage, false);
                                    } else {
                                        notificationManager(this, logMessage, false);
                                    }
                                } else if (PushFlags.DISPLAY_MESSAGE.getOrdinal() == flag) {

                                    RIDE_REJECTED = true;
                                    String message1 = jObj.getString("message");
                                    if (HomeActivity.activity == null) {
                                        notificationManager(this, "" + message1, false);
                                    } else {
                                        notificationManagerResume(this, "" + message1, false);
                                    }
                                }  else if (PushFlags.HEARTBEAT.getOrdinal() == flag) {
                                    try {
                                        String uuid = jObj.getString("uuid");
                                        //sendHeartbeatAckToServer(this, uuid, currentTimeUTC);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    } catch (Exception e) {
                        Log.e("Recieved exception message arg1...", "," + intent);
                        Log.e("exception", "," + e);
                    }
                }


            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }



    public String getNetworkName(Context context) {
        try {
            TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE));
            String simOperatorName = telephonyManager.getSimOperatorName();
            String operatorName = telephonyManager.getNetworkOperatorName();
            return simOperatorName + " " + operatorName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "not found";
    }
    //context.sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
    //context.sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));

}





