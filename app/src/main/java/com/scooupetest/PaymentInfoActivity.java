package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.models.AvailablePaymentMethods;
import com.scooupetest.models.ListCreditCard;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;
import com.flurry.android.FlurryAgent;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class PaymentInfoActivity extends BaseActivity {


    LinearLayout relative;
    Button backBtn;
    TextView title, tvError;
    ListView listView;
    ProgressBar progressBar;
    PaymentMethodsListAdapter paymentMethodListAdapter;
    List<ListCreditCard> availablePaymentMethodsList = new ArrayList<ListCreditCard>();
    int priviousDefaultPosition;

    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_info_activity);

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(PaymentInfoActivity.this, relative, 1134, 720, false);


        backBtn = (Button) findViewById(R.id.backBtn);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        tvError = (TextView) findViewById(R.id.tvError);
        tvError.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvExistingPaymentMethodText = (TextView) findViewById(R.id.tvExistingPaymentMethodText);
        tvExistingPaymentMethodText.setTypeface(Data.getFont(getApplicationContext()));

        listView = (ListView) findViewById(R.id.listView);
        paymentMethodListAdapter = new PaymentMethodsListAdapter();
        listView.setAdapter(paymentMethodListAdapter);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Button addNewPaymentMethodBtn = (Button) findViewById(R.id.btnAddPaymentMethod);
        addNewPaymentMethodBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        addNewPaymentMethodBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(PaymentInfoActivity.this, AddCard.class));

                overridePendingTransition(R.anim.right_in, R.anim.right_out);

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        tvError.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                tvError.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                getAvailablePaymentMethods(PaymentInfoActivity.this);
            }
        });

        paymentMethodListAdapter.notifyDataSetChanged();


    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ASSL.closeActivity(relative);
        System.gc();
    }

    /**
     * ASync for get truck from server
     */
    public void getAvailablePaymentMethods(final Activity activity) {

        if (AppStatus.getInstance(activity).isOnline(activity)) {


            RestClient.getApiService().getAvailablePaymentMethods(Data.userData.accessToken, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    Log.i("Server response faq ", "response = " + responseString);

                    try {

                        JSONObject jObj = new JSONObject(responseString);
                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                tvError.setVisibility(View.VISIBLE);
                                listView.setVisibility(View.GONE);
                                tvError.setText(errorMessage + " "+getString(R.string.tap_to_retry));
                                tvError.setEnabled(true);
                            }
                        } else {
                            Gson gc = new Gson();
                            availablePaymentMethodsList.clear();
                            AvailablePaymentMethods availablePaymentMethods = gc.fromJson(responseString, AvailablePaymentMethods.class);
                            availablePaymentMethodsList = availablePaymentMethods.getListCreditCards();

//                            if(FeaturesConfig.cashCardBoth)
//                            {
//                                ListCreditCard  noPaymentMethod = new ListCreditCard();
//                                noPaymentMethod.setPaymentTypeFlag(3);
//                                noPaymentMethod.setDefaultCard(1);
//                                noPaymentMethod.setId(0);
//                                noPaymentMethod.setCardAddedOn("");
//                                noPaymentMethod.setLast4Digits("");
//                                noPaymentMethod.setCorpEmail("");
//                                availablePaymentMethodsList.add(0, noPaymentMethod);
//                                for (int i = 1; i < availablePaymentMethodsList.size(); i++) {
//                                    if (availablePaymentMethodsList.get(i).getDefaultCard() == 1) {
//                                        noPaymentMethod.setDefaultCard(0);
//                                        break;
//                                    } else {
//                                        noPaymentMethod.setDefaultCard(1);
//                                    }
//                                }
//                            }


                            if (availablePaymentMethodsList.size() == 0) {
                                tvError.setVisibility(View.VISIBLE);
                                tvError.setText(getString(R.string.no_payment_method_added));
                                tvError.setEnabled(false);
                            } else {
                                tvError.setVisibility(View.GONE);
                                listView.setVisibility(View.VISIBLE);
                                paymentMethodListAdapter.notifyDataSetChanged();
                            }

//
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        tvError.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        tvError.setText(getString(R.string.error_occurred));
                        tvError.setEnabled(true);
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    progressBar.setVisibility(View.GONE);
                    tvError.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    tvError.setText(getString(R.string.error_occurred));
                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            tvError.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            tvError.setText(getString(R.string.no_internet_connection)+" "+getString(R.string.tap_to_retry));
            tvError.setEnabled(true);

        }

    }

    public void makeDefaultCard(final String id, final int position) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            for (int i = 0; i < availablePaymentMethodsList.size(); i++) {
                if (availablePaymentMethodsList.get(i).getDefaultCard() == 1) {
                    priviousDefaultPosition = i;
                }
                availablePaymentMethodsList.get(i).setDefaultCard(0);
            }
            availablePaymentMethodsList.get(position).setDefaultCard(1);
            paymentMethodListAdapter.notifyDataSetChanged();
            Log.i("id", id);
            Log.i("access_token", Data.userData.accessToken);

            RestClient.getApiService().makeDefaultCard(id, Data.userData.accessToken, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    com.scooupetest.utils.Log.v("Server response", "response = " + response);

                    try {
                        JSONObject jObj = new JSONObject(responseString);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");
                            for (int i = 0; i < availablePaymentMethodsList.size(); i++) {

                                availablePaymentMethodsList.get(i).setDefaultCard(0);
                            }
                            availablePaymentMethodsList.get(priviousDefaultPosition).setDefaultCard(1);
                            paymentMethodListAdapter.notifyDataSetChanged();

                            if (0 == flag) { // {"error": 'some
                                // parameter
                                // missing',"flag":0}//error
                                new DialogPopup().alertPopup(
                                        PaymentInfoActivity.this, "",
                                        errorMessage);

                            } else if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(PaymentInfoActivity.this);
                            } else {
                                new DialogPopup().alertPopup(
                                        PaymentInfoActivity.this, "",
                                        errorMessage);
                            }
                        } else {

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        for (int i = 0; i < availablePaymentMethodsList.size(); i++) {

                            availablePaymentMethodsList.get(i).setDefaultCard(0);
                        }
                        availablePaymentMethodsList.get(priviousDefaultPosition).setDefaultCard(1);
                        paymentMethodListAdapter.notifyDataSetChanged();
                        new DialogPopup().alertPopup(
                                PaymentInfoActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    com.scooupetest.utils.Log.e("request fail", error.toString());
                    for (int i = 0; i < availablePaymentMethodsList.size(); i++) {

                        availablePaymentMethodsList.get(i).setDefaultCard(0);
                    }
                    availablePaymentMethodsList.get(priviousDefaultPosition).setDefaultCard(1);
                    paymentMethodListAdapter.notifyDataSetChanged();

                    new DialogPopup().alertPopup(PaymentInfoActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(PaymentInfoActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }

    void deletePopup(final Activity activity, final String cardId, final int position) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            textHead.setVisibility(View.VISIBLE);
            textHead.setText(getString(R.string.delete));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textMessage.setText(getString(R.string.are_you_sure_to_delete_this_payment_method));

            ImageView imgHorizontalLine = (ImageView) dialog.findViewById(R.id.imgHorizontalLine);
            imgHorizontalLine.setVisibility(View.VISIBLE);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    deleteCard(cardId, position);
                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCard(final String id, final int position) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            availablePaymentMethodsList.remove(position);
            paymentMethodListAdapter.notifyDataSetChanged();
            Log.i("id", id);
            Log.i("access_token", Data.userData.accessToken);

            RestClient.getApiService().deleteCard(id, Data.userData.accessToken, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    Log.v("Server response", "response = " + response);

                    try {
                        JSONObject jObj = new JSONObject(s);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(PaymentInfoActivity.this);
                            } else {
                                getAvailablePaymentMethods(PaymentInfoActivity.this);
                                new DialogPopup().alertPopup(
                                        PaymentInfoActivity.this, "",
                                        errorMessage);
                            }
                        } else {

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        getAvailablePaymentMethods(PaymentInfoActivity.this);
                        new DialogPopup().alertPopup(
                                PaymentInfoActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    getAvailablePaymentMethods(PaymentInfoActivity.this);
                    new DialogPopup().alertPopup(PaymentInfoActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(PaymentInfoActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        getAvailablePaymentMethods(PaymentInfoActivity.this);

    }

    class ViewHolderHelp {
        TextView tvAccountDetails, tvAccountType, tvDelete, tvDefault;
        LinearLayout relative;
        Button btnMakeDefault;
        RelativeLayout relDelete, relativeChangeDefault;
        int id;
    }

    class PaymentMethodsListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderHelp holder;

        public PaymentMethodsListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return availablePaymentMethodsList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderHelp();
                convertView = mInflater.inflate(R.layout.payment_info_list_item, null);


                holder.tvAccountType = (TextView) convertView.findViewById(R.id.tvAccountType);
                holder.tvAccountType.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
                holder.tvAccountDetails = (TextView) convertView.findViewById(R.id.tvAccountDetails);
                holder.tvAccountDetails.setTypeface(Data.getFont(getApplicationContext()));
                holder.tvDelete = (TextView) convertView.findViewById(R.id.tvDeleteCard);
                holder.tvDelete.setTypeface(Data.getFont(getApplicationContext()));
                holder.tvDefault = (TextView) convertView.findViewById(R.id.tvDefault);
                holder.tvDefault.setTypeface(Data.getFont(getApplicationContext()));
                holder.btnMakeDefault = (Button) convertView.findViewById(R.id.btnMakeDefault);
                holder.relDelete = (RelativeLayout) convertView.findViewById(R.id.relDelete);
                holder.relativeChangeDefault = (RelativeLayout) convertView.findViewById(R.id.relChangeDefault);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderHelp) convertView.getTag();
            }

            holder.id = position;

            if (availablePaymentMethodsList.get(position).getDefaultCard() == 1) {
                holder.btnMakeDefault.setBackgroundResource(R.drawable.check_on);
                holder.tvDefault.setVisibility(View.VISIBLE);
                holder.tvDelete.setVisibility(View.GONE);
                Data.userData.corporateEmail = "" + availablePaymentMethodsList.get(position).getCorpEmail();
                Data.userData.accountNumber = "XXXX XXXX XXXX " + availablePaymentMethodsList.get(position).getLast4Digits();
                if (availablePaymentMethodsList.get(position).getPaymentTypeFlag() == 0) {
                    Data.userData.isCorporateAccount = 1;
                } else {
                    Data.userData.isCorporateAccount = 0;
                }

            } else {
                holder.btnMakeDefault.setBackgroundResource(R.drawable.check_off);
                holder.tvDefault.setVisibility(View.GONE);
                holder.tvDelete.setVisibility(View.VISIBLE);
            }
            holder.tvAccountDetails.setVisibility(View.VISIBLE);
            switch (availablePaymentMethodsList.get(position).getPaymentTypeFlag()) {
                case 0:
                    holder.tvAccountType.setText(getString(R.string.corporate_account));
                    holder.tvAccountDetails.setText("" + availablePaymentMethodsList.get(position).getCorpEmail());
                    break;
                case 1:
                    holder.tvAccountType.setText(getString(R.string.company_CC));
                    holder.tvAccountDetails.setText("XXXX XXXX XXXX " + availablePaymentMethodsList.get(position).getLast4Digits());
                    break;
                case 2:
                    holder.tvAccountType.setText(getString(R.string.personal_CC));
                    holder.tvAccountDetails.setText("XXXX XXXX XXXX " + availablePaymentMethodsList.get(position).getLast4Digits());
                    break;
                case 3:
                    holder.tvAccountDetails.setVisibility(View.GONE);
                    holder.tvAccountType.setText(getString(R.string.cash_payment));
                    holder.tvDelete.setVisibility(View.GONE);
                    break;
            }


//
            holder.tvDelete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    deletePopup(PaymentInfoActivity.this, "" + availablePaymentMethodsList.get(position).getId(), position);

                }
            });

            holder.relativeChangeDefault.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    makeDefaultCard("" + availablePaymentMethodsList.get(position).getId(), position);
                }
            });
            return convertView;


        }

    }
}