package com.scooupetest;

import org.json.JSONObject;

public interface AppInterruptHandler {

    public void refreshDriverLocations();

    public void onChangeStatePushReceived();

    public void setDefaulterPushReceived();
    public void collectCashPushRecieve();

    public void rideRequestAcceptedInterrupt(JSONObject jObj);

    public void driverArrivedInterrupt();

    public void onNoDriversAvailablePushRecieved(String logMessage);

    public void startRideForCustomer(final int flag);

    public void onSplitFareRequestRecieved(JSONObject jObj);

    public void customerEndRideInterrupt(JSONObject jObj);

    public void onWaitStartedPushRecieved(long waitTime);

    public void onWaitEndedPushRecieved(long waitTime);
    public void forceStateChange();


}
