package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.datastructure.EmergencyContactsData;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;


public class EmergencyActivity extends BaseActivity {



    RelativeLayout backBtn;


    static final int PICK_CONTACT = 1;
    ListView listViewEmergencyContacts;
    EmergencyContactsListAdapter emergencyContactsListAdapter;
    ArrayList<EmergencyContactsData> emergencyContactsList = new ArrayList<EmergencyContactsData>();
    LinearLayout linearNoContactsAdded,linearContactsExist;
    Button addContactsBtn;
    Boolean isErrorOccured=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        LinearLayout relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(EmergencyActivity.this, relative, 1134, 720, false);

        //Top Layout
        backBtn = (RelativeLayout) findViewById(R.id.backBtn);
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        //NoContactsAdded Layout
        linearNoContactsAdded=(LinearLayout)findViewById(R.id.linearNoContactsAdded);
        TextView tvSetEmergencyContacts = (TextView) findViewById(R.id.tvSetEmergencyContacts);
        tvSetEmergencyContacts.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvEmergencyContactsMessage = (TextView) findViewById(R.id.tvEmergencyContactsMessage);
        tvEmergencyContactsMessage.setTypeface(Data.getFont(getApplicationContext()));

        //ContactExist Layout
        linearContactsExist=(LinearLayout)findViewById(R.id.linearContactsExist);
        TextView tvEmergencyContacts = (TextView) findViewById(R.id.tvEmergencyContacts);
        tvEmergencyContacts.setTypeface(Data.getFont(getApplicationContext()));
        listViewEmergencyContacts = (ListView) findViewById(R.id.listViewEmergencyContacts);
        emergencyContactsListAdapter = new EmergencyContactsListAdapter();
        listViewEmergencyContacts.setAdapter(emergencyContactsListAdapter);

        addContactsBtn = (Button) findViewById(R.id.btnAddContacts);
        addContactsBtn.setTypeface(Data.getFont(getApplicationContext()));
        addContactsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                    startActivityForResult(intent, PICK_CONTACT);
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        getEmergencyContacts();

    }

    /**
     * ASync for save emergency number to server
     */
    public void saveEmergencyNumberAsync(final Activity activity, final String phoneNo,final String name) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("access_token", "=" + Data.userData.accessToken);

            Log.i("sos_phone_no", "=" + phoneNo);
            RestClient.getApiService().addEmergencyContact(Data.userData.accessToken, phoneNo,name, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    Log.e("Server response", "response = " + response);

                    try {
                        JSONObject jObj = new JSONObject(responseString);

                        int flag = jObj.getInt("status");

                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        } else if (flag == ApiResponseFlags.COMPLETE_INVENTORY.getOrdinal()) {

//                            new DialogPopup().alertPopup(activity, "", jObj.getString("message"));
                            emergencyContactsList.add(new EmergencyContactsData(name, phoneNo,jObj.getJSONObject("data").getString("id"),true));
                            emergencyContactsListAdapter.notifyDataSetChanged();
                            linearNoContactsAdded.setVisibility(View.GONE);
                            linearContactsExist.setVisibility(View.VISIBLE);
                            if(emergencyContactsList.size()>=5)
                            {
                                addContactsBtn.setVisibility(View.GONE);
                            }
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);

                }
            });



        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backBtn.performClick();
    }

    /**
     * API call to get Emergency Contacts from server
      */
    public void getEmergencyContacts() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this, getString(R.string.loading));
            Log.i("access_token", "=" + Data.userData.accessToken);

            RestClient.getApiService().getEmergencyContacts(Data.userData.accessToken, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {


                            String errorMessage = jObj
                                    .getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(EmergencyActivity.this);
                            } else {
                                isErrorOccured = true;
                                new DialogPopup().alertPopup(
                                        EmergencyActivity.this, "",
                                        errorMessage);
//                                linearNoContactsAdded.setVisibility(View.VISIBLE);
//                                addContactsBtn.setVisibility(View.VISIBLE);
                            }
                        } else {
                            emergencyContactsList.clear();
                            JSONArray emergencyContactsArray = jObj.getJSONObject("data").getJSONArray("sos");

                            for (int i = 0; i < emergencyContactsArray.length(); i++) {

                                JSONObject emergencyContactsObject = emergencyContactsArray.getJSONObject(i);
                                String phoneNumber = emergencyContactsObject.getString("sos_phone_no");
                                emergencyContactsList.add(new EmergencyContactsData(emergencyContactsObject.getString("sos_name"), phoneNumber, emergencyContactsObject.getString("id"),true));
                            }

                            if (emergencyContactsList.size() > 0) {
                                linearContactsExist.setVisibility(View.VISIBLE);
                                linearNoContactsAdded.setVisibility(View.GONE);
                                emergencyContactsListAdapter.notifyDataSetChanged();
                                if (emergencyContactsList.size()>=5)
                                {
                                    addContactsBtn.setVisibility(View.GONE);
                                }
                                else
                                {
                                    addContactsBtn.setVisibility(View.VISIBLE);
                                }

                            } else {
                                linearContactsExist.setVisibility(View.GONE);
                                linearNoContactsAdded.setVisibility(View.VISIBLE);
                                addContactsBtn.setVisibility(View.VISIBLE);
                            }

                            Log.v("json response = ", jObj.toString()
                                    + "");


                        }

                        DialogPopup.dismissLoadingDialog();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        isErrorOccured = true;
                        new DialogPopup().alertPopup(
                                EmergencyActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                        DialogPopup.dismissLoadingDialog();

                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());

                    new DialogPopup().alertPopup(EmergencyActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                    isErrorOccured = true;
                    DialogPopup.dismissLoadingDialog();
                }
            });
        } else {
            new DialogPopup().alertPopup(EmergencyActivity.this, "",
                    getString(R.string.check_internet_message));
            isErrorOccured=true;
        }

    }

    /**
     * To get selected contact Data from dialer app
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    if (data != null) {
                        Uri uri = data.getData();

                        if (uri != null) {
                            Cursor c = null;
                            try {
                                c = getContentResolver().query(uri, new String[]{
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                ContactsContract.CommonDataKinds.Phone.TYPE,ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                                        null, null, null);

                                if (c != null && c.moveToFirst()) {
                                    String number = c.getString(0);
                                    int type = c.getInt(1);
                                    number = number.replace(" ", "");
                                    number = number.replace("(", "");
                                    number = number.replace("/", "");
                                    number = number.replace(")", "");
                                    number = number.replace("N", "");
                                    number = number.replace(",", "");
                                    number = number.replace("*", "");
                                    number = number.replace(";", "");
                                    number = number.replace("#", "");
                                    number = number.replace("-", "");
                                    number = number.replace(".", "");
                                    Log.i("Type=" + type, "Number after trim=" + number);

                                    Boolean isRequsetAlreadySent = false;
                                    for (int i = 0; i < emergencyContactsList.size(); i++) {
                                        if (emergencyContactsList.get(i).phoneNo.equalsIgnoreCase(number)) {
                                            isRequsetAlreadySent = true;
                                            break;
                                        }
                                    }
                                    if (!isRequsetAlreadySent) {
                                        saveEmergencyNumberAsync(EmergencyActivity.this,number,c.getString(2));
                                    } else {
                                        new DialogPopup().alertPopup(
                                                EmergencyActivity.this, "",
                                                getString(R.string.contact_already_added));
                                    }
                                    Log.i("isRequsetAlreadySent=", "=" + isRequsetAlreadySent);
                                    Log.i("Type=" + type, "Number=" + number);
                                }
                            } finally {
                                if (c != null) {
                                    c.close();
                                }
                            }
                        }
                    }

                }
                break;

        }
    }


    void deletePopup(final Activity activity, final String contactId, final int position) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            textHead.setText(getString(R.string.delete));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textMessage.setText(getString(R.string.delete_contact_confirm_message));

            ImageView imgHorizontalLine = (ImageView) dialog.findViewById(R.id.imgHorizontalLine);
            imgHorizontalLine.setVisibility(View.VISIBLE);
            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    deleteContact(contactId, position);
                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteContact(final String id, final int position) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            emergencyContactsList.remove(position);
            emergencyContactsListAdapter.notifyDataSetChanged();
            addContactsBtn.setVisibility(View.VISIBLE);
            if(emergencyContactsList.size()<=0)
            {
                linearContactsExist.setVisibility(View.GONE);
                linearNoContactsAdded.setVisibility(View.VISIBLE);
            }

            Log.i("id", id);
            Log.i("access_token", Data.userData.accessToken);

            RestClient.getApiService().deleteEmergencyContact(Data.userData.accessToken, id, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    Log.v("Server response", "response = " + s);

                    try {
                        JSONObject jObj = new JSONObject(s);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(EmergencyActivity.this);
                            } else {
                                getEmergencyContacts();
                                new DialogPopup().alertPopup(
                                        EmergencyActivity.this, "",
                                        errorMessage);
                            }
                        } else {

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        getEmergencyContacts();
                        new DialogPopup().alertPopup(
                                EmergencyActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    getEmergencyContacts();
                    new DialogPopup().alertPopup(EmergencyActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(EmergencyActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }





    //Adapter to set Emergency Contacts
    class ViewHolderHelp {
        TextView name, number;
        LinearLayout relative;
        ImageView imgDeleteContact;
        int id;

    }

    class EmergencyContactsListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderHelp holder;

        public EmergencyContactsListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return emergencyContactsList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderHelp();
                convertView = mInflater.inflate(R.layout.list_item_emergency_contacts, null);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.name.setTypeface(Data.getFont(getApplicationContext()));
                holder.number = (TextView) convertView.findViewById(R.id.number);
                holder.number.setTypeface(Data.getFont(getApplicationContext()));
                holder.imgDeleteContact = (ImageView) convertView.findViewById(R.id.imgDeleteContact);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, ViewGroup.LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderHelp) convertView.getTag();
            }

            holder.id = position;

            holder.name.setText(emergencyContactsList.get(position).userName);

            holder.number.setText(emergencyContactsList.get(position).phoneNo);


            holder.imgDeleteContact.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.i("position", "==" + position);
                   deletePopup(EmergencyActivity.this,emergencyContactsList.get(position).id,position);

                }
            });

            return convertView;
        }

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus && isErrorOccured)
        {
            isErrorOccured=false;
            backBtn.performClick();
        }
    }
}
