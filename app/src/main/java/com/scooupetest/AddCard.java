package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.FeaturesConfig;
import com.scooupetest.utils.Utils;
import com.braintreepayments.api.Braintree;
import com.braintreepayments.api.models.CardBuilder;

import org.json.JSONObject;

import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;


public class AddCard extends BaseActivity {

    Button personalCreditCardDoneBtn, corporateAccountDoneButton;

    RelativeLayout relative, relativeDownArrow, relativeOutside;
    LinearLayout linearPaymentOptions, linearPersonalCreditCard, linearCorporateAccount;
    alihafizji.library.CreditCardEditText cardNumberEt;
    AutoCompleteTextView country;
    boolean isCardAdded = false;
    int selectedYear = 1, selectedMonth = 1, paymentTypeFlag;
    private String[] month_spinner = new String[12],
            year_spinner = new String[40];
    private TextView headerText, tvSelectedPaymentMethod, tvCorporateAccount, tvPersonalCreditCard, tvCorporateCreditCard;
    private Button backBtn;
    private EditText year, month, corporateEmailET, corporateCodeEt;
    private Button monthSpin;
    private Button yearSpin;

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_card);

        relative = (RelativeLayout) findViewById(R.id.relative);
        new ASSL(AddCard.this, relative, 1134, 720, false);

        headerText = (TextView) findViewById(R.id.title);
        headerText.setTypeface(Data.getFont(getApplicationContext()));


        backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });


        isCardAdded = false;
        cardNumberEt = (alihafizji.library.CreditCardEditText) findViewById(R.id.cardNumberEt);
        cardNumberEt.setTypeface(Data.getFont(getApplicationContext()));
        cardNumberEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                cardNumberEt.setError(null);
            }
        });
        for (int i = 0; i < 12; i++) {
            if (i < 9) {
                month_spinner[i] = "0" + (i + 1) + "";
            } else {
                month_spinner[i] = (i + 1) + "";
            }
        }

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        Log.v("today.year", today.year + "");

        for (int i = 0; i < 40; i++) {
            year_spinner[i] = (today.year + i) + "";
        }

        monthSpin = (Button) findViewById(R.id.monthSpinner);
        yearSpin = (Button) findViewById(R.id.yearSpinner);

        month = (EditText) findViewById(R.id.month);
        month.setTypeface(Data.getFont(getApplicationContext()));
        year = (EditText) findViewById(R.id.year);
        year.setTypeface(Data.getFont(getApplicationContext()));


        linearPersonalCreditCard = (LinearLayout) findViewById(R.id.linearPersonalCerditCard);
        personalCreditCardDoneBtn = (Button) findViewById(R.id.signUpBtn);
        personalCreditCardDoneBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);


        //linear corporate Account
        linearCorporateAccount = (LinearLayout) findViewById(R.id.linearCorporateAccount);
        corporateEmailET = (EditText) findViewById(R.id.corporateAccountEmailIdEt);
        corporateEmailET.setTypeface(Data.getFont(getApplicationContext()));
        corporateCodeEt = (EditText) findViewById(R.id.corporateCodeEt);
        corporateCodeEt.setTypeface(Data.getFont(getApplicationContext()));
        corporateAccountDoneButton = (Button) findViewById(R.id.corporateAccountDoneButton);
        corporateAccountDoneButton.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        corporateAccountDoneButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if ("".equalsIgnoreCase(corporateEmailET.getText().toString())) {
                    corporateEmailET.requestFocus();
                    corporateEmailET.setError("Please enter corporate email.");
                    return;
                } else if ("".equalsIgnoreCase(corporateCodeEt.getText().toString())) {
                    corporateCodeEt.requestFocus();
                    corporateCodeEt.setError("Please enter corporate code.");
                    return;
                }


                if (!isEmailValid(corporateEmailET.getText().toString())) {
                    corporateEmailET.requestFocus();
                    corporateEmailET.setError("Please enter valid email.");
                    return;

                }
                addCorporateAccount();

            }
        });


        linearPaymentOptions = (LinearLayout) findViewById(R.id.linearPaymentOptions);


        relativeDownArrow = (RelativeLayout) findViewById(R.id.relDownArrow);
        relativeOutside = (RelativeLayout) findViewById(R.id.relativeOutside);
        relativeOutside.setEnabled(false);
        relativeOutside.setClickable(false);
        linearPaymentOptions.setClickable(false);

        relativeDownArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(AddCard.this, corporateCodeEt);
                Log.i("Height", "==" + linearPaymentOptions.getHeight());
                TranslateAnimation anim = new TranslateAnimation(0, 0, -(int) (linearPaymentOptions.getHeight()), 0
                );
                anim.setDuration(500);
                anim.setFillAfter(true);
                linearPaymentOptions.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {
                        // TODO Auto-generated method stub
                        linearPersonalCreditCard.setVisibility(View.GONE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        linearPaymentOptions.setVisibility(View.VISIBLE);
                        Log.i("Height", "==" + linearPaymentOptions.getHeight());
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        // TODO Auto-generated method stub
                        relativeDownArrow.setVisibility(View.GONE);

                        tvSelectedPaymentMethod.setText(getString(R.string.select_payment_method));
                        corporateEmailET.setText("");
                        corporateCodeEt.setText("");
                        month.setText("");
                        year.setText("");
                        cardNumberEt.setText("");
                        relativeOutside.setEnabled(true);
                        relativeOutside.setClickable(true);
                        tvPersonalCreditCard.setClickable(true);
                        tvCorporateAccount.setClickable(true);
                        tvCorporateCreditCard.setClickable(true);


                    }
                });
            }
        });


        tvCorporateAccount = (TextView) findViewById(R.id.tvCorporateAccount);
        tvCorporateAccount.setTypeface(Data.getFont(getApplicationContext()));
        tvPersonalCreditCard = (TextView) findViewById(R.id.tvPersonalCreditCard);
        tvPersonalCreditCard.setTypeface(Data.getFont(getApplicationContext()));
        tvCorporateCreditCard = (TextView) findViewById(R.id.tvCorporateCreditCard);
        tvCorporateCreditCard.setTypeface(Data.getFont(getApplicationContext()));
        tvSelectedPaymentMethod = (TextView) findViewById(R.id.tvSelectedPaymentMethod);
        tvSelectedPaymentMethod.setTypeface(Data.getFont(getApplicationContext()));

        if (FeaturesConfig.isCorporateAccountEnable) {
            tvCorporateAccount.setVisibility(View.VISIBLE);
        } else {
            tvCorporateAccount.setVisibility(View.GONE);
        }

        personalCreditCardDoneBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if ("".equalsIgnoreCase(cardNumberEt.getText().toString())) {
                    cardNumberEt.requestFocus();
                    cardNumberEt.setError("Please enter card number");
                    return;
                } else if (cardNumberEt.getText().toString().length() < 14) {
                    cardNumberEt.requestFocus();
                    cardNumberEt.setError("Please enter valid card number");
                    return;
                }


                if (!isNumeric(month.getText().toString())) {
                    new DialogPopup().alertPopup(AddCard.this, "", "Please select month");
                    return;

                }

                if (!isNumeric(year.getText().toString())) {
                    new DialogPopup().alertPopup(AddCard.this, "", "Please select year");
                    return;

                }
                String currentYear = "" + Calendar.getInstance().get(Calendar.YEAR);
                if (currentYear.equalsIgnoreCase(year.getText().toString())) {
                    if (Integer.parseInt(month.getText().toString()) < (Calendar.getInstance().get(Calendar.MONTH) + 1)) {
                        new DialogPopup().alertPopup(AddCard.this, "", "Please select valid expiry date.");
                        return;
                    }

                }
                getBrainTreeClientToken(AddCard.this);

            }
        });


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        monthSpin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupPicker(AddCard.this, 0, month_spinner);
            }
        });
        yearSpin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupPicker(AddCard.this, 1, year_spinner);
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ASSL.closeActivity(relative);

        System.gc();
    }

    public void popupPicker(Activity acivity, final int type, final String items[]) {
        final Dialog dialog1 = new Dialog(acivity, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.popup_number_picker);
        new ASSL(this, (ViewGroup) dialog1.findViewById(R.id.root), 1134, 720, false);
        WindowManager.LayoutParams layoutparams = dialog1.getWindow().getAttributes();
        layoutparams.dimAmount = Data.dialogDimAmount;
        dialog1.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        final NumberPicker numberPicker = (NumberPicker) dialog1.findViewById(R.id.numberPicker1);
        TextView textViewType = (TextView) dialog1.findViewById(R.id.type);
        TextView textViewDone = (TextView) dialog1.findViewById(R.id.DoneTxt);
        textViewType.setTypeface(Data.getFont(getApplicationContext()));
        textViewDone.setTypeface(Data.getFont(getApplicationContext()));

        if (type == 0) {

            textViewType.setText("Select Month");
            numberPicker.setMaxValue(items.length);
            numberPicker.setMinValue(1);
            numberPicker.setDisplayedValues(items);
            numberPicker.setFocusable(true);
            numberPicker.setFocusableInTouchMode(true);
            numberPicker.setValue(selectedMonth);

        } else {

            textViewType.setText("Select Year");

            numberPicker.setMaxValue(items.length);

            numberPicker.setMinValue(1);

            numberPicker.setFocusable(true);

            numberPicker.setDisplayedValues(items);

            numberPicker.setFocusableInTouchMode(true);

            numberPicker.setValue(selectedYear);

        }

        textViewDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i("New Value", "=" + numberPicker.getValue());

                if (type == 0) {

                    month.setText(items[numberPicker.getValue() - 1]);
                    selectedMonth = numberPicker.getValue();

                } else {

                    year.setText(items[numberPicker.getValue() - 1]);
                    selectedYear = numberPicker.getValue();

                }

                dialog1.dismiss();

            }

        });

        dialog1.show();

    }


    /**
     * ASync getBrainTreeClientToken from server
     */
    public void getBrainTreeClientToken(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            DialogPopup.showLoadingDialog(activity, "Adding...");
            RestClient.getApiService().getBrainTreeClientToken(new Callback<String>() {
                @Override
                public void success(String response, Response arg1) {
                    Log.i("Server response", "response = " + response);

                    try {

                        JSONObject obj = new JSONObject(response);
                        Log.e("clientToken", "= " + obj.getString("clientToken"));
                        Braintree braintree = Braintree.getInstance(getApplicationContext(), obj.getString("clientToken"));
                        braintree.addListener(new Braintree.PaymentMethodNonceListener() {
                            public void onPaymentMethodNonce(String paymentMethodNonce) {
                                // Communicate the nonce to your server
                                Log.i("paymentMethodNonce", "==" + paymentMethodNonce);
                                addCard(paymentMethodNonce);
                            }
                        });

                        CardBuilder cardBuilder = new CardBuilder()
                                .cardNumber(cardNumberEt.getText().toString().replace("-", ""))
                                .expirationDate(month.getText().toString() + "/" + year.getText().toString());

                        braintree.tokenize(cardBuilder);
                        Log.i("cardNumber", "=" + cardNumberEt.getText().toString());
                        Log.i("cardNumber", "=" + cardNumberEt.getText().toString().replace("-", ""));
                        Log.i("expirationDate", "=" + month.getText().toString() + "/" + year.getText().toString());

                    } catch (Exception exception) {
                        exception.printStackTrace();
                        DialogPopup.dismissLoadingDialog();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }


                }

                @Override
                public void failure(RetrofitError error) {
                    Log.i("failure response", "response = " + error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }


    public void addCard(final String nounce) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            RestClient.getApiService().addCard(nounce, Data.userData.accessToken, paymentTypeFlag, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response arg1) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (0 == flag) {
                                new DialogPopup().alertPopup(
                                        AddCard.this, "",
                                        errorMessage);
                            } else if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(AddCard.this);
                            } else {
                                new DialogPopup().alertPopup(
                                        AddCard.this, "",
                                        errorMessage);
                            }
                        } else {
                            isCardAdded = true;
                            new DialogPopup().alertPopup(
                                    AddCard.this, "",
                                    jObj.getString("log"));
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(
                                AddCard.this, "",
                                Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.v("Server error", "error = " + error);
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(AddCard.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(AddCard.this, "",
                    getString(R.string.check_internet_message));
        }

    }


    public void addCorporateAccount() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this, "Adding...");

            Log.i("corporateEmailET", "==" + corporateEmailET.getText().toString());
            Log.i("corporateCodeEt", "==" + corporateCodeEt.getText().toString());
            RestClient.getApiService().addCorporateAccount(Data.userData.accessToken, corporateEmailET.getText().toString(), corporateCodeEt.getText().toString(), new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response arg1) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (0 == flag) {
                                new DialogPopup().alertPopup(
                                        AddCard.this, "",
                                        errorMessage);
                            } else if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(AddCard.this);
                            } else {
                                new DialogPopup().alertPopup(
                                        AddCard.this, "",
                                        errorMessage);
                            }
                        } else {
                            isCardAdded = true;
                            new DialogPopup().alertPopup(
                                    AddCard.this, "",
                                    jObj.getString("log"));
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(
                                AddCard.this, "",
                                Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.v("error response", "response = " + error.toString());
                    Log.v("error response", "response = " + error.getBody());
                    Log.v("error response", "response = " + error.getKind());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(AddCard.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(AddCard.this, "",
                    getString(R.string.check_internet_message));
        }

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus && isCardAdded) {
            isCardAdded = false;
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }

    }

    public void selectPaymentMethod(final View v) {

        Log.i("Height", "==" + linearPaymentOptions.getHeight());
        TranslateAnimation anim = new TranslateAnimation(0,
                0, 0, -(int) (linearPaymentOptions.getHeight()));
        anim.setDuration(500);
        anim.setFillAfter(true);

        linearPaymentOptions.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                linearPaymentOptions.setVisibility(View.GONE);
                relativeDownArrow.setVisibility(View.VISIBLE);
                relativeOutside.setEnabled(false);
                relativeOutside.setClickable(false);
                tvPersonalCreditCard.setClickable(false);
                tvCorporateAccount.setClickable(false);
                tvCorporateCreditCard.setClickable(false);

                switch (v.getId()) {
                    case R.id.tvPersonalCreditCard:
                        linearPersonalCreditCard.setVisibility(View.VISIBLE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        tvSelectedPaymentMethod.setText(getString(R.string.personal_CC));
                        paymentTypeFlag = 2;
                        break;
                    case R.id.tvCorporateAccount:
                        linearPersonalCreditCard.setVisibility(View.GONE);
                        linearCorporateAccount.setVisibility(View.VISIBLE);
                        tvSelectedPaymentMethod.setText(getString(R.string.corporate_account));
                        break;
                    case R.id.tvCorporateCreditCard:
                        linearPersonalCreditCard.setVisibility(View.VISIBLE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        tvSelectedPaymentMethod.setText(getString(R.string.company_CC));
                        paymentTypeFlag = 1;
                        break;
                    case R.id.relativeOutside:
                        linearPersonalCreditCard.setVisibility(View.GONE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        tvSelectedPaymentMethod.setText(getString(R.string.select_payment_method));
                        break;

                }

            }
        });
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}
