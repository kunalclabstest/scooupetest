package com.scooupetest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.StyleSpan;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.scooupetest.customlayouts.CustomInfoWindow;
import com.scooupetest.customlayouts.CustomMapMarkerCreator;
import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.customlayouts.PausableChronometer;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.datastructure.AppMode;
import com.scooupetest.datastructure.DriverInfo;
import com.scooupetest.datastructure.EndRideData;
import com.scooupetest.datastructure.HelpSection;
import com.scooupetest.datastructure.LatLngPair;
import com.scooupetest.datastructure.PassengerScreenMode;
import com.scooupetest.datastructure.ScheduleOperationMode;
import com.scooupetest.datastructure.SearchResult;
import com.scooupetest.locationfiles.AutoCompleteSearchResult;
import com.scooupetest.locationfiles.GPSForegroundLocationFetcher;
import com.scooupetest.locationfiles.GPSLocationUpdate;
import com.scooupetest.locationfiles.LocationFetcher;
import com.scooupetest.locationfiles.LocationUpdate;
import com.scooupetest.locationfiles.MapStateListener;
import com.scooupetest.locationfiles.MapUtils;
import com.scooupetest.locationfiles.TouchableMapFragment;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.DateOperations;
import com.scooupetest.utils.FeaturesConfig;
import com.scooupetest.utils.FlurryEventLogger;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Prefs;
import com.scooupetest.utils.Utils;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.BlurTransform;
import com.squareup.picasso.CircleTransform;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;
import com.squareup.picasso.RoundBorderTransform;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

@SuppressLint("DefaultLocale")
public class HomeActivity extends BaseActivity implements AppInterruptHandler, LocationUpdate, GPSLocationUpdate, TipPaymentListener {


    public static final int MAP_PATH_COLOR = Color.TRANSPARENT;
    public static final long LOCATION_UPDATE_TIME_PERIOD = 10000; //in milliseconds
    public static final double MAX_DISPLACEMENT_THRESHOLD = 200; //in meters
    public static final float LOW_POWER_ACCURACY_CHECK = 2000, HIGH_ACCURACY_ACCURACY_CHECK = 200;  //in meters
    public static final float WAIT_FOR_ACCURACY_UPPER_BOUND = 2000, WAIT_FOR_ACCURACY_LOWER_BOUND = 200;  //in meters
    public static final long MAX_TIME_BEFORE_LOCATION_UPDATE_REBOOT = 10 * 60000; //in milliseconds
    public static final double MAP_PAN_DISTANCE_CHECK = 50; // in meters

    public static int waitStart = 2;
    public static ArrayList<LatLngPair> deltaLatLngPairs = new ArrayList<LatLngPair>();
    //TODO check final variables
    public static AppMode appMode;
    static TipPaymentListener tipPaymentListener;
    static double totalDistance = -1, totalFare = 0;
    static long previousWaitTime = 0, previousRideTime = 0, customerWaitTime = 0, timeTillArrived = 0;
    static Location myLocation;
    static PassengerScreenMode passengerScreenMode;
    static AppInterruptHandler appInterruptHandler;
    static Activity activity;
    public CheckForGPSAccuracyTimer checkForGPSAccuracyTimer;
    public ASSL assl;
    public GPSForegroundLocationFetcher gpsForegroundLocationFetcher;
    public Dialog timeDialogAlert;

    public Thread autoCompleteThread;
    public boolean refreshingAutoComplete = false;
    public static boolean removedFavourite = false;
    public SlidingUpPanelLayout.PanelState previousPanelState;
    public long movedPixel = 0;
    DrawerLayout drawerLayout;
    RelativeLayout mainRelativeOutside;
    // views declaration
    Dialog splitFareDialog;
    CountDownTimer mCountDown;
    //menu bar
    LinearLayout menuLayout;
    ImageView profileImg;
    TextView userName;
    RelativeLayout inviteFriendRl;
    TextView inviteFriendText;

    RelativeLayout bookingsRl;
    TextView bookingsText;
    RelativeLayout fareDetailsRl;
    TextView fareDetailsText;
    RelativeLayout helpRl;
    TextView helpText;
    RelativeLayout languagePrefrencesRl;
    TextView languagePrefrencesText;
    RelativeLayout logoutRl;
    TextView logoutText;
    ViewFlipper tutorialFlipper;
    //Top RL
    RelativeLayout topRl;
    Boolean carSlideEnabled = false;
    Button menuBtn, backBtn; //, favBtn;
    TextView title;
    ImageView navigationBarLogo, imgPromoApplied;
    Button checkServerBtn, toggleDebugModeBtn;
    Button splitfareButton;
    //Passenger main layout
    RelativeLayout passengerMainLayout;
    ProgressBar progressBar;
    //Map layout
    RelativeLayout mapLayout;
    GoogleMap map;
    TouchableMapFragment mapFragment;
    //Initial layout
    RelativeLayout initialLayout, linearCarTypeParent;
    ImageButton imgSelectedCarType;
    TextView textViewAssigningInProgress;
    Button myLocationBtn, requestRideBtn, initialCancelRideBtn;
    RelativeLayout pickUpLocationRl, destinationLocationRl, addDestinationRl, HideDestinationRl;
    TextView tvPickUpLocation, tvDestinationLocation, pickUpLocationText, destinationLocationtext;
    ProgressBar pickUpLocationProgress;
    //    Initial info layout(ETA, fare estimation, max size, promo code)
    LinearLayout linearETA, linearMaxSize, linearFareEstimation, linearPromoCode;
    TextView tvETA, tvETAValue, tvFareEstimation, tvMaxSize, tvMaxSizeValue, tvPromoCode, tvPromoApplied;
    //SearchLayout
    LinearLayout linearSearchParent;
    EditText etLocation;
    RelativeLayout relClearSearchLocation;
    TextView tvLocation;
    ProgressBar searchLocationProgress;
    ImageView imgLocationIcon;
    ListView listViewSearch;
    SearchListAdapter searchListAdapter;
    ArrayList<AutoCompleteSearchResult> autoCompleteSearchResults = new ArrayList<AutoCompleteSearchResult>();
    boolean isSearchStartLocation = true, canChangeLocationText = true, surgeSlider = false;
    GetFormattedAddress getFormattedAddress;
    //Request Final Layout
    RelativeLayout requestFinalLayout, waitLayout, relativeSos;
    ImageView driverImage, driverCarImage;
    TextView driverName, driverTime, driverCarNumberText, tvTimerState;
    PausableChronometer waitCustomerChronometer;
    TextView inRideRideInProgress;
    CarTouchListenerFor2 carTouchListenerFor2;
    CarTouchListenerFor3 carTouchListenerFor3;
    CarTouchListenerFor4 carTouchListenerFor4;
    CarTouchListenerFor5 carTouchListenerFor5;
    ImageTouchListenerFor2 imageTouchListenerFor2;
    ImageTouchListenerFor3 imageTouchListenerFor3;
    ImageTouchListenerFor4 imageTouchListenerFor4;
    ImageTouchListenerFor5 imageTouchListenerFor5;
    ImageView passengerFreeRideIcon;
    Button customerInRideMyLocationBtn, btnCancelRideAfterAccept;
    LinearLayout linearArrivedTimeChronometer;
    PausableChronometer driverArrivedChronometer;
    com.sothree.slidinguppanel.SlidingUpPanelLayout slideLayout;

    //Add Favourite
    TextView addFavouriteHome, addFavouriteWork, homeLocationPlaceHolder, workLocationPlaceHolder;
    Button editWork, editHome;
    RelativeLayout rootHomeFavourite, rootWorkFavourite;
    public static boolean favouriteSelected = false;
    boolean crossPressed = false;


    // data variables declaration
    TextView minFareText, minFareValue, fareAfterText, fareAfterValue;
    Button fareInfoBtn;
    //Center Location Layout
    RelativeLayout centreLocationRl;
    ImageView centreLocationPin;

    double distanceAfterWaitStarted = 0;
    //Review layout

    LinearLayout endRideReviewRl, endRideBelowratingRl;
    ScrollView scrollEndRideReviewRl;

    TextView reviewMinFareText, reviewMinFareValue, reviewFareAfterText, reviewFareAfterValue;
    Button reviewFareInfoBtn, submitDriverRatingBtn, skipDriverRatingBtn;
    RatingBar ratingBar;
    EditText etDriverFeedback;

    //    End ride fare info layout
    LinearLayout paymentLayout, paymentUnsuccessfulLayout, dummyLayoutTosetUI, actualFareLayout, discountLayout, tipLayout;
    RelativeLayout endRideDriverInfoLayout;
    TextView tvPaymentUnsuccessful, tvPaymentUnsuccessfulmessage, tvTotalDistance, tvTotalTime, tvtipAmount, tvtipValue, tvActualFare, tvActualFareValue,
            tvPaybleAmount, tvPAybleAmountVAlue, tvDiscount, tvDiscountValue, tvPromoCodeWithValue, reviewUserName, driverRatingText;
    Button payByCashBtn, payByCardBtn, payIfPaymentUnsuccessfullBtn;
    ImageView reviewUserImgBlured, reviewUserImage, imgRatingStar1, imgRatingStar2, imgRatingStar3, imgRatingStar4, imgRatingStar5;
    Dialog forceDialog, cancelAfterAcceptDialog, fareEstimateErrorDialog;

    Location lastLocation;

    DecimalFormat decimalFormat = new DecimalFormat("#.#");

    Marker pickupLocationMarker, driverLocationMarker, currentLocationMarker, homeLocationMarker, workLocationMarker;
    boolean loggedOut = false,
            zoomedToMyLocation = false,
            mapTouchedOnce = false;
    boolean dontCallRefreshDriver = false, promoSlider = false, changeCardSlider = false, fareEstimationSlider = false, searchSlider = false, scheduleSlider = false;
    AlertDialog gpsDialogAlert;
    Dialog noDriversDialog;
    LocationFetcher lowPowerLF, highAccuracyLF;
    Handler reconnectionHandler = null;
    OnClickListener mapMyLocationClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (myLocation != null) {

                myLocationBtn.setVisibility(View.GONE);
                customerInRideMyLocationBtn.setVisibility(View.GONE);

                if (map.getCameraPosition().zoom < 12) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 12));
                } else if (map.getCameraPosition().zoom < 12) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 14));
                } else {
                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
                }
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.waiting_for_location), Toast.LENGTH_LONG).show();
                reconnectLocationFetchers();
            }
        }
    };
    ArrayList<CreatePathAsyncTask> createPathAsyncTasks = new ArrayList<HomeActivity.CreatePathAsyncTask>();
    LatLng source = null;
    //Customer's timer
    Timer timerDriverLocationUpdater;
    TimerTask timerTaskDriverLocationUpdater;
    ;
    //Customer's timer
    Timer timerUpdateDrivers;
    TimerTask timerTaskUpdateDrivers;
    //Both driver and customer
    Timer timerMapAnimateAndUpdateRideData;
    TimerTask timerTaskMapAnimateAndUpdateRideData;
    //Customer's timer
    Timer timerRequestRide;
    TimerTask timerTaskRequestRide;
    long requestRideLifeTime;
    long serverRequestStartTime = 0;
    long serverRequestEndTime = 0;
    long executionTime = -1;
    long requestPeriod = 60000;
    float mlastTouchX, mposX, mprevX, mdx;
    Bitmap carAndroidBitmap = null;
    LatLng farthestLatLng = null;
    //Driver ETA
    Timer timerUpdateETA;
    TimerTask timerTaskUpdateETA;
    private Rect rect;

    MarkerOptions markerOptionsWork, markerOptionsHome;

    public static boolean isServiceRunning(Context context, String className) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (className.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfUserDataNull(Activity activity) {
        Log.e("checkIfUserDataNull", "Data.userData = " + Data.userData);
        if (Data.userData == null) {
            activity.startActivity(new Intent(activity, SplashNewActivity.class));
            activity.finish();
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        } else {
            return false;
        }
    }

    public static void logoutUser(final Activity cont) {
        try {

            new FBLogoutNoIntent(cont).execute();

            PicassoTools.clearCache(Picasso.with(cont));

            cont.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    SharedPreferences pref = cont.getSharedPreferences("myPref", 0);
                    Editor editor = pref.edit();
                    editor.clear();
                    editor.commit();
                    Data.clearDataOnLogout(cont);
                    sessionExpireAlert(cont);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sessionExpireAlert(final Activity activity) {

        try {

            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_message_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, false);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setText(activity.getResources().getString(R.string.alert));
            textMessage.setText(activity.getResources().getString(R.string.your_login_session_expired) + " " +
                    activity.getString(R.string.customer_care_no));

            textHead.setTextColor(activity.getResources().getColorStateList(R.color.alert_dialog_heading_text_color));
            textHead.setVisibility(View.VISIBLE);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dialog.dismiss();
                        Intent intent = new Intent(activity, SplashNewActivity.class);
                        intent.putExtra("no_anim", "yes");
                        activity.startActivity(intent);
                        activity.finish();
                        activity.overridePendingTransition(
                                R.anim.left_in,
                                R.anim.left_out);
                    } catch (Exception e) {
                        Log.i("excption logout",
                                e.toString());
                    }
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializeGPSForegroundLocationFetcher();

        HomeActivity.appInterruptHandler = HomeActivity.this;
        HomeActivity.tipPaymentListener = HomeActivity.this;
        activity = this;

        loggedOut = false;
        zoomedToMyLocation = false;
        dontCallRefreshDriver = false;
        mapTouchedOnce = false;

        appMode = AppMode.NORMAL;
        DecimalFormatSymbols custom = new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        decimalFormat.setDecimalFormatSymbols(custom);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mainRelativeOutside = (RelativeLayout) findViewById(R.id.mainRelativeOutside);
        assl = new ASSL(HomeActivity.this, mainRelativeOutside, 1134, 720, false);

        drawerLayout.setDrawerListener(new DrawerListener() {

            @Override
            public void onDrawerStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onDrawerSlide(View arg0, float arg1) {
                // TODO Auto-generated method stub
                if (carSlideEnabled) {
                    carSlideEnabled = false;
                    fixCarSliderPosition();
                }
            }

            @Override
            public void onDrawerOpened(View arg0) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(tvPickUpLocation.getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View arg0) {
                // TODO Auto-generated method stub

                pickUpLocationProgress.setVisibility(View.GONE);
                pickUpLocationText.setVisibility(View.VISIBLE);


            }
        });

        //Swipe menu
        menuLayout = (LinearLayout) findViewById(R.id.menuLayout);


        tutorialFlipper = (ViewFlipper) findViewById(R.id.tutorial_flipper);
        tutorialFlipper.setInAnimation(this, R.anim.right_in);
        tutorialFlipper.setOutAnimation(this, R.anim.right_out);
        Button requestRideTutorialBtn = (Button) findViewById(R.id.requestRideTutorialBtn);
        requestRideTutorialBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        profileImg = (ImageView) findViewById(R.id.profileImg);
        userName = (TextView) findViewById(R.id.userName);
        userName.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        TextView tvEditProfile = (TextView) findViewById(R.id.tvEditProfile);
        tvEditProfile.setTypeface(Data.getFont(getApplicationContext()));
        inviteFriendRl = (RelativeLayout) findViewById(R.id.inviteFriendRl);
        inviteFriendText = (TextView) findViewById(R.id.inviteFriendText);
        inviteFriendText.setTypeface(Data.getFont(getApplicationContext()));


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        bookingsRl = (RelativeLayout) findViewById(R.id.bookingsRl);
        bookingsText = (TextView) findViewById(R.id.bookingsText);
        bookingsText.setTypeface(Data.getFont(getApplicationContext()));

        fareDetailsRl = (RelativeLayout) findViewById(R.id.fareDetailsRl);
        fareDetailsText = (TextView) findViewById(R.id.fareDetailsText);
        fareDetailsText.setTypeface(Data.getFont(getApplicationContext()));
        RelativeLayout paymentInfoRl = (RelativeLayout) findViewById(R.id.paymentInfoRl);
        TextView paymentinfoText = (TextView) findViewById(R.id.paymentInfoText);
        paymentinfoText.setTypeface(Data.getFont(getApplicationContext()));
        if (FeaturesConfig.cashOnly) {
            paymentInfoRl.setVisibility(View.GONE);
        } else {
            paymentInfoRl.setVisibility(View.VISIBLE);
        }

        RelativeLayout emergencyRl = (RelativeLayout) findViewById(R.id.emergencyRl);
        TextView emergencyText = (TextView) findViewById(R.id.emergencyText);
        emergencyText.setTypeface(Data.getFont(getApplicationContext()));
        if (FeaturesConfig.isSosEnable) {
            emergencyRl.setVisibility(View.VISIBLE);
        }

        helpRl = (RelativeLayout) findViewById(R.id.helpRl);
        helpText = (TextView) findViewById(R.id.helpText);
        helpText.setTypeface(Data.getFont(getApplicationContext()));

        if (FeaturesConfig.isChangeLanguagesEnable) {
            languagePrefrencesRl = (RelativeLayout) findViewById(R.id.languagePrefrencesRl);
            languagePrefrencesText = (TextView) findViewById(R.id.languagePrefrencesText);
            languagePrefrencesText.setTypeface(Data.getFont(getApplicationContext()));
            languagePrefrencesRl.setVisibility(View.VISIBLE);
            languagePrefrencesRl.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, SelectLanguageActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);

                }
            });

        }

        logoutRl = (RelativeLayout) findViewById(R.id.logoutRl);
        logoutText = (TextView) findViewById(R.id.logoutText);
        logoutText.setTypeface(Data.getFont(getApplicationContext()));

        //Home and Work Favourite
        addFavouriteHome = (TextView) findViewById(R.id.addHomeFavourite);
        addFavouriteWork = (TextView) findViewById(R.id.addWorkFavourite);
        homeLocationPlaceHolder = (TextView) findViewById(R.id.homeLocationPlaceHolder);
        workLocationPlaceHolder = (TextView) findViewById(R.id.workLocationPlaceHolder);
        editHome = (Button) findViewById(R.id.editHome);
        editWork = (Button) findViewById(R.id.editWork);
        rootHomeFavourite = (RelativeLayout) findViewById(R.id.rootHomeFavourite);
        rootWorkFavourite = (RelativeLayout) findViewById(R.id.rootWorkFavourite);

        markerOptionsWork = new MarkerOptions();
        markerOptionsWork.title("Work location");
        markerOptionsWork.snippet("");
        markerOptionsWork.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createWorkMarker(HomeActivity.this, assl)));

        markerOptionsHome = new MarkerOptions();
        markerOptionsHome.title("Home location");
        markerOptionsHome.snippet("");
        markerOptionsHome.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createHomeMarker(HomeActivity.this, assl)));

        //Top RL
        topRl = (RelativeLayout) findViewById(R.id.topRl);
        menuBtn = (Button) findViewById(R.id.menuBtn);
        backBtn = (Button) findViewById(R.id.backBtn);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        navigationBarLogo = (ImageView) findViewById(R.id.navigationBarLogo);
        imgPromoApplied = (ImageView) findViewById(R.id.imgPromoApplied);
        checkServerBtn = (Button) findViewById(R.id.checkServerBtn);
        toggleDebugModeBtn = (Button) findViewById(R.id.toggleDebugModeBtn);
//		favBtn = (Button) findViewById(R.id.favBtn);

        splitfareButton = (Button) findViewById(R.id.splitFareButton);
        splitfareButton.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
//		splitfareButton.setVisibility(8);


        menuBtn.setVisibility(View.VISIBLE);
        navigationBarLogo.setVisibility(View.VISIBLE);
        backBtn.setVisibility(View.GONE);
        title.setVisibility(View.GONE);


        //Map Layout
        mapLayout = (RelativeLayout) findViewById(R.id.mapLayout);
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        mapFragment = ((TouchableMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));

        //Passenger main layout
        passengerMainLayout = (RelativeLayout) findViewById(R.id.passengerMainLayout);


        //Initial layout
        initialLayout = (RelativeLayout) findViewById(R.id.initialLayout);
        textViewAssigningInProgress = (TextView) findViewById(R.id.textViewAssigningInProgress);
        textViewAssigningInProgress.setTypeface(Data.getFont(getApplicationContext()));

        myLocationBtn = (Button) findViewById(R.id.myLocationBtn);
        requestRideBtn = (Button) findViewById(R.id.requestRideBtn);
        requestRideBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        initialCancelRideBtn = (Button) findViewById(R.id.initialCancelRideBtn);
        initialCancelRideBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        linearCarTypeParent = (RelativeLayout) findViewById(R.id.carTypeLinearParent);


        //Slide Layout
        slideLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        if (slideLayout != null) {
            Log.i("slide initialized on Create", "slider Initialiezed" + slideLayout);
        } else {
            Log.i("slide initialized on Create not", "slider Initialiezed" + slideLayout);
        }
        slideLayout.setPanelHeight((int) (165 * ASSL.Yscale()));
        slideLayout.setParalaxOffset((int) (90 * ASSL.Yscale()));

        imgSelectedCarType = (ImageButton) findViewById(R.id.imgSelectedCarType);
        LayoutParams params=linearCarTypeParent.getLayoutParams();

        switch (Data.fareStructureArrayList.size()) {
            case 2:
                imageTouchListenerFor2 = new ImageTouchListenerFor2();
                carTouchListenerFor2 = new CarTouchListenerFor2();
                imgSelectedCarType.setOnTouchListener(imageTouchListenerFor2);
                linearCarTypeParent.setOnTouchListener(carTouchListenerFor2);
                params.width=(int)(445*ASSL.Xscale());
                linearCarTypeParent.setLayoutParams(params);
                break;
            case 3:
                imageTouchListenerFor3 = new ImageTouchListenerFor3();
                carTouchListenerFor3 = new CarTouchListenerFor3();
                imgSelectedCarType.setOnTouchListener(imageTouchListenerFor3);
                linearCarTypeParent.setOnTouchListener(carTouchListenerFor3);
                params.width=(int)(644*ASSL.Xscale());
                linearCarTypeParent.setLayoutParams(params);
                break;
            case 4:
                imageTouchListenerFor4 = new ImageTouchListenerFor4();
                carTouchListenerFor4 = new CarTouchListenerFor4();
                imgSelectedCarType.setOnTouchListener(imageTouchListenerFor4);
                linearCarTypeParent.setOnTouchListener(carTouchListenerFor4);
                params.width=(int)(634*ASSL.Xscale());
                linearCarTypeParent.setLayoutParams(params);
                break;
            case 5:
                imageTouchListenerFor5 = new ImageTouchListenerFor5();
                carTouchListenerFor5 = new CarTouchListenerFor5();
                imgSelectedCarType.setOnTouchListener(imageTouchListenerFor5);
                linearCarTypeParent.setOnTouchListener(carTouchListenerFor5);
                params.width=(int)(652*ASSL.Xscale());
                linearCarTypeParent.setLayoutParams(params);
                break;

        }


        imgSelectedCarType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (movedPixel < 5) {
                    if (slideLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    } else if (slideLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    }
                }
            }
        });


//        carTypeParent = (RelativeLayout) findViewById(R.id.carTypeParent);

        pickUpLocationProgress = (ProgressBar) findViewById(R.id.pickUpProgress);

        pickUpLocationRl = (RelativeLayout) findViewById(R.id.pickUpLocationRl);
        destinationLocationRl = (RelativeLayout) findViewById(R.id.DestinationLocationRl);
        addDestinationRl = (RelativeLayout) findViewById(R.id.rlPlusDestinationIcon);
        HideDestinationRl = (RelativeLayout) findViewById(R.id.rlHideDestinationIcon);
        tvPickUpLocation = (TextView) findViewById(R.id.tvPickUpLocation);
        tvPickUpLocation.setTypeface(Data.getFont(getApplicationContext()));
        tvDestinationLocation = (TextView) findViewById(R.id.tvDestinationLocation);
        tvDestinationLocation.setTypeface(Data.getFont(getApplicationContext()));
        pickUpLocationText = (TextView) findViewById(R.id.pickUpLocationText);
        pickUpLocationText.setTypeface(Data.getFont(getApplicationContext()));
        destinationLocationtext = (TextView) findViewById(R.id.DestinationLocationText);
        destinationLocationtext.setTypeface(Data.getFont(getApplicationContext()));


        //search Layout
        linearSearchParent = (LinearLayout) findViewById(R.id.linearLayoutSearch);

        etLocation = (EditText) findViewById(R.id.etLocation);
        etLocation.setTypeface(Data.getFont(getApplicationContext()));
        relClearSearchLocation = (RelativeLayout) findViewById(R.id.relClearSearchLocation);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvLocation.setTypeface(Data.getFont(getApplicationContext()));
        searchLocationProgress = (ProgressBar) findViewById(R.id.progressBarSearch);
        searchLocationProgress.setVisibility(View.GONE);
        imgLocationIcon = (ImageView) findViewById(R.id.imgLocationIcon);
        imgLocationIcon.setVisibility(View.VISIBLE);
        listViewSearch = (ListView) findViewById(R.id.listViewSearch);
        searchListAdapter = new SearchListAdapter();
        listViewSearch.setAdapter(searchListAdapter);


        slideLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                imgSelectedCarType.setClickable(false);
                linearCarTypeParent.setClickable(false);
                imgSelectedCarType.setEnabled(false);
                linearCarTypeParent.setEnabled(false);
            }

            @Override
            public void onPanelCollapsed(View view) {
                imgSelectedCarType.setClickable(true);
                linearCarTypeParent.setClickable(true);
                imgSelectedCarType.setEnabled(true);
                linearCarTypeParent.setEnabled(true);
                if (promoSlider) {
                    promoSlider = false;
                    startActivity(new Intent(HomeActivity.this, PromoCodeActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                }

                if (changeCardSlider) {
                    changeCardSlider = false;
                    startActivity(new Intent(HomeActivity.this, PaymentInfoActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                }
                if (fareEstimationSlider) {
                    fareEstimationSlider = false;
                    Intent intent = new Intent(HomeActivity.this, FareEstimationActivity.class);
                    intent.putExtra("PickupLocation", pickUpLocationText.getText().toString());
                    intent.putExtra("DestinationLocation", destinationLocationtext.getText().toString());
                    intent.putExtra("CarType", pickUpLocationText.getText().toString());
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);

                }
                fixCarSliderPosition();

            }

            @Override
            public void onPanelExpanded(View view) {
                imgSelectedCarType.setClickable(true);
                linearCarTypeParent.setClickable(true);
                imgSelectedCarType.setEnabled(true);
                linearCarTypeParent.setEnabled(true);
                fixCarSliderPosition();
            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {
                if (surgeSlider) {
                    surgeSlider = false;
                    startActivity(new Intent(activity, SurgePricingActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);

                }
                if (scheduleSlider) {
                    scheduleSlider = false;
                    switchToScheduleScreen(activity);
                }
                if (searchSlider) {
                    searchSlider = false;
                    passengerScreenMode = PassengerScreenMode.P_SEARCH;
                    switchPassengerScreen(passengerScreenMode);
                }

            }
        });


        etLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (crossPressed) {
                    rootHomeFavourite.setVisibility(View.VISIBLE);
                    rootWorkFavourite.setVisibility(View.VISIBLE);
                    crossPressed = false;
                } else {
                    if (etLocation.getText().toString().length() == 0) {
                        rootHomeFavourite.setVisibility(View.VISIBLE);
                        rootWorkFavourite.setVisibility(View.VISIBLE);
                    } else {
                        rootHomeFavourite.setVisibility(View.GONE);
                        rootWorkFavourite.setVisibility(View.GONE);
                    }
                }
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                Log.i("search text", "==" + s.toString().trim());
                if (map != null) {
                    if (s.length() > 0) {
                        getSearchResults(s.toString().trim(), map.getCameraPosition().target);
                        relClearSearchLocation.setVisibility(View.VISIBLE);
                    } else {
                        relClearSearchLocation.setVisibility(View.GONE);
                    }
                }
            }
        });

        etLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                Utils.showSoftKeyboard(HomeActivity.this, etLocation);
            }
        });
        relClearSearchLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Data.destinationLatLng = null;
                etLocation.setText("");
                crossPressed = true;

            }
        });


        pickUpLocationRl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                etLocation.setHint(getResources().getString(R.string.search_pickup_location));
                etLocation.setText(pickUpLocationText.getText().toString());
                etLocation.setSelection(etLocation.getText().length());
                tvLocation.setText(getResources().getString(R.string.pickup_location));
                Utils.showSoftKeyboard(HomeActivity.this, etLocation);
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                isSearchStartLocation = true;
                rootWorkFavourite.setVisibility(View.VISIBLE);
                rootHomeFavourite.setVisibility(View.VISIBLE);
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                searchSlider = true;

            }
        });
        destinationLocationRl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                etLocation.setHint(getResources().getString(R.string.search_destination_location));
                etLocation.setText(destinationLocationtext.getText().toString());
                etLocation.setSelection(etLocation.getText().length());
                tvLocation.setText(getResources().getString(R.string.destination_location));
                Utils.showSoftKeyboard(HomeActivity.this, etLocation);
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                isSearchStartLocation = false;
                rootWorkFavourite.setVisibility(View.VISIBLE);
                rootHomeFavourite.setVisibility(View.VISIBLE);
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                searchSlider = true;
            }
        });


        HideDestinationRl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                destinationLocationRl.setVisibility(View.GONE);
                if (Data.destinationLatLng != null) {
                    Data.destinationLatLng = null;
                }
                destinationLocationtext.setText("");
                addDestinationRl.setVisibility(View.VISIBLE);
            }
        });
        addDestinationRl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (destinationLocationRl.getVisibility() == View.GONE) {

                    destinationLocationRl.setVisibility(View.VISIBLE);
                    if (Data.destinationLatLng != null) {
                        Data.destinationLatLng = null;
                    }
                    destinationLocationtext.setText("");
                    addDestinationRl.setVisibility(View.GONE);

                }

            }
        });
//    Initial info layout(ETA, fare estimation, max size, promo code)

        linearETA = (LinearLayout) findViewById(R.id.linearETAParent);
        linearETA.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tvETA = (TextView) findViewById(R.id.tvETA);
        tvETA.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        tvETAValue = (TextView) findViewById(R.id.tvETAValue);
        tvETAValue.setTypeface(Data.getFont(getApplicationContext()));


        linearMaxSize = (LinearLayout) findViewById(R.id.linearMaxSizeParent);
        linearMaxSize.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tvMaxSize = (TextView) findViewById(R.id.tvMaxSize);
        tvMaxSize.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        tvMaxSizeValue = (TextView) findViewById(R.id.tvMaxSizeValue);
        tvMaxSizeValue.setTypeface(Data.getFont(getApplicationContext()));


        linearFareEstimation = (LinearLayout) findViewById(R.id.linearFareEstimation);
        tvFareEstimation = (TextView) findViewById(R.id.tvFareEstimation);
        tvFareEstimation.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        linearFareEstimation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Destination Latlng in fareEstimation", "" + Data.destinationLatLng);
                Log.e("Edit text Destinatio in fareEstimation", "Kuch toh mach gyi" + destinationLocationtext.getText().toString());

                if (Data.isTapable()) {
                    if (Data.destinationLatLng != null && !destinationLocationtext.getText().toString().equals("")) {
                        fareEstimationSlider = true;
                        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                    } else {
                        fareEstimationAlertPopup(HomeActivity.this, "", getString(R.string.enter_destination_location));
                    }
                }
            }
        });

        tvPromoCode = (TextView) findViewById(R.id.tvPromoCode);
        tvPromoCode.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        tvPromoApplied = (TextView) findViewById(R.id.tvPromoApplied);
        tvPromoApplied.setTypeface(Data.getFont(getApplicationContext()));
        linearPromoCode = (LinearLayout) findViewById(R.id.linearPromoCodeParent);
        linearPromoCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Data.isTapable()) {
                    promoSlider = true;
                    slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }

            }
        });


        //Request Final Layout
        requestFinalLayout = (RelativeLayout) findViewById(R.id.requestFinalLayout);


        linearArrivedTimeChronometer = (LinearLayout) findViewById(R.id.linearChronometreParent);
        driverArrivedChronometer = (PausableChronometer) findViewById(R.id.arrivedTimeChronometer);
        driverArrivedChronometer.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);


        driverImage = (ImageView) findViewById(R.id.driverImage);
        driverCarImage = (ImageView) findViewById(R.id.driverCarImage);

        driverName = (TextView) findViewById(R.id.driverName);
        driverName.setTypeface(Data.getFont(getApplicationContext()));
        driverTime = (TextView) findViewById(R.id.driverTime);
        driverTime.setTypeface(Data.getFont(getApplicationContext()));
        tvTimerState = (TextView) findViewById(R.id.tvTimerState);
        tvTimerState.setTypeface(Data.getFont(getApplicationContext()));
        driverCarNumberText = (TextView) findViewById(R.id.driverCarNumberText);
        driverCarNumberText.setTypeface(Data.getFont(getApplicationContext()));

        waitLayout = (RelativeLayout) findViewById(R.id.waitLayout);
        waitCustomerChronometer = (PausableChronometer) findViewById(R.id.waitCustomerChronometer);
        waitCustomerChronometer.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);


        inRideRideInProgress = (TextView) findViewById(R.id.inRideRideInProgress);
        inRideRideInProgress.setTypeface(Data.getFont(getApplicationContext()));
        passengerFreeRideIcon = (ImageView) findViewById(R.id.passengerFreeRideIcon);
        customerInRideMyLocationBtn = (Button) findViewById(R.id.customerInRideMyLocationBtn);

        minFareText = (TextView) findViewById(R.id.minFareText);
        minFareText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        minFareValue = (TextView) findViewById(R.id.minFareValue);
        minFareValue.setTypeface(Data.getFont(getApplicationContext()));
        fareAfterText = (TextView) findViewById(R.id.fareAfterText);
        fareAfterText.setTypeface(Data.getFont(getApplicationContext()));
        fareAfterValue = (TextView) findViewById(R.id.fareAfterValue);
        fareAfterValue.setTypeface(Data.getFont(getApplicationContext()));
        fareInfoBtn = (Button) findViewById(R.id.fareInfoBtn);

        btnCancelRideAfterAccept = (Button) findViewById(R.id.btnCancelAfterAccept);
        btnCancelRideAfterAccept.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        btnCancelRideAfterAccept.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                cancelAfterAcceptPopup(HomeActivity.this);


            }
        });
        relativeSos = (RelativeLayout) findViewById(R.id.relSosParent);
        relativeSos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomeActivity.this, SendStatusActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
//                sosAsync(HomeActivity.this);
            }
        });


        //Center location layout
        centreLocationRl = (RelativeLayout) findViewById(R.id.centreLocationRl);
        centreLocationPin = (ImageView) findViewById(R.id.centreLocationPin);


        //Review Layout
        endRideReviewRl = (LinearLayout) findViewById(R.id.endRideReviewRl);
        scrollEndRideReviewRl = (ScrollView) findViewById(R.id.scrollEndRideReviewRl);
        endRideBelowratingRl = (LinearLayout) findViewById(R.id.belowLayoutRating);
        driverRatingText = (TextView) findViewById(R.id.RatingText);
        driverRatingText.setTypeface(Data.getFont(getApplicationContext()));
        etDriverFeedback = (EditText) findViewById(R.id.ratingEditText);
        etDriverFeedback.setTypeface(Data.getFont(getApplicationContext()));
        ratingBar = (RatingBar) findViewById(R.id.RatingBar);
        submitDriverRatingBtn = (Button) findViewById(R.id.ratingSubmitBtn);
        submitDriverRatingBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        skipDriverRatingBtn = (Button) findViewById(R.id.ratingSkipBtn);
        skipDriverRatingBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        reviewUserImgBlured = (ImageView) findViewById(R.id.reviewUserImgBlured);
        reviewUserImage = (ImageView) findViewById(R.id.reviewUserImage);


//        End ride fare info layout
        endRideDriverInfoLayout = (RelativeLayout) findViewById(R.id.endRideDriverInfoLayout);
        reviewUserName = (TextView) findViewById(R.id.reviewUserName);
        reviewUserName.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        imgRatingStar1 = (ImageView) findViewById(R.id.imgRatingStar1);
        imgRatingStar2 = (ImageView) findViewById(R.id.imgRatingStar2);
        imgRatingStar3 = (ImageView) findViewById(R.id.imgRatingStar3);
        imgRatingStar4 = (ImageView) findViewById(R.id.imgRatingStar4);
        imgRatingStar5 = (ImageView) findViewById(R.id.imgRatingStar5);

        paymentLayout = (LinearLayout) findViewById(R.id.paymentLayout);

        dummyLayoutTosetUI = (LinearLayout) findViewById(R.id.linearDummyLayout);
        paymentUnsuccessfulLayout = (LinearLayout) findViewById(R.id.paymentUnsuccessfullLayout);
        tvPaymentUnsuccessful = (TextView) findViewById(R.id.tvPaymentUnsuccessful);
        tvPaymentUnsuccessful.setTypeface(Data.getFont(getApplicationContext()));
        tvPaymentUnsuccessfulmessage = (TextView) findViewById(R.id.tvPaymentUnsuccessfullMessage);
        tvPaymentUnsuccessfulmessage.setTypeface(Data.getFont(getApplicationContext()));

        tvPromoCodeWithValue = (TextView) findViewById(R.id.tvPromoCodeWithValue);
        tvPromoCodeWithValue.setTypeface(Data.getFont(getApplicationContext()));
        tvtipAmount = (TextView) findViewById(R.id.tvtipAmount);
        tvtipAmount.setTypeface(Data.getFont(getApplicationContext()));
        tvtipValue = (TextView) findViewById(R.id.tvtipValue);
        tvtipValue.setTypeface(Data.getFont(getApplicationContext()));
        tvTotalDistance = (TextView) findViewById(R.id.tvTotalDistance);
        tvTotalDistance.setTypeface(Data.getFont(getApplicationContext()));
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
        tvTotalTime.setTypeface(Data.getFont(getApplicationContext()));
        discountLayout = (LinearLayout) findViewById(R.id.discountLayout);
        tipLayout = (LinearLayout) findViewById(R.id.tipLayout);
        actualFareLayout = (LinearLayout) findViewById(R.id.actualFareLayout);
        tvActualFare = (TextView) findViewById(R.id.tvActualFare);
        tvActualFare.setTypeface(Data.getFont(getApplicationContext()));
        tvActualFareValue = (TextView) findViewById(R.id.tvActualFareValue);
        tvActualFareValue.setTypeface(Data.getFont(getApplicationContext()));
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
        tvDiscount.setTypeface(Data.getFont(getApplicationContext()));
        tvDiscountValue = (TextView) findViewById(R.id.tvDiscountValue);
        tvDiscountValue.setTypeface(Data.getFont(getApplicationContext()));
        tvPaybleAmount = (TextView) findViewById(R.id.tvPayableAmount);
        tvPaybleAmount.setTypeface(Data.getFont(getApplicationContext()));
        tvPAybleAmountVAlue = (TextView) findViewById(R.id.tvPayableAmountValue);
        tvPAybleAmountVAlue.setTypeface(Data.getFont(getApplicationContext()));


        payByCashBtn = (Button) findViewById(R.id.payByCashBtn);
        payByCashBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        payByCardBtn = (Button) findViewById(R.id.payByCardBtn);
        payByCardBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        payIfPaymentUnsuccessfullBtn = (Button) findViewById(R.id.payBtn);
        payIfPaymentUnsuccessfullBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        payByCashBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GCMIntentService.clearNotifications(HomeActivity.this);
                if (Data.isTapable()) {
                    try {

                        if (FeaturesConfig.isTipEnable) {
                            new TippingDialog(HomeActivity.this).showDialog(String.valueOf(String.format(Locale.US, "%.2f", (Data.totalFare))), "0");
                        } else {
                            payFare(HomeActivity.this, "0");
                        }

                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        payByCardBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GCMIntentService.clearNotifications(HomeActivity.this);
                if (Data.isTapable()) {

                    try {
                        if (FeaturesConfig.isTipEnable) {
                            new TippingDialog(HomeActivity.this).showDialog(String.valueOf(String.format(Locale.US, "%.2f", (Data.totalFare))), "1");
                        } else {
                            payFare(HomeActivity.this, "1");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        payIfPaymentUnsuccessfullBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GCMIntentService.clearNotifications(HomeActivity.this);
                if (Data.isTapable()) {
                    if (FeaturesConfig.isTipEnable) {
                        if (FeaturesConfig.cashOnly) {
                            new TippingDialog(HomeActivity.this).showDialog(String.valueOf(String.format(Locale.US, "%.2f", (Data.totalFare))), "0");
                        } else {
                            new TippingDialog(HomeActivity.this).showDialog(String.valueOf(String.format(Locale.US, "%.2f", (Data.totalFare))), "1");
                        }

                    } else {
                        if (FeaturesConfig.cashOnly) {
                            payFare(HomeActivity.this, "0");
                        } else {
                            payFare(HomeActivity.this, "1");
                        }

                    }
                }
            }
        });


        reviewMinFareText = (TextView) findViewById(R.id.reviewMinFareText);
        reviewMinFareText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        reviewMinFareValue = (TextView) findViewById(R.id.reviewMinFareValue);
        reviewMinFareValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareAfterText = (TextView) findViewById(R.id.reviewFareAfterText);
        reviewFareAfterText.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareAfterValue = (TextView) findViewById(R.id.reviewFareAfterValue);
        reviewFareAfterValue.setTypeface(Data.getFont(getApplicationContext()));
        reviewFareInfoBtn = (Button) findViewById(R.id.reviewFareInfoBtn);

        //Top bar events
        menuBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(menuLayout);

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
                switchPassengerScreen(passengerScreenMode);
            }
        });


        checkServerBtn.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                String message = "";

                if (Data.SERVER_URL.equalsIgnoreCase(Data.TRIAL_SERVER_URL)) {
                    message = "Current server is TRIAL. " + Data.TRIAL_SERVER_URL;
                } else if (Data.SERVER_URL.equalsIgnoreCase(Data.LIVE_SERVER_URL)) {
                    message = "Current server is LIVE. " + Data.LIVE_SERVER_URL;
                } else if (Data.SERVER_URL.equalsIgnoreCase(Data.DEV_SERVER_URL)) {
                    message = "Current server is DEV. " + Data.DEV_SERVER_URL;
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                FlurryEventLogger.checkServerPressed(Data.userData.accessToken);


                return false;
            }
        });

        toggleDebugModeBtn.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                confirmDebugPasswordPopup(HomeActivity.this);
                FlurryEventLogger.debugPressed(Data.userData.accessToken);
                return false;
            }
        });


        splitfareButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, SplitFareActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);

            }
        });

        tvEditProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, profile.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
//				FlurryEventLogger.shareScreenOpened(Data.userData.accessToken);
            }
        });
        inviteFriendRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ShareActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                FlurryEventLogger.shareScreenOpened(Data.userData.accessToken);
            }
        });

        fareDetailsRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendToFareDetails();
            }
        });

        paymentInfoRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, PaymentInfoActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                FlurryEventLogger.paymentInfoScreenOpened(Data.userData.accessToken);
            }
        });

        emergencyRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, EmergencyActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);

            }
        });

        helpRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, HelpActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                FlurryEventLogger.helpScreenOpened(Data.userData.accessToken);
            }
        });


        bookingsRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, RidesActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);

                FlurryEventLogger.rideScreenOpened(Data.userData.accessToken);
            }
        });


        logoutRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (passengerScreenMode == PassengerScreenMode.P_INITIAL || passengerScreenMode == PassengerScreenMode.P_SEARCH) {
                    logoutPopup(HomeActivity.this);
                    FlurryEventLogger.logoutPressed(Data.userData.accessToken);
                } else {
                    new DialogPopup().alertPopup(activity, "", getString(R.string.cant_logout));
                    FlurryEventLogger.logoutPressedBetweenRide(Data.userData.accessToken);
                }


            }
        });


        menuLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        // Customer initial layout events
        requestRideBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (requestRideBtn.getText().toString().equalsIgnoreCase(getString(R.string.request_ride))) {
                        if (checkWorkingTime()) {
                            if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                                if (myLocation != null) {
                                    if (Data.isTapable()) {
                                        callAnAutoPopup(HomeActivity.this);
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.waiting_for_location), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.check_internet_message));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        initialCancelRideBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if ("".equalsIgnoreCase(Data.cSessionId)) {

                    if (checkForGPSAccuracyTimer != null) {
                        if (checkForGPSAccuracyTimer.isRunning) {
                            checkForGPSAccuracyTimer.stopTimer();
                            customerUIBackToInitialAfterCancel();
                        }
                    }
                } else {
                    cancelCustomerRequestAsync(HomeActivity.this);
                }
            }
        });


        RelativeLayout relativeCallDriver = (RelativeLayout) findViewById(R.id.rlCall);
        RelativeLayout relativeMessageDriver = (RelativeLayout) findViewById(R.id.rlMessage);
        relativeCallDriver.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + Data.assignedDriverInfo.phoneNumber));
                startActivity(callIntent);
                FlurryEventLogger.callDriverPressed(Data.userData.accessToken, Data.assignedDriverInfo.userId,
                        Data.assignedDriverInfo.phoneNumber);
            }
        });

        relativeMessageDriver.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri sms_uri = Uri.parse("smsto:" + Data.assignedDriverInfo.phoneNumber);
                Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
                startActivity(sms_intent);
            }
        });


        fareInfoBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendToFareDetails();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            //
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating <= 0) {
                    ratingBar.setRating(1);
                }
            }
        });


        skipDriverRatingBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(HomeActivity.this, etDriverFeedback);
                skipFeedbackForCustomerAsync(HomeActivity.this, Data.cEngagementId);
            }
        });

        submitDriverRatingBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int rating = (int) ratingBar.getRating();
                Utils.hideSoftKeyboard(HomeActivity.this, etDriverFeedback);
                String feedbackStr = etDriverFeedback.getText().toString().trim();
                if (rating > 0) {
                    if (feedbackStr.length() > 300) {
                        etDriverFeedback.requestFocus();
                        etDriverFeedback.setError("Review must be in 300 letters.");
                    } else {
                        submitFeedbackToDriverAsync(HomeActivity.this, Data.cEngagementId, Data.cDriverId, "" + rating, feedbackStr);

                        FlurryEventLogger.reviewSubmitted(Data.userData.accessToken, Data.cEngagementId);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please rate the service.", Toast.LENGTH_LONG).show();
                }
            }
        });
        etDriverFeedback.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                Log.i("action", "" + actionId);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // TODO do something
                    submitDriverRatingBtn.performClick();
//                    Utils.hideSoftKeyboard(HomeActivity.this, etDriverFeedback);
                    handled = true;
                }
                return handled;
            }
        });


        // End ride review layout events
        scrollEndRideReviewRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        reviewFareInfoBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                fareInfoBtn.performClick();
            }
        });


        lastLocation = null;

        // map object initialized
        if (map != null) {
            map.getUiSettings().setZoomGesturesEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setMyLocationEnabled(true);
            map.getUiSettings().setTiltGesturesEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            //30.7500, 76.7800

            if (0 == Data.latitude && 0 == Data.longitude) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.7500, 76.7800), 14));
            } else {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Data.latitude, Data.longitude), 14));
            }


            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng arg0) {
                    Log.e("arg0", "=" + arg0);
                }
            });

            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                @Override
                public boolean onMarkerClick(Marker arg0) {

                    if (arg0.getTitle().equalsIgnoreCase("pickup location")) {

                        CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, "Your Pickup Location", "");
                        map.setInfoWindowAdapter(customIW);

                        return false;
                    } else if (arg0.getTitle().equalsIgnoreCase("customer_current_location")) {

                        CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, arg0.getSnippet(), "");
                        map.setInfoWindowAdapter(customIW);

                        return true;
                    } else if (arg0.getTitle().equalsIgnoreCase("start ride location")) {

                        CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, "Start Location", "");
                        map.setInfoWindowAdapter(customIW);

                        return false;
                    } else if (arg0.getTitle().equalsIgnoreCase("driver position")) {

                        CustomInfoWindow customIW = new CustomInfoWindow(HomeActivity.this, "Driver Location", "");
                        map.setInfoWindowAdapter(customIW);

                        return false;
                    } else if (arg0.getTitle().equalsIgnoreCase("driver shown to customer")) {
                        if (appMode == AppMode.DEBUG) {
                            String driverId = arg0.getSnippet();
                            try {
                                DriverInfo driverInfo = Data.driverInfos.get(Data.driverInfos.indexOf(new DriverInfo(driverId)));
                                debugDriverInfoPopup(HomeActivity.this, driverInfo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        return true;
                    } else {
                        return true;
                    }
                }
            });


            new MapStateListener(map, mapFragment, this) {
                @Override
                public void onMapTouched() {
                    // Map touched

                }

                @Override
                public void onMapReleased() {
                    // Map released

                    if (myLocation != null) {

                        double difference = MapUtils.distance(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                                new LatLng(map.getCameraPosition().target.latitude, map.getCameraPosition().target.longitude));
                        Log.e("difference", "" + difference);
                        if (difference > 1.2 && !(map.getCameraPosition().target.latitude == myLocation.getLatitude() &&
                                map.getCameraPosition().target.longitude == myLocation.getLongitude())) {
                            myLocationBtn.setVisibility(View.VISIBLE);
                            customerInRideMyLocationBtn.setVisibility(View.VISIBLE);
                        } else {
                            myLocationBtn.setVisibility(View.GONE);
                            customerInRideMyLocationBtn.setVisibility(View.GONE);
                        }
                    }

                    Log.e("Map released", "");

                    if (slideLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                        if (passengerScreenMode == PassengerScreenMode.P_INITIAL) {
                            slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                            Log.i("PanelState Map Released", "" + slideLayout.getPanelState());
                        }
                    }

                }

                @Override
                public void onMapUnsettled() {

                    Log.e("Map unsettled", "");

                }

                @Override
                public void onMapSettled() {
                    // Map settled


                    Log.e("Map settled", "");

                    if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                        if (canChangeLocationText) {
                            pickUpLocationProgress.setVisibility(View.VISIBLE);
                            pickUpLocationText.setVisibility(View.GONE);
                        }
                        callMapTouchedRefreshDrivers();
                    } else {
                        pickUpLocationProgress.setVisibility(View.GONE);
                        pickUpLocationText.setVisibility(View.VISIBLE);
                        pickUpLocationText.setText(getString(R.string.unable_to_find_location));
                    }


                }
            };


            myLocationBtn.setOnClickListener(mapMyLocationClick);

            customerInRideMyLocationBtn.setOnClickListener(mapMyLocationClick);

        }


        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);


            if (passengerScreenMode == null) {
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
            }


            switchUserScreen();

            startUIAfterGettingUserStatus();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void callMapTouchedRefreshDrivers() {
        try {
            if (passengerScreenMode == PassengerScreenMode.P_INITIAL || passengerScreenMode == PassengerScreenMode.P_SEARCH) {
                if (Data.userData.canChangeLocation == 1) {
                    Data.pickupLatLng = map.getCameraPosition().target;

                    if (!dontCallRefreshDriver) {
                        findNearbyDrivers(Data.pickupLatLng, false);

                    }
                    double distanceFromHome = 130;
                    double distanceFromWork = 130;

                    try {
                        if (!Data.favouriteHome.equalsIgnoreCase("")) {
                            distanceFromHome = MapUtils.distance(Data.pickupLatLng, Prefs.with(getApplicationContext()).getObject("fav_home_latLng", LatLng.class));
                        }
                        if (!Data.favouriteWork.equalsIgnoreCase("")) {
                            distanceFromWork = MapUtils.distance(Data.pickupLatLng, Prefs.with(getApplicationContext()).getObject("fav_work_latLng", LatLng.class));
                        }
                        if (distanceFromHome < 120) {
                            favouriteSelected = true;
                            pickUpLocationText.setText(getString(R.string.home));
                        } else if (distanceFromWork < 120) {
                            favouriteSelected = true;
                            pickUpLocationText.setText(getString(R.string.work));
                        } else if (distanceFromHome < 120 && distanceFromWork < 120) {
                            if (distanceFromHome < distanceFromWork) {
                                favouriteSelected = true;
                                pickUpLocationText.setText(getString(R.string.home));
                            } else {
                                favouriteSelected = true;
                                pickUpLocationText.setText(getString(R.string.work));
                            }
                        } else {
                            favouriteSelected = false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (canChangeLocationText && !favouriteSelected) {
                            getFormattedAddress = new GetFormattedAddress();
                            getFormattedAddress.execute();
                        }
                        canChangeLocationText = true;
                        favouriteSelected = false;
                        pickUpLocationProgress.setVisibility(View.GONE);
                        pickUpLocationText.setVisibility(View.VISIBLE);
                        canChangeLocationText = true;

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } else {
                    if (myLocation != null) {
                        Data.pickupLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        if (!dontCallRefreshDriver) {
                            findNearbyDrivers(Data.pickupLatLng, false);

                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startUIAfterGettingUserStatus() {


        if (passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
            initiateRequestRide(false);
        } else if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {
            switchPassengerScreen(passengerScreenMode);
        } else if (passengerScreenMode == PassengerScreenMode.P_RIDE_END) {
            displayCouponApplied(Data.couponJSON);
            clearSPData();
            switchPassengerScreen(passengerScreenMode);
        } else {
            switchPassengerScreen(passengerScreenMode);
        }


    }

    public void sendToFareDetails() {
        HelpParticularActivity.helpSection = HelpSection.FARE_DETAILS;
        startActivity(new Intent(HomeActivity.this, HelpParticularActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        FlurryEventLogger.fareDetailsOpened(Data.userData.accessToken);
    }


    public void fixCarSliderPosition() {
        switch (Data.fareStructureArrayList.size()) {
            case 2:
                if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    Data.defaultCarType = 0;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 0;
                    }
                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX >= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth()) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    Data.defaultCarType = 1;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 1;
                    }
                    mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                }
                break;

            case 3:
                if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 4 - imgSelectedCarType.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    Data.defaultCarType = 0;

                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 0;
                    }

                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > linearCarTypeParent.getWidth() / 4 - imgSelectedCarType.getWidth() / 2 && mposX < (((linearCarTypeParent.getWidth() / 4) * 3) - imgSelectedCarType.getWidth() / 2)) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    Data.defaultCarType = 1;

                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 1;
                    }
                    mposX = linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX >= (((3 * linearCarTypeParent.getWidth()) / 4) - imgSelectedCarType.getWidth() / 2) && mposX <= linearCarTypeParent.getWidth()) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    Data.defaultCarType = 2;

                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 2;
                    }

                    mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;
                }
                break;
            case 4:
                if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 4.87) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    Data.defaultCarType = 0;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 0;
                        changeCarType();
                    }

                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > linearCarTypeParent.getWidth() / 4.87 && mposX <= linearCarTypeParent.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    Data.defaultCarType = 1;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 1;
                        changeCarType();
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 2.82 - imgSelectedCarType.getWidth() / 2);
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > linearCarTypeParent.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() / 1.26) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    Data.defaultCarType = 2;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 2;
                        changeCarType();
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 1.55 - imgSelectedCarType.getWidth() / 2);
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > linearCarTypeParent.getWidth() / 1.26 && mposX <= linearCarTypeParent.getWidth()) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4);
                    Data.defaultCarType = 3;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 3;
                        changeCarType();
                    }
                    mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                }
                break;
            case 5:
                if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 6.1 - imgSelectedCarType.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    Data.defaultCarType = 0;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 0;
                    }
                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > linearCarTypeParent.getWidth() / 6.1 - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 2.57) - imgSelectedCarType.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    Data.defaultCarType = 1;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 1;
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 3.625 - imgSelectedCarType.getWidth() / 2);
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > linearCarTypeParent.getWidth() / 2.57 - imgSelectedCarType.getWidth() / 2 && mposX < (linearCarTypeParent.getWidth() / 1.63) - imgSelectedCarType.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    Data.defaultCarType = 2;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 2;
                    }
                    mposX = linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX >= linearCarTypeParent.getWidth() / 1.63 - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 1.19) - imgSelectedCarType.getWidth() / 2) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4);
                    Data.defaultCarType = 3;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 3;
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 1.38 - imgSelectedCarType.getWidth() / 2);
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                } else if (mposX > (linearCarTypeParent.getWidth() / 1.19) - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth()) {
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_5);
                    Data.defaultCarType = 4;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 4;
                    }
                    mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    mprevX = mposX;

                }
                break;

        }
    }



    public void initiateRequestRide(boolean newRequest) {

        if (newRequest) {
            Data.cSessionId = "";
            Data.cEngagementId = "";

            if (Data.userData.canChangeLocation == 1) {
                if (Data.pickupLatLng == null) {
                    Data.pickupLatLng = map.getCameraPosition().target;
                }
                double distance = MapUtils.distance(Data.pickupLatLng, new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                if (distance > MAP_PAN_DISTANCE_CHECK) {
                    switchRequestRideUI();
                    startTimerRequestRide();
                } else {
                    checkForGPSAccuracyTimer = new CheckForGPSAccuracyTimer(HomeActivity.this, 0, 5000, System.currentTimeMillis(), 60000);
                }
            } else {
                checkForGPSAccuracyTimer = new CheckForGPSAccuracyTimer(HomeActivity.this, 0, 5000, System.currentTimeMillis(), 60000);
            }
        } else {
            if (Data.pickupLatLng == null) {
                if (myLocation == null) {
                    Data.pickupLatLng = new LatLng(0, 0);
                } else {
                    Data.pickupLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                }
            }

            Data.cEngagementId = "";
            switchRequestRideUI();
            startTimerRequestRide();
        }

    }

    public void switchRequestRideUI() {
        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        Editor editor = pref.edit();
        editor.putString(Data.SP_C_SESSION_ID, Data.cSessionId);
        editor.putString(Data.SP_TOTAL_DISTANCE, "0");
        editor.putString(Data.SP_LAST_LATITUDE, "" + Data.pickupLatLng.latitude);
        editor.putString(Data.SP_LAST_LONGITUDE, "" + Data.pickupLatLng.longitude);
        editor.commit();

        cancelTimerUpdateDrivers();

        HomeActivity.passengerScreenMode = PassengerScreenMode.P_ASSIGNING;
        switchPassengerScreen(passengerScreenMode);
    }


    public void reconnectLocationFetchers() {
        if (reconnectionHandler == null) {
            disconnectGPSListener();
            destroyFusedLocationFetchers();
            reconnectionHandler = new Handler();
            reconnectionHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    connectGPSListener();
                    initializeFusedLocationFetchers();
                    reconnectionHandler.removeCallbacks(this);
                    reconnectionHandler = null;
                }
            }, 2000);
        }
    }

    public void setUserData() {
        try {
            userName.setText(Data.userData.userName);
            Data.userData.userImage = Data.userData.userImage.replace("http://graph.facebook", "https://graph.facebook");
            try {

                Log.e("parsed user image url", "" + Data.userData.userImage);
                Picasso.with(HomeActivity.this).load(Data.userData.userImage).skipMemoryCache().transform(new CircleTransform()).into(profileImg);

            } catch (Exception e) {
            }

            if (Data.userData.isPromoApplied == 0) {
                tvPromoApplied.setVisibility(View.GONE);
            } else {
                tvPromoApplied.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchUserScreen() {

        passengerMainLayout.setVisibility(View.VISIBLE);

    }

    public void switchPassengerScreen(PassengerScreenMode mode) {

        initializeFusedLocationFetchers();

        if (currentLocationMarker != null) {
            currentLocationMarker.remove();
        }

        saveDataOnPause(false);
        if (mode == PassengerScreenMode.P_RIDE_END) {

            if (SplitFareActivity.splitFareActivity != null) {
                SplitFareActivity.splitFareActivity.finish();
                SplitFareActivity.splitFareActivity.overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
//                startAutomaticReviewHandler();
            mapLayout.setVisibility(View.GONE);
            scrollEndRideReviewRl.setVisibility(View.VISIBLE);
            setEndRideData();


        } else {
//                stopAutomaticReviewhandler();
            mapLayout.setVisibility(View.VISIBLE);
            scrollEndRideReviewRl.setVisibility(View.GONE);
//            topRl.setBackgroundColor(getResources().getColor(R.color.white));
        }


        switch (mode) {

            case P_INITIAL:
                Data.isWaitStart = 0;
                Data.isArrived = 0;
                timeTillArrived = 0;
                customerWaitTime = 0;
                Data.tip = 0;
                cancelTimerUpdateETA();
                tvMaxSizeValue.setText("" + Data.fareStructureArrayList.get(Data.defaultCarType).maxSize + " " + getString(R.string.people));
                requestRideBtn.setText(getString(R.string.request_ride));
                requestRideBtn.setBackgroundResource(R.drawable.main_interaction_btn_selector);
                splitfareButton.setVisibility(View.GONE);
                addDestinationRl.setVisibility(View.VISIBLE);
                linearSearchParent.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (Data.favouriteHome.equalsIgnoreCase("") && pickUpLocationText.getText().toString().equalsIgnoreCase(getString(R.string.home))) {
                    callMapTouchedRefreshDrivers();
                }
                if (Data.favouriteWork.equalsIgnoreCase("") && pickUpLocationText.getText().toString().equalsIgnoreCase(getString(R.string.work))) {
                    callMapTouchedRefreshDrivers();
                }

                if (Data.favouriteWork.equalsIgnoreCase("")) {
                    destinationLocationtext.setText("");
                }
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                Log.i("PanelState initial", "" + slideLayout.getPanelState());
                imgPromoApplied.setVisibility(View.GONE);
                cancelDriverLocationUpdateTimer();
                cancelTimerRequestRide();
                if (Data.tutorial == 1) {
                    tutorialFlipper.setVisibility(View.VISIBLE);
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else {
                    tutorialFlipper.setVisibility(View.GONE);
                }

                if (destinationLocationRl.getVisibility() == View.GONE) {
                    addDestinationRl.setVisibility(View.VISIBLE);
                } else {
                    addDestinationRl.setVisibility(View.GONE);
                }

                try {
                    pickupLocationMarker.remove();
                } catch (Exception e) {
                }
                try {
                    driverLocationMarker.remove();
                } catch (Exception e) {
                }


                if (!GCMIntentService.RIDE_REJECTED) {
                    GCMIntentService.clearNotifications(getApplicationContext());
                }

                initialLayout.setVisibility(View.VISIBLE);
                requestFinalLayout.setVisibility(View.GONE);


                try {
                    if (Data.userData.canChangeLocation == 1) {
                        centreLocationRl.setVisibility(View.VISIBLE);
                    } else {
                        centreLocationRl.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                pickUpLocationRl.setVisibility(View.VISIBLE);
                initialCancelRideBtn.setVisibility(View.GONE);
                textViewAssigningInProgress.setVisibility(View.GONE);

                menuBtn.setVisibility(View.VISIBLE);
                navigationBarLogo.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                title.setVisibility(View.GONE);

                Log.e("Data.latitude", "=" + Data.latitude);
                Log.e("myLocation", "=" + myLocation);

                if (Data.latitude != 0 && Data.longitude != 0) {
                    showDriverMarkersAndPanMap(new LatLng(Data.latitude, Data.longitude));
                } else if (myLocation != null) {
                    showDriverMarkersAndPanMap(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                }
                if (Data.isSplitPopupShow == 1) {
                    try {
                        if (mCountDown != null) {
                            mCountDown.cancel();
                        }
                        splitFarePopup(HomeActivity.this, Data.splitFareData);
                        mCountDown = new CountDownTimer(Data.splitFareData.getInt("time") * 1000, 1000) {// CountDownTimer(edittext1.getText()+edittext2.getText())
                            // also parse it to long

                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {

                                if (splitFareDialog != null && splitFareDialog.isShowing()) {
                                    splitFareDialog.dismiss();
                                    Data.isSplitPopupShow = 0;
                                }

                            }
                        }.start();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }

                startTimerUpdateDrivers();


                break;

            case P_SEARCH:
                cancelTimerUpdateETA();
                initialLayout.setVisibility(View.GONE);
                linearSearchParent.setVisibility(View.VISIBLE);
                requestFinalLayout.setVisibility(View.GONE);
                centreLocationRl.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                tutorialFlipper.setVisibility(View.GONE);

                if (Data.favouriteHome.equalsIgnoreCase("")) {
                    Data.fromFavouriteHome = false;
                    editHome.setVisibility(View.GONE);
                    addFavouriteHome.setText(getString(R.string.add_home_location));
                    homeLocationPlaceHolder.setVisibility(View.GONE);
                } else {
                    addFavouriteHome.setText(Data.favouriteHome);
                    editHome.setVisibility(View.VISIBLE);
                    homeLocationPlaceHolder.setVisibility(View.VISIBLE);
                }

                if (Data.favouriteWork.equalsIgnoreCase("")) {
                    editWork.setVisibility(View.GONE);
                    addFavouriteWork.setText(getString(R.string.add_work_location));
                    workLocationPlaceHolder.setVisibility(View.GONE);
                } else {
                    workLocationPlaceHolder.setVisibility(View.VISIBLE);
                    editWork.setVisibility(View.VISIBLE);
                    addFavouriteWork.setText(Data.favouriteWork);
                }
                try {
                    homeLocationMarker.remove();
                    workLocationMarker.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;


            case P_ASSIGNING:
                Data.isWaitStart = 0;
                cancelTimerUpdateETA();


                linearSearchParent.setVisibility(View.GONE);
                initialLayout.setVisibility(View.VISIBLE);
                requestFinalLayout.setVisibility(View.GONE);
                centreLocationRl.setVisibility(View.GONE);
                tutorialFlipper.setVisibility(View.GONE);
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                Log.i("PanelState", "" + slideLayout.getPanelState());
                progressBar.setVisibility(View.VISIBLE);
                if (map != null) {
                    map.clear();
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.title("pickup location");
                    markerOptions.snippet("");
                    markerOptions.position(Data.pickupLatLng);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPinMarkerBitmap(HomeActivity.this, assl)));

                    pickupLocationMarker = map.addMarker(markerOptions);
                }
                try {
                    homeLocationMarker.remove();
                    workLocationMarker.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                imgPromoApplied.setVisibility(View.GONE);
                pickUpLocationRl.setVisibility(View.GONE);
                destinationLocationRl.setVisibility(View.GONE);
                initialCancelRideBtn.setVisibility(View.VISIBLE);
                textViewAssigningInProgress.setVisibility(View.VISIBLE);

                menuBtn.setVisibility(View.VISIBLE);
                navigationBarLogo.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                title.setVisibility(View.GONE);

                cancelTimerUpdateDrivers();

                break;


            case P_REQUEST_FINAL:

                if (map != null) {
                    Data.isWaitStart = 0;
                    if (Data.pickupLatLng == null) {
                        Log.e("Data.mapTarget", "=null");
                        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                        String lat = pref.getString(Data.SP_LAST_LATITUDE, "0");
                        String lng = pref.getString(Data.SP_LAST_LONGITUDE, "0");
                        Data.pickupLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                    } else {
                        Log.e("Data.mapTarget", "=not null");
                    }

                    Log.e("Data.mapTarget", "=" + Data.pickupLatLng);
                    Log.e("Data.assignedDriverInfo.latLng", "=" + Data.assignedDriverInfo.latLng);

                    map.clear();
                    try {
                        homeLocationMarker.remove();
                        workLocationMarker.remove();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.title("pickup location");
                    markerOptions.snippet("");
                    markerOptions.position(Data.pickupLatLng);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPinMarkerBitmap(HomeActivity.this, assl)));

                    pickupLocationMarker = map.addMarker(markerOptions);

                    MarkerOptions markerOptions1 = new MarkerOptions();
                    markerOptions1.title("driver position");
                    markerOptions1.snippet("");
                    markerOptions1.position(Data.assignedDriverInfo.latLng);
                    markerOptions1.rotation((float) Data.assignedDriverInfo.bearing);
                    markerOptions1.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createCarMarkerBitmap(HomeActivity.this, assl)));
                    markerOptions1.anchor(0.5f, 0.7f);

                    driverLocationMarker = map.addMarker(markerOptions1);

                    Log.i("marker added", "REQUEST_FINAL");
                }


                setAssignedDriverData();
                tutorialFlipper.setVisibility(View.GONE);
                btnCancelRideAfterAccept.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                setFareInfo(minFareValue, fareAfterValue, fareAfterText);
                imgPromoApplied.setVisibility(View.GONE);
                linearSearchParent.setVisibility(View.GONE);
                initialLayout.setVisibility(View.GONE);
                requestFinalLayout.setVisibility(View.VISIBLE);
                centreLocationRl.setVisibility(View.GONE);
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                Log.i("PanelState Request Final", "" + slideLayout.getPanelState());
                driverTime.setVisibility(View.VISIBLE);

                waitLayout.setVisibility(View.GONE);

                menuBtn.setVisibility(View.VISIBLE);
                navigationBarLogo.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                title.setVisibility(View.GONE);
                relativeSos.setVisibility(View.GONE);
//				favBtn.setVisibility(View.GONE);


                startDriverLocationUpdateTimer();

                cancelTimerUpdateDrivers();

                if (FeaturesConfig.isArrivedFeatureEnable) {
                    if (Data.isArrived == 1) {
                        long timeR = (long) HomeActivity.timeTillArrived;
                        int hR = (int) (timeR / 3600000);
                        int mR = (int) (timeR - hR * 3600000) / 60000;
                        int sR = (int) (timeR - hR * 3600000 - mR * 60000) / 1000;
                        String hhR = hR < 10 ? "0" + hR : hR + "";
                        String mmR = mR < 10 ? "0" + mR : mR + "";
                        String ssR = sR < 10 ? "0" + sR : sR + "";
                        driverArrivedChronometer.setText(hhR + ":" + mmR + ":" + ssR);

                        driverArrivedChronometer.eclipsedTime = (long) HomeActivity.timeTillArrived;
                        driverArrivedChronometer.start();
                        linearArrivedTimeChronometer.setVisibility(View.VISIBLE);
                        inRideRideInProgress.setText(getString(R.string.driver_arrived));
                        cancelTimerUpdateETA();
                        tvTimerState.setText(getString(R.string.arrived_time));
                        driverTime.setVisibility(View.GONE);

                    } else {
                        startTimerUpdateETA();
                        inRideRideInProgress.setText(getString(R.string.driver_on_way));
                        tvTimerState.setText(getString(R.string.eta));
                        linearArrivedTimeChronometer.setVisibility(View.GONE);

                    }
                } else {
                    startTimerUpdateETA();
                    inRideRideInProgress.setText(getString(R.string.driver_on_way));
                    tvTimerState.setText(getString(R.string.eta));
                    linearArrivedTimeChronometer.setVisibility(View.GONE);

                }


                break;


            case P_IN_RIDE:

                cancelTimerUpdateDrivers();
                cancelTimerUpdateETA();
                cancelDriverLocationUpdateTimer();
                progressBar.setVisibility(View.GONE);
                startMapAnimateAndUpdateRideDataTimer();
                splitfareButton.setVisibility(View.VISIBLE);
                btnCancelRideAfterAccept.setVisibility(View.GONE);
                tutorialFlipper.setVisibility(View.GONE);

                if (map != null) {
                    map.clear();
                }

                if (cancelAfterAcceptDialog != null) {
                    cancelAfterAcceptDialog.dismiss();
                }

                setAssignedDriverData();
                setFareInfo(minFareValue, fareAfterValue, fareAfterText);
                linearSearchParent.setVisibility(View.GONE);
                initialLayout.setVisibility(View.GONE);
                requestFinalLayout.setVisibility(View.VISIBLE);
                centreLocationRl.setVisibility(View.GONE);

                driverTime.setVisibility(View.GONE);
                tvTimerState.setText(getString(R.string.wait_time));
                linearArrivedTimeChronometer.setVisibility(View.GONE);
                inRideRideInProgress.setText(getString(R.string.ride_in_progress));
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                waitLayout.setVisibility(View.VISIBLE);

                menuBtn.setVisibility(View.VISIBLE);
                navigationBarLogo.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                title.setVisibility(View.GONE);
                imgPromoApplied.setVisibility(View.GONE);
                if (FeaturesConfig.isSosEnable) {
                    relativeSos.setVisibility(View.VISIBLE);
                }
                Log.i("Data.customerWaitTime", "==" + HomeActivity.customerWaitTime);
                long time = HomeActivity.customerWaitTime;
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String hh = h < 10 ? "0" + h : h + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                waitCustomerChronometer.setText(hh + ":" + mm + ":" + ss);
                waitCustomerChronometer.eclipsedTime = HomeActivity.customerWaitTime;
                try {
                    homeLocationMarker.remove();
                    workLocationMarker.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Data.isWaitStart == 1) {
                    waitCustomerChronometer.start();
                } else {
                    waitCustomerChronometer.stop();
                }
//				favBtn.setVisibility(View.GONE);


                break;

            case P_RIDE_END:
                Data.isWaitStart = 0;
                cancelTimerUpdateETA();
                cancelTimerRequestRide();

                try {
                    homeLocationMarker.remove();
                    workLocationMarker.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                splitfareButton.setVisibility(View.GONE);
                cancelMapAnimateAndUpdateRideDataTimer();
                linearSearchParent.setVisibility(View.GONE);
                initialLayout.setVisibility(View.GONE);
                requestFinalLayout.setVisibility(View.GONE);
                tutorialFlipper.setVisibility(View.GONE);
                centreLocationRl.setVisibility(View.GONE);
                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                menuBtn.setVisibility(View.VISIBLE);
                navigationBarLogo.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                title.setVisibility(View.GONE);
                if (Data.destinationLatLng != null) {
                    Data.destinationLatLng = null;
                }
                destinationLocationtext.setText("");


                if (Data.isPaymentSuccessfull.equalsIgnoreCase("1")) {
                    etDriverFeedback.setText("");
                    ratingBar.setRating(5);
                    Data.isPaymentSuccessfull = "0";
                    Data.pickupLatLng = new LatLng(0, 0);
                    Data.cSessionId = "";

                    endRideDriverInfoLayout.setVisibility(View.GONE);
                    dummyLayoutTosetUI.setVisibility(View.GONE);
                    paymentUnsuccessfulLayout.setVisibility(View.GONE);
                    payIfPaymentUnsuccessfullBtn.setVisibility(View.GONE);
                    payByCardBtn.setVisibility(View.GONE);
                    payByCashBtn.setVisibility(View.GONE);
                    endRideBelowratingRl.setVisibility(View.VISIBLE);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

                    if (FeaturesConfig.isTipEnable) {

                        actualFareLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (240 * ASSL.Xscale()), LayoutParams.MATCH_PARENT));
                        discountLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (240 * ASSL.Xscale()), LayoutParams.MATCH_PARENT));
                        tipLayout.setVisibility(View.VISIBLE);
                    } else {
                        actualFareLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (360 * ASSL.Xscale()), LayoutParams.MATCH_PARENT));
                        discountLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (360 * ASSL.Xscale()), LayoutParams.MATCH_PARENT));
                        tipLayout.setVisibility(View.GONE);
                    }
                    tvtipValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US, "%.2f", Data.tip)));
                    tvPAybleAmountVAlue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US, "%.2f", Data.totalFare - Data.discount + Data.tip)));


                } else {

                    actualFareLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (360 * ASSL.Xscale()), LayoutParams.MATCH_PARENT));
                    discountLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (360 * ASSL.Xscale()), LayoutParams.MATCH_PARENT));
                    tipLayout.setVisibility(View.GONE);
                    
                    endRideDriverInfoLayout.setVisibility(View.VISIBLE);
                    endRideBelowratingRl.setVisibility(View.GONE);

                    if (FeaturesConfig.cashOnly || FeaturesConfig.cardOnly) {
                        dummyLayoutTosetUI.setVisibility(View.VISIBLE);
                        payByCardBtn.setVisibility(View.GONE);
                        payByCashBtn.setVisibility(View.GONE);
                        payIfPaymentUnsuccessfullBtn.setVisibility(View.VISIBLE);

                    } else {
                        dummyLayoutTosetUI.setVisibility(View.GONE);
                        payByCardBtn.setVisibility(View.VISIBLE);
                        payByCashBtn.setVisibility(View.VISIBLE);
                        payIfPaymentUnsuccessfullBtn.setVisibility(View.GONE);
                        if (Data.defaulterFlag.equalsIgnoreCase("1")) {
                            payByCashBtn.setEnabled(false);
                            payByCashBtn.setBackgroundResource(R.drawable.disable_grey_btn);
                            payByCashBtn.setTextColor(getResources().getColorStateList(R.color.white));
                            new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.cash_payment_disable_message) + " " + getString(R.string.customer_care_no));
                        } else {
                            payByCashBtn.setEnabled(true);
                            payByCashBtn.setBackgroundResource(R.drawable.main_interaction_btn_selector);
                            payByCashBtn.setTextColor(getResources().getColorStateList(R.color.white));
                        }

                    }
                    if (FeaturesConfig.isAutomaticPaymentEnable) {
                        paymentUnsuccessfulLayout.setVisibility(View.VISIBLE);
                        dummyLayoutTosetUI.setVisibility(View.GONE);
                    } else {
                        paymentUnsuccessfulLayout.setVisibility(View.GONE);
                    }

                }


                waitCustomerChronometer.eclipsedTime = 0;
                HomeActivity.customerWaitTime = 0;
                onWaitEndedPushRecieved(customerWaitTime);

                break;

            default:
                cancelTimerUpdateETA();
                imgPromoApplied.setVisibility(View.GONE);
                initialLayout.setVisibility(View.VISIBLE);
                requestFinalLayout.setVisibility(View.GONE);
                scrollEndRideReviewRl.setVisibility(View.GONE);
                centreLocationRl.setVisibility(View.GONE);
                linearSearchParent.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                menuBtn.setVisibility(View.VISIBLE);
                tutorialFlipper.setVisibility(View.GONE);
                navigationBarLogo.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                title.setVisibility(View.GONE);
                try {
                    homeLocationMarker.remove();
                    workLocationMarker.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }


    }

    public void flipTutorial(View view) {
        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        tutorialFlipper.setFlipInterval(1500);
        tutorialFlipper.showNext();
    }

    public void finishTutorial(View view) {
        tutorialFlipper.setVisibility(View.GONE);
        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        Data.tutorial = 0;

    }


    public void setFareInfo(TextView minFareValue, TextView fareAfterValue, TextView fareAfterText) {
        try {
            Log.i("Fixed Fare", "==" + Data.fareStructureArrayList.get(Data.defaultCarType).fixedFare);
            Log.i("Car type", "==" + Data.defaultCarType);
            String mileStr = "";

            if (decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).thresholdDistance).equalsIgnoreCase("1")) {
                mileStr = " " + getString(R.string.distance_unit);
            } else {
                mileStr = " " + getString(R.string.distance_unit) + "s";
            }
            minFareValue.setText(getResources().getString(R.string.currency_symbol) + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).fixedFare) + " for "
                    + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).thresholdDistance) + mileStr);
            fareAfterValue.setText(getResources().getString(R.string.currency_symbol) + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).farePerDistanceUnit) + " per " + getString(R.string.distance_unit) + " + " + getResources().getString(R.string.currency_symbol)
                    + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).farePerMin) + " per min");

            SpannableString sstr = new SpannableString("Fare");
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            sstr.setSpan(bss, 0, sstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            fareAfterText.setText("");
            fareAfterText.append(sstr);

            fareAfterText.append(" (after " + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).thresholdDistance) + mileStr + ")");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setEndRideData() {
        double totalDistance = Data.totalDistance;

        if (decimalFormat.format(totalDistance).equalsIgnoreCase("1")) {
            tvTotalDistance.setText(getString(R.string.total_distance) + ": " + decimalFormat.format(totalDistance) + " " + getString(R.string.distance_unit));
        } else {
            tvTotalDistance.setText(getString(R.string.total_distance) + ": " + decimalFormat.format(totalDistance) + " " + getString(R.string.distance_unit) + "s");
        }

//			reviewRideTimeValue.setText(Data.rideTime+" min");
        if (Double.parseDouble(Data.rideTime) == 60) {
            tvTotalTime.setText(getString(R.string.total_time) + ": 1 Hr");
        } else if (Double.parseDouble(Data.rideTime) > 60 && Double.parseDouble(Data.rideTime) < 120) {
            tvTotalTime.setText(getString(R.string.total_time) + ": " + (int) (Double.parseDouble(Data.rideTime) / 60) + " Hr " + (int) (Double.parseDouble(Data.rideTime) % 60) + " Min");
        } else if (Double.parseDouble(Data.rideTime) >= 120) {
            tvTotalTime.setText(getString(R.string.total_time) + ": " + (int) (Double.parseDouble(Data.rideTime) / 60) + " Hrs " + (int) (Double.parseDouble(Data.rideTime) % 60) + " Min");
        } else {
            tvTotalTime.setText(getString(R.string.total_time) + ": " + (int) (Double.parseDouble(Data.rideTime) / 1) + " Min");
        }

        if (Data.discount == 0) {
            tvPromoCodeWithValue.setText("");
        } else {
            tvPromoCodeWithValue.setVisibility(View.VISIBLE);
            tvPromoCodeWithValue.setText(getResources().getString(R.string.promo_code) + ": " + Data.endRideData.promoCode);
        }

        // tvtipValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US,"%.2f", Data.tip)));

        tipLayout.setVisibility(View.GONE);
        tvActualFareValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US, "%.2f", Data.totalFare)));
        tvDiscountValue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US, "%.2f", Data.discount)));
        tvPAybleAmountVAlue.setText(getResources().getString(R.string.currency_symbol) + String.valueOf(String.format(Locale.US, "%.2f", Data.totalFare - Data.discount + Data.tip)));

        reviewUserName.setText(Data.assignedDriverInfo.name);

        Data.assignedDriverInfo.image = Data.assignedDriverInfo.image.replace("http://graph.facebook", "https://graph.facebook");
        try {
            Picasso.with(HomeActivity.this).load(Data.assignedDriverInfo.image).skipMemoryCache().transform(new BlurTransform()).into(reviewUserImgBlured);
        } catch (Exception e) {
        }
        try {
            Picasso.with(HomeActivity.this).load(Data.assignedDriverInfo.image).skipMemoryCache().transform(new CircleTransform()).into(reviewUserImage);
        } catch (Exception e) {
        }


//        set Driver Rating
        imgRatingStar1.setBackgroundResource(R.drawable.small_star_empty);
        imgRatingStar2.setBackgroundResource(R.drawable.small_star_empty);
        imgRatingStar3.setBackgroundResource(R.drawable.small_star_empty);
        imgRatingStar4.setBackgroundResource(R.drawable.small_star_empty);
        imgRatingStar5.setBackgroundResource(R.drawable.small_star_empty);
        for (int i = 1; i <= Data.assignedDriverInfo.rating; i++) {
            switch (i) {
                case 1:
                    imgRatingStar1.setBackgroundResource(R.drawable.small_star_filled);
                    break;
                case 2:
                    imgRatingStar2.setBackgroundResource(R.drawable.small_star_filled);
                    break;
                case 3:
                    imgRatingStar3.setBackgroundResource(R.drawable.small_star_filled);
                    break;
                case 4:
                    imgRatingStar4.setBackgroundResource(R.drawable.small_star_filled);
                    break;
                case 5:
                    imgRatingStar5.setBackgroundResource(R.drawable.small_star_filled);
                    break;


            }
        }

//        setFareInfo(reviewMinFareValue, reviewFareAfterValue, reviewFareAfterText);
    }

    public void setAssignedDriverData() {
        driverName.setText(Data.assignedDriverInfo.name);
        if (!"".equalsIgnoreCase(Data.assignedDriverInfo.carNumber)) {
            driverCarNumberText.setText(Data.assignedDriverInfo.carNumber);
        } else {
            driverCarNumberText.setText("");
        }

        updateAssignedDriverETA();

        Data.assignedDriverInfo.image = Data.assignedDriverInfo.image.replace("http://graph.facebook", "https://graph.facebook");
        try {
            Picasso.with(HomeActivity.this).load(Data.assignedDriverInfo.image).skipMemoryCache().transform(new RoundBorderTransform()).into(driverImage);
        } catch (Exception e) {
        }

        Log.i("Data.assignedDriverInfo.carImage", "==" + Data.assignedDriverInfo.carImage);
        Data.assignedDriverInfo.carImage = Data.assignedDriverInfo.carImage.replace("http://graph.facebook", "https://graph.facebook");
        Log.i("Data.assignedDriverInfo.carImage", "==" + Data.assignedDriverInfo.carImage);
        if (!Data.assignedDriverInfo.carImage.equalsIgnoreCase("")) {
            try {
                Picasso.with(HomeActivity.this).load(Data.assignedDriverInfo.carImage).skipMemoryCache().transform(new RoundBorderTransform()).into(driverCarImage);
            } catch (Exception e) {
            }
        } else {
            Log.i("Data.assignedDriverInfo.carImage", "==in else" + Data.assignedDriverInfo.carImage);
//			driverCarImage.setBackgroundResource(R.drawable.taxi_placeholder);
            driverCarImage.setImageDrawable(getResources().getDrawable(R.drawable.taxi_placeholder));
        }

        if (1 == Data.assignedDriverInfo.freeRide) {
            passengerFreeRideIcon.setVisibility(View.VISIBLE);
        } else {
            passengerFreeRideIcon.setVisibility(View.GONE);
        }

    }

    public void updateAssignedDriverETA() {
        if ("".equalsIgnoreCase(Data.assignedDriverInfo.durationToReach) || "no".equalsIgnoreCase(Data.assignedDriverInfo.durationToReach)) {
            driverTime.setText("");
        } else {
            if (Locale.getDefault().getLanguage().equalsIgnoreCase("hi")) {
                driverTime.setText(Data.assignedDriverInfo.durationToReach + " " + getResources().getString(R.string.will_arrive_in));
            } else {
                driverTime.setText(Data.assignedDriverInfo.durationToReach);
            }
        }
    }

    @Override
    public synchronized void onGPSLocationChanged(Location location) {
        drawLocationChanged(location);
    }

    void buildAlertMessageNoGps() {
        if (!(((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                ((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
            if (gpsDialogAlert != null && gpsDialogAlert.isShowing()) {
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.on_gps_message))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                ;
                gpsDialogAlert = null;
                gpsDialogAlert = builder.create();
                gpsDialogAlert.show();
            }
        } else {
            if (gpsDialogAlert != null && gpsDialogAlert.isShowing()) {
                gpsDialogAlert.dismiss();
            }
        }
    }


    public void initializeGPSForegroundLocationFetcher() {
        if (gpsForegroundLocationFetcher == null) {
            gpsForegroundLocationFetcher = new GPSForegroundLocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD);
        }
    }

    public void connectGPSListener() {
        disconnectGPSListener();
        try {
            initializeGPSForegroundLocationFetcher();
            gpsForegroundLocationFetcher.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnectGPSListener() {
        try {
            if (gpsForegroundLocationFetcher != null) {
                gpsForegroundLocationFetcher.destroy();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (slideLayout != null) {
            Log.i("slide initialized on Resume", "slider Initialiezed" + slideLayout);
        } else {
            Log.i("slide initialized on resume not", "slider Initialiezed" + slideLayout);
        }
        if (!checkIfUserDataNull(HomeActivity.this)) {
            setUserData();
            //		SplashNewActivity.isLastLocationUpdateFine(HomeActivity.this);

            if (passengerScreenMode == PassengerScreenMode.P_INITIAL || passengerScreenMode == PassengerScreenMode.P_SEARCH) {
                if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                    startTimerUpdateDrivers();
                }

            }


            connectGPSListener();

            initializeFusedLocationFetchers();


            if (Data.isAcceptedMaximumFare) {
                Data.isAcceptedMaximumFare = false;
//                slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                initiateRequestRide(true);

            } else {
                if (passengerScreenMode == PassengerScreenMode.P_INITIAL) {
                    if (Data.tutorial != 1) {
                        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        Log.i("PanelState onResume", "" + slideLayout.getPanelState());
                    }
                } else {
                    slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                }
            }

        }

        if (passengerScreenMode == PassengerScreenMode.P_SEARCH) {
            if (Data.favouriteHome.equalsIgnoreCase("")) {
                editHome.setVisibility(View.GONE);
                addFavouriteHome.setText(getString(R.string.add_home_location));
                homeLocationPlaceHolder.setVisibility(View.GONE);
                Data.fromFavouriteHome = false;
            } else {
                addFavouriteHome.setText(Data.favouriteHome);
                editHome.setVisibility(View.VISIBLE);
                homeLocationPlaceHolder.setVisibility(View.VISIBLE);
                Data.fromFavouriteHome = false;
            }
            if (removedFavourite) {
                removedFavourite = false;
                etLocation.setText("");
            }
            if (Data.favouriteWork.equalsIgnoreCase("")) {
                editWork.setVisibility(View.GONE);
                addFavouriteWork.setText(getString(R.string.add_work_location));
                workLocationPlaceHolder.setVisibility(View.GONE);
                Data.fromFavouriteWork = false;
            } else {
                workLocationPlaceHolder.setVisibility(View.VISIBLE);
                editWork.setVisibility(View.VISIBLE);
                addFavouriteWork.setText(Data.favouriteWork);
                Data.fromFavouriteWork = false;
            }
        }
    }

    public void saveDataOnPause(final boolean stopWait) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                    Editor editor = pref.edit();

                    if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {

                        editor.putString(Data.SP_CUSTOMER_SCREEN_MODE, Data.P_REQUEST_FINAL);

                        editor.putString(Data.SP_C_ENGAGEMENT_ID, Data.cEngagementId);
                        editor.putString(Data.SP_C_DRIVER_ID, Data.cDriverId);
                        editor.putString(Data.SP_C_LATITUDE, "" + Data.assignedDriverInfo.latLng.latitude);
                        editor.putString(Data.SP_C_LONGITUDE, "" + Data.assignedDriverInfo.latLng.longitude);
                        editor.putString(Data.SP_C_DRIVER_NAME, Data.assignedDriverInfo.name);
                        editor.putString(Data.SP_C_DRIVER_IMAGE, Data.assignedDriverInfo.image);
                        editor.putString(Data.SP_C_DRIVER_CAR_IMAGE, Data.assignedDriverInfo.carImage);
                        editor.putString(Data.SP_C_DRIVER_PHONE, Data.assignedDriverInfo.phoneNumber);
                        editor.putString(Data.SP_C_DRIVER_RATING, "" + Data.assignedDriverInfo.rating);
                        editor.putString(Data.SP_C_DRIVER_DISTANCE, Data.assignedDriverInfo.distanceToReach);
                        editor.putString(Data.SP_C_DRIVER_DURATION, Data.assignedDriverInfo.durationToReach);


                    } else if (passengerScreenMode == PassengerScreenMode.P_IN_RIDE) {

                        editor.putString(Data.SP_CUSTOMER_SCREEN_MODE, Data.P_IN_RIDE);

                        editor.putString(Data.SP_C_ENGAGEMENT_ID, Data.cEngagementId);
                        editor.putString(Data.SP_C_DRIVER_ID, Data.cDriverId);
                        editor.putString(Data.SP_C_LATITUDE, "" + Data.assignedDriverInfo.latLng.latitude);
                        editor.putString(Data.SP_C_LONGITUDE, "" + Data.assignedDriverInfo.latLng.longitude);
                        editor.putString(Data.SP_C_DRIVER_NAME, Data.assignedDriverInfo.name);
                        editor.putString(Data.SP_C_DRIVER_IMAGE, Data.assignedDriverInfo.image);
                        editor.putString(Data.SP_C_DRIVER_CAR_IMAGE, Data.assignedDriverInfo.carImage);
                        editor.putString(Data.SP_C_DRIVER_PHONE, Data.assignedDriverInfo.phoneNumber);
                        editor.putString(Data.SP_C_DRIVER_RATING, "" + Data.assignedDriverInfo.rating);
                        editor.putString(Data.SP_C_DRIVER_DISTANCE, Data.assignedDriverInfo.distanceToReach);
                        editor.putString(Data.SP_C_DRIVER_DURATION, Data.assignedDriverInfo.durationToReach);


                        editor.putString(Data.SP_TOTAL_DISTANCE, "" + totalDistance);

                        if (HomeActivity.this.lastLocation != null) {
                            editor.putString(Data.SP_LAST_LATITUDE, "" + HomeActivity.this.lastLocation.getLatitude());
                            editor.putString(Data.SP_LAST_LONGITUDE, "" + HomeActivity.this.lastLocation.getLongitude());
                        }


                    } else {
                        editor.putString(Data.SP_CUSTOMER_SCREEN_MODE, "");
                    }

                    editor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Override
    protected void onPause() {

        GCMIntentService.clearNotifications(getApplicationContext());
        saveDataOnPause(false);

        try {
            cancelTimerUpdateDrivers();
            disconnectGPSListener();

        } catch (Exception e) {
            e.printStackTrace();
        }

        destroyFusedLocationFetchers();

        super.onPause();

    }

    @Override
    public void onBackPressed() {
        try {
            if (PassengerScreenMode.P_SEARCH == passengerScreenMode) {
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
                switchPassengerScreen(passengerScreenMode);
            } else {
                if (slideLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    if (Data.tutorial != 1) {
                        slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    } else {
                        ActivityCompat.finishAffinity(this);
                    }
                } else {
                    ActivityCompat.finishAffinity(this);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ActivityCompat.finishAffinity(this);
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (createPathAsyncTasks != null) {
                createPathAsyncTasks.clear();
            }
            saveDataOnPause(true);

            GCMIntentService.clearNotifications(HomeActivity.this);

            disconnectGPSListener();

            destroyFusedLocationFetchers();

            ASSL.closeActivity(mainRelativeOutside);
            cancelTimerUpdateDrivers();

            appInterruptHandler = null;

            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    public synchronized void drawLocationChanged(Location location) {
        try {
            if (map != null) {
                HomeActivity.myLocation = location;
                zoomToCurrentLocationAtFirstLocationFix(location);

                updatePickupLocation(location);

                if (passengerScreenMode == PassengerScreenMode.P_IN_RIDE) {

                    final LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                    if (Utils.compareDouble(totalDistance, -1.0) == 0) {
                        lastLocation = null;
                        Log.i("lastLocation made null", "=" + lastLocation);
                    }

                    Log.i("lastLocation", "=" + lastLocation);
                    Log.i("totalDistance", "=" + totalDistance);


                    if (lastLocation != null) {
                        final LatLng lastLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                        addLatLngPathToDistance(lastLatLng, currentLatLng);
                    } else {
                        if (Utils.compareDouble(totalDistance, -1.0) == 0) {
                            totalDistance = 0;
                        }
                        displayOldPath();

                        addLatLngPathToDistance(Data.startRidePreviousLatLng, currentLatLng);
                    }

                    lastLocation = location;

                    saveDataOnPause(false);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized void addLatLngPathToDistance(final LatLng lastLatLng, final LatLng currentLatLng) {
        try {
            double displacement = MapUtils.distance(lastLatLng, currentLatLng);
            Log.i("displacement", "=" + displacement);


            if (Utils.compareDouble(displacement, MAX_DISPLACEMENT_THRESHOLD) == -1) {

                boolean validDistance = updateTotalDistance(lastLatLng, currentLatLng, displacement);
                if (validDistance) {

                    map.addPolyline(new PolylineOptions()
                            .add(lastLatLng, currentLatLng)
                            .width(5)
                            .color(MAP_PATH_COLOR).geodesic(true));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Database.getInstance(HomeActivity.this).insertPolyLine(lastLatLng, currentLatLng);
                            Database.getInstance(HomeActivity.this).close();
                        }
                    }).start();
                }


            } else {
                callGooglePathAPI(lastLatLng, currentLatLng, displacement);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void callGooglePathAPI(LatLng lastLatLng, LatLng currentLatLng, double displacement) {
        if (createPathAsyncTasks == null) {
            createPathAsyncTasks = new ArrayList<HomeActivity.CreatePathAsyncTask>();
        }
        CreatePathAsyncTask createPathAsyncTask = new CreatePathAsyncTask(lastLatLng, currentLatLng, displacement);
        if (!createPathAsyncTasks.contains(createPathAsyncTask)) {
            createPathAsyncTasks.add(createPathAsyncTask);
            createPathAsyncTask.execute();
        }
    }

    public synchronized void displayOldPath() {

        try {
            ArrayList<Pair<LatLng, LatLng>> path = Database.getInstance(HomeActivity.this).getSavedPath();
            Database.getInstance(HomeActivity.this).close();

            LatLng firstLatLng = null;

            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.width(5);
            polylineOptions.color(MAP_PATH_COLOR);
            polylineOptions.geodesic(true);

            for (Pair<LatLng, LatLng> pair : path) {
                LatLng src = pair.first;
                LatLng dest = pair.second;

                if (firstLatLng == null) {
                    firstLatLng = src;
                }

                polylineOptions.add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude));
            }

            map.addPolyline(polylineOptions);

            if (firstLatLng == null) {
                firstLatLng = Data.startRidePreviousLatLng;
            }

            if (firstLatLng != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.snippet("");
                markerOptions.title("start ride location");
                markerOptions.position(firstLatLng);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPinMarkerBitmap(HomeActivity.this, assl)));
                map.addMarker(markerOptions);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized boolean updateTotalDistance(LatLng lastLatLng, LatLng currentLatLng, double deltaDistance) {
        boolean validDistance = false;
        if (deltaDistance > 0.0) {
            LatLngPair latLngPair = new LatLngPair(lastLatLng, currentLatLng, deltaDistance);

            Log.e("latLngPair to add", "=" + latLngPair);

            if (HomeActivity.deltaLatLngPairs == null) {
                HomeActivity.deltaLatLngPairs = new ArrayList<LatLngPair>();
            }

            if (!HomeActivity.deltaLatLngPairs.contains(latLngPair)) {
                totalDistance = totalDistance + deltaDistance;
                HomeActivity.deltaLatLngPairs.add(latLngPair);
                validDistance = true;
            }
        }
        Log.e("HomeActivity.deltaLatLngPairs", "=" + HomeActivity.deltaLatLngPairs);
        return validDistance;
    }

    public synchronized void drawPath(String result, double displacementToCompare, LatLng source, LatLng destination) {
        try {

            final JSONObject json = new JSONObject(result);

            JSONObject leg0 = json.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0);
            double distanceOfPath = leg0.getJSONObject("distance").getDouble("value");


            if (Utils.compareDouble(distanceOfPath, (displacementToCompare * 1.8)) <= 0) {                                                        // distance would be approximately correct

                JSONArray routeArray = json.getJSONArray("routes");
                JSONObject routes = routeArray.getJSONObject(0);
                JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                String encodedString = overviewPolylines.getString("points");
                List<LatLng> list = MapUtils.decodeDirectionsPolyline(encodedString);

//			    	totalDistance = totalDistance + distanceOfPath;
                boolean validDistance = updateTotalDistance(source, destination, distanceOfPath);
                if (validDistance) {


                    for (int z = 0; z < list.size() - 1; z++) {
                        LatLng src = list.get(z);
                        LatLng dest = list.get(z + 1);
                        map.addPolyline(new PolylineOptions()
                                .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude))
                                .width(5)
                                .color(MAP_PATH_COLOR).geodesic(true));
                        Database.getInstance(this).insertPolyLine(src, dest);
                    }
                    Database.getInstance(this).close();
                }
            } else {                                                                                                    // displacement would be correct
//	    		totalDistance = totalDistance + displacementToCompare;
                boolean validDistance = updateTotalDistance(source, destination, displacementToCompare);
                if (validDistance) {


                    map.addPolyline(new PolylineOptions()
                            .add(new LatLng(source.latitude, source.longitude), new LatLng(destination.latitude, destination.longitude))
                            .width(5)
                            .color(MAP_PATH_COLOR).geodesic(true));
                    Database.getInstance(this).insertPolyLine(source, destination);
                    Database.getInstance(this).close();
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDriverMarkerForCustomer(DriverInfo driverInfo) {
        if (carAndroidBitmap == null) {
            carAndroidBitmap = CustomMapMarkerCreator.createCarMarkerBitmap(HomeActivity.this, assl);
        }

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.title("driver shown to customer");
        markerOptions.snippet("" + driverInfo.userId);
        markerOptions.position(driverInfo.latLng);
        markerOptions.rotation((float) driverInfo.bearing);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(carAndroidBitmap));
        markerOptions.anchor(0.5f, 0.7f);
        map.addMarker(markerOptions);
    }

    public void addCurrentLocationAddressMarker(LatLng latLng) {
        try {
            if (Data.userData.canChangeLocation == 0) {
                if (currentLocationMarker != null) {
                    currentLocationMarker.remove();
                }
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title("customer_current_location");
                markerOptions.snippet("");
                markerOptions.position(latLng);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(CustomMapMarkerCreator.createPinMarkerBitmap(HomeActivity.this, assl)));
                currentLocationMarker = map.addMarker(markerOptions);
            }
        } catch (Exception e) {
        }
    }

    public void showDriverMarkersAndPanMap(final LatLng userLatLng) {
        if (passengerScreenMode == PassengerScreenMode.P_INITIAL || passengerScreenMode == PassengerScreenMode.P_SEARCH) {
            if (map != null) {
                map.clear();
                if (map != null && !Data.favouriteHome.equalsIgnoreCase("")) {
                    markerOptionsHome.position(Prefs.with(getApplicationContext()).getObject("fav_home_latLng", LatLng.class));
                    homeLocationMarker = map.addMarker(markerOptionsHome);
                } else {
                    try {
                        homeLocationMarker.remove();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (map != null && !Data.favouriteWork.equalsIgnoreCase("")) {
                    markerOptionsWork.position(Prefs.with(getApplicationContext()).getObject("fav_work_latLng", LatLng.class));
                    workLocationMarker = map.addMarker(markerOptionsWork);
                } else {
                    try {
                        workLocationMarker.remove();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                addCurrentLocationAddressMarker(userLatLng);
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                farthestLatLng = null;
                double maxDistance = 0;

                for (int i = 0; i < Data.driverInfos.size(); i++) {
                    addDriverMarkerForCustomer(Data.driverInfos.get(i));
                    if (MapUtils.distance(userLatLng, Data.driverInfos.get(i).latLng) > maxDistance) {
                        maxDistance = MapUtils.distance(userLatLng, Data.driverInfos.get(i).latLng);
                        farthestLatLng = Data.driverInfos.get(i).latLng;
                    }
                }

                if (!mapTouchedOnce) {
                    if (farthestLatLng != null) {
                        boundsBuilder.include(new LatLng(userLatLng.latitude, farthestLatLng.longitude));
                        boundsBuilder.include(new LatLng(farthestLatLng.latitude, userLatLng.longitude));
                        boundsBuilder.include(new LatLng(userLatLng.latitude, ((2 * userLatLng.longitude) - farthestLatLng.longitude)));
                        boundsBuilder.include(new LatLng(((2 * userLatLng.latitude) - farthestLatLng.latitude), userLatLng.longitude));
                    }

                    boundsBuilder.include(userLatLng);

                    try {
                        final LatLngBounds bounds = boundsBuilder.build();
                        final float minScaleRatio = Math.min(ASSL.Xscale(), ASSL.Yscale());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (farthestLatLng != null) {
//                                        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) (160 * minScaleRatio)), 1000, null);
                                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(userLatLng.latitude, userLatLng.longitude), 15));

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                mapTouchedOnce = true;
                            }
                        }, 1000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

    /**
     * ASync for cancelCustomerRequestAsync from server
     */
    public void cancelCustomerRequestAsync(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("session_id", "=" + Data.cSessionId);

            RestClient.getApiService().cancelCustomerRequestAsync(Data.userData.accessToken, Data.cSessionId, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.e("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        int flag = jObj.getInt("flag");

                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        } else {
                            customerUIBackToInitialAfterCancel();
                            FlurryEventLogger.cancelRequestPressed(Data.userData.accessToken, Data.cSessionId);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    callAndHandleStateRestoreAPI();
                }
            });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }

    public void customerUIBackToInitialAfterCancel() {
        cancelTimerRequestRide();

        passengerScreenMode = PassengerScreenMode.P_INITIAL;
        switchPassengerScreen(passengerScreenMode);
        callMapTouchedRefreshDrivers();
        if (map != null && pickupLocationMarker != null) {
            pickupLocationMarker.remove();
        }

    }

    public void initializeStartRideVariables() {
        if (createPathAsyncTasks != null) {
            createPathAsyncTasks.clear();
        }

        lastLocation = null;

        HomeActivity.previousWaitTime = 0;
        HomeActivity.previousRideTime = 0;
        HomeActivity.totalDistance = -1;
        customerWaitTime = 0;
        waitCustomerChronometer.eclipsedTime = 0;

        if (HomeActivity.deltaLatLngPairs == null) {
            HomeActivity.deltaLatLngPairs = new ArrayList<LatLngPair>();
        }
        HomeActivity.deltaLatLngPairs.clear();

        clearRideSPData();

        waitStart = 2;
    }

    public void displayCouponApplied(JSONObject jObj) {
        try {


            if (jObj.has("coupon")) {

                if (jObj.getInt("coupon") == 1) {
                    imgPromoApplied.setVisibility(View.VISIBLE);
                } else {
                    imgPromoApplied.setVisibility(View.GONE);
                }

            } else {

                imgPromoApplied.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            imgPromoApplied.setVisibility(View.GONE);

        }
    }

    public void submitFeedbackToDriverAsync(final Activity activity, String engagementId, String ratingReceiverId, String givenRating, String feedbackText) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("given_rating", givenRating);
            Log.i("engagement_id", engagementId);
            Log.i("driver_id", ratingReceiverId);
            Log.i("feedback", feedbackText);


            RestClient.getApiService().submitFeedbackToDriverAsync(Data.userData.accessToken, givenRating, engagementId, ratingReceiverId, feedbackText, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.i("Server response", "response = " + response);
                    try {
                        jObj = new JSONObject(response);
                        int flag = jObj.getInt("flag");
                        if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                            HomeActivity.logoutUser(activity);
                        } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                            String errorMessage = jObj.getString("error");
                            new DialogPopup().alertPopup(activity, "", errorMessage);
                        } else if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                            String message = jObj.getString("message");
                            new DialogPopup().alertPopup(activity, "", message);
                        } else if (ApiResponseFlags.ACTION_COMPLETE.getOrdinal() == flag) {

                            switchUserScreen();
                            if (jObj.has("having_promo")) {
                                Data.userData.isPromoApplied = jObj.getInt("having_promo");
                            }
                            setUserData();
                            canChangeLocationText = true;
                            etDriverFeedback.setText("");
                            Utils.hideSoftKeyboard(HomeActivity.this, etDriverFeedback);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                            passengerScreenMode = PassengerScreenMode.P_INITIAL;
                            switchPassengerScreen(passengerScreenMode);
                            callMapTouchedRefreshDrivers();
                            Toast.makeText(activity, getString(R.string.feedback_submitted), Toast.LENGTH_SHORT).show();

                        } else {
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }
                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }

    public void skipFeedbackForCustomerAsync(final Activity activity, String engagementId) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", engagementId);

            RestClient.getApiService().skipFeedbackForCustomerAsync(Data.userData.accessToken, engagementId, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.i("Server response", "response = " + response);
                    try {
                        jObj = new JSONObject(response);
                        int flag = jObj.getInt("flag");
                        if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                            HomeActivity.logoutUser(activity);
                        } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                            String errorMessage = jObj.getString("error");
                            new DialogPopup().alertPopup(activity, "", errorMessage);
                        } else if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                            String message = jObj.getString("message");
                            new DialogPopup().alertPopup(activity, "", message);
                        } else if (ApiResponseFlags.ACTION_COMPLETE.getOrdinal() == flag) {

                            switchUserScreen();
                            if (jObj.has("having_promo")) {
                                Data.userData.isPromoApplied = jObj.getInt("having_promo");
                            }

                            setUserData();

                            canChangeLocationText = true;
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                            passengerScreenMode = PassengerScreenMode.P_INITIAL;
                            switchPassengerScreen(passengerScreenMode);
                            callMapTouchedRefreshDrivers();
                        } else {
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }
                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }


    /**
     * ASync for logout from server
     */
    public void logoutAsync(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, "Please Wait ...");

            RestClient.getApiService().logoutAsync(Data.userData.accessToken, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        } else {

                            PicassoTools.clearCache(Picasso.with(HomeActivity.this));

                            try {
                                Session.getActiveSession().closeAndClearTokenInformation();
                            } catch (Exception e) {
                                Log.v("Logout", "Error" + e);
                            }

                            GCMIntentService.clearNotifications(HomeActivity.this);

                            Data.clearDataOnLogout(HomeActivity.this);


                            passengerScreenMode = PassengerScreenMode.P_INITIAL;

                            loggedOut = true;

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
            Log.i("access_token", "=" + Data.userData.accessToken);
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }

    void logoutPopup(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            textHead.setVisibility(View.VISIBLE);
            textHead.setText(getResources().getString(R.string.logout));
            textHead.setVisibility(View.VISIBLE);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            ImageView imgHorizontalLine = (ImageView) dialog.findViewById(R.id.imgHorizontalLine);
            imgHorizontalLine.setVisibility(View.VISIBLE);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    logoutAsync(activity);
                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void splitFarePopup(final Activity activity, final JSONObject jobj) {
        try {
            Log.i("Dialog", "Dialog");
            splitFareDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            splitFareDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            splitFareDialog.setContentView(R.layout.split_request_dialog);

            FrameLayout frameLayout = (FrameLayout) splitFareDialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = splitFareDialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            splitFareDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            splitFareDialog.setCancelable(false);
            splitFareDialog.setCanceledOnTouchOutside(false);


//			"user_name": "Renuka",
//		    "time": 30,
//		    "flag": 11,
//		    "split_id": 84,
//		    "user_image": "http://taximust.s3.amazonaws.com/user_profile/7277-2665-userImage.jpg"

            TextView tvPhoneNumber = (TextView) splitFareDialog.findViewById(R.id.textPhoneNumber);
            tvPhoneNumber.setTypeface(Data.getFont(activity));
            tvPhoneNumber.setText(jobj.getString("phone_no"));
            TextView textMessage = (TextView) splitFareDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textMessage.setText(jobj.getString("user_name") + " " + getString(R.string.split_fare_invitation));
            ImageView imgUserPic = (ImageView) splitFareDialog.findViewById(R.id.reviewUserImage);
            String userImage = jobj.getString("user_image").replace("http://graph.facebook", "https://graph.facebook");

            try {
                Picasso.with(HomeActivity.this).load(userImage).skipMemoryCache().transform(new CircleTransform()).into(imgUserPic);
            } catch (Exception e) {
            }
            Button btnAccept = (Button) splitFareDialog.findViewById(R.id.btnAccept);
            btnAccept.setTypeface(Data.getFont(activity));
            Button btnReject = (Button) splitFareDialog.findViewById(R.id.btnReject);
            btnReject.setTypeface(Data.getFont(activity));

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        acceptDeclineSplitFareRequest(HomeActivity.this, jobj.getString("split_id"), jobj.getInt("car_type"), 0);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        acceptDeclineSplitFareRequest(HomeActivity.this, jobj.getString("split_id"), jobj.getInt("car_type"), 1);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            });
            Log.i("Dialog1", "Dialog1");
            splitFareDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void noDriverAvailablePopup(final Activity activity, String message) {
        try {
            if (noDriversDialog != null && noDriversDialog.isShowing()) {
                noDriversDialog.dismiss();
            }
            noDriversDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            noDriversDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            noDriversDialog.setContentView(R.layout.custom_message_dialog);

            FrameLayout frameLayout = (FrameLayout) noDriversDialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = noDriversDialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            noDriversDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            noDriversDialog.setCancelable(false);
            noDriversDialog.setCanceledOnTouchOutside(false);

            TextView textHead = (TextView) noDriversDialog.findViewById(R.id.textHead);
            ImageView textImg = (ImageView) noDriversDialog.findViewById(R.id.textImg);
            textImg.setVisibility(View.GONE);
            textHead.setVisibility(View.GONE);
            TextView textMessage = (TextView) noDriversDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));


            if ("".equalsIgnoreCase(message)) {
                textMessage.setText(getString(R.string.driver_busy_message));
            } else {
                textMessage.setText(message);

            }


            Button btnOk = (Button) noDriversDialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    noDriversDialog.dismiss();
//					noDriverAsync(activity);
                    noDriversDialog = null;
                }
            });

            noDriversDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ASync for no drivers from server
     */
    public void noDriverAsync(final Activity activity) {
        if (AppStatus.getInstance(activity).isOnline(activity)) {

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            if (myLocation != null) {
                Data.latitude = myLocation.getLatitude();
                Data.longitude = myLocation.getLongitude();
            }

            RestClient.getApiService().noDriverAsync(Data.userData.accessToken, "" + Data.latitude, "" + Data.longitude, new Callback<String>() {
                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                }
            });
        }
    }


    public void startDriverLocationUpdateTimer() {

        try {
            if (timerTaskDriverLocationUpdater != null) {
                timerTaskDriverLocationUpdater.cancel();
                timerTaskDriverLocationUpdater = null;
            }

            if (timerDriverLocationUpdater != null) {
                timerDriverLocationUpdater.cancel();
                timerDriverLocationUpdater.purge();
                timerDriverLocationUpdater = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            timerDriverLocationUpdater = new Timer();

            timerTaskDriverLocationUpdater = new TimerTask() {

                @Override
                public void run() {
                    if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

                        if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {

                            RestClient.getApiService().getDriverLocation(Data.userData.accessToken, Data.assignedDriverInfo.userId, new Callback<String>() {
                                @Override
                                public void success(String result, Response response) {
                                    if (result.contains("SERVER_TIMEOUT")) {
                                    } else {
                                        try {
                                            JSONObject jObj = new JSONObject(result);

                                            if (!jObj.isNull("error")) {

                                                String errorMessage = jObj.getString("error");
                                                if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                                    HomeActivity.logoutUser(activity);
                                                } else {

                                                }
                                            } else {


                                                JSONArray data = jObj.getJSONArray("data");
                                                final JSONObject data0 = data.getJSONObject(0);
                                                final LatLng driverCurrentLatLng = new LatLng(data0.getDouble("current_location_latitude"),
                                                        data0.getDouble("current_location_longitude"));

                                                if (Data.assignedDriverInfo != null) {
                                                    Data.assignedDriverInfo.latLng = driverCurrentLatLng;
                                                }

                                                runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {
                                                                driverLocationMarker.setPosition(driverCurrentLatLng);
                                                                if (data0.has("driver_car_direction_angle")) {
                                                                    driverLocationMarker.setRotation((float) data0.getDouble("driver_car_direction_angle"));
                                                                }
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });

                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("get_driver_current_location hit error", "" + error.toString());
                                }
                            });
                        }
                    }

                }
            };


            timerDriverLocationUpdater.scheduleAtFixedRate(timerTaskDriverLocationUpdater, 10, 15000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelDriverLocationUpdateTimer() {
        try {
            if (timerTaskDriverLocationUpdater != null) {
                timerTaskDriverLocationUpdater.cancel();
                timerTaskDriverLocationUpdater = null;
            }

            if (timerDriverLocationUpdater != null) {
                timerDriverLocationUpdater.cancel();
                timerDriverLocationUpdater.purge();
                timerDriverLocationUpdater = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startTimerUpdateDrivers() {
        cancelTimerUpdateDrivers();
        try {
            timerUpdateDrivers = new Timer();
            timerTaskUpdateDrivers = new TimerTask() {
                @Override
                public void run() {
                    try {
                        if (HomeActivity.appInterruptHandler != null) {
                            HomeActivity.appInterruptHandler.refreshDriverLocations();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            timerUpdateDrivers.scheduleAtFixedRate(timerTaskUpdateDrivers, 100, 60000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void forceStateChange() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    try {
                        if (forceDialog != null && forceDialog.isShowing()) {
                            forceDialog.dismiss();
                        }
                    } catch (Exception e) {
                    }

                    forceDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
                    forceDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
                    forceDialog.setContentView(R.layout.custom_message_dialog);

                    FrameLayout frameLayout = (FrameLayout) forceDialog.findViewById(R.id.rv);
                    new ASSL(activity, frameLayout, 1134, 720, false);

                    WindowManager.LayoutParams layoutParams = forceDialog.getWindow().getAttributes();
                    layoutParams.dimAmount = 0.6f;
                    forceDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    forceDialog.setCancelable(false);
                    forceDialog.setCanceledOnTouchOutside(false);


                    TextView textHead = (TextView) forceDialog.findViewById(R.id.textHead);
                    textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
                    TextView textMessage = (TextView) forceDialog.findViewById(R.id.textMessage);
                    textMessage.setTypeface(Data.getFont(activity));

                    textMessage.setMovementMethod(new ScrollingMovementMethod());
                    textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

                    textMessage.setText(getString(R.string.forcefully_end_message));

                    textHead.setVisibility(View.VISIBLE);

                    Button btnOk = (Button) forceDialog.findViewById(R.id.btnOk);
                    btnOk.setTypeface(Data.getFont(activity));

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                forceDialog.dismiss();
                                if (myLocation != null) {
                                    myLocationBtn.performClick();
                                }
                                callAndHandleStateRestoreAPI();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });

                    forceDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void cancelTimerUpdateDrivers() {
        try {
            if (timerTaskUpdateDrivers != null) {
                timerTaskUpdateDrivers.cancel();
                timerTaskUpdateDrivers = null;
            }
            if (timerUpdateDrivers != null) {
                timerUpdateDrivers.cancel();
                timerUpdateDrivers.purge();
                timerUpdateDrivers = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startMapAnimateAndUpdateRideDataTimer() {
        cancelMapAnimateAndUpdateRideDataTimer();
        try {
            timerMapAnimateAndUpdateRideData = new Timer();

            timerTaskMapAnimateAndUpdateRideData = new TimerTask() {

                @Override
                public void run() {
                    try {

                        if (myLocation != null && map != null) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            timerMapAnimateAndUpdateRideData.scheduleAtFixedRate(timerTaskMapAnimateAndUpdateRideData, 100, 60000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelMapAnimateAndUpdateRideDataTimer() {
        try {
            if (timerTaskMapAnimateAndUpdateRideData != null) {
                timerTaskMapAnimateAndUpdateRideData.cancel();
                timerTaskMapAnimateAndUpdateRideData = null;
            }

            if (timerMapAnimateAndUpdateRideData != null) {
                timerMapAnimateAndUpdateRideData.cancel();
                timerMapAnimateAndUpdateRideData.purge();
                timerMapAnimateAndUpdateRideData = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<LatLng> getLatLngListFromPath(String result) {
        try {
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = MapUtils.decodeDirectionsPolyline(encodedString);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<LatLng>();
        }
    }

    void callAnAutoPopup(final Activity activity) {
        try {
            String getAuto = getString(R.string.when);
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            new ASSL(activity, (FrameLayout) dialog.findViewById(R.id.rv), 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setVisibility(View.VISIBLE);
            if (Data.userData.canSchedule == 1) {
                btnOk.setText(getString(R.string.now));
                btnCancel.setText(getString(R.string.later));

            } else {
                btnOk.setText(getString(R.string.confirm));
                btnCancel.setText(getString(R.string.cancel));
                getAuto = getString(R.string.request_ride_confirmation_message);
            }

            if (Data.userData.canChangeLocation == 1) {
                Data.pickupLatLng = map.getCameraPosition().target;
                double distance = MapUtils.distance(Data.pickupLatLng, new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                if (distance > MAP_PAN_DISTANCE_CHECK) {
                    textMessage.setText(getString(R.string.different_pickup_location));
                } else {
                    textMessage.setText(getAuto);
                }
            } else {
                textMessage.setText(getAuto);
            }


            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                    if (Data.driverInfos.size() > 0) {
                        if (Data.surgeFactor > 1) {
                            surgeSlider = true;
                            slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

                        } else {
                            initiateRequestRide(true);
                        }

                    } else {
                        noDriverAvailablePopup(activity, "");
                    }
                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    if (Data.userData != null) {
                        if (Data.userData.canSchedule == 1) {
                            scheduleSlider = true;
                            slideLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

                        }
                    }
                }
            });


            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.findViewById(R.id.rl1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });


            dialog.findViewById(R.id.rv).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//					dialog.dismiss();
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    DateOperations dateOperations;

    public DateOperations getDateOperations() {
        if (dateOperations == null) {
            dateOperations = new DateOperations();
        }
        return dateOperations;
    }

    void cancelAfterAcceptPopup(final Activity activity) {
        try {

            String message = getString(R.string.cancel_ride_message);
            if (!FeaturesConfig.cashOnly) {
                long timeDiff = (getDateOperations().getTimeDifference(DateOperations.getCurrentTime(), DateOperations.utcToLocal(Data.rideAcceptedTime)));

                Log.i("Ride Accept Time", "" + DateOperations.utcToLocal(Data.rideAcceptedTime));
                Log.i("Time differnce in minutes", "" + timeDiff * 0.00001667);
                Log.i("Threshhold time", "" + Data.fareStructureArrayList.get(Data.defaultCarType).cancelThresholdTime);
                if (Data.isArrived == 1 || (timeDiff * 0.00001667) >= Data.fareStructureArrayList.get(Data.defaultCarType).cancelThresholdTime) {
                    message = getString(R.string.cancel_after_threshold_time_message);
                }

            }

            cancelAfterAcceptDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            cancelAfterAcceptDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            cancelAfterAcceptDialog.setContentView(R.layout.custom_two_btn_dialog);

            new ASSL(activity, (FrameLayout) cancelAfterAcceptDialog.findViewById(R.id.rv), 1134, 720, true);

            WindowManager.LayoutParams layoutParams = cancelAfterAcceptDialog.getWindow().getAttributes();
            layoutParams.dimAmount = 0.6f;
            cancelAfterAcceptDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            cancelAfterAcceptDialog.setCancelable(false);
            cancelAfterAcceptDialog.setCanceledOnTouchOutside(false);


//			TextView textHead = (TextView) dialog.findViewById(R.id.textHead); textHead.setTypeface(Data.regularFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) cancelAfterAcceptDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
//
            textMessage.setText(message);


            Button btnOk = (Button) cancelAfterAcceptDialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity));
            Button btnCancel = (Button) cancelAfterAcceptDialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));
            Button crossbtn = (Button) cancelAfterAcceptDialog.findViewById(R.id.crossbtn);
            crossbtn.setVisibility(View.VISIBLE);

            btnOk.setText(getString(R.string.ok));
            btnCancel.setText(getString(R.string.cancel));

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelAfterAcceptDialog.dismiss();
                    cancelRideAfterAccept(activity);
                }

            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelAfterAcceptDialog.dismiss();

                }
            });


            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelAfterAcceptDialog.dismiss();
                }
            });


            cancelAfterAcceptDialog.findViewById(R.id.rl1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });


            cancelAfterAcceptDialog.findViewById(R.id.rv).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//					dialog.dismiss();
                }
            });


            cancelAfterAcceptDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void switchToScheduleScreen(Activity activity) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        calendar.add(Calendar.MINUTE, 5);
        ScheduleRideActivity.selectedScheduleCalendar = calendar;
        ScheduleRideActivity.selectedScheduleLatLng = Data.pickupLatLng;
        ScheduleRideActivity.pickupAddress = pickUpLocationText.getText().toString();
        ScheduleRideActivity.selectedDestinationLatLng = Data.destinationLatLng;
        ScheduleRideActivity.destinationAddress = destinationLocationtext.getText().toString();
        ScheduleRideActivity.scheduleOperationMode = ScheduleOperationMode.INSERT;
        activity.startActivity(new Intent(activity, ScheduleRideActivity.class));
        activity.overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    public void confirmDebugPasswordPopup(final Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.edittext_confirm_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            final EditText etCode = (EditText) dialog.findViewById(R.id.etCode);
            etCode.setTypeface(Data.getFont(activity));

            textHead.setText("Confirm Debug Password");
            textMessage.setText("Please enter password to continue.");

            textHead.setVisibility(View.GONE);
            textMessage.setVisibility(View.GONE);

            final Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
            btnConfirm.setTypeface(Data.getFont(activity));
            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String code = etCode.getText().toString().trim();
                    if ("".equalsIgnoreCase(code)) {
                        etCode.requestFocus();
                        etCode.setError("Code can't be empty.");
                    } else {
                        if (Data.DEBUG_PASSWORD.equalsIgnoreCase(code)) {
                            dialog.dismiss();
                            changeDebugModePopup(activity);
                        } else {
                            etCode.requestFocus();
                            etCode.setError("Code not matched.");
                        }
                    }
                }

            });


            etCode.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    int result = actionId & EditorInfo.IME_MASK_ACTION;
                    switch (result) {
                        case EditorInfo.IME_ACTION_DONE:
                            btnConfirm.performClick();
                            break;

                        case EditorInfo.IME_ACTION_NEXT:
                            break;

                        default:
                    }
                    return true;
                }
            });

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void changeDebugModePopup(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));


            if (appMode == AppMode.DEBUG) {
                textMessage.setText("App is in DEBUG mode.\nChange to:");
            } else if (appMode == AppMode.NORMAL) {
                textMessage.setText("App is in NORMAL mode.\nChange to:");
            }


            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity));
            btnOk.setText("NORMAL");
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));
            btnCancel.setText("DEBUG");

            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));
            crossbtn.setVisibility(View.VISIBLE);


            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appMode = AppMode.NORMAL;
                    dialog.dismiss();
                }


            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appMode = AppMode.DEBUG;
                    dialog.dismiss();
                }

            });

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void debugDriverInfoPopup(final Activity activity, final DriverInfo driverInfo) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setText("Driver Info");
            textMessage.setText(driverInfo.toString());

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity));
            btnOk.setText("Call");
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    Intent callIntent = new Intent(Intent.ACTION_VIEW);
                    callIntent.setData(Uri.parse("tel:" + driverInfo.phoneNumber));
                    startActivity(callIntent);
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);


            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (loggedOut) {
                loggedOut = false;

                cancelTimerUpdateDrivers();
                Intent intent = new Intent(HomeActivity.this, SplashNewActivity.class);
                intent.putExtra("no_anim", "yes");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            } else {
                buildAlertMessageNoGps();
            }
        }

    }

    @Override
    public void startRideForCustomer(final int flag) {
        try {
            if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {
                Log.e("in ", "herestartRideForCustomer");

                if (flag == 0) {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Log.i("in in herestartRideForCustomer  run class", "=");
                            initializeStartRideVariables();
                            Data.isArrived = 1;
                            passengerScreenMode = PassengerScreenMode.P_IN_RIDE;
                            switchPassengerScreen(passengerScreenMode);
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            try {
                                if (cancelAfterAcceptDialog != null && cancelAfterAcceptDialog.isShowing()) {
                                    cancelAfterAcceptDialog.dismiss();
                                    cancelAfterAcceptDialog = null;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            Log.i("in in herestartRideForCustomer  run class", "=");
                            passengerScreenMode = PassengerScreenMode.P_INITIAL;
                            switchPassengerScreen(passengerScreenMode);

                            GCMIntentService.RIDE_REJECTED = false;

                            new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.driver_cancel_ride));
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * ASync for cancelCustomerRequestAsync from server
     */
    public void cancelRideAfterAccept(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", "=" + Data.cEngagementId);
            RestClient.getApiService().cancelRideAfterAccept(Data.cEngagementId, Data.userData.accessToken, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    Log.e("Server response", "response = " + response);

                    try {
                        JSONObject jObj = new JSONObject(responseString);


                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        } else {
                            customerUIBackToInitialAfterCancel();
                            FlurryEventLogger.cancelRequestPressed(Data.userData.accessToken, Data.cEngagementId);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
//							new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                    callAndHandleStateRestoreAPI();
                }
            });

        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }


    @Override
    public void refreshDriverLocations() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    callMapTouchedRefreshDrivers();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // 0 = not found   1 = accept
    @Override
    public void rideRequestAcceptedInterrupt(JSONObject jObj) {

        try {

            if (jObj.getString("session_id").equalsIgnoreCase(Data.cSessionId)) {
                cancelTimerUpdateDrivers();
                cancelTimerRequestRide();
                fetchAcceptedDriverInfoAndChangeState(jObj);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void driverArrivedInterrupt() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    try {
                        if (cancelAfterAcceptDialog != null && cancelAfterAcceptDialog.isShowing()) {
                            cancelAfterAcceptDialog.dismiss();
                        }
                    } catch (Exception e) {
                    }

                    if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {
                        Data.isArrived = 1;
                        timeTillArrived = 0;
                        switchPassengerScreen(passengerScreenMode);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }


//

    @Override
    public void onSplitFareRequestRecieved(final JSONObject jObj) {
        // TODO Auto-generated method stub
        try {
            if (passengerScreenMode == PassengerScreenMode.P_INITIAL) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            GCMIntentService.RIDE_REJECTED = false;
                            if (mCountDown != null) {
                                mCountDown.cancel();
                            }
                            Log.i("onSplitFareRequestRecieved", "=" + jObj);
                            splitFarePopup(HomeActivity.this, jObj);
                            Log.i("time", "=" + jObj.getInt("time"));

                            mCountDown = new CountDownTimer(jObj.getInt("time") * 1000, 1000) {// CountDownTimer(edittext1.getText()+edittext2.getText())
                                // also parse it to long

                                public void onTick(long millisUntilFinished) {
                                }

                                public void onFinish() {

                                    if (splitFareDialog != null && splitFareDialog.isShowing()) {
                                        splitFareDialog.dismiss();
                                        Data.isSplitPopupShow = 0;

                                    }

                                }
                            }.start();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchAcceptedDriverInfoAndChangeState(JSONObject jObj) {
        try {
            cancelTimerRequestRide();
            passengerScreenMode = PassengerScreenMode.P_REQUEST_FINAL;
//


            Data.cSessionId = jObj.getString("session_id");
            Data.cEngagementId = jObj.getString("engagement_id");

            Data.cDriverId = jObj.getString("driver_id");
            Data.rideAcceptedTime = jObj.getString("accept_time").replace("T", " ");
            String userName = jObj.getString("user_name");
            String driverPhone = jObj.getString("phone");
            String driverImage = jObj.getString("user_pic");
            String driverCarImage = jObj.getString("car_pic");
            double latitude = jObj.getDouble("current_lat");
            double longitude = jObj.getDouble("current_long");
            double pickupLatitude, pickupLongitude;
            if (jObj.has("pickup_lat")) {
                pickupLatitude = jObj.getDouble("pickup_lat");
                pickupLongitude = jObj.getDouble("pickup_long");
            } else {
                if (myLocation != null) {
                    pickupLatitude = myLocation.getLatitude();
                    pickupLongitude = myLocation.getLongitude();
                } else {
                    pickupLatitude = map.getCameraPosition().target.latitude;
                    pickupLongitude = map.getCameraPosition().target.longitude;
                }
            }
            String carNumber = "";
            if (jObj.has("car_no")) {
                carNumber = jObj.getString("car_no");
            }

            int freeRide = 0;
            if (jObj.has("free_ride")) {
                freeRide = jObj.getInt("free_ride");
            }

            int driverRating = 4;

            Data.pickupLatLng = new LatLng(pickupLatitude, pickupLongitude);
            Double driverBearing = 0.0;
            if (jObj.has("driver_car_direction_angle")) {
                driverBearing = jObj.getDouble("driver_car_direction_angle");
            }

            Data.assignedDriverInfo = new DriverInfo(Data.cDriverId, latitude, longitude, userName,
                    driverImage, driverCarImage, driverPhone, driverRating, carNumber, freeRide, driverBearing);

            Log.i("hiiii", "Every thing is working fine");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("assignedDriverInfo", "=" + Data.assignedDriverInfo);
                    Log.e("myLocation", "=" + myLocation);
                    switchPassengerScreen(passengerScreenMode);
//                    if (getDistanceTimeAddress != null) {
//                        getDistanceTimeAddress.cancel(true);
//                    }
                    if (myLocation != null) {
                        findNearbyDrivers(Data.pickupLatLng, true);
//                        getDistanceTimeAddress = new GetDistanceTimeAddress(Data.pickupLatLng, true);
//                        getDistanceTimeAddress.execute();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchAcceptedDriverStartRideInfoAndChangeState(JSONObject jObj) {
        try {
            Log.i("in ride screen data", "===" + jObj.toString());
            cancelTimerRequestRide();
            passengerScreenMode = PassengerScreenMode.P_IN_RIDE;


            Data.cSessionId = jObj.getString("session_id");
            Data.cEngagementId = jObj.getString("engagement_id");

            Data.cDriverId = jObj.getString("driver_id");

            String userName = jObj.getString("user_name");
            String driverPhone = jObj.getString("phone");
            String driverImage = jObj.getString("user_pic");
            String driverCarImage = jObj.getString("car_pic");
            double latitude = jObj.getDouble("current_lat");
            double longitude = jObj.getDouble("current_long");
            int driverRating = jObj.getInt("driver_rating");


            String carNumber = "";
            if (jObj.has("car_no")) {
                carNumber = jObj.getString("car_no");
            }

            int freeRide = 0;
            if (jObj.has("free_ride")) {
                freeRide = jObj.getInt("free_ride");
            }

            Double driverBearing = 0.0;
            if (jObj.has("driver_car_direction_angle")) {
                driverBearing = jObj.getDouble("driver_car_direction_angle");
            }


            Data.assignedDriverInfo = new DriverInfo(Data.cDriverId, latitude, longitude, userName,
                    driverImage, driverCarImage, driverPhone, driverRating, carNumber, freeRide, driverBearing);


            Data.startRidePreviousLatLng = Data.pickupLatLng;
            initializeStartRideVariables();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switchPassengerScreen(passengerScreenMode);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void clearRideSPData() {


        customerWaitTime = 0;
        waitCustomerChronometer.eclipsedTime = 0;
        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        Editor editor = pref.edit();

        editor.putString(Data.SP_TOTAL_DISTANCE, "-1");
        editor.putString(Data.SP_WAIT_TIME, "0");
        editor.putString(Data.SP_RIDE_TIME, "0");
        editor.putString(Data.SP_RIDE_START_TIME, "" + System.currentTimeMillis());
        editor.putString(Data.SP_LAST_LATITUDE, "0");
        editor.putString(Data.SP_LAST_LONGITUDE, "0");

        editor.commit();

        Database.getInstance(this).deleteSavedPath();
        Database.getInstance(this).close();

    }

    public void clearSPData() {

        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        Editor editor = pref.edit();

        editor.putString(Data.SP_TOTAL_DISTANCE, "-1");
        editor.putString(Data.SP_WAIT_TIME, "0");
        editor.putString(Data.SP_RIDE_TIME, "0");
        editor.putString(Data.SP_RIDE_START_TIME, "" + System.currentTimeMillis());
        editor.putString(Data.SP_LAST_LATITUDE, "0");
        editor.putString(Data.SP_LAST_LONGITUDE, "0");

        editor.putString(Data.SP_CUSTOMER_SCREEN_MODE, "");

        editor.putString(Data.SP_C_SESSION_ID, "");
        editor.putString(Data.SP_C_ENGAGEMENT_ID, "");
        editor.putString(Data.SP_C_DRIVER_ID, "");
        editor.putString(Data.SP_C_LATITUDE, "0");
        editor.putString(Data.SP_C_LONGITUDE, "0");
        editor.putString(Data.SP_C_DRIVER_NAME, "");
        editor.putString(Data.SP_C_DRIVER_IMAGE, "");
        editor.putString(Data.SP_C_DRIVER_CAR_IMAGE, "");
        editor.putString(Data.SP_C_DRIVER_PHONE, "");
        editor.putString(Data.SP_C_DRIVER_RATING, "");
        editor.putString(Data.SP_C_DRIVER_DISTANCE, "0");
        editor.putString(Data.SP_C_DRIVER_DURATION, "");

        editor.putString(Data.SP_C_TOTAL_DISTANCE, "0");
        editor.putString(Data.SP_C_TOTAL_FARE, "0");
        editor.putString(Data.SP_C_WAIT_TIME, "0");
        editor.putString(Data.SP_C_RIDE_TIME, "0");

        editor.commit();

        Database.getInstance(this).deleteSavedPath();
        Database.getInstance(this).close();

    }

    @Override
    public void customerEndRideInterrupt(final JSONObject jObj) {
        try {
            if (passengerScreenMode == PassengerScreenMode.P_IN_RIDE) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        displayCouponApplied(jObj);
                        lastLocation = null;
                        clearSPData();
                        map.clear();
                        Data.defaulterFlag = "0";
                        try {

                            Data.assignedDriverInfo.rating = jObj.getInt("driver_rating");
                            Data.endRideData = new EndRideData(jObj.getString("promo_code"));
                            Log.i("Driver rating in end ride push", "===" + jObj.getInt("driver_rating"));
                            Log.i("Promo Code in end ride push", "===" + jObj.getString("promo_code"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                        passengerScreenMode = PassengerScreenMode.P_RIDE_END;
                        switchPassengerScreen(passengerScreenMode);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChangeStatePushReceived() {
        try {
            callAndHandleStateRestoreAPI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
        FlurryAgent.onEvent("HomeActivity started");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    public void initializeFusedLocationFetchers() {
        destroyFusedLocationFetchers();
        if (passengerScreenMode != PassengerScreenMode.P_IN_RIDE) {
            if (myLocation == null) {
                if (lowPowerLF == null) {
                    lowPowerLF = new LocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD, 0);
                }
                if (highAccuracyLF == null) {
                    highAccuracyLF = new LocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD, 2);
                }
            } else {
                if (highAccuracyLF == null) {
                    highAccuracyLF = new LocationFetcher(HomeActivity.this, LOCATION_UPDATE_TIME_PERIOD, 2);
                }
            }
        }
    }

    public void destroyFusedLocationFetchers() {
        destroyLowPowerFusedLocationFetcher();
        destroyHighAccuracyFusedLocationFetcher();
    }

    public void destroyLowPowerFusedLocationFetcher() {
        try {
            if (lowPowerLF != null) {
                lowPowerLF.destroy();
                lowPowerLF = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void destroyHighAccuracyFusedLocationFetcher() {
        try {
            if (highAccuracyLF != null) {
                highAccuracyLF.destroy();
                highAccuracyLF = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void onLocationChanged(Location location, int priority) {
        if (passengerScreenMode != PassengerScreenMode.P_IN_RIDE) {
            if (priority == 0) {
                if (location.getAccuracy() <= LOW_POWER_ACCURACY_CHECK) {
                    HomeActivity.myLocation = location;
                    zoomToCurrentLocationAtFirstLocationFix(location);
                    updatePickupLocation(location);
                }
            } else if (priority == 2) {
                destroyLowPowerFusedLocationFetcher();
                if (location.getAccuracy() <= HIGH_ACCURACY_ACCURACY_CHECK) {
                    HomeActivity.myLocation = location;
                    zoomToCurrentLocationAtFirstLocationFix(location);
                    updatePickupLocation(location);
                }
            }
        } else {
            destroyFusedLocationFetchers();
        }
    }

    public void zoomToCurrentLocationAtFirstLocationFix(Location location) {
        try {
            if (map != null) {
                if (!zoomedToMyLocation) {
                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                    zoomedToMyLocation = true;
                    callMapTouchedRefreshDrivers();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates PickupLocation on first location fix, when passenger state is P_ASSIGNING_DRIVERS
     * and saving it in shared preferences.
     *
     * @param location location changed in listeners
     */
    public void updatePickupLocation(Location location) {

        if (passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
            if (Data.pickupLatLng.latitude == 0 && Data.pickupLatLng.longitude == 0) {
                Data.pickupLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                Editor editor = pref.edit();
                editor.putString(Data.SP_C_SESSION_ID, Data.cSessionId);
                editor.putString(Data.SP_TOTAL_DISTANCE, "0");
                editor.putString(Data.SP_LAST_LATITUDE, "" + Data.pickupLatLng.latitude);
                editor.putString(Data.SP_LAST_LONGITUDE, "" + Data.pickupLatLng.longitude);
                editor.commit();

                if (myLocation != null) {
                    findNearbyDrivers(Data.pickupLatLng, false);
//
                }
            }
        } else if (passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {
            if ("".equalsIgnoreCase(Data.assignedDriverInfo.durationToReach)) {

                if (myLocation != null) {
                    Data.assignedDriverInfo.durationToReach = "no";
                    findNearbyDrivers(Data.pickupLatLng, true);

                }
            }
        }

    }

    public boolean checkWorkingTime() {
        return true;
    }

    public void startTimerRequestRide() {
        cancelTimerRequestRide();
        try {

            requestRideLifeTime = ((2 * 60 * 1000) + (1 * 60 * 1000));
            serverRequestStartTime = 0;
            serverRequestEndTime = 0;
            executionTime = -1;
            requestPeriod = 60000;

            timerRequestRide = new Timer();
            timerTaskRequestRide = new TimerTask() {
                @Override
                public void run() {

                    final long startTime = System.currentTimeMillis();

                    try {
                        Log.e("request_ride execution", "=" + (serverRequestEndTime - executionTime));
                        if (executionTime >= serverRequestEndTime) {
                            cancelTimerRequestRide();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
                                        noDriverAvailablePopup(HomeActivity.this, "");
                                        HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
                                        switchPassengerScreen(passengerScreenMode);
                                    }
                                }
                            });
                        } else {
                            if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if ("".equalsIgnoreCase(Data.cSessionId)) {
                                            initialCancelRideBtn.setBackgroundResource(R.drawable.disable_grey_btn);
                                            initialCancelRideBtn.setTextColor(getResources().getColorStateList(R.color.white));
                                        } else {
                                            initialCancelRideBtn.setBackgroundResource(R.drawable.light_grey_btn_selector);
                                            initialCancelRideBtn.setTextColor(getResources().getColorStateList(R.drawable.white_color_selector));
                                        }
                                    }
                                });

                                //need to change

                                String manualDestinationLatitude = "", manualDestinationLongitude = "", manualDestinationAddress = "";
                                String currentLatitude = "" + Data.pickupLatLng.latitude, currentLongitude = "" + Data.pickupLatLng.longitude;
                                int duplicateFlag = 1;
                                String locationAccuracy = "";
                                if (destinationLocationtext.getText().toString().length() > 2) {
                                    try {

                                        if (Data.destinationLatLng != null) {
                                            manualDestinationLatitude = "" + Data.destinationLatLng.latitude;
                                            manualDestinationLongitude = "" + Data.destinationLatLng.longitude;
                                            manualDestinationAddress = destinationLocationtext.getText().toString();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } // end catch
                                }
                                if (myLocation != null) {
                                    currentLatitude = "" + myLocation.getLatitude();
                                    currentLongitude = "" + myLocation.getLongitude();
                                }
                                if ("".equalsIgnoreCase(Data.cSessionId)) {
                                    duplicateFlag = 0;
                                    if (myLocation != null && myLocation.hasAccuracy()) {
                                        locationAccuracy = "" + myLocation.getAccuracy();
                                    }
                                }

                                RestClient.getApiService().requestRide(Data.userData.accessToken, "" + Data.pickupLatLng.latitude, "" + Data.pickupLatLng.longitude,
                                        "" + Data.defaultCarType, "" + Data.getCurrentOffset(), manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress,
                                        currentLatitude, currentLongitude, "" + duplicateFlag, locationAccuracy, new Callback<String>() {
                                            @Override
                                            public void success(String s, Response response) {

                                                Log.e("response of request_ride", "=" + response);

                                                if (s.contains("SERVER_TIMEOUT")) {
                                                    Log.e("timeout", "=");
                                                } else {
                                                    try {
                                                        JSONObject jObj = new JSONObject(s);
                                                        if (!jObj.isNull("error")) {
                                                            final String errorMessage = jObj.getString("error");
                                                            final int flag = jObj.getInt("flag");
                                                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                                                cancelTimerRequestRide();
                                                                HomeActivity.logoutUser(activity);
                                                            } else {
                                                                if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                                                    cancelTimerRequestRide();
                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
                                                                                new DialogPopup().alertPopup(HomeActivity.this, "", errorMessage);
                                                                                HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
                                                                                switchPassengerScreen(passengerScreenMode);
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } else {
                                                            int flag = jObj.getInt("flag");

                                                            if (ApiResponseFlags.ASSIGNING_DRIVERS.getOrdinal() == flag) {
                                                                final String log = jObj.getString("log");
                                                                Log.e("ASSIGNING_DRIVERS log", "=" + log);
                                                                final String start_time = jObj.getString("start_time");
                                                                if (executionTime == -1) {
                                                                    serverRequestStartTime = new DateOperations().getMilliseconds(start_time);
                                                                    serverRequestEndTime = serverRequestStartTime + requestRideLifeTime;
                                                                    long stopTime = System.currentTimeMillis();
                                                                    long elapsedTime = stopTime - startTime;
                                                                    executionTime = serverRequestStartTime + elapsedTime;
                                                                }

                                                                Data.cSessionId = jObj.getString("session_id");
                                                            } else if (ApiResponseFlags.RIDE_ACCEPTED.getOrdinal() == flag) {
                                                                if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
                                                                    cancelTimerRequestRide();
                                                                    fetchAcceptedDriverInfoAndChangeState(jObj);
                                                                }
                                                            } else if (ApiResponseFlags.RIDE_STARTED.getOrdinal() == flag) {
                                                                if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
                                                                    cancelTimerRequestRide();
                                                                    fetchAcceptedDriverStartRideInfoAndChangeState(jObj);
                                                                }
                                                            } else if (ApiResponseFlags.NO_DRIVERS_AVAILABLE.getOrdinal() == flag) {
                                                                final String log = jObj.getString("log");
                                                                Log.e("NO_DRIVERS_AVAILABLE log", "=" + log);
                                                                cancelTimerRequestRide();
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
                                                                            noDriverAvailablePopup(HomeActivity.this, log);
                                                                            HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
                                                                            switchPassengerScreen(passengerScreenMode);
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                            }
                                        });

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if ("".equalsIgnoreCase(Data.cSessionId)) {
                                            initialCancelRideBtn.setBackgroundResource(R.drawable.disable_grey_btn);
                                            initialCancelRideBtn.setTextColor(getResources().getColorStateList(R.color.white));
                                        } else {
                                            initialCancelRideBtn.setBackgroundResource(R.drawable.light_grey_btn_selector);
                                            initialCancelRideBtn.setTextColor(getResources().getColorStateList(R.drawable.white_color_selector));
                                        }
                                    }
                                });

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    long stopTime = System.currentTimeMillis();
                    long elapsedTime = stopTime - startTime;

                    if (executionTime != -1) {
                        if (elapsedTime >= requestPeriod) {
                            executionTime = executionTime + elapsedTime;
                        } else {
                            executionTime = executionTime + requestPeriod;
                        }
                    }
                    Log.i("request_ride execution", "=" + (serverRequestEndTime - executionTime));

                }
            };

            timerRequestRide.scheduleAtFixedRate(timerTaskRequestRide, 0, requestPeriod);
            textViewAssigningInProgress.setText(getString(R.string.assigning_driver));
            FlurryEventLogger.requestRidePressed(Data.userData.accessToken, Data.pickupLatLng);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelTimerRequestRide() {

        try {
            if (timerTaskRequestRide != null) {
                timerTaskRequestRide.cancel();
                timerTaskRequestRide = null;
            }
            if (timerRequestRide != null) {
                timerRequestRide.cancel();
                timerRequestRide.purge();
                timerRequestRide = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onNoDriversAvailablePushRecieved(final String logMessage) {
        cancelTimerRequestRide();
        if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_ASSIGNING) {
            HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    noDriverAvailablePopup(HomeActivity.this, logMessage);
                    HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
                    switchPassengerScreen(passengerScreenMode);
                }
            });
        }
    }

    public void callAndHandleStateRestoreAPI() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Data.userData != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogPopup.showLoadingDialog(HomeActivity.this, getString(R.string.loading));
                        }
                    });

                    String resp = new JSONParser().getUserStatus(HomeActivity.this, Data.userData.accessToken);
                    if (resp.contains("SERVER_TIMEOUT")) {
                        String resp1 = new JSONParser().getUserStatus(HomeActivity.this, Data.userData.accessToken);
                        if (resp1.contains("SERVER_TIMEOUT")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new DialogPopup().alertPopup(HomeActivity.this, "", Data.SERVER_NOT_RESOPNDING_MSG);
                                }
                            });
                        }
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogPopup.dismissLoadingDialog();
                            callMapTouchedRefreshDrivers();
                            startUIAfterGettingUserStatus();
                        }
                    });
                }
            }
        }).start();
    }


    public void changeCarType() {

        if (map != null) {
            map.clear();
        }
        Data.driverInfos.clear();
        tvMaxSizeValue.setText("" + Data.fareStructureArrayList.get(Data.defaultCarType).maxSize + " " + getString(R.string.people));
        findNearbyDrivers(map.getCameraPosition().target, false);

    }

    @Override
    public void setFareWithTip(String tip, String totalFare) {

        etDriverFeedback.setText("");
        ratingBar.setRating(5);

        Data.pickupLatLng = new LatLng(0, 0);
        Data.cSessionId = "";
        Data.isPaymentSuccessfull = "1";
        switchPassengerScreen(passengerScreenMode);
//        paymentUnsuccessfulLayout.setVisibility(View.GONE);
//        endRideBelowratingRl.setVisibility(View.VISIBLE);

    }

    @Override
    public void setPaymentButtonLayout() {
        if (Data.defaulterFlag.equalsIgnoreCase("1")) {
            payByCashBtn.setBackgroundResource(R.drawable.disable_grey_btn);
            payByCashBtn.setTextColor(getResources().getColorStateList(R.color.white));
        } else {
            payByCashBtn.setBackgroundResource(R.drawable.main_interaction_btn_selector);
            payByCashBtn.setTextColor(getResources().getColorStateList(R.color.white));
        }
//        payByCardBtn.setBackgroundResource(R.drawable.white_btn_with_stroke);
//        payByCardBtn.setTextColor(getResources().getColor(R.color.main_interaction_color));

    }

    /**
     * ASync for acceptDeclineSplitFareRequest
     */
    public void acceptDeclineSplitFareRequest(final Activity activity, String splitId, final int carType, final int respnseFlag) {
        try {
            if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

                DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
                Log.i("access_token", "=" + Data.userData.accessToken);
                Log.i("split_id", "=" + splitId);
                Log.i("user_response_flag", "=" + respnseFlag);

                RestClient.getApiService().acceptDeclineSplitFareRequest(Data.userData.accessToken, splitId, respnseFlag, new Callback<String>() {
                    private JSONObject jObj;

                    @Override
                    public void success(String response, Response r) {
                        Log.v("Server response", "response = " + response);

                        try {
                            jObj = new JSONObject(response);

                            if (!jObj.isNull("error")) {

                                String errorMessage = jObj.getString("error");
                                if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                    HomeActivity.logoutUser(activity);
                                } else {
                                    new DialogPopup().alertPopup(activity, "", errorMessage);
                                }
                                if (jObj.getString("flag").equalsIgnoreCase("103")) {
                                    Data.isSplitPopupShow = 0;
                                    splitFareDialog.dismiss();
                                }
                            } else {
                                if (respnseFlag == 0) {
//
                                    try {
                                        callAndHandleStateRestoreAPI();
                                        Data.defaultCarType = carType;
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }
                                }

                            }
                            Data.isSplitPopupShow = 0;
                            splitFareDialog.dismiss();

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }

                        DialogPopup.dismissLoadingDialog();

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("request fail", error.toString());
                        DialogPopup.dismissLoadingDialog();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                    }
                });
            } else {
                new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * ASync for start ride in  driver mode from server
     */
    public void payFare(final Activity activity, String paymentType) {
        if (AppStatus.getInstance(activity.getApplicationContext()).isOnline(activity.getApplicationContext())) {


            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            android.util.Log.i("access_token", "=" + Data.userData.accessToken);
            android.util.Log.i("engagement_id", "=" + Data.cEngagementId);
            Log.i("to_pay", "=" + Data.totalFare);


            RestClient.getApiService().payFare(Data.userData.accessToken, "0", "" + Data.cEngagementId, paymentType, "" + (Data.totalFare - Data.discount), new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    android.util.Log.e("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            String errorMessage = jObj.getString("error");

                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        } else {


                            Data.defaulterFlag = "0";
                            etDriverFeedback.setText("");
                            ratingBar.setRating(5);

                            Data.pickupLatLng = new LatLng(0, 0);
                            Data.cSessionId = "";
                            Data.isPaymentSuccessfull = "1";
//                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            switchPassengerScreen(passengerScreenMode);
                            new DialogPopup().alertPopup(activity, "",
                                    jObj.getString("log"));
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    android.util.Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {

            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));

        }

    }

    @Override
    public void setDefaulterPushReceived() {
        Log.i("setDefaulterPushReceived", "=");
        HomeActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Data.defaulterFlag = "1";
                if (FeaturesConfig.cashCardBoth) {
                    new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.cash_payment_disable_message) + " " + getString(R.string.customer_care_no));
                }
                payByCashBtn.setEnabled(false);
                payByCashBtn.setBackgroundResource(R.drawable.disable_grey_btn);
                payByCashBtn.setTextColor(getResources().getColorStateList(R.color.white));
            }
        });

    }


    @Override
    public void collectCashPushRecieve() {
        Log.i("collectCashPushRecieve", "=");
        HomeActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (passengerScreenMode == PassengerScreenMode.P_RIDE_END) {
                    Data.defaulterFlag = "0";
                    etDriverFeedback.setText("");
                    ratingBar.setRating(5);
                    Data.pickupLatLng = new LatLng(0, 0);
                    Data.cSessionId = "";
                    Data.isPaymentSuccessfull = "1";
                    switchPassengerScreen(passengerScreenMode);
                    new DialogPopup().alertPopup(activity, "",
                            getString(R.string.please_pay_cash));
                }
            }
        });

    }

    public synchronized void getSearchResults(final String searchText, final LatLng latLng) {
        try {
            if (!refreshingAutoComplete) {

                searchLocationProgress.setVisibility(View.VISIBLE);
                imgLocationIcon.setVisibility(View.GONE);
                if (autoCompleteThread != null) {
                    autoCompleteThread.interrupt();
                }

                autoCompleteThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        refreshingAutoComplete = true;
//                        autoCompleteSearchResults.clear();
                        autoCompleteSearchResults.addAll(MapUtils.getAutoCompleteSearchResultsFromGooglePlaces(searchText, latLng, HomeActivity.this));
                        Log.i("setSearchResultsToList", "==");
                        refreshingAutoComplete = false;
                        recallLatestTextSearch(searchText, latLng);
                        setSearchResultsToList();
                        autoCompleteThread = null;


                    }
                });
                autoCompleteThread.start();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized void recallLatestTextSearch(final String searchText, final LatLng latLng) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String currentText = etLocation.getText().toString().trim();
                    if (searchText.equalsIgnoreCase(currentText)) {

                    } else {

                        if (currentText.length() > 0) {
                            if (map != null) {
                                autoCompleteSearchResults.clear();
                                searchListAdapter.notifyDataSetChanged();
                                Log.i("currentText", "==" + currentText);
                                getSearchResults(currentText, latLng);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public synchronized void setSearchResultsToList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try {
                    if (etLocation.getText().toString().equalsIgnoreCase("")) {
                        autoCompleteSearchResults.clear();
                    }

                    searchListAdapter.notifyDataSetChanged();
                    searchLocationProgress.setVisibility(View.GONE);
                    imgLocationIcon.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public synchronized void getSearchResultFromPlaceId(final String placeId) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                SearchResult searchResult = MapUtils.getSearchResultsFromPlaceIdGooglePlaces(placeId, HomeActivity.this);
                setSearchResultToMapAndText(searchResult);

            }
        }).start();
    }

    public synchronized void setSearchResultToMapAndText(final SearchResult searchResult) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                pickUpLocationProgress.setVisibility(View.GONE);
//                pickUpLocationText.setVisibility(View.VISIBLE);
                if (map != null && searchResult != null) {
                    if (isSearchStartLocation) {
                        canChangeLocationText = false;
                        pickUpLocationText.setText(searchResult.name);
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(searchResult.latLng, 14), 1000, null);

                    } else {
                        Data.destinationLatLng = searchResult.latLng;
                        destinationLocationtext.setText(searchResult.name);

                    }
                }
            }
        });
    }

    public void findNearbyDrivers(final LatLng destination, final boolean driverAcceptPushRecieved) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            try {
                if (driverAcceptPushRecieved) {
                    passengerScreenMode = PassengerScreenMode.P_REQUEST_FINAL;
                    switchPassengerScreen(passengerScreenMode);
                } else {
                    addCurrentLocationAddressMarker(destination);
                }
//            nearestDriverProgress.setVisibility(View.VISIBLE);
//            nearestDriverText.setVisibility(View.GONE);

                dontCallRefreshDriver = false;
                if (!driverAcceptPushRecieved) {
                    Log.i("Hit===find a driver", "====");
                    Log.i("accessToken", "=" + Data.userData.accessToken);
                    Log.i("destination.latitude", "=" + destination.latitude);
                    Log.i("destination.longitude", "=" + destination.longitude);

                    RestClient.getApiService().findNearbyDrivers(Data.userData.accessToken, destination.latitude, destination.longitude, Data.defaultCarType, new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {
                            try {
                                Log.i("find_a_driver response", "===" + s);
                                JSONObject jObj = new JSONObject(s);
                                if (!jObj.isNull("error")) {

                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    }
                                    tvETAValue.setText(getResources().getString(R.string.no_drivers_nearby));
                                } else {
                                    new JSONParser().parseDriversToShow(jObj, "data");
                                    Log.i("AFter Hit===", "After Hittttt response");
                                    if (Data.driverInfos.size() > 0) {

                                        updateApproximatePickupTime(destination, driverAcceptPushRecieved);
                                        if (Data.latitude != 0 && Data.longitude != 0) {
                                            showDriverMarkersAndPanMap(new LatLng(Data.latitude, Data.longitude));
                                        } else if (myLocation != null) {
                                            showDriverMarkersAndPanMap(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                                        }

                                    } else {
                                        tvETAValue.setText(getResources().getString(R.string.no_drivers_nearby));
                                    }
                                    Data.surgeFactor = jObj.getDouble("fare_factor");
                                    Data.expiryMinutes = jObj.getInt("expiry_min");

                                    dontCallRefreshDriver = true;
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            dontCallRefreshDriver = false;
                                        }
                                    }, 1000);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {

                            Log.i("failure response", "===" + error.toString());
                        }
                    });

                } else {
                    updateApproximatePickupTime(destination, driverAcceptPushRecieved);
                    SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
                    Editor editor = pref.edit();
                    editor.putString(Data.SP_CUSTOMER_SCREEN_MODE, Data.P_REQUEST_FINAL);

                    editor.putString(Data.SP_C_ENGAGEMENT_ID, Data.cEngagementId);
                    editor.putString(Data.SP_C_DRIVER_ID, Data.cDriverId);
                    editor.putString(Data.SP_C_LATITUDE, "" + Data.assignedDriverInfo.latLng.latitude);
                    editor.putString(Data.SP_C_LONGITUDE, "" + Data.assignedDriverInfo.latLng.longitude);
                    editor.putString(Data.SP_C_DRIVER_NAME, Data.assignedDriverInfo.name);
                    editor.putString(Data.SP_C_DRIVER_IMAGE, Data.assignedDriverInfo.image);
                    editor.putString(Data.SP_C_DRIVER_CAR_IMAGE, Data.assignedDriverInfo.carImage);
                    editor.putString(Data.SP_C_DRIVER_PHONE, Data.assignedDriverInfo.phoneNumber);
                    editor.putString(Data.SP_C_DRIVER_RATING, "" + Data.assignedDriverInfo.rating);
                    editor.putString(Data.SP_C_DRIVER_DISTANCE, Data.assignedDriverInfo.distanceToReach);
                    editor.putString(Data.SP_C_DRIVER_DURATION, Data.assignedDriverInfo.durationToReach);

                    editor.commit();
                    if (HomeActivity.passengerScreenMode == PassengerScreenMode.P_REQUEST_FINAL) {
                        updateAssignedDriverETA();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//            new DialogPopup().alertPopup(HomeActivity.this, "", getString(R.string.check_internet_message));
        }
    }

    public void updateApproximatePickupTime(LatLng destination, final boolean driverAcceptPushRecieved) {
        String origins = "";
        if (!driverAcceptPushRecieved) {
            for (int i = 0; i < Data.driverInfos.size(); i++) {
                if (Data.driverInfos.size() == i) {
                    origins = origins + Data.driverInfos.get(i).latLng.latitude + "," + Data.driverInfos.get(i).latLng.longitude;
                } else {
                    origins = origins + Data.driverInfos.get(i).latLng.latitude + "," + Data.driverInfos.get(i).latLng.longitude + "|";
                }

            }

        } else {
            origins = Data.assignedDriverInfo.latLng.latitude + "," + Data.assignedDriverInfo.latLng.longitude;
        }

        RestClient.getDistanceMatrixApiService().getDistanceMatrixData(origins, destination.latitude + "," + destination.longitude, "EN", new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                try {
                    Log.i("Response of Matrix API ", "==" + s.toString());
                    String approximatePickupTimeString = "";
                    JSONObject jsonObject = new JSONObject(s);
                    String status = jsonObject.getString("status");
                    if ("OK".equalsIgnoreCase(status)) {
                        int smallest = Integer.MAX_VALUE;
                        String distance = "", duration = "";
                        Log.i("length", "==" + jsonObject.getJSONArray("rows").length());
                        for (int i = 0; i < jsonObject.getJSONArray("rows").length(); i++) {
                            JSONObject element0 = jsonObject.getJSONArray("rows").getJSONObject(i).getJSONArray("elements").getJSONObject(0);
                            if (element0.has("duration")) {
                                if (smallest > element0.getJSONObject("duration").getDouble("value")) {
                                    smallest = element0.getJSONObject("duration").getInt("value");
                                    duration = element0.getJSONObject("duration").getString("text");
                                    distance = element0.getJSONObject("distance").getString("text");

                                }
                            }
                        }
                        Log.i("approximatePickupTimeString", "==" + approximatePickupTimeString);
                        Log.i("smallest", "==" + smallest);
                        if (duration.equalsIgnoreCase("")) {
                            approximatePickupTimeString = getResources().getString(R.string.could_not_find_approximate_pickup_time);
                        } else {
                            approximatePickupTimeString = duration.toUpperCase();
                        }

                        Log.i("Data.driverInfos.size()", "==" + Data.driverInfos.size());
                        if (Data.driverInfos.size() > 0) {
                            tvETAValue.setText(approximatePickupTimeString);
                        } else {
                            tvETAValue.setText(getResources().getString(R.string.no_drivers_nearby));
                        }

                        if (driverAcceptPushRecieved) {
                            Data.assignedDriverInfo.distanceToReach = distance;
                            Data.assignedDriverInfo.durationToReach = duration;
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                Log.i("Error in Matrix API ", "==" + error.toString());
            }
        });
    }

    @Override
    public void onWaitStartedPushRecieved(final long waitTime) {

        HomeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("onWaitStartedPushRecieved", "==" + waitTime);
                long time = (long) waitTime / 1000;
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String hh = h < 10 ? "0" + h : h + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                waitCustomerChronometer.setText(hh + ":" + mm + ":" + ss);
                waitCustomerChronometer.start();
                waitCustomerChronometer.eclipsedTime = (long) waitTime / 1000;

            }
        });


    }

    @Override
    public void onWaitEndedPushRecieved(final long waitTime) {
        Log.i("onWaitEndedPushRecieved", "==" + waitTime);

        HomeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.e("In stoop ui thread", "==" + "fvscxbdghn");
                long time = waitTime * 1000;
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String hh = h < 10 ? "0" + h : h + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                waitCustomerChronometer.setText(hh + ":" + mm + ":" + ss);
                waitCustomerChronometer.eclipsedTime = HomeActivity.customerWaitTime;
                Log.e("In wait ui thread", "==" + waitCustomerChronometer.getText());
                waitCustomerChronometer.stop();

            }
        });
    }

    public void startTimerUpdateETA() {
        cancelTimerUpdateETA();
        try {
            timerUpdateETA = new Timer();
            timerTaskUpdateETA = new TimerTask() {


                @Override
                public void run() {
                    if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                        RestClient.getDistanceMatrixApiService().getDistanceMatrixData(Data.pickupLatLng.latitude + "," + Data.pickupLatLng.longitude, Data.assignedDriverInfo.latLng.latitude + "," + Data.assignedDriverInfo.latLng.longitude, "EN", new Callback<String>() {

                            @Override
                            public void success(String s, Response response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    String status = jsonObject.getString("status");
                                    if ("OK".equalsIgnoreCase(status)) {
                                        JSONObject element0 = jsonObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0);
                                        Data.assignedDriverInfo.durationToReach = element0.getJSONObject("duration").getString("text");
                                        Log.i("Duration in timer", "===" + Data.assignedDriverInfo.durationToReach);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                updateAssignedDriverETA();
                                            }
                                        });
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.e("Update Driver ETA hit failure", "" + error.toString());
                            }
                        });
                    }

                }
            };
            timerUpdateETA.scheduleAtFixedRate(timerTaskUpdateETA, 100, 60000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelTimerUpdateETA() {
        try {
            if (timerTaskUpdateETA != null) {
                timerTaskUpdateETA.cancel();
                timerTaskUpdateETA = null;
            }
            if (timerUpdateETA != null) {
                timerUpdateETA.cancel();
                timerUpdateETA.purge();
                timerUpdateETA = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class FBLogoutNoIntent extends AsyncTask<Void, Void, String> {

        Activity act;

        public FBLogoutNoIntent(Activity act) {
            this.act = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(Void... urls) {
            try {
                try {
                    Session.getActiveSession().closeAndClearTokenInformation();
                } catch (Exception e) {
                    Log.v("Logout", "Error" + e);
                }

                SharedPreferences pref = act.getSharedPreferences("myPref", 0);
                Editor editor = pref.edit();
                editor.clear();
                editor.commit();
            } catch (Exception e) {
                Log.v("e", e.toString());

            }

            return "";
        }

        @Override
        protected void onPostExecute(String text) {

        }
    }

    class CreatePathAsyncTask extends AsyncTask<Void, Void, String> {
        String url;
        double displacementToCompare;
        LatLng source, destination;

        CreatePathAsyncTask(LatLng source, LatLng destination, double displacementToCompare) {
            this.source = source;
            this.destination = destination;
            this.url = MapUtils.makeDirectionsURL(source, destination);
            this.displacementToCompare = displacementToCompare;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return RestClient.getDirectionsUrl().getDirectionUrl(source.latitude + "," + source.longitude, destination.latitude + "," + destination.longitude, "false");
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                drawPath(result, displacementToCompare, source, destination);

            }
            createPathAsyncTasks.remove(this);
        }


        @Override
        public boolean equals(Object o) {
            try {
                if ((((CreatePathAsyncTask) o).source == this.source) && (((CreatePathAsyncTask) o).destination == this.destination)) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    class CheckForGPSAccuracyTimer {

        public Timer timer;
        public TimerTask timerTask;

        public long startTime, lifeTime, endTime, period, executionTime;
        public boolean isRunning = false;

        public CheckForGPSAccuracyTimer(Context context, long delay, long period, long startTime, long lifeTime) {
            Log.i("CheckForGPSAccuracyTimer before start myLocation = ", "=" + myLocation);
            isRunning = false;
            if (myLocation != null) {
                if (myLocation.hasAccuracy()) {
                    float accuracy = myLocation.getAccuracy();
                    if (accuracy > HomeActivity.WAIT_FOR_ACCURACY_UPPER_BOUND) {
                        displayLessAccurateToast(context);
                    } else if (accuracy <= HomeActivity.WAIT_FOR_ACCURACY_UPPER_BOUND
                            && accuracy > HomeActivity.WAIT_FOR_ACCURACY_LOWER_BOUND) {
                        startTimer(context, delay, period, startTime, lifeTime);
                        HomeActivity.this.switchRequestRideUI();
                    } else if (accuracy <= HomeActivity.WAIT_FOR_ACCURACY_LOWER_BOUND) {
                        HomeActivity.this.switchRequestRideUI();
                        HomeActivity.this.startTimerRequestRide();
                    } else {
                        displayLessAccurateToast(context);
                    }
                } else {
                    displayLessAccurateToast(context);
                }
            } else {
                displayLessAccurateToast(context);
            }
        }


        public void displayLessAccurateToast(Context context) {
            Toast.makeText(context, "Please wait for sometime. We need to get your more accurate location.", Toast.LENGTH_LONG).show();
        }

        public void initRequestRideUi() {
            stopTimer();
            HomeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HomeActivity.this.startTimerRequestRide();
                }
            });
        }

        public void stopRequest(Context context) {
            displayLessAccurateToast(context);
            stopTimer();
            HomeActivity.this.customerUIBackToInitialAfterCancel();
        }

        public void startTimer(final Context context, long delay, long period, long startTime, long lifeTime) {
            stopTimer();
            isRunning = true;

            this.startTime = startTime;
            this.lifeTime = lifeTime;
            this.endTime = startTime + lifeTime;
            this.period = period;
            this.executionTime = -1;

            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    HomeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                long start = System.currentTimeMillis();
                                if (executionTime == -1) {
                                    executionTime = CheckForGPSAccuracyTimer.this.startTime;
                                }

                                if (executionTime >= CheckForGPSAccuracyTimer.this.endTime) {
                                    //Timer finished
                                    if (myLocation != null) {
                                        if (myLocation.hasAccuracy()) {
                                            float accuracy = myLocation.getAccuracy();
                                            if (accuracy > HomeActivity.WAIT_FOR_ACCURACY_UPPER_BOUND) {
                                                stopRequest(context);
                                            } else {
                                                initRequestRideUi();
                                            }
                                        } else {
                                            stopRequest(context);
                                        }
                                    } else {
                                        stopRequest(context);
                                    }
                                } else {
                                    //Check for location accuracy
                                    Log.i("CheckForGPSAccuracyTimer myLocation = ", "=" + myLocation);
                                    if (myLocation != null) {
                                        if (myLocation.hasAccuracy()) {
                                            float accuracy = myLocation.getAccuracy();
                                            if (accuracy <= HomeActivity.WAIT_FOR_ACCURACY_LOWER_BOUND) {
                                                initRequestRideUi();
                                            }
                                        }
                                    }
                                }
                                long stop = System.currentTimeMillis();
                                long elapsedTime = stop - start;
                                if (executionTime != -1) {
                                    if (elapsedTime >= CheckForGPSAccuracyTimer.this.period) {
                                        executionTime = executionTime + elapsedTime;
                                    } else {
                                        executionTime = executionTime + CheckForGPSAccuracyTimer.this.period;
                                    }
                                }
                                Log.i("WaitForGPSAccuracyTimerTask execution", "=" + (CheckForGPSAccuracyTimer.this.endTime - executionTime));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            };
            timer.scheduleAtFixedRate(timerTask, delay, period);
            isRunning = true;
            //textViewAssigningInProgress
            textViewAssigningInProgress.setText("Assigning driver could take up to 4 minutes...");
        }

        public void stopTimer() {
            try {
                isRunning = false;
                Log.e("WaitForGPSAccuracyTimerTask", "stopTimer");
                startTime = 0;
                lifeTime = 0;
                if (timerTask != null) {
                    timerTask.cancel();
                    timerTask = null;
                }
                if (timer != null) {
                    timer.cancel();
                    timer.purge();
                    timer = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    class ViewHolderSearchItem {
        TextView textViewSearchName, textViewSearchAddress;
        LinearLayout relative;
        int id;
    }

    class SearchListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderSearchItem holder;

        public SearchListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return autoCompleteSearchResults.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderSearchItem();
                convertView = mInflater.inflate(R.layout.list_item_search_item, null);

                holder.textViewSearchName = (TextView) convertView.findViewById(R.id.textViewSearchName);
                holder.textViewSearchName.setTypeface(Data.getFont(HomeActivity.this));
                holder.textViewSearchAddress = (TextView) convertView.findViewById(R.id.textViewSearchAddress);
                holder.textViewSearchAddress.setTypeface(Data.getFont(HomeActivity.this));
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);

                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderSearchItem) convertView.getTag();
            }


            holder.id = position;

            try {
                holder.textViewSearchName.setText(autoCompleteSearchResults.get(position).name);
                holder.textViewSearchAddress.setText(autoCompleteSearchResults.get(position).address);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.relative.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder = (ViewHolderSearchItem) v.getTag();
                    Utils.hideSoftKeyboard(HomeActivity.this, etLocation);
                    AutoCompleteSearchResult autoCompleteSearchResult = autoCompleteSearchResults.get(holder.id);
                    if (!"".equalsIgnoreCase(autoCompleteSearchResult.placeId)) {
                        if (isSearchStartLocation) {
                            canChangeLocationText = false;
                            pickUpLocationText.setText(autoCompleteSearchResult.name);
                        } else {
                            destinationLocationtext.setText(autoCompleteSearchResult.name);


                        }
                        getSearchResultFromPlaceId(autoCompleteSearchResult.placeId);
                        passengerScreenMode = PassengerScreenMode.P_INITIAL;
                        switchPassengerScreen(passengerScreenMode);

                    }
                }
            });
            return convertView;
        }

        @Override
        public void notifyDataSetChanged() {
            try {

                if (autoCompleteSearchResults.size() > 1) {
                    if (autoCompleteSearchResults.contains(new AutoCompleteSearchResult("No results found", "", ""))) {
                        autoCompleteSearchResults.remove(autoCompleteSearchResults.indexOf(new AutoCompleteSearchResult("No results found", "", "")));
                    }
                }
                super.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public class ImageTouchListenerFor2 implements View.OnTouchListener {

        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    mlastTouchX = event.getX();
                    carSlideEnabled = true;
                    movedPixel = 0;
                    Log.i("mlastTouchX", "==" + mlastTouchX);
                    break;

                case MotionEvent.ACTION_UP:
                    carSlideEnabled = false;
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                        Data.defaultCarType = 0;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 0;
                        }
                        mposX = 0;
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX >= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth()) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                        Data.defaultCarType = 1;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 1;
                        }
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    }
                    if (movedPixel < 5) {
                        imgSelectedCarType.performClick();
                        movedPixel = 0;
                    }

                    break;

                case MotionEvent.ACTION_MOVE:
                    carSlideEnabled = true;
                    mdx = event.getX() - mlastTouchX;
                    mposX = mprevX + mdx;
                    Log.i("mposX", "==" + mposX);
                    Log.i("mposX + view.getWidth()", "==" + mposX + view.getWidth());

                    movedPixel++;
                    Log.i("moved Pixel", movedPixel + " ");
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    } else if (mposX < 0) {
                        mposX = 0;
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                    } else if (mposX > linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth() / 2) {
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.setTranslationX(mposX);
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    }
                    Log.i("Event y", "" + event.getY());


                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1_on_slide);

                    } else if (mposX >= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2_on_slide);
                    }

                    break;

            }
            return false;
        }

    }

    class CarTouchListenerFor2 implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            android.util.Log.i("==", "event.getX()" + event.getX());
            if (event.getAction() == MotionEvent.ACTION_MOVE) {

                previousPanelState = slideLayout.getPanelState();

            }

            if ((event.getAction() == MotionEvent.ACTION_UP)) {

                if (event.getX() >= 0 && event.getX() <= (linearCarTypeParent.getWidth() / 2)) {
                    Data.defaultCarType = 0;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 0;
                    }
                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(0);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    mprevX = mposX;

                } else if (event.getX() > linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2 && event.getX() <= (linearCarTypeParent.getWidth())) {
                    Data.defaultCarType = 1;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 1;
                    }
                    mposX = (linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth());
                    imgSelectedCarType.animate().setDuration(250).translationX((linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()));
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    mprevX = mposX;

                }
            }
            movedPixel = 0;


            return false;
        }
    }

    public class ImageTouchListenerFor3 implements View.OnTouchListener {

        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    mlastTouchX = event.getX();
                    movedPixel = 0;
                    carSlideEnabled = true;

                    Log.i("mlastTouchX", "==" + mlastTouchX);
                    break;

                case MotionEvent.ACTION_UP:
                    carSlideEnabled = false;
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 4 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                        Data.defaultCarType = 0;

                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 0;
                        }
                        mposX = 0;
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > linearCarTypeParent.getWidth() / 4 - imgSelectedCarType.getWidth() / 2 && mposX < (((linearCarTypeParent.getWidth() / 4) * 3) - imgSelectedCarType.getWidth() / 2)) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                        Data.defaultCarType = 1;

                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 1;
                        }
                        mposX = linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2;
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX >= (((3 * linearCarTypeParent.getWidth()) / 4) - imgSelectedCarType.getWidth() / 2) && mposX <= linearCarTypeParent.getWidth()) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                        Data.defaultCarType = 2;

                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 2;
                        }
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    }
                    if (movedPixel < 5) {
                        imgSelectedCarType.performClick();
                        movedPixel = 0;
                    }

                    break;

                case MotionEvent.ACTION_MOVE:
                    carSlideEnabled = true;
                    mdx = event.getX() - mlastTouchX;
                    mposX = mprevX + mdx;
                    Log.i("mposX", "==" + mposX);
                    Log.i("mposX + view.getWidth()", "==" + mposX + view.getWidth());

                    movedPixel++;
                    Log.i("moved Pixel", movedPixel + " ");
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    } else if (mposX < 0) {
                        mposX = 0;
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                    } else if (mposX > linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth() / 2) {
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.setTranslationX(mposX);
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    }
                    Log.i("Event y", "" + event.getY());


                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 4 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1_on_slide);

                    } else if (mposX > (linearCarTypeParent.getWidth() / 4) - imgSelectedCarType.getWidth() / 2 && mposX < (((linearCarTypeParent.getWidth() / 4) * 3) - imgSelectedCarType.getWidth() / 2)) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2_on_slide);

                    } else if (mposX >= (((3 * linearCarTypeParent.getWidth()) / 4) - imgSelectedCarType.getWidth() / 2) && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3_on_slide);
                    }

                    break;

            }
            return false;
        }

    }

    class CarTouchListenerFor3 implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            android.util.Log.i("==", "event.getX()" + event.getX());
            if (event.getAction() == MotionEvent.ACTION_MOVE) {

                previousPanelState = slideLayout.getPanelState();

            }

            if ((event.getAction() == MotionEvent.ACTION_UP)) {
                movedPixel = 0;
                if (event.getX() >= 0 && event.getX() <= (linearCarTypeParent.getWidth() / 4)) {
                    Data.defaultCarType = 0;

                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 0;
                    }
                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(0);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 4) && event.getX() <= (3 * (linearCarTypeParent.getWidth() / 4))) {
                    Data.defaultCarType = 1;

                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 1;
                    }
                    mposX = (linearCarTypeParent.getWidth() / 2 - (imgSelectedCarType.getWidth() / 2));
                    imgSelectedCarType.animate().setDuration(250).translationX((linearCarTypeParent.getWidth() / 2 - (imgSelectedCarType.getWidth() / 2)));
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    mprevX = mposX;

                } else if (event.getX() > (3 * (linearCarTypeParent.getWidth() / 4)) && event.getX() <= (linearCarTypeParent.getWidth())) {
                    Data.defaultCarType = 2;

                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 2;
                    }
                    mposX = (linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth());
                    imgSelectedCarType.animate().setDuration(250).translationX((linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()));
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    mprevX = mposX;

                }
            }


            return false;
        }
    }

    public class ImageTouchListenerFor4 implements View.OnTouchListener {

        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    carSlideEnabled = true;
                    mlastTouchX = event.getX();
                    movedPixel = 0;
                    Log.i("mlastTouchX", "==" + mlastTouchX);
                    break;

                case MotionEvent.ACTION_UP:
                    carSlideEnabled = false;
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 4.87 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                        Data.defaultCarType = 0;
                        if (Data.defaultCarType != Data.previousCarType) {
                            Data.previousCarType = 0;
                            changeCarType();
                        }
                        mposX = 0;
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > linearCarTypeParent.getWidth() / 4.87 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                        Data.defaultCarType = 1;
                        if (Data.defaultCarType != Data.previousCarType) {
                            Data.previousCarType = 1;
                            changeCarType();
                        }
                        mposX = (float) (linearCarTypeParent.getWidth() / 2.82 - imgSelectedCarType.getWidth() / 2);
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() / 1.26 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                        Data.defaultCarType = 2;
                        if (Data.defaultCarType != Data.previousCarType) {
                            Data.previousCarType = 2;
                            changeCarType();
                        }
                        mposX = (float) (linearCarTypeParent.getWidth() / 1.55 - imgSelectedCarType.getWidth() / 2);
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > linearCarTypeParent.getWidth() / 1.26 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4);
                        Data.defaultCarType = 3;
                        if (Data.defaultCarType != Data.previousCarType) {
                            Data.previousCarType = 3;
                            changeCarType();
                        }
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    carSlideEnabled = true;
                    mdx = event.getX() - mlastTouchX;
                    mposX = mprevX + mdx;
                    Log.i("mposX", "==" + mposX);
                    Log.i("mposX + view.getWidth()", "==" + mposX + view.getWidth());

                    movedPixel++;
                    Log.i("moved Pixel", movedPixel + " ");
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    } else if (mposX < 0) {
                        mposX = 0;
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                    } else if (mposX > linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth() / 2) {
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.setTranslationX(mposX);
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    }
                    Log.i("Event y", "" + event.getY());


                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 4.87 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1_on_slide);

                    } else if (mposX > (linearCarTypeParent.getWidth() / 4.87) - imgSelectedCarType.getWidth() / 2 && mposX < linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2_on_slide);

                    } else if (mposX >= (linearCarTypeParent.getWidth() / 2) - imgSelectedCarType.getWidth() / 2 && mposX < linearCarTypeParent.getWidth() / 1.26 - imgSelectedCarType.getWidth() / 2) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3_on_slide);
                    } else if (mposX >= linearCarTypeParent.getWidth() / 1.26 - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4_on_slide);
                    }

                    break;

            }
            return false;
        }

    }

    class CarTouchListenerFor4 implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            android.util.Log.i("==", "event.getX()" + event.getX());
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                previousPanelState = slideLayout.getPanelState();
            }

            if ((event.getAction() == MotionEvent.ACTION_UP)) {
                if (event.getX() >= 0 && event.getX() <= (linearCarTypeParent.getWidth() / 4.87)) {
                    Data.defaultCarType = 0;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 0;
                        changeCarType();
                    }
                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(0);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 4.87) && event.getX() <= (linearCarTypeParent.getWidth() / 2)) {
                    Data.defaultCarType = 1;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 1;
                        changeCarType();
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 2.82 - (imgSelectedCarType.getWidth() / 2));
                    imgSelectedCarType.animate().setDuration(250).translationX((float) (linearCarTypeParent.getWidth() / 2.82 - (imgSelectedCarType.getWidth() / 2)));
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 2) && event.getX() <= (linearCarTypeParent.getWidth() / 1.26)) {
                    Data.defaultCarType = 2;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 2;
                        changeCarType();
                    }
                    mposX = (float) ((linearCarTypeParent.getWidth() / 1.55) - (imgSelectedCarType.getWidth() / 2));
                    imgSelectedCarType.animate().setDuration(250).translationX((float) (linearCarTypeParent.getWidth() / 1.55 - (imgSelectedCarType.getWidth() / 2)));
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 1.26) && event.getX() <= (linearCarTypeParent.getWidth())) {
                    Data.defaultCarType = 3;
                    if (Data.defaultCarType != Data.previousCarType) {
                        Data.previousCarType = 3;
                        changeCarType();
                    }
                    mposX = (linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth());
                    imgSelectedCarType.animate().setDuration(250).translationX((linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()));
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4);
                    mprevX = mposX;

                }
            }


            return false;
        }
    }

    public class ImageTouchListenerFor5 implements View.OnTouchListener {

        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    carSlideEnabled = true;
                    mlastTouchX = event.getX();
                    movedPixel = 0;
                    Log.i("mlastTouchX", "==" + mlastTouchX);
                    break;

                case MotionEvent.ACTION_UP:
                    carSlideEnabled = false;
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 6.1 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                        Data.defaultCarType = 0;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 0;
                        }
                        mposX = 0;
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > linearCarTypeParent.getWidth() / 6.1 - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 2.57) - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                        Data.defaultCarType = 1;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 1;
                        }
                        mposX = (float) (linearCarTypeParent.getWidth() / 3.625 - imgSelectedCarType.getWidth() / 2);
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > linearCarTypeParent.getWidth() / 2.57 - imgSelectedCarType.getWidth() / 2 && mposX < (linearCarTypeParent.getWidth() / 1.63) - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                        Data.defaultCarType = 2;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 2;
                        }
                        mposX = linearCarTypeParent.getWidth() / 2 - imgSelectedCarType.getWidth() / 2;
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX >= linearCarTypeParent.getWidth() / 1.63 - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 1.19) - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4);
                        Data.defaultCarType = 3;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 3;
                        }
                        mposX = (float) (linearCarTypeParent.getWidth() / 1.38 - imgSelectedCarType.getWidth() / 2);
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    } else if (mposX > (linearCarTypeParent.getWidth() / 1.19) - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth()) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_5);
                        Data.defaultCarType = 4;
                        if (Data.defaultCarType != Data.previousCarType) {
                            changeCarType();
                            Data.previousCarType = 4;
                        }
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                        mprevX = mposX;

                    }
                    if (movedPixel < 5) {
                        imgSelectedCarType.performClick();
                        movedPixel = 0;
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    carSlideEnabled = true;
                    mdx = event.getX() - mlastTouchX;
                    mposX = mprevX + mdx;
                    Log.i("mposX", "==" + mposX);
                    Log.i("mposX + view.getWidth()", "==" + mposX + view.getWidth());

                    movedPixel++;
                    Log.i("moved Pixel", movedPixel + " ");
                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    } else if (mposX < 0) {
                        mposX = 0;
                        imgSelectedCarType.setTranslationX(mposX);
                        mprevX = mposX;
                    } else if (mposX > linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth() / 2) {
                        mposX = linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth();
                        imgSelectedCarType.setTranslationX(mposX);
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_5);
                    }
                    Log.i("Event y", "" + event.getY());

                    if (mposX >= 0 && mposX <= linearCarTypeParent.getWidth() / 6.10 - imgSelectedCarType.getWidth() / 2) {
                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1_on_slide);

                    } else if (mposX > (linearCarTypeParent.getWidth() / 6.10) - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 2.57 - imgSelectedCarType.getWidth() / 2)) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2_on_slide);

                    } else if (mposX > (linearCarTypeParent.getWidth() / 2.57) - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 1.63)) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3_on_slide);

                    } else if (mposX > (linearCarTypeParent.getWidth() / 1.63) - imgSelectedCarType.getWidth() / 2 && mposX <= (linearCarTypeParent.getWidth() / 1.19) - imgSelectedCarType.getWidth() / 2) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4_on_slide);

                    } else if (mposX > (linearCarTypeParent.getWidth() / 1.19) - imgSelectedCarType.getWidth() / 2 && mposX <= linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth()) {

                        imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_5_on_slide);
                    }

                    break;

            }
            return false;
        }

    }

    class CarTouchListenerFor5 implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            android.util.Log.i("==", "event.getX()" + event.getX());
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                previousPanelState = slideLayout.getPanelState();
            }

            if ((event.getAction() == MotionEvent.ACTION_UP)) {
                if (event.getX() >= 0 && event.getX() <= (linearCarTypeParent.getWidth() / 6.10)) {
                    Data.defaultCarType = 0;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 0;
                    }
                    mposX = 0;
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_1);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 6.10) && event.getX() <= (linearCarTypeParent.getWidth() / 2.57)) {
                    Data.defaultCarType = 1;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 1;
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 3.625 - (imgSelectedCarType.getWidth() / 2));
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_2);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 2.57) && event.getX() <= (linearCarTypeParent.getWidth() / 1.63)) {
                    Data.defaultCarType = 2;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 2;
                    }
                    mposX = (linearCarTypeParent.getWidth() / 2 - (imgSelectedCarType.getWidth() / 2));
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_3);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 1.63) && event.getX() <= (linearCarTypeParent.getWidth() / 1.19)) {
                    Data.defaultCarType = 3;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 3;
                    }
                    mposX = (float) (linearCarTypeParent.getWidth() / 1.38 - (imgSelectedCarType.getWidth() / 2));
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_4);
                    mprevX = mposX;

                } else if (event.getX() > (linearCarTypeParent.getWidth() / 1.19) && event.getX() <= (linearCarTypeParent.getWidth())) {
                    Data.defaultCarType = 4;
                    if (Data.defaultCarType != Data.previousCarType) {
                        changeCarType();
                        Data.previousCarType = 4;
                    }
                    mposX = (linearCarTypeParent.getWidth() - imgSelectedCarType.getWidth());
                    imgSelectedCarType.animate().setDuration(250).translationX(mposX);
                    imgSelectedCarType.setBackgroundResource(R.drawable.vehicle_5);
                    mprevX = mposX;

                }
            }


            return false;
        }
    }


    //Favourite Functions
    public void addFavouriteHome(View view) {
        if (Data.favouriteHome.equalsIgnoreCase("")) {
            Data.currentPickupText = etLocation.getText().toString();
            startActivity(new Intent(HomeActivity.this, FavouriteLocationActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            Data.fromFavouriteHome = true;
        } else {
            if (isSearchStartLocation) {
                Utils.hideSoftKeyboard(HomeActivity.this, etLocation);
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
                switchPassengerScreen(passengerScreenMode);
                favouriteSelected = true;
                pickUpLocationText.setText(getString(R.string.home));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(Prefs.with(getApplicationContext()).getObject("fav_home_latLng", LatLng.class), 14), 1000, null);
            } else {
                Utils.hideSoftKeyboard(HomeActivity.this, etLocation);
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
                switchPassengerScreen(passengerScreenMode);
                Data.destinationLatLng = Prefs.with(getApplicationContext()).getObject("fav_home_latLng", LatLng.class);
                destinationLocationtext.setText(getString(R.string.home));
            }

        }
    }

    public void addFavouriteWork(View view) {
        if (Data.favouriteWork.equalsIgnoreCase("")) {
            Data.currentPickupText = etLocation.getText().toString();
            startActivity(new Intent(HomeActivity.this, FavouriteLocationActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            Data.fromFavouriteWork = true;
        } else {
            if (isSearchStartLocation) {
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
                switchPassengerScreen(passengerScreenMode);
                Utils.hideSoftKeyboard(HomeActivity.this, etLocation);
                favouriteSelected = true;
                pickUpLocationText.setText(getString(R.string.work));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(Prefs.with(getApplicationContext()).getObject("fav_work_latLng", LatLng.class), 14), 1000, null);
            } else {
                passengerScreenMode = PassengerScreenMode.P_INITIAL;
                switchPassengerScreen(passengerScreenMode);
                Utils.hideSoftKeyboard(HomeActivity.this, etLocation);
                Data.destinationLatLng = Prefs.with(getApplicationContext()).getObject("fav_work_latLng", LatLng.class);
                destinationLocationtext.setText(getString(R.string.work));
            }

        }
    }

    public void editHomeFavourite(View view) {
        startActivity(new Intent(HomeActivity.this, FavouriteLocationActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        Data.fromFavouriteHome = true;
    }

    public void editWorkFavourite(View view) {
        startActivity(new Intent(HomeActivity.this, FavouriteLocationActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        Data.fromFavouriteWork = true;
    }

    class GetFormattedAddress extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Data.previousLatLng = Data.pickupLatLng;
        }

        protected String doInBackground(Void... urls) {
            if (canChangeLocationText) {
                canChangeLocationText = false;
                Data.formatedAddress = MapUtils.getGAPIAddress(Data.pickupLatLng);
                return "";
            }
            return "";

        }

        @Override
        protected void onPostExecute(String text) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pickUpLocationText.setText(Data.formatedAddress);
                    pickUpLocationProgress.setVisibility(View.GONE);
                    pickUpLocationText.setVisibility(View.VISIBLE);
                    canChangeLocationText = true;
                    if (Data.previousLatLng != Data.pickupLatLng) {
                        new GetFormattedAddress().execute();
                    }
                }
            });
        }
    }

    public void fareEstimationAlertPopup(Activity activity, String title, String message) {
        try {
            try {
                if (fareEstimateErrorDialog != null && fareEstimateErrorDialog.isShowing()) {
                    fareEstimateErrorDialog.dismiss();
                }
            } catch (Exception e) {
            }
            if ("".equalsIgnoreCase(title)) {
                title = activity.getResources().getString(R.string.alert);
            }

            fareEstimateErrorDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            fareEstimateErrorDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            fareEstimateErrorDialog.setContentView(R.layout.custom_message_dialog);

            FrameLayout frameLayout = (FrameLayout) fareEstimateErrorDialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, false);

            WindowManager.LayoutParams layoutParams = fareEstimateErrorDialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            fareEstimateErrorDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            fareEstimateErrorDialog.setCancelable(false);
            fareEstimateErrorDialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) fareEstimateErrorDialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) fareEstimateErrorDialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            ImageView textImg = (ImageView) fareEstimateErrorDialog.findViewById(R.id.textImg);

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setText(title);
            textMessage.setText(message);

            textHead.setVisibility(View.GONE);
            textImg.setVisibility(View.GONE);

            Button btnOk = (Button) fareEstimateErrorDialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    addDestinationRl.performClick();
                    destinationLocationRl.performClick();
                    fareEstimateErrorDialog.dismiss();
                }

            });

            fareEstimateErrorDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}





