package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.FeaturesConfig;
import com.scooupetest.utils.Utils;
import com.braintreepayments.api.Braintree;
import com.braintreepayments.api.models.CardBuilder;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;


public class FinalRegisterScreen extends BaseActivity {

    static final Pattern CODE_PATTERN = Pattern
            .compile("^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35{3}){11})$");
    protected String a;
    Button personalCreditCardDoneBtn, corporateAccountDoneButton;
    RelativeLayout relative, relativeDownArrow, relativeOutside;
    LinearLayout linearPaymentOptions, linearPersonalCreditCard, linearCorporateAccount;
    boolean loginDataFetched = false, showOtpDialog = false, alreadyRegistered = false;
    String otpAlertString = "";
    alihafizji.library.CreditCardEditText cardNumberEt;
    AutoCompleteTextView country;
    boolean sendToOtpScreen = false;
    int otpFlag = 0, selectedYear = 1, selectedMonth = 1, paymentTypeFlag;
    private String[] month_spinner = new String[12],
            year_spinner = new String[40];
    private TextView headerText, tvSelectedPaymentMethod, tvCorporateAccount, tvPersonalCreditCard, tvCorporateCreditCard;
    private Button backBtn;
    private EditText year, month, corporateEmailET, corporateCodeEt;
    private Button monthSpin;
    private Button yearSpin;

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_card);
        loginDataFetched = false;

        relative = (RelativeLayout) findViewById(R.id.relative);
        new ASSL(FinalRegisterScreen.this, relative, 1134, 720, false);


        cardNumberEt = (alihafizji.library.CreditCardEditText) findViewById(R.id.cardNumberEt);
        cardNumberEt.setTypeface(Data.getFont(getApplicationContext()));
        cardNumberEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                cardNumberEt.setError(null);
            }
        });
        for (int i = 0; i < 12; i++) {
            if (i < 9) {
                month_spinner[i] = "0" + (i + 1) + "";
            } else {
                month_spinner[i] = (i + 1) + "";
            }
        }

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        //	today.year

        Log.v("today.year", today.year + "");

        for (int i = 0; i < 40; i++) {
            year_spinner[i] = (today.year + i) + "";
        }

        monthSpin = (Button) findViewById(R.id.monthSpinner);
        yearSpin = (Button) findViewById(R.id.yearSpinner);

        month = (EditText) findViewById(R.id.month);
        month.setTypeface(Data.getFont(getApplicationContext()));
        year = (EditText) findViewById(R.id.year);
        year.setTypeface(Data.getFont(getApplicationContext()));

        personalCreditCardDoneBtn = (Button) findViewById(R.id.signUpBtn);
        personalCreditCardDoneBtn.setTypeface(Data.getFont(getApplicationContext()));

        Button skipButton = (Button) findViewById(R.id.btnSkip);
        if(FeaturesConfig.cardOnly)
        {
            skipButton.setVisibility(View.GONE);
        }
        else
        {
            skipButton.setVisibility(View.VISIBLE);
        }

        skipButton.setTypeface(Data.getFont(getApplicationContext()));

        TextView extraTextForScroll = (TextView) findViewById(R.id.extraTextForScroll);

        headerText = (TextView) findViewById(R.id.title);
        headerText.setTypeface(Data.getFont(getApplicationContext()));

        backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FinalRegisterScreen.this, RegisterScreen.class);
                intent.putExtra("back_from_otp", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });


        skipButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                paymentTypeFlag=3;

                corporateEmailET.setText("");
                corporateCodeEt.setText("");

                if (RegisterScreen.facebookLogin) {
                    sendFacebookSignupValues(FinalRegisterScreen.this, "");

                } else {
                    sendSignupValues(FinalRegisterScreen.this, "");
                }

            }
        });

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        monthSpin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupPicker(FinalRegisterScreen.this, 0, month_spinner);
            }
        });
        yearSpin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupPicker(FinalRegisterScreen.this, 1, year_spinner);
            }
        });


        linearPersonalCreditCard = (LinearLayout) findViewById(R.id.linearPersonalCerditCard);
        personalCreditCardDoneBtn = (Button) findViewById(R.id.signUpBtn);
        personalCreditCardDoneBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);


        //linear corporate Account
        linearCorporateAccount = (LinearLayout) findViewById(R.id.linearCorporateAccount);
        corporateEmailET = (EditText) findViewById(R.id.corporateAccountEmailIdEt);
        corporateEmailET.setTypeface(Data.getFont(getApplicationContext()));
        corporateCodeEt = (EditText) findViewById(R.id.corporateCodeEt);
        corporateCodeEt.setTypeface(Data.getFont(getApplicationContext()));
        corporateAccountDoneButton = (Button) findViewById(R.id.corporateAccountDoneButton);
        corporateAccountDoneButton.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        corporateAccountDoneButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if ("".equalsIgnoreCase(corporateEmailET.getText().toString())) {
                    corporateEmailET.requestFocus();
                    corporateEmailET.setError("Please enter corporate email.");
                    return;
                } else if ("".equalsIgnoreCase(corporateCodeEt.getText().toString())) {
                    corporateCodeEt.requestFocus();
                    corporateCodeEt.setError("Please enter corporate code.");
                    return;
                }


                if (!isEmailValid(corporateEmailET.getText().toString())) {
                    corporateEmailET.requestFocus();
                    corporateEmailET.setError("Please enter valid email.");
                    return;

                }
                if (RegisterScreen.facebookLogin) {
                    sendFacebookSignupValues(FinalRegisterScreen.this, "");

                } else {
                    sendSignupValues(FinalRegisterScreen.this, "");
                }

            }
        });


        linearPaymentOptions = (LinearLayout) findViewById(R.id.linearPaymentOptions);


        relativeDownArrow = (RelativeLayout) findViewById(R.id.relDownArrow);
        relativeOutside = (RelativeLayout) findViewById(R.id.relativeOutside);
        relativeOutside.setEnabled(false);
        relativeOutside.setClickable(false);
        linearPaymentOptions.setClickable(false);

        relativeDownArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(FinalRegisterScreen.this, corporateCodeEt);
                Log.i("Height", "==" + linearPaymentOptions.getHeight());
                TranslateAnimation anim = new TranslateAnimation(0, 0, -(int) (linearPaymentOptions.getHeight()), 0
                );
                anim.setDuration(500);
                anim.setFillAfter(true);
                linearPaymentOptions.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {
                        // TODO Auto-generated method stub
                        linearPersonalCreditCard.setVisibility(View.GONE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        linearPaymentOptions.setVisibility(View.VISIBLE);
                        Log.i("Height", "==" + linearPaymentOptions.getHeight());
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        // TODO Auto-generated method stub
                        relativeDownArrow.setVisibility(View.GONE);

                        tvSelectedPaymentMethod.setText(getString(R.string.select_payment_method));
                        corporateEmailET.setText("");
                        corporateCodeEt.setText("");
                        month.setText("");
                        year.setText("");
                        cardNumberEt.setText("");
                        relativeOutside.setEnabled(true);
                        relativeOutside.setClickable(true);
                        tvPersonalCreditCard.setClickable(true);
                        tvCorporateAccount.setClickable(true);
                        tvCorporateCreditCard.setClickable(true);


                    }
                });
            }
        });


        tvCorporateAccount = (TextView) findViewById(R.id.tvCorporateAccount);
        tvCorporateAccount.setTypeface(Data.getFont(getApplicationContext()));
        tvPersonalCreditCard = (TextView) findViewById(R.id.tvPersonalCreditCard);
        tvPersonalCreditCard.setTypeface(Data.getFont(getApplicationContext()));
        tvCorporateCreditCard = (TextView) findViewById(R.id.tvCorporateCreditCard);
        tvCorporateCreditCard.setTypeface(Data.getFont(getApplicationContext()));
        tvSelectedPaymentMethod = (TextView) findViewById(R.id.tvSelectedPaymentMethod);
        tvSelectedPaymentMethod.setTypeface(Data.getFont(getApplicationContext()));

        if (FeaturesConfig.isCorporateAccountEnable) {
            tvCorporateAccount.setVisibility(View.VISIBLE);
        } else {
            tvCorporateAccount.setVisibility(View.GONE);
        }

        personalCreditCardDoneBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if ("".equalsIgnoreCase(cardNumberEt.getText().toString())) {
                    cardNumberEt.requestFocus();
                    cardNumberEt.setError("Please enter card number");
                    return;
                } else if (cardNumberEt.getText().toString().length() < 14) {
                    cardNumberEt.requestFocus();
                    cardNumberEt.setError("Please enter valid card number");
                    return;
                }


                if (!isNumeric(month.getText().toString())) {
//					 month.setError("Please select month");
                    new DialogPopup().alertPopup(FinalRegisterScreen.this, "", "Please select month");
                    return;

                }

                if (!isNumeric(year.getText().toString())) {
//					 year.setError("Please select year");
                    new DialogPopup().alertPopup(FinalRegisterScreen.this, "", "Please select year");
                    return;

                }
                String currentYear = "" + Calendar.getInstance().get(Calendar.YEAR);
                if (currentYear.equalsIgnoreCase(year.getText().toString())) {
                    if (Integer.parseInt(month.getText().toString()) < (Calendar.getInstance().get(Calendar.MONTH) + 1)) {
                        new DialogPopup().alertPopup(FinalRegisterScreen.this, "", "Please select valid expiry date.");
                        return;
                    }

                }

                getBrainTreeClientToken(FinalRegisterScreen.this);

            }
        });


    }

    /**
     * ASync for register from server
     */
    public void sendSignupValues(final Activity activity, final String paymentNounce) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            resetFlags();
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            Log.i("user_name", "=" + Data.userName);
            Log.i("ph_no", "=" + Data.phoneNo);
            Log.i("email", "=" + Data.emailId);
            Log.i("password", "=" + Data.password);
            Log.i("otp", "=" + "");
            Log.i("device_token", "=" + Data.deviceToken);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("country", "=" + Data.country);
            Log.i("device_name", "=" + Data.deviceName);
            Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

            if (Data.locationFetcher != null) {
                Data.latitude = Data.locationFetcher.getLatitude();
                Data.longitude = Data.locationFetcher.getLongitude();
            }
            Data.corporateEmail = corporateEmailET.getText().toString();
            Data.corporteCode = corporateCodeEt.getText().toString();
            Data.paymentTypeFlag = paymentTypeFlag;
            Log.i("corporateEmail", "=" + Data.corporateEmail);
            Log.i("corporteCode", "=" + Data.corporteCode);
            Log.i("paymentTypeFlag", "=" + Data.paymentTypeFlag);
            RestClient.getApiService().sendSignupValues(Data.userName, Data.phoneNo, Data.emailId, Data.password, "", Data.DEVICE_TYPE, Data.uniqueDeviceId, Data.deviceToken,
                    Data.latitude, Data.longitude, Data.country, Data.deviceName, Data.appVersion, Data.osVersion, Data.hearAboutText, "0", "0", paymentNounce, "",
                    Data.paymentTypeFlag, Data.corporateEmail, Data.corporteCode, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);


                                boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);

                                if (!newUpdate) {

                                    if (!jObj.isNull("error")) {
                                        int flag = jObj.getInt("flag");
                                        String errorMessage = jObj.getString("error");

                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            HomeActivity.logoutUser(activity);
                                        } else if (0 == flag) {
                                            Data.phoneNo = jObj.getString("phone_no");

                                            otpFlag = 0;
                                            sendToOtpScreen = true;
                                            Data.nounce = paymentNounce;

                                        } else {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                        DialogPopup.dismissLoadingDialog();
                                    } else {
                                        new JSONParser().parseLoginData(activity, response);

                                        Database.getInstance(FinalRegisterScreen.this).insertEmail(Data.emailId);
                                        Database.getInstance(FinalRegisterScreen.this).close();

                                        loginDataFetched = true;

                                        DialogPopup.dismissLoadingDialog();

                                    }
                                } else {
                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });


        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }

    /**
     * ASync for login from server
     */
    public void sendFacebookSignupValues(final Activity activity, final String paymentNounce) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            resetFlags();
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            Log.i("user_fb_id", "=" + Data.fbId);
            Log.i("user_fb_name", "=" + Data.fbFirstName + " " + Data.fbLastName);
            Log.i("fb_access_token", "=" + Data.fbAccessToken);
            Log.i("username", "=" + Data.fbUserName);
            Log.i("fb_mail", "=" + Data.fbUserEmail);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("device_token", "=" + Data.deviceToken);
            Log.i("country", "=" + Data.country);
            Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("device_name", "=" + Data.deviceName);
            Log.i("device_type", "=" + Data.DEVICE_TYPE);
            Log.i("otp", "=" + "");
            Log.i("ph_no", "=" + Data.phoneNo);
            Log.i("password", "=" + Data.password);
            Log.i("referral_code", "=" + Data.hearAboutText);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

            Data.corporateEmail = corporateEmailET.getText().toString();
            Data.corporteCode = corporateCodeEt.getText().toString();
            Data.paymentTypeFlag = paymentTypeFlag;
            Log.i("corporateEmail", "=" + Data.corporateEmail);
            Log.i("corporteCode", "=" + Data.corporteCode);
            Log.i("paymentTypeFlag", "=" + Data.paymentTypeFlag);

            RestClient.getApiService().sendFacebookLoginValues(Data.fbId, Data.fbFirstName + " " + Data.fbLastName, Data.fbAccessToken, "" + Data.fbUserName, Data.fbUserEmail,
                    Data.latitude, Data.longitude, Data.deviceToken, Data.country, Data.appVersion, Data.osVersion, Data.deviceName, Data.DEVICE_TYPE, Data.uniqueDeviceId, "",
                    Data.phoneNo, /*Data.password,*/ Data.hearAboutText, paymentNounce, Data.paymentTypeFlag, Data.corporateEmail, Data.corporteCode, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);


                                boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);

                                if (!newUpdate) {
                                    if (!jObj.isNull("error")) {
                                        int flag = jObj.getInt("flag");
                                        String errorMessage = jObj.getString("error");
                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            HomeActivity.logoutUser(activity);
                                        } else if (2 == flag) {
                                            Data.phoneNo = jObj.getString("phone_no");
                                            otpFlag = 1;
                                            sendToOtpScreen = true;
                                            Data.nounce = paymentNounce;

                                        } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                            alreadyRegistered = true;
                                        } else {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                        DialogPopup.dismissLoadingDialog();
                                    } else {
                                        new JSONParser().parseLoginData(activity, response);
                                        loginDataFetched = true;
                                        Database.getInstance(FinalRegisterScreen.this).insertEmail(Data.fbUserEmail);
                                        Database.getInstance(FinalRegisterScreen.this).close();
                                        DialogPopup.dismissLoadingDialog();
                                    }
                                } else {
                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                DialogPopup.dismissLoadingDialog();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus && Data.isOpenGooglePlay) {
            Data.isOpenGooglePlay = false;
        } else if (hasFocus && loginDataFetched) {
            loginDataFetched = false;


            startActivity(new Intent(FinalRegisterScreen.this, HomeActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }else   if(hasFocus && alreadyRegistered){
            alreadyRegistered = false;
            startActivity(new Intent(FinalRegisterScreen.this, SplashLogin.class));
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            finish();
        } else if (hasFocus && sendToOtpScreen) {
            sendIntentToOtpScreen();
        }

    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    boolean isPhoneValid(CharSequence phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FinalRegisterScreen.this, RegisterScreen.class);
        intent.putExtra("back_from_otp", true);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ASSL.closeActivity(relative);

        System.gc();
    }

    public void resetFlags() {
        loginDataFetched = false;
        sendToOtpScreen = false;
        otpFlag = 0;
    }

    /**
     * Send intent to otp screen by making required data objects
     * flag 0 for email, 1 for Facebook
     */
    public void sendIntentToOtpScreen() {
        if (0 == otpFlag) {
            RegisterScreen.facebookLogin = false;
            OTPConfirmScreen.intentFromRegister = true;
            OTPConfirmScreen.emailRegisterData = new EmailRegisterData(Data.userName, Data.emailId, Data.phoneNo, Data.password, Data.hearAboutText);
            startActivity(new Intent(FinalRegisterScreen.this, OTPConfirmScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        } else if (1 == otpFlag) {
            RegisterScreen.facebookLogin = true;
            OTPConfirmScreen.intentFromRegister = true;
            OTPConfirmScreen.facebookRegisterData = new FacebookRegisterData(Data.phoneNo,/* Data.password,*/ Data.hearAboutText);
            startActivity(new Intent(FinalRegisterScreen.this, OTPConfirmScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
    }

    public void popupPicker(Activity acivity, final int type,
                            final String items[]) {

        final Dialog dialog1 = new Dialog(acivity,

                android.R.style.Theme_Holo_Dialog_NoActionBar);

        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog1.setContentView(R.layout.popup_number_picker);

        new ASSL(this, (ViewGroup) dialog1.findViewById(R.id.root), 1134, 720, false);

        WindowManager.LayoutParams layoutparams = dialog1.getWindow()

                .getAttributes();

        layoutparams.dimAmount = Data.dialogDimAmount;

        dialog1.getWindow()

                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog1.setCancelable(false);

        dialog1.setCanceledOnTouchOutside(false);

        final NumberPicker numberPicker = (NumberPicker) dialog1

                .findViewById(R.id.numberPicker1);

        numberPicker

                .setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);


        TextView textViewType = (TextView) dialog1.findViewById(R.id.type);

        TextView textViewDone = (TextView) dialog1.findViewById(R.id.DoneTxt);

        textViewType.setTypeface(Data.getFont(getApplicationContext()));

        textViewDone.setTypeface(Data.getFont(getApplicationContext()));

        if (type == 0) {

            textViewType.setText("Select Month");

            numberPicker.setMaxValue(items.length);

            numberPicker.setMinValue(1);

            numberPicker.setDisplayedValues(items);

            numberPicker.setFocusable(true);

            numberPicker.setFocusableInTouchMode(true);

            numberPicker.setValue(selectedMonth);

        } else {

            textViewType.setText("Select Year");

            numberPicker.setMaxValue(items.length);

            numberPicker.setMinValue(1);

            numberPicker.setFocusable(true);

            numberPicker.setDisplayedValues(items);

            numberPicker.setFocusableInTouchMode(true);

            numberPicker.setValue(selectedYear);

        }

        textViewDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i("New Value", "=" + numberPicker.getValue());

                if (type == 0) {

                    month.setText(items[numberPicker.getValue() - 1]);
                    selectedMonth = numberPicker.getValue();

                } else {

                    year.setText(items[numberPicker.getValue() - 1]);
                    selectedYear = numberPicker.getValue();

                }

                dialog1.dismiss();

            }

        });

        dialog1.show();

    }

    /**
     * ASync getBrainTreeClientToken from server
     */
    public void getBrainTreeClientToken(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            RestClient.getApiService().getBrainTreeClientToken(new Callback<String>() {
                @Override
                public void success(String response, Response arg1) {
                    Log.i("Server response", "response = " + response);

                    try {

                        JSONObject obj = new JSONObject(response);
                        Log.e("clientToken", "= " + obj.getString("clientToken"));
                        Braintree braintree = Braintree.getInstance(getApplicationContext(), obj.getString("clientToken"));
                        braintree.addListener(new Braintree.PaymentMethodNonceListener() {
                            public void onPaymentMethodNonce(String paymentMethodNonce) {
                                // Communicate the nonce to your server
                                Log.i("paymentMethodNonce", "==" + paymentMethodNonce);
                                if (RegisterScreen.facebookLogin) {
                                    sendFacebookSignupValues(FinalRegisterScreen.this, paymentMethodNonce);

                                } else {
                                    sendSignupValues(FinalRegisterScreen.this, paymentMethodNonce);
                                }
                            }
                        });

                        CardBuilder cardBuilder = new CardBuilder()
                                .cardNumber(cardNumberEt.getText().toString().replace("-", ""))
                                .expirationDate(month.getText().toString() + "/" + year.getText().toString());

                        braintree.tokenize(cardBuilder);
                        Log.i("cardNumber", "=" + cardNumberEt.getText().toString());
                        Log.i("cardNumber", "=" + cardNumberEt.getText().toString().replace("-", ""));
                        Log.i("expirationDate", "=" + month.getText().toString() + "/" + year.getText().toString());

                    } catch (Exception exception) {
                        exception.printStackTrace();
                        DialogPopup.dismissLoadingDialog();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }


                }

                @Override
                public void failure(RetrofitError error) {
                    Log.i("failure response", "response = " + error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }

    public void selectPaymentMethod(final View v) {

        Log.i("Height", "==" + linearPaymentOptions.getHeight());
        TranslateAnimation anim = new TranslateAnimation(0,
                0, 0, -(int) (linearPaymentOptions.getHeight()));
        anim.setDuration(500);
        anim.setFillAfter(true);

        linearPaymentOptions.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                linearPaymentOptions.setVisibility(View.GONE);
                relativeDownArrow.setVisibility(View.VISIBLE);
                relativeOutside.setEnabled(false);
                relativeOutside.setClickable(false);
                tvPersonalCreditCard.setClickable(false);
                tvCorporateAccount.setClickable(false);
                tvCorporateCreditCard.setClickable(false);

                switch (v.getId()) {
                    case R.id.tvPersonalCreditCard:
                        linearPersonalCreditCard.setVisibility(View.VISIBLE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        tvSelectedPaymentMethod.setText(getString(R.string.personal_CC));
                        paymentTypeFlag = 2;
                        break;
                    case R.id.tvCorporateAccount:
                        linearPersonalCreditCard.setVisibility(View.GONE);
                        linearCorporateAccount.setVisibility(View.VISIBLE);
                        tvSelectedPaymentMethod.setText(getString(R.string.corporate_account));
                        paymentTypeFlag = 0;
                        break;
                    case R.id.tvCorporateCreditCard:
                        linearPersonalCreditCard.setVisibility(View.VISIBLE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        tvSelectedPaymentMethod.setText(getString(R.string.company_CC));
                        paymentTypeFlag = 1;
                        break;
                    case R.id.relativeOutside:
                        linearPersonalCreditCard.setVisibility(View.GONE);
                        linearCorporateAccount.setVisibility(View.GONE);
                        tvSelectedPaymentMethod.setText(getString(R.string.select_payment_method));
                        break;

                }

            }
        });
    }

    class CustomAdapter<T> extends ArrayAdapter<String> {
        public CustomAdapter(Context context, int textViewResourceId,
                             String[] countryList) {

            super(context, textViewResourceId, countryList);

        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view
                    .findViewById(android.R.id.text1);

            textView.setText("");
            return view;
        }
    }


}
