package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.models.AppliedPromoCodes;
import com.scooupetest.models.PromoData;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.FlurryEventLogger;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Utils;
import com.flurry.android.FlurryAgent;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class PromoCodeActivity extends BaseActivity {


    RelativeLayout relative;

    Button backBtn;
    TextView title;

    EditText editTextPromoCode;
    Button buttonApplyPromoCode;
    ProgressBar progressBar;
    ListView listView;
    PromoCodesListAdapter promoCodesListAdapter;
    List<PromoData> appliedPromoCodesList = new ArrayList<PromoData>();
    Dialog dialog;
    int priviousDefaultPosition;

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promo_code);

        relative = (RelativeLayout) findViewById(R.id.relative);
        new ASSL(PromoCodeActivity.this, relative, 1134, 720, false);


        backBtn = (Button) findViewById(R.id.backBtn);
        title = (TextView) findViewById(R.id.header);
        title.setTypeface(Data.getFont(getApplicationContext()));
        listView = (ListView) findViewById(R.id.listViewPromoCode);
        promoCodesListAdapter = new PromoCodesListAdapter();
        listView.setAdapter(promoCodesListAdapter);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        editTextPromoCode = (EditText) findViewById(R.id.editTextPromoCode);
        editTextPromoCode.setTypeface(Data.getFont(getApplicationContext()));
        buttonApplyPromoCode = (Button) findViewById(R.id.btnApply);
        buttonApplyPromoCode.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        buttonApplyPromoCode.setEnabled(false);
        buttonApplyPromoCode.setBackgroundResource(R.drawable.main_interaction_color_disable_btn);


        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });


        buttonApplyPromoCode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String promoCode = editTextPromoCode.getText().toString().trim();
                if (promoCode.length() > 0) {
                    Utils.hideSoftKeyboard(PromoCodeActivity.this, editTextPromoCode);
                    applyPromoCodeAPI(PromoCodeActivity.this, promoCode);
                    FlurryEventLogger.promoCodeTried(Data.userData.accessToken, promoCode);
                } else {
                    editTextPromoCode.requestFocus();
                    editTextPromoCode.setError("Code can't be empty");
                }
            }
        });

        editTextPromoCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                editTextPromoCode.setError(null);
            }
        });

        editTextPromoCode.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        buttonApplyPromoCode.performClick();
                        return false;

                    case EditorInfo.IME_ACTION_NEXT:
                        return false;

                    default:
                        return false;
                }
            }
        });

        editTextPromoCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    buttonApplyPromoCode.setEnabled(true);
                    buttonApplyPromoCode.setBackgroundResource(R.drawable.main_interaction_btn_selector);
                } else {
                    buttonApplyPromoCode.setEnabled(false);
                    buttonApplyPromoCode.setBackgroundResource(R.drawable.main_interaction_color_disable_btn);
                }
            }
        });

        promoCodesListAdapter.notifyDataSetChanged();
        getAvailablePromoCodes(PromoCodeActivity.this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

    /**
     * API call for applying promo code to server
     */
    public void applyPromoCodeAPI(final Activity activity, final String promoCode) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));


            RestClient.getApiService().applyPromoCodeAPI(Data.userData.accessToken, promoCode, new Callback<String>() {
                        public JSONObject jObj;
//                            @Override
//                            public void success(UserData user, Response response) {
//
//                                Toast.makeText(getApplicationContext(), "FirstName = "+user.getUser().getFirstName(),Toast.LENGTH_LONG).show();
//                                textView.setText("Retrofit response time = "+(System.currentTimeMillis()-oldTime)+" ms");
//                            }

                        @Override
                        public void failure(final RetrofitError error) {


                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void success(String response, Response arg1) {
                            Log.i("Server response", "response = " + response);
                            try {
                                jObj = new JSONObject(response);
                                if (!jObj.isNull("error")) {
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {
                                    int flag = jObj.getInt("flag");
                                    if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                        String message = jObj.getString("message");
                                        editTextPromoCode.setText("");
                                        new DialogPopup().alertPopup(activity, "", message);
                                        FlurryEventLogger.promoCodeApplied(Data.userData.accessToken, promoCode, message);
                                    }

                                    getAvailablePromoCodes(activity);
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);

                            }
                            DialogPopup.dismissLoadingDialog();

                        }
                    }
            );
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }
    }

    void alertPopup(Activity activity, String message) {
        try {
            try {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            } catch (Exception e) {

            }

            dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_message_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            ImageView textImg = (ImageView) dialog.findViewById(R.id.textImg);
            textImg.setVisibility(View.GONE);

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setText("");
            textMessage.setText(message);

            textHead.setVisibility(View.GONE);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ASync for get Account info from server
     */
    public void getAvailablePromoCodes(final Activity activity) {

        if (AppStatus.getInstance(activity).isOnline(activity)) {
            RestClient.getApiService().getAvailablePromoCodes(Data.userData.accessToken, new Callback<String>() {
                        public JSONObject jObj;

                        //
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBar.setVisibility(View.GONE);
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }

                        @Override
                        public void success(String response, Response arg1) {
                            Log.i("Server response", "response = " + response);
                            try {
                                jObj = new JSONObject(response);
                                if (!jObj.isNull("error")) {
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                } else {
                                    Gson gc = new Gson();
                                    appliedPromoCodesList.clear();
                                    AppliedPromoCodes appliedPromoCodes = gc.fromJson(response, AppliedPromoCodes.class);
                                    appliedPromoCodesList = appliedPromoCodes.getPromoData();
                                    if (appliedPromoCodesList.size() == 0) {
                                        Data.userData.isPromoApplied = 0;
                                        Toast.makeText(getApplicationContext(), "No promo code added", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Data.userData.isPromoApplied = 1;
                                        listView.setVisibility(View.VISIBLE);
                                        promoCodesListAdapter.notifyDataSetChanged();
                                    }
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);

                            }
                            progressBar.setVisibility(View.GONE);

                        }
                    }
            );
        } else {
            progressBar.setVisibility(View.GONE);
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }


    }

    public void makeDefaultCode(final String id, final int position) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            for (int i = 0; i < appliedPromoCodesList.size(); i++) {
                if (appliedPromoCodesList.get(i).getIsDefault() == 1) {
                    priviousDefaultPosition = i;
                }
                appliedPromoCodesList.get(i).setIsDefault(0);
            }
            appliedPromoCodesList.get(position).setIsDefault(1);
            promoCodesListAdapter.notifyDataSetChanged();
            Log.i("id", id);
            Log.i("access_token", Data.userData.accessToken);

            RestClient.getApiService().makeDefaultPromoCode(id, Data.userData.accessToken, new Callback<String>() {
                @Override
                public void success(String responseString, Response response) {
                    com.scooupetest.utils.Log.v("Server response", "response = " + response);

                    try {
                        JSONObject jObj = new JSONObject(responseString);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");
                            for (int i = 0; i < appliedPromoCodesList.size(); i++) {

                                appliedPromoCodesList.get(i).setIsDefault(0);
                            }
                            appliedPromoCodesList.get(priviousDefaultPosition).setIsDefault(1);
                            promoCodesListAdapter.notifyDataSetChanged();

                            if (0 == flag) { // {"error": 'some
                                // parameter
                                // missing',"flag":0}//error
                                new DialogPopup().alertPopup(
                                        PromoCodeActivity.this, "",
                                        errorMessage);

                            } else if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(PromoCodeActivity.this);
                            } else {
                                new DialogPopup().alertPopup(
                                        PromoCodeActivity.this, "",
                                        errorMessage);
                            }
                        } else {

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        for (int i = 0; i < appliedPromoCodesList.size(); i++) {

                            appliedPromoCodesList.get(i).setIsDefault(0);
                        }
                        appliedPromoCodesList.get(priviousDefaultPosition).setIsDefault(1);
                        promoCodesListAdapter.notifyDataSetChanged();
                        new DialogPopup().alertPopup(
                                PromoCodeActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    com.scooupetest.utils.Log.e("request fail", error.toString());
                    for (int i = 0; i < appliedPromoCodesList.size(); i++) {

                        appliedPromoCodesList.get(i).setIsDefault(0);
                    }
                    appliedPromoCodesList.get(priviousDefaultPosition).setIsDefault(1);
                    promoCodesListAdapter.notifyDataSetChanged();

                    new DialogPopup().alertPopup(PromoCodeActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(PromoCodeActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }

    class ViewHolderCoupon {
        ImageView imgPromoCode, imgIsDefaultCode;
        RelativeLayout relative, relativeOuterBorder, relativeMakeDefault, relativeItemParent;
        TextView tvExpiryDate;


        int id;
    }

    class PromoCodesListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderCoupon holder;

        public PromoCodesListAdapter() {

            this.mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return appliedPromoCodesList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderCoupon();
                convertView = mInflater.inflate(R.layout.list_item_promo_code, null);

                holder.imgPromoCode = (ImageView) convertView.findViewById(R.id.imgPromoCode);
                holder.imgIsDefaultCode = (ImageView) convertView.findViewById(R.id.imgDefaultCode);
                holder.relative = (RelativeLayout) convertView.findViewById(R.id.relative);
                holder.relativeOuterBorder = (RelativeLayout) convertView.findViewById(R.id.relativeOuterBorder);
                holder.relativeMakeDefault = (RelativeLayout) convertView.findViewById(R.id.relativeMakeDefault);
                holder.relativeItemParent = (RelativeLayout) convertView.findViewById(R.id.relativeItemParent);

                holder.tvExpiryDate = (TextView) convertView.findViewById(R.id.tvValidDate);
                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, ViewGroup.LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderCoupon) convertView.getTag();
            }

            holder.id = position;

            Log.i("imageUrl", "==" + appliedPromoCodesList.get(position).getPromoImage());
            Log.i("getIsDefault", "==" + appliedPromoCodesList.get(position).getIsDefault());

            try {
                Picasso.with(PromoCodeActivity.this).load(appliedPromoCodesList.get(position).getPromoImage()).fit().into(holder.imgPromoCode);
                if (appliedPromoCodesList.get(position).getIsDefault() == 1) {
                    holder.imgIsDefaultCode.setBackgroundResource(R.drawable.checked);
                    holder.relativeOuterBorder.setBackgroundResource(R.drawable.main_interaction_thick_border_rounded);
                } else {
                    holder.imgIsDefaultCode.setBackgroundResource(R.drawable.unchecked);
                    holder.relativeOuterBorder.setBackgroundResource(R.drawable.main_interaction_border_rounded_normal);
                }
                holder.tvExpiryDate.setText(appliedPromoCodesList.get(position).getExpiryDate());
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.relativeItemParent.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    alertPopup(PromoCodeActivity.this, appliedPromoCodesList.get(position).getDescription());
                }
            });
            holder.relativeMakeDefault.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    makeDefaultCode(appliedPromoCodesList.get(position).getPromoId(), position);
                }
            });
            return convertView;
        }

    }

}
