package com.scooupetest;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;

import com.scooupetest.datastructure.DriverInfo;
import com.scooupetest.datastructure.EndRideData;
import com.scooupetest.datastructure.FareStructure;
import com.scooupetest.datastructure.UserData;
import com.scooupetest.locationfiles.LocationFetcher;
import com.scooupetest.utils.Prefs;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Stores common static data for access for all activities across the application
 *
 * @author shankar
 */
public class Data {

    public static final float dialogDimAmount = 0.0f;

    public static final String APP_NAME = "Autoverdi";
    public static final String INVALID_ACCESS_TOKEN = "invalid access token";
    public static final String DEBUG_PASSWORD = "3131";
    public static final String SHARED_PREF_NAME = "myPref", SETTINGS_SHARED_PREF_NAME = "settingsPref";
    public static final String SP_ACCESS_TOKEN_KEY = "access_token",

    SP_TOTAL_DISTANCE = "total_distance",
            SP_WAIT_TIME = "wait_time",
            SP_RIDE_TIME = "ride_time",
            SP_RIDE_START_TIME = "ride_start_time",
            SP_LAST_LATITUDE = "last_latitude",
            SP_LAST_LONGITUDE = "last_longitude",

    SP_CUSTOMER_SCREEN_MODE = "customer_screen_mode",

    SP_C_SESSION_ID = "c_session_id",
            SP_C_ENGAGEMENT_ID = "c_engagement_id",
            SP_C_DRIVER_ID = "c_driver_id",
            SP_C_LATITUDE = "c_latitude",
            SP_C_LONGITUDE = "c_longitude",
            SP_C_DRIVER_NAME = "c_driver_name",
            SP_C_DRIVER_IMAGE = "c_driver_image",
            SP_C_DRIVER_CAR_IMAGE = "c_driver_car_image",
            SP_C_DRIVER_PHONE = "c_driver_phone",
            SP_C_DRIVER_RATING = "c_driver_rating",
            SP_C_DRIVER_DISTANCE = "c_driver_distance",
            SP_C_DRIVER_DURATION = "c_driver_duration",

    SP_C_TOTAL_DISTANCE = "c_total_distance",
            SP_C_TOTAL_FARE = "c_total_fare",
            SP_C_WAIT_TIME = "c_wait_time",
            SP_C_RIDE_TIME = "c_ride_time";
    public static final String SP_SERVER_LINK = "sp_server_link";
    public static final String SP_SELECTED_LANGUAGE = "LANG";
    public static final String DEV_SERVER_URL = "http://taxidev3.clicklabs.in:18000";
    public static final String LIVE_SERVER_URL = "http://taximust.clicklabs.in:9001";
    public static final String TRIAL_SERVER_URL = "http://scooupe-taxi.clicklabs.in:3003";
    public static final String DEFAULT_SERVER_URL = TRIAL_SERVER_URL;

    public static final String SERVER_ERROR_MSG = "Connection lost. Please try again later.";
    public static final String SERVER_NOT_RESOPNDING_MSG = "Connection lost. Please try again later.";
    public static final String CHECK_INTERNET_MSG = "Check your internet connection.";

    public static final String DEVICE_TYPE = "0";
    public static final int SOCKET_TIMEOUT = 30000;
    public static final int CONNECTION_TIMEOUT = 30000;
    public static final int MAX_RETRIES = 0;
    public static final int RETRY_TIMEOUT = 1000;
    public static Context context;
    //TODO change flurry key

    public static int defaultCarType = 0,previousCarType= 0;
    public static String P_RIDE_END = "P_RIDE_END", P_IN_RIDE = "P_IN_RIDE", P_REQUEST_FINAL = "P_REQUEST_FINAL",
            P_ASSIGNING = "P_ASSIGNING";
    public static LatLng startRidePreviousLatLng;
    public static LatLng previousLatLng;
    public static String formatedAddress= "";

    //TODO
    public static String SERVER_URL = DEFAULT_SERVER_URL;
    public static double latitude, longitude, surgeFactor = 1;
    public static int expiryMinutes = 1;
    public static Boolean isAcceptedMaximumFare = false;
    public static ArrayList<DriverInfo> driverInfos = new ArrayList<DriverInfo>();
    public static UserData userData;
    public static LocationFetcher locationFetcher;
    public static String deviceToken = "", country = "", deviceName = "", osVersion = "", uniqueDeviceId = "", packageName = "";
    public static int appVersion, paymentTypeFlag = 0;
    public static String phoneNo = "", countryCode = "", phoneNoWithoutCountryCode = "";
    public static String password = "";
    public static String emailId = "", hearAboutText = "", nounce = "", corporateEmail = "", corporteCode = "";
    public static String userName = "", firstName = "", lastName = "";
    public static String cEngagementId = "", cDriverId = "", cSessionId = "",rideAcceptedTime="";
    public static DriverInfo assignedDriverInfo;
    public static EndRideData endRideData;
    public static int featureConfigKeys = 1;
    public static boolean isOpenGooglePlay = false;
    public static double totalDistance = 0, totalFare = 0, tip =0;
    public static String waitTime = "", rideTime = "", isPaymentSuccessfull = "", defaulterFlag = "";
    public static int isSplitPopupShow = 0, isWaitStart = 0, isArrived = 0;
    public static JSONObject splitFareData;
    public static double discount = 0, CO2Saved = 0;
    public static JSONObject couponJSON;
    public static LatLng pickupLatLng, destinationLatLng;
    public static String fbAccessToken = "", fbId = "", fbFirstName = "", fbLastName = "", fbUserName = "", fbUserEmail = "";
    public static ArrayList<FareStructure> fareStructureArrayList = new ArrayList<FareStructure>();
    public static Typeface fontFamily;                                                                // fonts declaration
    public static long last_press = 0;
    public static int button_press_delay = 600;    //delay in milliseconds
    public static int tutorial = 0;
    public static String favouriteHome = "";
    public static String favouriteWork = "";
    public static String currentPickupText = "";
    public static Boolean fromFavouriteHome = false;
    public static Boolean fromFavouriteWork = false;


    public static void clearDataOnLogout(Context context) {
        try {
            driverInfos = new ArrayList<DriverInfo>();
            userData = null;
            locationFetcher = null;
            deviceToken = "";
            country = "";
            deviceName = "";
            appVersion = 0;
            osVersion = "";
            cEngagementId = "";
            cDriverId = "";
            assignedDriverInfo = null;
            pickupLatLng = null;
            destinationLatLng = null;
            fbAccessToken = "";
            fbId = "";
            fbFirstName = "";
            fbLastName = "";
            fbUserName = "";
            fbUserEmail = "";

            Prefs.with(context).save("fav_home_latLng","");
            Prefs.with(context).save("fav_work_latLng", "");
            Prefs.with(context).save("favourite_home", "");
            Prefs.with(context).save("favourite_work", "");

            Prefs.with(context).save("USER_DATA", "");
            SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
            Editor editor = pref.edit();
            editor.putString(Data.SP_ACCESS_TOKEN_KEY, "");
            editor.clear();
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Typeface getFont(Context appContext) {                                            // accessing fonts functions
        if (fontFamily == null) {
            fontFamily = Typeface.createFromAsset(appContext.getAssets(), "fonts/TitilliumWeb-Regular.ttf");
        }
        return fontFamily;
    }



    public static Boolean isTapable() {
        if (last_press + button_press_delay < System.currentTimeMillis()) {
            last_press = System.currentTimeMillis();
            return true;
        } else {
            return false;
        }
    }


    public static long getCurrentOffset() {
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();
        return TimeUnit.SECONDS.convert(mGMTOffset, TimeUnit.MILLISECONDS);
    }

}
