package com.scooupetest;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Validation;
import com.scooupetest.utils.Validation.TextType;
import com.flurry.android.FlurryAgent;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class ChangePassword extends BaseActivity {

    private EditText etOldPassword, etNewPassword, etConfirmPassword;
    private boolean isPasswordChanged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_activity);
        LinearLayout relative = (LinearLayout) findViewById(R.id.relroot);
        new ASSL(ChangePassword.this, relative, 1134, 720, false);
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_android));

        etOldPassword = (EditText) findViewById(R.id.etOldPassword);
        etOldPassword.setTypeface(Data.getFont(getApplicationContext()));


        etNewPassword = (EditText) findViewById(R.id.etNewPassword);
        etNewPassword.setTypeface(Data.getFont(getApplicationContext()));

        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        etConfirmPassword.setTypeface(Data.getFont(getApplicationContext()));

        Validation validation = new Validation();
        validation.setValidationFilter(TextType.Password, etOldPassword);
        validation.setValidationFilter(TextType.Password, etNewPassword);
        validation.setValidationFilter(TextType.Password, etConfirmPassword);


        TextView headerText = (TextView) findViewById(R.id.title);
        headerText.setTypeface(Data.getFont(getApplicationContext()));

        Button backBtn = (Button) findViewById(R.id.backBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etOldPassword.getWindowToken(), 0);
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

//		


        Button doneButton = (Button) findViewById(R.id.doneBtn);
        doneButton.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);


        doneButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etOldPassword.getWindowToken(), 0);
                if (etOldPassword.getText().toString().length() >= 6) {


                    if (etNewPassword.getText().toString().length() >= 6) {
//						if(etNewPassword
//						.getText()
//						.toString()
//						.equalsIgnoreCase(
//								etOldPassword.getText().toString()))
//						{
//							etOldPassword.setError(null);
//
//							etOldPassword.setError(null);
//
//							etNewPassword.setError("Old and new password is same.");
//							etNewPassword.requestFocus();
//						}

                        if (etNewPassword
                                .getText()
                                .toString()
                                .equalsIgnoreCase(
                                        etConfirmPassword.getText().toString())) {
                            resetPassword();

                        } else {


                            etOldPassword.setError(null);

                            etNewPassword.setError(null);

                            etConfirmPassword.setError("Password mismatch!");
                            etConfirmPassword.requestFocus();

                        }

                    } else {

                        if (etNewPassword.getText().toString().length() == 0)

                        {
                            etOldPassword.setError(null);
                            etConfirmPassword.setError(null);
                            etNewPassword.setError("Please fill new password.");
                            etNewPassword.requestFocus();
                        } else

                        {
                            etOldPassword.setError(null);
                            etConfirmPassword.setError(null);
                            etNewPassword.setError("Password should be 6 character long!");
                            etNewPassword.requestFocus();
                        }
                    }
                } else {
                    etNewPassword.setError(null);
                    etConfirmPassword.setError(null);
                    if (etOldPassword.getText().toString().length() == 0) {
                        etOldPassword.setError("Please fill old password.");
                    } else {
                        etOldPassword.setError("Old password should be 6 character long!");
                    }


                    etOldPassword.requestFocus();
                }
            }
        });


    }

    // The Activity's onStart method
    @Override
    protected void onStart() {
        super.onStart();
        // Set up the Flurry session
        FlurryAgent.onStartSession(this,getString(R.string.flurry_key));
    }

    // The Activity's onStop method
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etOldPassword.getWindowToken(), 0);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    public void resetPassword() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this, getString(R.string.loading));

            Log.i("access_token", Data.userData.accessToken);
            Log.i("old_password", etOldPassword.getText().toString() + "");
            Log.i("new_password", etNewPassword.getText().toString() + "");


            RestClient.getApiService().resetPassword(Data.userData.accessToken, etOldPassword.getText().toString() + "", etNewPassword.getText().toString() + "", new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                HomeActivity.logoutUser(ChangePassword.this);

                            } else {
                                new DialogPopup().alertPopup(
                                        ChangePassword.this, "", errorMessage);

                            }

                        } else {

                            new DialogPopup().alertPopup(ChangePassword.this,
                                    "", jObj.getString("log"));
                            isPasswordChanged = true;
                            etOldPassword.setText("");
                            etNewPassword.setText("");
                            etConfirmPassword.setText("");


                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(ChangePassword.this, "",
                                Data.SERVER_ERROR_MSG);
                    }
                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(ChangePassword.this, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });


        } else {
            new DialogPopup().alertPopup(ChangePassword.this, "",
                    getString(R.string.check_internet_message));
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && isPasswordChanged) {
            isPasswordChanged = false;
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }
}
