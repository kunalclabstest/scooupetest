package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Prefs;
import com.flurry.android.FlurryAgent;

import java.util.Arrays;
import java.util.List;

import rmn.androidscreenlibrary.ASSL;

public class SelectLanguageActivity extends BaseActivity {


    LinearLayout relative;

    Button backBtn;
    TextView title;

    ListView listViewHelp;

    LanguagesListAdapter languagesListAdapter;

    List<String> availableLanguagesList;



    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_language);

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(SelectLanguageActivity.this, relative, 1134, 720, false);


        backBtn = (Button) findViewById(R.id.backBtn);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));

        listViewHelp = (ListView) findViewById(R.id.listViewHelp);

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });


        String[] myResArray =getResources().getStringArray(R.array.lang_select);
        availableLanguagesList = Arrays.asList(myResArray);
        languagesListAdapter = new LanguagesListAdapter();
        listViewHelp.setAdapter(languagesListAdapter);

    }




    public void performBackPressed() {

            finish();
            overridePendingTransition(R.anim.left_in, R.anim.left_out);

    }

    @Override
    public void onBackPressed() {
        performBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

    class ViewHolderHelp {
        TextView name;
        LinearLayout relative;
        int id;
    }

    class LanguagesListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderHelp holder;

        public LanguagesListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return availableLanguagesList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderHelp();
                convertView = mInflater.inflate(R.layout.help_list_item, null);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.name.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);

                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderHelp) convertView.getTag();
            }

            holder.id = position;

            try {
                String[] g = availableLanguagesList.get(position).split(",");
                holder.name.setText(g[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.relative.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder = (ViewHolderHelp) v.getTag();

                        confirmationPopup(SelectLanguageActivity.this,position);

                }
            });


            return convertView;
        }

    }


    void confirmationPopup(final Activity activity,final int position) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            textHead.setVisibility(View.VISIBLE);
            textHead.setText(getString(R.string.change_language));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            textMessage.setText(getString(R.string.select_language_confirmation_message));

            ImageView imgHorizontalLine = (ImageView) dialog.findViewById(R.id.imgHorizontalLine);
            imgHorizontalLine.setVisibility(View.VISIBLE);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity), Typeface.BOLD);
            Boolean isAlreadySelected=false;
            try {
                String languageSelected = Prefs.with(getApplicationContext()).getString(Data.SP_SELECTED_LANGUAGE,"default");
                String[] g = availableLanguagesList.get(position).split(",");
                Prefs.with(getApplicationContext()).save(Data.SP_SELECTED_LANGUAGE, g[1]);
                if(g[1].equalsIgnoreCase(languageSelected)){
                    isAlreadySelected=true;
                    textMessage.setText(getString(R.string.language_already_selected_message));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            final Boolean finalIsAlreadySelected = isAlreadySelected;

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (finalIsAlreadySelected) {
                        performBackPressed();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SplashNewActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }

                dialog.dismiss();

            }

        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}

