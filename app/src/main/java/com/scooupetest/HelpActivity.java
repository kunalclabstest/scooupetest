package com.scooupetest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.HelpSection;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.FlurryEventLogger;
import com.flurry.android.FlurryAgent;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class HelpActivity extends BaseActivity {


    LinearLayout relative;

    Button backBtn;
    TextView title;

    ListView listViewHelp;
    RelativeLayout helpExpandedRl;

    ProgressBar progressBarHelp;
    TextView textViewInfoDisplay;
    WebView helpWebview;

    HelpListAdapter helpListAdapter;

    ArrayList<HelpSection> helpSections = new ArrayList<HelpSection>();
    HelpSection selectedHelpSection;


    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(HelpActivity.this, relative, 1134, 720, false);


        backBtn = (Button) findViewById(R.id.backBtn);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));

        listViewHelp = (ListView) findViewById(R.id.listViewHelp);
        helpListAdapter = new HelpListAdapter();
        listViewHelp.setAdapter(helpListAdapter);

        helpExpandedRl = (RelativeLayout) findViewById(R.id.helpExpandedRl);
        helpExpandedRl.setVisibility(View.GONE);


        progressBarHelp = (ProgressBar) findViewById(R.id.progressBarHelp);
        textViewInfoDisplay = (TextView) findViewById(R.id.textViewInfoDisplay);
        textViewInfoDisplay.setTypeface(Data.getFont(getApplicationContext()));
        helpWebview = (WebView) findViewById(R.id.helpWebview);


        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });


        textViewInfoDisplay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (selectedHelpSection != null) {
                    getHelpAsync(HelpActivity.this, selectedHelpSection);
                }
            }
        });


        helpSections.clear();
        helpSections.add(HelpSection.MAIL_US);
        helpSections.add(HelpSection.CALL_US);
        helpSections.add(HelpSection.FAQ);
        helpSections.add(HelpSection.ABOUT);
        helpSections.add(HelpSection.TERMS);
        helpSections.add(HelpSection.PRIVACY);


        helpListAdapter.notifyDataSetChanged();


    }


    public void openHelpData(HelpSection helpSection, String data, boolean errorOccured) {
        if (errorOccured) {
            textViewInfoDisplay.setVisibility(View.VISIBLE);
            textViewInfoDisplay.setText(data);
            helpWebview.setVisibility(View.GONE);
        } else {
            textViewInfoDisplay.setVisibility(View.GONE);
            helpWebview.setVisibility(View.VISIBLE);
            loadHTMLContent(data);
        }
        selectedHelpSection = helpSection;
        title.setText("" + helpSection.getName(getResources()));

    }

    public void loadHTMLContent(String data) {
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        helpWebview.loadDataWithBaseURL("", data, mimeType, encoding, "");
    }

    public void openMailIntentToSupport() {

        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + getString(R.string.app_name) + " Support" + "&body=" + "" + "&to=" + getString(R.string.customer_care_email));
        testIntent.setData(data);
        startActivity(testIntent);
    }

    public void openCallIntent(String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_VIEW);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(callIntent);
    }

    /**
     * ASync for get rides from server
     */
    public void getHelpAsync(final Activity activity, final HelpSection helpSection) {

        if (AppStatus.getInstance(activity).isOnline(activity)) {

            helpExpandedRl.setVisibility(View.VISIBLE);
            progressBarHelp.setVisibility(View.VISIBLE);
            textViewInfoDisplay.setVisibility(View.GONE);
            helpWebview.setVisibility(View.GONE);
            loadHTMLContent("");

            Log.e("helpSection", "=" + helpSection);

            RestClient.getApiService().getHelpAsync("" + helpSection.getOrdinal(), new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.i("Server response faq ", "response = " + response);
                    try {
                        jObj = new JSONObject(response);
                        if (!jObj.isNull("error")) {
                            String errorMessage = jObj.getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                openHelpData(helpSection, "Some error occured. Tap to retry.", true);
                            }
                        } else {
                            String data = jObj.getString("data");
                            openHelpData(helpSection, data, false);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        openHelpData(helpSection, "Some error occured. Tap to retry.", true);
                    }
                    progressBarHelp.setVisibility(View.GONE);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    progressBarHelp.setVisibility(View.GONE);
                    openHelpData(helpSection, "Some error occured. Tap to retry.", true);
                }

            });

        } else {
            openHelpData(helpSection, "No internet connection. Tap to retry.", true);
        }
    }

    public void performBackPressed() {

        if (helpExpandedRl.getVisibility() == View.VISIBLE) {
            helpExpandedRl.setVisibility(View.GONE);
            title.setText(getString(R.string.help));
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }

    @Override
    public void onBackPressed() {
        performBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

    class ViewHolderHelp {
        TextView name;
        LinearLayout relative;
        int id;
    }

    class HelpListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderHelp holder;

        public HelpListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return helpSections.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderHelp();
                convertView = mInflater.inflate(R.layout.help_list_item, null);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.name.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);

                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderHelp) convertView.getTag();
            }

            holder.id = position;

            holder.name.setText(helpSections.get(position).getName(getResources()));

            holder.relative.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder = (ViewHolderHelp) v.getTag();

                    switch (helpSections.get(holder.id)) {
                        case MAIL_US:
                            openMailIntentToSupport();
                            FlurryEventLogger.mailToSupportPressed(Data.userData.accessToken);
                            break;

                        case CALL_US:
                            openCallIntent(getString(R.string.customer_care_no));
                            FlurryEventLogger.callToSupportPressed(Data.userData.accessToken);
                            break;

                        default:
                            if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                                getHelpAsync(HelpActivity.this, helpSections.get(holder.id));
                                FlurryEventLogger.particularHelpOpened(helpSections.get(holder.id).getName(getResources()), Data.userData.accessToken);
                            } else {
                                new DialogPopup().alertPopup(HelpActivity.this, "",
                                        getString(R.string.check_internet_message));
                            }

                    }
                }
            });


            return convertView;
        }

    }

}

