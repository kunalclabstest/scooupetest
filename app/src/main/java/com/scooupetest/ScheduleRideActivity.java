package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scooupetest.customlayouts.CustomDateTimePicker;
import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.datastructure.FutureSchedule;
import com.scooupetest.datastructure.HelpSection;
import com.scooupetest.datastructure.ScheduleOperationMode;
import com.scooupetest.datastructure.ScheduleScreenMode;
import com.scooupetest.datastructure.SearchResult;
import com.scooupetest.locationfiles.AutoCompleteSearchResult;
import com.scooupetest.locationfiles.MapStateListener;
import com.scooupetest.locationfiles.MapUtils;
import com.scooupetest.locationfiles.TouchableMapFragment;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.DateOperations;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Utils;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class ScheduleRideActivity extends BaseActivity {


    public static Calendar selectedScheduleCalendar = Calendar.getInstance();
    public static LatLng selectedScheduleLatLng;
    public static LatLng selectedDestinationLatLng;
    public static String destinationAddress = "", pickupAddress = "";
    public static FutureSchedule editableFutureSchedule;
    public static ScheduleOperationMode scheduleOperationMode = ScheduleOperationMode.INSERT;
    public Thread autoCompleteThread;
    public boolean refreshingAutoComplete = false;
    RelativeLayout relative;
    Button backBtn, cancelBtn;
    GoogleMap map;
    TouchableMapFragment mapFragment;
    RelativeLayout scheduleOptionsMainRl,relClearDestination;
    TextView scheduleRideText, tvPickUpDestinationLocation, tvSearchedText;
    LinearLayout schedulePickupLocationLinear, scheduleDestinationLocationLinear;
    TextView schedulePickupLocationText, schedulePickupLocationValue, scheduleDestinationLocationText, scheduleDestinationLocationValue;
    LinearLayout scheduleDateTimeLinear;
    TextView scheduleDateTimeText, scheduleDateTimeValue;
    Button scheduleBtn;
    RelativeLayout scheduleSetPickupLocationRl;
    Button scheduleMyLocationBtn, pickThisLocationBtn;
    ImageView imgPickupDestinationRing;
    LinearLayout searchListRl;
    ListView searchListView;
    CustomDateTimePicker customDateTimePicker;
    Boolean selectDestination = false;
    //SearchLayout
    RelativeLayout searchBarRl;
    RelativeLayout relClearSearchLocation;
    LinearLayout linearSearchParent;
    EditText etLocation;
    TextView tvLocation;
    ProgressBar searchLocationProgress, progressBarSearch;
    ImageView imgLocationIcon;
    ListView listViewSearch;
    SearchListAdapter searchListAdapter;
    ArrayList<AutoCompleteSearchResult> autoCompleteSearchResults = new ArrayList<AutoCompleteSearchResult>();
    boolean isSearchStartLocation = true, canChangeLocationText = true;
    Location myLocation;
    boolean zoomedToMyLocation = false;
    ScheduleScreenMode scheduleScreenMode;
    OnMyLocationChangeListener onMyLocationChangeListener = new OnMyLocationChangeListener() {

        @Override
        public void onMyLocationChange(Location arg0) {
            if (!zoomedToMyLocation && selectedScheduleLatLng != null) {
                map.animateCamera(CameraUpdateFactory.newLatLng(selectedScheduleLatLng));
            }
            myLocation = arg0;
            zoomedToMyLocation = true;
        }
    };
    OnClickListener mapMyLocationClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (myLocation != null) {

                scheduleMyLocationBtn.setVisibility(View.GONE);

                if (map.getCameraPosition().zoom < 12) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 12));
                } else if (map.getCameraPosition().zoom < 17) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 17));
                } else {
                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
                }
            } else {
                Toast.makeText(getApplicationContext(), "Waiting for your location...", Toast.LENGTH_LONG).show();
            }
        }
    };
    Thread schedulePickupLocationAddressFetcherThread;

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_ride);

        zoomedToMyLocation = false;

        relative = (RelativeLayout) findViewById(R.id.relative);
        new ASSL(ScheduleRideActivity.this, relative, 1134, 720, false);


        backBtn = (Button) findViewById(R.id.backBtn);
        cancelBtn = (Button) findViewById(R.id.cancelBtn);
        cancelBtn.setTypeface(Data.getFont(getApplicationContext()));

        scheduleOptionsMainRl = (RelativeLayout) findViewById(R.id.scheduleOptionsMainRl);
        scheduleRideText = (TextView) findViewById(R.id.scheduleRideText);
        scheduleRideText.setTypeface(Data.getFont(getApplicationContext()));
        tvPickUpDestinationLocation = (TextView) findViewById(R.id.tvPickUpDestinationLocation);
        tvPickUpDestinationLocation.setTypeface(Data.getFont(getApplicationContext()));
        imgPickupDestinationRing = (ImageView) findViewById(R.id.imgRing);
        schedulePickupLocationLinear = (LinearLayout) findViewById(R.id.linearPickupLocationParent);
        scheduleDestinationLocationLinear = (LinearLayout) findViewById(R.id.linearDestinationLocationParent);
        schedulePickupLocationText = (TextView) findViewById(R.id.schedulePickupLocationText);
        schedulePickupLocationText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        schedulePickupLocationValue = (TextView) findViewById(R.id.schedulePickupLocationValue);
        schedulePickupLocationValue.setTypeface(Data.getFont(getApplicationContext()));
        scheduleDestinationLocationText = (TextView) findViewById(R.id.scheduleDestinationLocationText);
        scheduleDestinationLocationText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        scheduleDestinationLocationValue = (TextView) findViewById(R.id.scheduleDestinationLocationValue);
        scheduleDestinationLocationValue.setTypeface(Data.getFont(getApplicationContext()));
        scheduleDateTimeLinear = (LinearLayout) findViewById(R.id.linearDateTimeParent);
        scheduleDateTimeText = (TextView) findViewById(R.id.scheduleDateTimeText);
        scheduleDateTimeText.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        scheduleDateTimeValue = (TextView) findViewById(R.id.scheduleDateTimeValue);
        scheduleDateTimeValue.setTypeface(Data.getFont(getApplicationContext()));
        scheduleBtn = (Button) findViewById(R.id.scheduleBtn);
        scheduleBtn.setTypeface(Data.getFont(getApplicationContext()));

         relClearDestination=(RelativeLayout) findViewById(R.id.relClearDestination);
        relClearDestination.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleDestinationLocationValue.setText("");
                selectedDestinationLatLng=null;
                relClearDestination.setVisibility(View.GONE);
            }
        });

        Button scheduleCancelBtn = (Button) findViewById(R.id.scheduleCancelBtn);
        scheduleCancelBtn.setTypeface(Data.getFont(getApplicationContext()));
        scheduleCancelBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });


        scheduleSetPickupLocationRl = (RelativeLayout) findViewById(R.id.scheduleSetPickupLocationRl);
        scheduleMyLocationBtn = (Button) findViewById(R.id.scheduleMyLocationBtn);
        pickThisLocationBtn = (Button) findViewById(R.id.pickThisLocationBtn);
        pickThisLocationBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        searchListRl = (LinearLayout) findViewById(R.id.searchListRl);
        searchListView = (ListView) findViewById(R.id.searchListView);
        tvSearchedText = (TextView) findViewById(R.id.tvSearchedText);
        tvSearchedText.setTypeface(Data.getFont(getApplicationContext()));


        //search Layout
        searchBarRl = (RelativeLayout) findViewById(R.id.searchBarRl);
        linearSearchParent = (LinearLayout) findViewById(R.id.linearLayoutSearch);
        relClearSearchLocation = (RelativeLayout) findViewById(R.id.relClearSearchLocation);
        etLocation = (EditText) findViewById(R.id.etLocation);
        etLocation.setTypeface(Data.getFont(getApplicationContext()));
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvLocation.setTypeface(Data.getFont(getApplicationContext()));
        searchLocationProgress = (ProgressBar) findViewById(R.id.progressBarSearch);
        searchLocationProgress.setVisibility(View.GONE);
        progressBarSearch = (ProgressBar) findViewById(R.id.searchProgress);
        searchLocationProgress.setVisibility(View.GONE);
        imgLocationIcon = (ImageView) findViewById(R.id.imgLocationIcon);
        imgLocationIcon.setVisibility(View.VISIBLE);
        listViewSearch = (ListView) findViewById(R.id.listViewSearch);
        searchListAdapter = new SearchListAdapter();
        listViewSearch.setAdapter(searchListAdapter);

        etLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                Log.i("search text", "==" + s.toString().trim());
                if (map != null) {
                    if (s.length() > 0) {
                        getSearchResults(s.toString().trim(), map.getCameraPosition().target);
                        relClearSearchLocation.setVisibility(View.VISIBLE);
                    } else {
                        relClearSearchLocation.setVisibility(View.GONE);
                    }
                }
            }
        });

        etLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                Utils.showSoftKeyboard(ScheduleRideActivity.this, etLocation);
            }
        });

        relClearSearchLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.setText("");

            }
        });

        searchBarRl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                if (selectDestination) {
                    etLocation.setHint(getResources().getString(R.string.search_destination_location));
                    tvLocation.setText(getResources().getString(R.string.destination_location));
                } else {
                    etLocation.setHint(getResources().getString(R.string.search_pickup_location));
                    tvLocation.setText(getResources().getString(R.string.pickup_location));
                }
                etLocation.setText(tvSearchedText.getText().toString());
                etLocation.setSelection(etLocation.getText().length());
                Utils.showSoftKeyboard(ScheduleRideActivity.this, etLocation);
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                isSearchStartLocation = true;
                scheduleScreenMode = ScheduleScreenMode.SEARCH;
                switchScheduleScreen(scheduleScreenMode);
            }
        });


        if (ScheduleOperationMode.MODIFY == scheduleOperationMode) {
            scheduleRideText.setText("Modify Schedule");
            scheduleBtn.setText(getString(R.string.reschedule_ride));
        } else {
            scheduleRideText.setText("Schedule Ride");
            scheduleBtn.setText(getString(R.string.schedule_ride));
        }


        LatLng latLng;
        if (selectedScheduleLatLng == null) {
            if (HomeActivity.myLocation != null) {
                latLng = new LatLng(HomeActivity.myLocation.getLatitude(), HomeActivity.myLocation.getLongitude());
            } else if (Data.latitude != 0 && Data.longitude != 0) {
                latLng = new LatLng(Data.latitude, Data.longitude);
            } else {
                latLng = new LatLng(30.7500, 76.7800);
            }
            selectedScheduleLatLng = latLng;

        } else {
            latLng = selectedScheduleLatLng;
        }

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        mapFragment = ((TouchableMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        if (map != null) {
            map.getUiSettings().setZoomGesturesEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setMyLocationEnabled(true);
            map.getUiSettings().setTiltGesturesEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));

            map.setOnMyLocationChangeListener(onMyLocationChangeListener);
            scheduleMyLocationBtn.setOnClickListener(mapMyLocationClick);
        }


        new MapStateListener(map, mapFragment, this) {
            @Override
            public void onMapTouched() {
                // Map touched

            }

            @Override
            public void onMapReleased() {
                // Map released

                if (myLocation != null) {
                    double difference  = MapUtils.distance(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                            new LatLng(map.getCameraPosition().target.latitude , map.getCameraPosition().target.longitude));
                    Log.e("difference", ""+difference);
                    if (difference > 1.2 && !(myLocation.getLongitude() == map.getCameraPosition().target.longitude &&
                            myLocation.getLatitude() == map.getCameraPosition().target.latitude)){
                        scheduleMyLocationBtn.setVisibility(View.VISIBLE);
                    }else {
                        scheduleMyLocationBtn.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Waiting for your location...", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onMapUnsettled() {
                // Map unsettled

            }

            @Override
            public void onMapSettled() {
                // Map settled
                if (canChangeLocationText) {
                    progressBarSearch.setVisibility(View.VISIBLE);
                    tvSearchedText.setVisibility(View.GONE);
                    setSearchBarText();
                }
                canChangeLocationText = true;


            }
        };


        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });


        // schedule layout
        scheduleOptionsMainRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        schedulePickupLocationLinear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectDestination = false;
                tvPickUpDestinationLocation.setText("PICK UP LOCATION");
                imgPickupDestinationRing.setBackgroundResource(R.drawable.pickup_color_ring);
                if (selectedScheduleLatLng != null) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(selectedScheduleLatLng, 14));
                }
                scheduleScreenMode = ScheduleScreenMode.PICK_LOCATION;
                switchScheduleScreen(scheduleScreenMode);
            }
        });

        scheduleDestinationLocationLinear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectDestination = true;
                if (selectedDestinationLatLng != null) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(selectedDestinationLatLng, 14));
                }
                tvPickUpDestinationLocation.setText("DESTINATION LOCATION");
                imgPickupDestinationRing.setBackgroundResource(R.drawable.destination_color_ring);
                scheduleScreenMode = ScheduleScreenMode.PICK_LOCATION;
                switchScheduleScreen(scheduleScreenMode);
            }
        });

        scheduleDateTimeLinear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                customDateTimePicker.set24HourFormat(false);
                customDateTimePicker.setTimePickerIntervalInMinutes(5);
                customDateTimePicker.setDate(selectedScheduleCalendar);
                customDateTimePicker.showDialog();
            }
        });

        scheduleBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (selectedScheduleLatLng != null) {
                    if (ScheduleOperationMode.MODIFY == scheduleOperationMode) {
                        if (editableFutureSchedule != null) {
                            modifyScheduleRideAsync(ScheduleRideActivity.this, editableFutureSchedule.pickupId, selectedScheduleCalendar, selectedScheduleLatLng, selectedDestinationLatLng);
                        }
                    } else {
                        insertScheduleRideAsync(ScheduleRideActivity.this, selectedScheduleCalendar, selectedScheduleLatLng, selectedDestinationLatLng);
                    }
                } else {
                    Toast.makeText(ScheduleRideActivity.this, "Please wait while we get your pickup address.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        pickThisLocationBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (map != null) {
                    getSchedulePickupLocationAddress(map.getCameraPosition().target, tvSearchedText.getText().toString());
                    scheduleScreenMode = ScheduleScreenMode.INITIAL;
                    switchScheduleScreen(scheduleScreenMode);
                }
            }
        });

        customDateTimePicker = new CustomDateTimePicker(this,
                new CustomDateTimePicker.ICustomDateTimeListener() {

                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {

                        Calendar currentCalendar = Calendar.getInstance();

                        if (Data.userData != null) {
                            currentCalendar.add(Calendar.MINUTE, Data.userData.schedulingLimitMinutes);
                        }

                        Date currentOffsetDate = currentCalendar.getTime();
                        Date selectedDate = calendarSelected.getTime();
                        long diff = selectedDate.getTime() - currentOffsetDate.getTime();

                        if (diff > 0) {
                            selectedScheduleCalendar = calendarSelected;
                            setScheduleDateTimeValue(selectedScheduleCalendar);
                        } else {
                            Toast.makeText(ScheduleRideActivity.this, "Schedule time must be after " + Data.userData.schedulingLimitMinutes + " minutes than current time", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });


        scheduleScreenMode = ScheduleScreenMode.INITIAL;
        switchScheduleScreen(scheduleScreenMode);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (selectedScheduleCalendar == null) {
            selectedScheduleCalendar = Calendar.getInstance();
        }

        if (selectedScheduleLatLng != null && !pickupAddress.equalsIgnoreCase("")) {
            schedulePickupLocationValue.setText(pickupAddress);
//            getSchedulePickupLocationAddress(selectedScheduleLatLng, searchBarEditText.getText().toString());
        }
        if (selectedDestinationLatLng != null && !destinationAddress.equalsIgnoreCase("")) {
            scheduleDestinationLocationValue.setText(destinationAddress);
            relClearDestination.setVisibility(View.VISIBLE);
        }

        setScheduleDateTimeValue(selectedScheduleCalendar);

    }


//    class ViewHolderScheduleSearch {
//        TextView name;
//        LinearLayout relative;
//        int id;
//    }
//
//    class ScheduleSearchListAdapter extends BaseAdapter {
//        LayoutInflater mInflater;
//        ViewHolderScheduleSearch holder;
//
//        public ScheduleSearchListAdapter() {
//            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        }
//
//        @Override
//        public int getCount() {
//            return searchResults.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return position;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//
//
//            if (convertView == null) {
//
//                holder = new ViewHolderScheduleSearch();
//                convertView = mInflater.inflate(R.layout.list_item_search, null);
//
//                holder.name = (TextView) convertView.findViewById(R.id.name);
//                holder.name.setTypeface(Data.getFont(getApplicationContext()));
//                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
//
//                holder.relative.setTag(holder);
//
//                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
//                ASSL.DoMagic(holder.relative);
//
//                convertView.setTag(holder);
//            } else {
//                holder = (ViewHolderScheduleSearch) convertView.getTag();
//            }
//
//
//            holder.id = position;
//
//            holder.name.setText("" + searchResults.get(position).name + "\n" + searchResults.get(position).address);
//
//            holder.relative.setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    holder = (ViewHolderScheduleSearch) v.getTag();
//                    SearchResult searchResult = searchResults.get(holder.id);
//                    if (searchResult.latLng != null) {
//                        searchBarEditText.setText(searchResult.address);
//                        scheduleScreenMode = ScheduleScreenMode.PICK_LOCATION;
//                        switchScheduleScreen(scheduleScreenMode);
//                        map.animateCamera(CameraUpdateFactory.newLatLng(searchResult.latLng), 1000, null);
//                    }
//                }
//            });
//            return convertView;
//        }
//
//    }

    public void switchScheduleScreen(ScheduleScreenMode mode) {

        switch (mode) {

            case INITIAL:
                scheduleOptionsMainRl.setVisibility(View.VISIBLE);
                scheduleSetPickupLocationRl.setVisibility(View.GONE);

                backBtn.setVisibility(View.VISIBLE);
                cancelBtn.setVisibility(View.GONE);
                break;

            case PICK_LOCATION:
                scheduleOptionsMainRl.setVisibility(View.GONE);
                scheduleSetPickupLocationRl.setVisibility(View.VISIBLE);
                linearSearchParent.setVisibility(View.GONE);

                backBtn.setVisibility(View.GONE);
                cancelBtn.setVisibility(View.VISIBLE);
                break;

            case SEARCH:
                scheduleOptionsMainRl.setVisibility(View.GONE);
                scheduleSetPickupLocationRl.setVisibility(View.VISIBLE);
                linearSearchParent.setVisibility(View.VISIBLE);

                backBtn.setVisibility(View.GONE);
                cancelBtn.setVisibility(View.VISIBLE);

                break;

        }
    }

    public void performBackPressed() {
        Utils.hideSoftKeyboard(ScheduleRideActivity.this, etLocation);
        if (ScheduleScreenMode.SEARCH == scheduleScreenMode) {
            scheduleScreenMode = ScheduleScreenMode.PICK_LOCATION;
            switchScheduleScreen(scheduleScreenMode);
        } else if (ScheduleScreenMode.PICK_LOCATION == scheduleScreenMode) {
            scheduleScreenMode = ScheduleScreenMode.INITIAL;
            switchScheduleScreen(scheduleScreenMode);
        } else {
            backFinish();
        }
    }

    public void backFinish() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    public void setScheduleDateTimeValue(final Calendar calendar) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String dayLongName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                String monthShortName = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
                String amOrPm = calendar.getDisplayName(Calendar.AM_PM, Calendar.LONG, Locale.getDefault());
                String hourLongName = (calendar.get(Calendar.HOUR) < 10) ? "0" + calendar.get(Calendar.HOUR) : "" + calendar.get(Calendar.HOUR);
                if (calendar.get(Calendar.HOUR) == 0) {
                    hourLongName = "12";
                }
                String minuteLongName = (calendar.get(Calendar.MINUTE) < 10) ? "0" + calendar.get(Calendar.MINUTE) : "" + calendar.get(Calendar.MINUTE);
                scheduleDateTimeValue.setText(dayLongName + ", " + calendar.get(Calendar.DATE) + " " + monthShortName + " " + calendar.get(Calendar.YEAR)
                        + ", " + hourLongName + ":" + minuteLongName + " " + amOrPm);
            }
        });
    }

    public void setSchedulePickupLocationAddress(final String address) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                schedulePickupLocationValue.setText(address);

            }
        });
    }

    public void setScheduleDestinationLocationAddress(final String address) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scheduleDestinationLocationValue.setText(address);
                relClearDestination.setVisibility(View.VISIBLE);
            }
        });
    }


//    public void setSearchResultsToList() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                searchBtn.setVisibility(View.VISIBLE);
//                progressBarSearch.setVisibility(View.GONE);
//
//                if (searchResults.size() == 0) {
//                    searchResults.add(new SearchResult("No results found", "", null));
//                }
//
//                scheduleSearchListAdapter.notifyDataSetChanged();
//            }
//        });
//    }
//
//    public void getSearchResults(final String searchText) {
//        searchBtn.setVisibility(View.GONE);
//        progressBarSearch.setVisibility(View.VISIBLE);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                searchResults.clear();
//                searchResults.addAll(MapUtils.getSearchResultsFromGooglePlaces(searchText));
//                setSearchResultsToList();
//            }
//        }).start();
//    }

    public void getSchedulePickupLocationAddress(final LatLng schedulePickupLatLng, final String address) {

        stopSchedulePickupLocationAddressFetcherThread();
        if (selectDestination) {
            selectedDestinationLatLng = null;
            setScheduleDestinationLocationAddress("Loading address...");
        } else {
            selectedScheduleLatLng = null;
            setSchedulePickupLocationAddress("Loading address...");
        }

        try {
            schedulePickupLocationAddressFetcherThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (selectDestination) {
                        selectDestination = false;
//						setScheduleDestinationLocationAddress(MapUtils.getGAPIAddress(schedulePickupLatLng));
                        setScheduleDestinationLocationAddress(address);
                        selectedDestinationLatLng = schedulePickupLatLng;

                    } else {
                        if (address.trim().length() == 0) {
                            setSchedulePickupLocationAddress(MapUtils.getGAPIAddress(schedulePickupLatLng));
                        } else {
                            setSchedulePickupLocationAddress(address);
                        }
//						setSchedulePickupLocationAddress(MapUtils.getGAPIAddress(schedulePickupLatLng));

                        selectedScheduleLatLng = schedulePickupLatLng;
                    }

                }
            });
            schedulePickupLocationAddressFetcherThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void stopSchedulePickupLocationAddressFetcherThread() {
        try {
            if (schedulePickupLocationAddressFetcherThread != null) {
                schedulePickupLocationAddressFetcherThread.interrupt();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        schedulePickupLocationAddressFetcherThread = null;
    }

    /**
     * ASync for inserting schedule ride event to server
     */
    public void insertScheduleRideAsync(final Activity activity, Calendar selectedCalendar, LatLng selectedLatLng, LatLng selectedDestinationLatLng) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            Double manualLat = 0.0;
            Double manualLong = 0.0;
            String manualAddress = "";
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));


            Log.i("manual_destination_address", "=" + scheduleDestinationLocationValue.getText().toString());
            Log.e("Server hit=", "=" + Data.SERVER_URL + "/insert_pickup_schedule");
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("latitude", "=" + selectedLatLng.latitude);
            Log.i("longitude", "=" + selectedLatLng.longitude);
            Log.i("pickup_time", "=" + DateOperations.getTimeStampfromCalendar(selectedCalendar));


            if (selectedDestinationLatLng != null) {
                manualLat = selectedDestinationLatLng.latitude;
                manualLong = selectedDestinationLatLng.longitude;
                manualAddress = scheduleDestinationLocationValue.getText().toString();

                Log.i("manual_destination_latitude", "==" + selectedDestinationLatLng.latitude);
                Log.i("manual_destination_longitude", "==" + selectedDestinationLatLng.longitude);

            }
            RestClient.getApiService().insertScheduleRideAsync(Data.userData.accessToken, "" + selectedLatLng.latitude, "" + selectedLatLng.longitude, "" + DateOperations.getTimeStampfromCalendar(selectedCalendar),
                    Data.defaultCarType, manualLat, manualLong, manualAddress, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else {
                                        scheduleOptionsMainRl.setVisibility(View.GONE);
                                        new DialogPopup().alertPopupWithListener(activity, "", errorMessage, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                scheduleScreenMode = ScheduleScreenMode.INITIAL;
                                                switchScheduleScreen(scheduleScreenMode);
                                            }
                                        });
                                    }
                                } else {
                                    int flag = jObj.getInt("flag");
                                    if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                        scheduleOptionsMainRl.setVisibility(View.GONE);
                                        new DialogPopup().alertPopupWithListener(activity, "", jObj.getString("message"), new View.OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                activity.finish();
                                                activity.overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                            }
                                        });
                                    }

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                scheduleOptionsMainRl.setVisibility(View.GONE);
                                new DialogPopup().alertPopupWithListener(activity, "", Data.SERVER_ERROR_MSG, new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        scheduleScreenMode = ScheduleScreenMode.INITIAL;
                                        switchScheduleScreen(scheduleScreenMode);
                                    }
                                });

                            }

                            DialogPopup.dismissLoadingDialog();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            scheduleOptionsMainRl.setVisibility(View.GONE);
                            new DialogPopup().alertPopupWithListener(activity, "", Data.SERVER_NOT_RESOPNDING_MSG, new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    scheduleScreenMode = ScheduleScreenMode.INITIAL;
                                    switchScheduleScreen(scheduleScreenMode);
                                }
                            });

                        }
                    });
        } else {
            scheduleOptionsMainRl.setVisibility(View.GONE);
            new DialogPopup().alertPopupWithListener(activity, "", getString(R.string.check_internet_message), new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    scheduleScreenMode = ScheduleScreenMode.INITIAL;
                    switchScheduleScreen(scheduleScreenMode);
                }
            });
        }
    }

    /**
     * ASync for modifying schedule ride event to server
     */
    public void modifyScheduleRideAsync(final Activity activity, String pickupId, Calendar selectedCalendar, LatLng selectedLatLng, LatLng selectedDestinationLatLng) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            Double manualLat = 0.0;
            Double manualLong = 0.0;
            String manualAddress = "";

            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("manual_destination_address", "=" + scheduleDestinationLocationValue.getText().toString());
            Log.e("Server hit=", "=" + Data.SERVER_URL + "/modify_pickup_schedule");
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("pickup_id", "=" + pickupId);
            Log.i("latitude", "=" + selectedLatLng.latitude);
            Log.i("longitude", "=" + selectedLatLng.longitude);
            Log.i("pickup_time", "=" + DateOperations.getTimeStampfromCalendar(selectedCalendar));

            if (selectedDestinationLatLng != null) {
                manualLat = selectedDestinationLatLng.latitude;
                manualLong = selectedDestinationLatLng.longitude;
                manualAddress = scheduleDestinationLocationValue.getText().toString();
                Log.i("manual_destination_latitude", "==" + selectedDestinationLatLng.latitude);
                Log.i("manual_destination_longitude", "==" + selectedDestinationLatLng.longitude);
            }

            RestClient.getApiService().modifyScheduleRideAsync(Data.userData.accessToken, pickupId, selectedLatLng.latitude, selectedLatLng.longitude, "" + DateOperations.getTimeStampfromCalendar(selectedCalendar),
                    manualLat, manualLong, manualAddress, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);

                                if (!jObj.isNull("error")) {

                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (ApiResponseFlags.SHOW_ERROR_MESSAGE.getOrdinal() == flag) {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else {
                                        scheduleOptionsMainRl.setVisibility(View.GONE);
                                        new DialogPopup().alertPopupWithListener(activity, "", errorMessage, new View.OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                scheduleScreenMode = ScheduleScreenMode.INITIAL;
                                                switchScheduleScreen(scheduleScreenMode);
                                            }
                                        });
                                    }
                                } else {
                                    int flag = jObj.getInt("flag");
                                    if (ApiResponseFlags.SHOW_MESSAGE.getOrdinal() == flag) {
                                        scheduleOptionsMainRl.setVisibility(View.GONE);
                                        new DialogPopup().alertPopupWithListener(activity, "", jObj.getString("message"), new View.OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                activity.finish();
                                                activity.overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                            }
                                        });
                                    }

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                scheduleOptionsMainRl.setVisibility(View.GONE);
                                new DialogPopup().alertPopupWithListener(activity, "", Data.SERVER_ERROR_MSG, new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        scheduleScreenMode = ScheduleScreenMode.INITIAL;
                                        switchScheduleScreen(scheduleScreenMode);
                                    }
                                });

                            }

                            DialogPopup.dismissLoadingDialog();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            scheduleOptionsMainRl.setVisibility(View.GONE);
                            new DialogPopup().alertPopupWithListener(activity, "", Data.SERVER_NOT_RESOPNDING_MSG, new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    scheduleScreenMode = ScheduleScreenMode.INITIAL;
                                    switchScheduleScreen(scheduleScreenMode);
                                }
                            });
                        }
                    });
        } else {
            scheduleOptionsMainRl.setVisibility(View.GONE);
            new DialogPopup().alertPopupWithListener(activity, "", getString(R.string.check_internet_message), new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    scheduleScreenMode = ScheduleScreenMode.INITIAL;
                    switchScheduleScreen(scheduleScreenMode);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        performBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

    public void setSearchBarText() {

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(ScheduleRideActivity.this, Locale.getDefault());
            addresses = geocoder.getFromLocation(map.getCameraPosition().target.latitude, map.getCameraPosition().target.longitude, 1);

            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getAddressLine(1);
            String country = addresses.get(0).getAddressLine(2);
            progressBarSearch.setVisibility(View.GONE);
            tvSearchedText.setText(address + ", " + city);

            tvSearchedText.setVisibility(View.VISIBLE);
            tvSearchedText.clearFocus();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public synchronized void getSearchResults(final String searchText, final LatLng latLng) {
        try {
            if (!refreshingAutoComplete) {

                searchLocationProgress.setVisibility(View.VISIBLE);
                imgLocationIcon.setVisibility(View.GONE);
                if (autoCompleteThread != null) {
                    autoCompleteThread.interrupt();
                }

                autoCompleteThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        refreshingAutoComplete = true;
                        autoCompleteSearchResults.clear();
                        autoCompleteSearchResults.addAll(MapUtils.getAutoCompleteSearchResultsFromGooglePlaces(searchText, latLng,ScheduleRideActivity.this));
                        Log.i("setSearchResultsToList", "==");
                        refreshingAutoComplete = false;
                        recallLatestTextSearch(searchText, latLng);
                        setSearchResultsToList();
                        autoCompleteThread = null;


                    }
                });
                autoCompleteThread.start();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized void recallLatestTextSearch(final String searchText, final LatLng latLng) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String currentText = etLocation.getText().toString().trim();
                if (searchText.equalsIgnoreCase(currentText)) {

                } else {

                    if (currentText.length() > 0) {
                        if (map != null) {
                            autoCompleteSearchResults.clear();
                            searchListAdapter.notifyDataSetChanged();
                            Log.i("currentText", "==" + currentText);
                            getSearchResults(currentText, latLng);
                        }
                    }
                }
            }
        });
    }


    public synchronized void setSearchResultsToList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try {
                    if (etLocation.getText().toString().equalsIgnoreCase("")) {
                        autoCompleteSearchResults.clear();
                    }
//                    if(autoCompleteSearchResults.size() == 0){
//                        autoCompleteSearchResults.add(new AutoCompleteSearchResult("No results found", "", ""));
//                    }
                    searchListAdapter.notifyDataSetChanged();
                    searchLocationProgress.setVisibility(View.GONE);
                    imgLocationIcon.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public synchronized void getSearchResultFromPlaceId(final String placeId) {
//        pickUpLocationProgress.setVisibility(View.VISIBLE);
////        pickUpLocationText.setVisibility(View.GONE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                SearchResult searchResult = MapUtils.getSearchResultsFromPlaceIdGooglePlaces(placeId,ScheduleRideActivity.this);
                setSearchResultToMapAndText(searchResult);
            }
        }).start();
    }

    public synchronized void setSearchResultToMapAndText(final SearchResult searchResult) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                pickUpLocationProgress.setVisibility(View.GONE);
//                pickUpLocationText.setVisibility(View.VISIBLE);
                if (map != null && searchResult != null) {
                    canChangeLocationText = false;
                    tvSearchedText.setText(searchResult.name);
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(searchResult.latLng, 14), 1000, null);
                }
            }
        });
    }

    class ViewHolderSearchItem {
        TextView textViewSearchName, textViewSearchAddress;
        LinearLayout relative;
        int id;
    }

    class SearchListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderSearchItem holder;

        public SearchListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return autoCompleteSearchResults.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderSearchItem();
                convertView = mInflater.inflate(R.layout.list_item_search_item, null);

                holder.textViewSearchName = (TextView) convertView.findViewById(R.id.textViewSearchName);
                holder.textViewSearchName.setTypeface(Data.getFont(ScheduleRideActivity.this));
                holder.textViewSearchAddress = (TextView) convertView.findViewById(R.id.textViewSearchAddress);
                holder.textViewSearchAddress.setTypeface(Data.getFont(ScheduleRideActivity.this));
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);

                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderSearchItem) convertView.getTag();
            }


            holder.id = position;

            try {
                holder.textViewSearchName.setText(autoCompleteSearchResults.get(position).name);
                holder.textViewSearchAddress.setText(autoCompleteSearchResults.get(position).address);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.relative.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder = (ViewHolderSearchItem) v.getTag();
                    Utils.hideSoftKeyboard(ScheduleRideActivity.this, etLocation);
                    AutoCompleteSearchResult autoCompleteSearchResult = autoCompleteSearchResults.get(holder.id);
                    if (!"".equalsIgnoreCase(autoCompleteSearchResult.placeId)) {

                        canChangeLocationText = false;
                        tvSearchedText.setText(autoCompleteSearchResult.name);
                        getSearchResultFromPlaceId(autoCompleteSearchResult.placeId);
                        scheduleScreenMode = ScheduleScreenMode.PICK_LOCATION;
                        switchScheduleScreen(scheduleScreenMode);

                    }
                }
            });
            return convertView;
        }

        @Override
        public void notifyDataSetChanged() {
            try {

                if (autoCompleteSearchResults.size() > 1) {
                    if (autoCompleteSearchResults.contains(new AutoCompleteSearchResult("No results found", "", ""))) {
                        autoCompleteSearchResults.remove(autoCompleteSearchResults.indexOf(new AutoCompleteSearchResult("No results found", "", "")));
                    }
                }
                super.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


}
