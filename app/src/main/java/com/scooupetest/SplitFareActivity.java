package com.scooupetest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.SplitFareData;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;
import com.flurry.android.FlurryAgent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class SplitFareActivity extends BaseActivity {

    static final int PICK_CONTACT = 1;
    public static Activity splitFareActivity;
    RelativeLayout relative;
    Button backBtn;
    ListView listViewSplitFareInfo;
    SplitFareListAdapter splitFareListAdapter;
    ArrayList<SplitFareData> splitFareInfoList = new ArrayList<SplitFareData>();
//    GetSplitFareDataFromServer getSplitFareDataFromServer;
    boolean cancelAsyncTask = false;
    private Button addCustomerBTN;
    private TextView tvNoCustomer;
    private ProgressBar progressBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.split_fare_activity);
        relative = (RelativeLayout) findViewById(R.id.relative);
        new ASSL(SplitFareActivity.this, relative, 1134, 720, false);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
        splitFareActivity = this;


        backBtn = (Button) findViewById(R.id.backBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        addCustomerBTN = (Button) findViewById(R.id.addNewBTN);
        addCustomerBTN.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        addCustomerBTN.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                    startActivityForResult(intent, PICK_CONTACT);
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
//				Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//				startActivityForResult(intent, PICK_CONTACT);

            }
        });
        listViewSplitFareInfo = (ListView) findViewById(R.id.listViewSplitFare);
        splitFareListAdapter = new SplitFareListAdapter();
        listViewSplitFareInfo.setAdapter(splitFareListAdapter);
        listViewSplitFareInfo.setVisibility(View.GONE);
        tvNoCustomer = (TextView) findViewById(R.id.noCustomerText);
        tvNoCustomer.setTypeface(Data.getFont(this));
        TextView title = (TextView) findViewById(R.id.header);
        title.setTypeface(Data.getFont(this));
        getSplitFareInfo();

    }

    // The Activity's onStart method
    @Override
    protected void onStart() {
        super.onStart();
        // Set up the Flurry session
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    // The Activity's onStop method
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        // System.gc();
    }

    public void sendSplitFareRequest(final String phoneNumber) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this,getString(R.string.loading));

            Log.i("phone_no", "==" + phoneNumber);
            Log.i("engagement_id", "==" + Data.cEngagementId);
            Log.i("access_token", Data.userData.accessToken);

            RestClient.getApiService().sendSplitFareRequest(phoneNumber, Data.cEngagementId, Data.userData.accessToken, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (0 == flag) { // {"error": 'some
                                // parameter
                                // missing',"flag":0}//error
                                new DialogPopup().alertPopup(
                                        SplitFareActivity.this, "",
                                        errorMessage);
                            } else if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(SplitFareActivity.this);
                            } else if (2 == flag) { // {"error":"incorrect password","flag":2}//error
                                new DialogPopup().alertPopup(
                                        SplitFareActivity.this, "",
                                        errorMessage);
                            } else {
                                new DialogPopup().alertPopup(
                                        SplitFareActivity.this, "",
                                        errorMessage);
                            }
                        } else {

                            splitFareInfoList.clear();
                            JSONArray splitFareInfoArray = jObj.getJSONArray("split_fare_data");

                            for (int i = 0; i < splitFareInfoArray.length(); i++) {
                                JSONObject splitFareObject = splitFareInfoArray.getJSONObject(i);
                                String phoneNumber = splitFareObject.getString("phone_no");
                                phoneNumber = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
                                splitFareInfoList.add(new SplitFareData(splitFareObject.getString("user_name"), phoneNumber, splitFareObject.getInt("status"), splitFareObject.getString("split_id")));
                            }
                            if (splitFareInfoList.size() > 0) {
                                listViewSplitFareInfo.setVisibility(View.VISIBLE);
                                tvNoCustomer.setVisibility(View.GONE);
                                splitFareListAdapter.notifyDataSetChanged();
                                getSplitFareData();

                            } else {
                                listViewSplitFareInfo.setVisibility(View.GONE);
                                tvNoCustomer.setVisibility(View.VISIBLE);
                            }
                            new DialogPopup().alertPopup(
                                    SplitFareActivity.this, "",
                                    jObj.getString("log"));

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(
                                SplitFareActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(SplitFareActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(SplitFareActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }


    public void reSendSplitFareRequest(final String splitId) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this, getString(R.string.loading));

            Log.i("split_id", "==" + splitId);
            Log.i("access_token", Data.userData.accessToken);

            RestClient.getApiService().reSendSplitFareRequest(splitId, Data.userData.accessToken, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");

                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(SplitFareActivity.this);
                            } else {
                                new DialogPopup().alertPopup(
                                        SplitFareActivity.this, "",
                                        errorMessage);
                            }
                        } else {


                            JSONArray splitFareInfoArray = jObj.getJSONArray("split_fare_data");
                            splitFareInfoList.clear();
                            for (int i = 0; i < splitFareInfoArray.length(); i++) {
                                JSONObject splitFareObject = splitFareInfoArray.getJSONObject(i);
                                String phoneNumber = splitFareObject.getString("phone_no");
                                phoneNumber = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
                                splitFareInfoList.add(new SplitFareData(splitFareObject.getString("user_name"), phoneNumber, splitFareObject.getInt("status"), splitFareObject.getString("split_id")));
                            }
                            if (splitFareInfoList.size() > 0) {
                                splitFareListAdapter.notifyDataSetChanged();
                                listViewSplitFareInfo.setVisibility(View.VISIBLE);
                                tvNoCustomer.setVisibility(View.GONE);


                            } else {
                                listViewSplitFareInfo.setVisibility(View.GONE);
                                tvNoCustomer.setVisibility(View.VISIBLE);
                            }

                            new DialogPopup().alertPopup(
                                    SplitFareActivity.this, "",
                                    jObj.getString("log"));

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(
                                SplitFareActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(SplitFareActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(SplitFareActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }

    public void getSplitFareInfo() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            //DialogPopup.showLoadingDialog(this, "Loading...");
            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", "=" + Data.cEngagementId);


            RestClient.getApiService().getSplitFareInfo(Data.userData.accessToken, Data.cEngagementId, new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.v("Server response", "response = " + response);

                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            int flag = jObj.getInt("flag");
                            String errorMessage = jObj
                                    .getString("error");
                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(SplitFareActivity.this);
                            } else {
                                new DialogPopup().alertPopup(
                                        SplitFareActivity.this, "",
                                        errorMessage);
                            }
                        } else {
                            splitFareInfoList.clear();
                            JSONArray splitFareInfoArray = jObj.getJSONArray("split_fare_data");

                            for (int i = 0; i < splitFareInfoArray.length(); i++) {

                                JSONObject splitFareObject = splitFareInfoArray.getJSONObject(i);
                                String phoneNumber = splitFareObject.getString("phone_no");
                                phoneNumber = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
                                splitFareInfoList.add(new SplitFareData(splitFareObject.getString("user_name"), phoneNumber, splitFareObject.getInt("status"), splitFareObject.getString("split_id")));
                            }
                            progressBar1.setVisibility(View.GONE);
                            if (splitFareInfoList.size() > 0) {
                                listViewSplitFareInfo.setVisibility(View.VISIBLE);
                                tvNoCustomer.setVisibility(View.GONE);
                                splitFareListAdapter.notifyDataSetChanged();
                                getSplitFareData();

                            } else {
                                listViewSplitFareInfo.setVisibility(View.GONE);
                                tvNoCustomer.setVisibility(View.VISIBLE);
                            }

                            Log.v("json response = ", jObj.toString()
                                    + "");

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        progressBar1.setVisibility(View.GONE);
                        new DialogPopup().alertPopup(
                                SplitFareActivity.this, "",
                                Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    progressBar1.setVisibility(View.GONE);
                    new DialogPopup().alertPopup(SplitFareActivity.this,
                            "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {
            new DialogPopup().alertPopup(SplitFareActivity.this, "",
                    getString(R.string.check_internet_message));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    if (data != null) {
                        Uri uri = data.getData();

                        if (uri != null) {
                            Cursor c = null;
                            try {
                                c = getContentResolver().query(uri, new String[]{
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                ContactsContract.CommonDataKinds.Phone.TYPE},
                                        null, null, null);

                                if (c != null && c.moveToFirst()) {
                                    String number = c.getString(0);
                                    int type = c.getInt(1);
                                    number = number.replace(" ", "");
                                    number = number.replace("(", "");
                                    number = number.replace("/", "");
                                    number = number.replace(")", "");
                                    number = number.replace("N", "");
                                    number = number.replace(",", "");
                                    number = number.replace("*", "");
                                    number = number.replace(";", "");
                                    number = number.replace("#", "");
                                    number = number.replace("-", "");
                                    number = number.replace(".", "");
                                    Log.i("Type=" + type, "Number after trim=" + number);
                                    if (number.length() > 10) {
                                        number = number.substring(number.length() - 10, number.length());
                                    }
                                    Boolean isRequsetAlreadySent = false;
                                    for (int i = 0; i < splitFareInfoList.size(); i++) {
                                        if (splitFareInfoList.get(i).phoneNo.equalsIgnoreCase(number)) {
                                            isRequsetAlreadySent = true;
                                            break;
                                        }
                                    }
                                    if (!isRequsetAlreadySent) {
                                        sendSplitFareRequest(number);
                                    } else {
                                        new DialogPopup().alertPopup(
                                                SplitFareActivity.this, "",
                                                getString(R.string.contact_already_added));
                                    }
                                    Log.i("isRequsetAlreadySent=", "=" + isRequsetAlreadySent);
                                    Log.i("Type=" + type, "Number=" + number);
                                }
                            } finally {
                                if (c != null) {
                                    c.close();
                                }
                            }
                        }
                    }


                }
                break;

        }
    }


    class ViewHolderHelp {
        TextView name, number, status;
        LinearLayout relative;
        int id;
        Button btnResend;
    }

    class SplitFareListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderHelp holder;

        public SplitFareListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return splitFareInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderHelp();
                convertView = mInflater.inflate(R.layout.split_fare_list_item, null);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.name.setTypeface(Data.getFont(getApplicationContext()));
                holder.number = (TextView) convertView.findViewById(R.id.number);
                holder.number.setTypeface(Data.getFont(getApplicationContext()));
                holder.status = (TextView) convertView.findViewById(R.id.status);
                holder.status.setTypeface(Data.getFont(getApplicationContext()));
                holder.btnResend = (Button) convertView.findViewById(R.id.btnResend);
                holder.btnResend.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);

                holder.relative.setLayoutParams(new ListView.LayoutParams(720, LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderHelp) convertView.getTag();
            }

            holder.id = position;

            holder.name.setText(splitFareInfoList.get(position).userName);

            holder.number.setText(splitFareInfoList.get(position).phoneNo);
            if (splitFareInfoList.get(position).status == 0) {
                holder.btnResend.setVisibility(View.GONE);
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setText("Request sent");
                holder.status.setTextColor(getResources().getColorStateList(R.color.black));
            } else if (splitFareInfoList.get(position).status == 2) {
                holder.btnResend.setVisibility(View.GONE);
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setText("Rejected");
                holder.status.setTextColor(getResources().getColorStateList(R.color.destination_ring_color));
            } else if (splitFareInfoList.get(position).status == 1) {
                holder.btnResend.setVisibility(View.GONE);
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setText("Accepted");
                holder.status.setTextColor(getResources().getColorStateList(R.color.green_btn));
            } else if (splitFareInfoList.get(position).status == 3) {
                holder.btnResend.setVisibility(View.VISIBLE);
                holder.status.setVisibility(View.GONE);
            }

            holder.btnResend.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.i("position", "==" + position);
                    reSendSplitFareRequest(splitFareInfoList.get(position).splitId);
                    // TODO Auto-generated method stub

                }
            });

            return convertView;
        }

    }

    public void getSplitFareData() {
        RestClient.getApiService().getSplitFareDataFromServer(Data.userData.accessToken,Data.cEngagementId, new Callback<String>() {
            @Override
            public void success(final String result, Response response) {
                Log.v("result  =", result + ",");
                if (!result.equalsIgnoreCase("")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                splitFareInfoList.clear();
                                JSONObject jObj = new JSONObject(result);
                                JSONArray splitFareInfoArray = jObj.getJSONArray("split_fare_data");

                                for (int i = 0; i < splitFareInfoArray.length(); i++) {

                                    JSONObject splitFareObject = splitFareInfoArray.getJSONObject(i);
                                    String phoneNumber = splitFareObject.getString("phone_no");
                                    phoneNumber = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
                                    splitFareInfoList.add(new SplitFareData(splitFareObject.getString("user_name"), phoneNumber, splitFareObject.getInt("status"), splitFareObject.getString("split_id")));
                                }
                                progressBar1.setVisibility(View.GONE);
                                tvNoCustomer.setVisibility(View.GONE);
                                splitFareListAdapter.notifyDataSetChanged();

                                getSplitFareData();

                            } catch (Exception e) {
                                Log.v("exceptione", e.toString());

                                try {
                                   getSplitFareData();

                                } catch (Exception e2) {
                                    // TODO: handle exception
                                }
                            }

                        }
                    });

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

}
