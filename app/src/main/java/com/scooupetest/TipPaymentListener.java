package com.scooupetest;

public interface TipPaymentListener {

    public void setFareWithTip(String tip, String totalFare);

    public void setPaymentButtonLayout();

}
