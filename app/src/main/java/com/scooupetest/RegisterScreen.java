package com.scooupetest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.facebook.FacebookLoginCallback;
import com.scooupetest.facebook.FacebookLoginHelper;
import com.scooupetest.locationfiles.LocationFetcher;
import com.scooupetest.locationfiles.LocationUpdate;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.DeviceTokenGenerator;
import com.scooupetest.utils.FeaturesConfig;
import com.scooupetest.utils.FlurryEventLogger;
import com.scooupetest.utils.IDeviceTokenReceiver;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Utils;
import com.scooupetest.utils.Validation;
import com.scooupetest.utils.Validation.TextType;
import com.countrypicker.CountryPicker;
import com.countrypicker.CountryPickerListener;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class RegisterScreen extends BaseActivity implements LocationUpdate {

    public static boolean facebookLogin = false;
    Button backBtn;
    TextView title;
    Button registerWithFacebookBtn;
    TextView orText, tvCountryCode;
    EditText firstNameEt, lastNameEt, hearAboutUsET, emailIdEt, phoneNoEt, passwordEt, confirmPasswordEt;
    Button signUpBtn;
    TextView extraTextForScroll;
    ScrollView scroll;
    LinearLayout relative;
    String name = "", emailId = "", phoneNo = "", password = "";
    boolean loginDataFetched = false, sendToOtpScreen = false;
    int otpFlag = 0;
    FacebookLoginCallback facebookLoginCallback = new FacebookLoginCallback() {
        @Override
        public void facebookLoginDone() {
            facebookLogin = true;

            passwordEt.setVisibility(View.GONE);
            confirmPasswordEt.setVisibility(View.GONE);
            firstNameEt.setText(Data.fbFirstName);
            lastNameEt.setText(Data.fbLastName);
            emailIdEt.setText(Data.fbUserEmail);

            firstNameEt.setEnabled(false);
            lastNameEt.setEnabled(false);
            emailIdEt.setEnabled(false);
            FlurryEventLogger.registerViaFBClicked(Data.fbId);
        }
    };

    public void resetFlags() {
        loginDataFetched = false;
        sendToOtpScreen = false;
        otpFlag = 0;
    }

    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
        FlurryAgent.onEvent("Register started");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        loginDataFetched = false;

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(RegisterScreen.this, relative, 1134, 720, false);
//		getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.register_background));
        backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setTypeface(Data.getFont(getApplicationContext()));
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));

        registerWithFacebookBtn = (Button) findViewById(R.id.registerWithFacebookBtn);
        registerWithFacebookBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        orText = (TextView) findViewById(R.id.orText);
        orText.setTypeface(Data.getFont(getApplicationContext()));

        scroll = (ScrollView) findViewById(R.id.scroll);

        firstNameEt = (EditText) findViewById(R.id.firstNameEt);
        firstNameEt.setTypeface(Data.getFont(getApplicationContext()));
        lastNameEt = (EditText) findViewById(R.id.lastNameEt);
        lastNameEt.setTypeface(Data.getFont(getApplicationContext()));
        hearAboutUsET = (EditText) findViewById(R.id.aboutUsET);
        hearAboutUsET.setTypeface(Data.getFont(getApplicationContext()));
        emailIdEt = (EditText) findViewById(R.id.emailIdEt);
        emailIdEt.setTypeface(Data.getFont(getApplicationContext()));
        phoneNoEt = (EditText) findViewById(R.id.phoneNoEt);
        phoneNoEt.setTypeface(Data.getFont(getApplicationContext()));
        tvCountryCode = (TextView) findViewById(R.id.tvCountryCode);
        tvCountryCode.setTypeface(Data.getFont(getApplicationContext()));
        passwordEt = (EditText) findViewById(R.id.passwordEt);
        passwordEt.setTypeface(Data.getFont(getApplicationContext()));
        confirmPasswordEt = (EditText) findViewById(R.id.confirmPasswordEt);
        confirmPasswordEt.setTypeface(Data.getFont(getApplicationContext()));

        Validation validation = new Validation();
//		validation.setValidationFilter(TextType.PhoneNumber, phoneNoEt);
        validation.setValidationFilter(TextType.FirstName, firstNameEt);
        validation.setValidationFilter(TextType.LastName, lastNameEt);
        validation.setValidationFilter(TextType.Password, passwordEt);
        validation.setValidationFilter(TextType.Password, confirmPasswordEt);

        signUpBtn = (Button) findViewById(R.id.signUpBtn);
        signUpBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        extraTextForScroll = (TextView) findViewById(R.id.extraTextForScroll);


        if(facebookLogin){
            passwordEt.setVisibility(View.GONE);
            confirmPasswordEt.setVisibility(View.GONE);
        }
        registerWithFacebookBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new FacebookLoginHelper().openFacebookSession(RegisterScreen.this, facebookLoginCallback, true);
//				new FacebookLoginCreator().openFacebookSession(RegisterScreen.this, facebookLoginCallback, true);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });

        firstNameEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                firstNameEt.setError(null);

            }
        });


        firstNameEt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                firstNameEt.setError(null);
            }
        });

        emailIdEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                emailIdEt.setError(null);
            }
        });

        phoneNoEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                phoneNoEt.setError(null);
            }
        });

        passwordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                passwordEt.setError(null);
            }
        });

        confirmPasswordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                confirmPasswordEt.setError(null);
            }
        });

        tvCountryCode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final CountryPicker picker = CountryPicker.newInstance("Select Country");
                picker.setListener(new CountryPickerListener() {

                    @Override
                    public void onSelectCountry(String name, String code) {
//						Toast.makeText(
//								RegisterScreen.this,
//								"Country Name: " + name + " - Code: " + code
//										+ " - Currency: "
//										+ CountryPicker.getCurrencyCode(code),
//								Toast.LENGTH_SHORT).show();
                        Log.i("phone code", GetCountryPhoneCode(code));
                        tvCountryCode.setText("+" + GetCountryPhoneCode(code));
                        picker.dismiss();
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                // TODO Auto-generated method stub

            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Utils.hideSoftKeyboard(RegisterScreen.this, firstNameEt);
                String name = firstNameEt.getText().toString().trim();
                if (name.length() > 0) {
                    name = name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
                }
                String hearAboutUsText = hearAboutUsET.getText().toString().trim();
                String emailId = emailIdEt.getText().toString().trim();
                boolean noFbEmail = false;

                if (facebookLogin && emailId.equalsIgnoreCase("")) {
                    emailId = "n@n.c";
                    noFbEmail = true;
                }


                String phoneNo = phoneNoEt.getText().toString().trim();
                String password = "";
                String confirmPassword = "";
                if(!facebookLogin){
                    password = passwordEt.getText().toString().trim();
                    confirmPassword = confirmPasswordEt.getText().toString().trim();}
                Data.lastName = lastNameEt.getText().toString();


                if ("".equalsIgnoreCase(name)) {
                    firstNameEt.requestFocus();
                    firstNameEt.setError("Please enter first name");
                } else {
                    if ("".equalsIgnoreCase(emailId)) {
                        emailIdEt.requestFocus();
                        emailIdEt.setError("Please enter email id");
                    } else {
                        if ("".equalsIgnoreCase(phoneNo)) {
                            phoneNoEt.requestFocus();
                            phoneNoEt.setError("Please enter phone number");
                        } else {
                            //TODO remove extra characters phoneNo
                            Data.phoneNoWithoutCountryCode = phoneNo;
                            phoneNo = phoneNo.replace(" ", "");
                            phoneNo = phoneNo.replace("(", "");
                            phoneNo = phoneNo.replace("/", "");
                            phoneNo = phoneNo.replace(")", "");
                            phoneNo = phoneNo.replace("N", "");
                            phoneNo = phoneNo.replace(",", "");
                            phoneNo = phoneNo.replace("*", "");
                            phoneNo = phoneNo.replace(";", "");
                            phoneNo = phoneNo.replace("#", "");
                            phoneNo = phoneNo.replace("-", "");
                            phoneNo = phoneNo.replace(".", "");

                            if (phoneNo.length() >= 7) {
                                phoneNo = tvCountryCode.getText().toString().trim() + phoneNo;
//								if(phoneNo.charAt(0) == '0' || phoneNo.charAt(0) == '1' || phoneNo.contains("+")){
//									phoneNoEt.requestFocus();
//									phoneNoEt.setError("Please enter valid phone number");
//								}
//								else{
//									phoneNo = "+91" + phoneNo;

                                if ("".equalsIgnoreCase(password) && !facebookLogin) {
                                    passwordEt.requestFocus();
                                    passwordEt.setError("Please enter password");
                                } else {
                                    if ("".equalsIgnoreCase(confirmPassword) && !facebookLogin) {
                                        confirmPasswordEt.requestFocus();
                                        confirmPasswordEt.setError("Please confirm password");
                                    } else {
                                        if (isEmailValid(emailId) && !emailId.startsWith(".")) {
                                            if (isPhoneValid(phoneNo)) {
                                                if (password.equals(confirmPassword)) {
                                                    if (password.length() >= 6 || facebookLogin) {

                                                        if (facebookLogin) {
                                                            if (noFbEmail) {
                                                                emailId = "";
                                                            }
                                                            RegisterScreen.facebookLogin = true;
                                                            OTPConfirmScreen.facebookRegisterData = new FacebookRegisterData(phoneNo,/* password,*/ hearAboutUsText);
//																sendFacebookSignupValues(RegisterScreen.this, referralCode, phoneNo, password);
                                                            FlurryEventLogger.facebookSignupClicked(Data.fbUserEmail);

                                                        } else {
                                                            RegisterScreen.facebookLogin = false;
//																sendSignupValues(RegisterScreen.this, name, referralCode, emailId, phoneNo, password);
                                                            OTPConfirmScreen.emailRegisterData = new EmailRegisterData(name + " " + Data.lastName, emailId, phoneNo, password, hearAboutUsText);
                                                            FlurryEventLogger.emailSignupClicked(emailId);

                                                        }
                                                        Data.phoneNo = phoneNo;
                                                        Data.countryCode = tvCountryCode.getText().toString();
                                                        Data.password = password;

                                                        Data.firstName = name;
                                                        Data.userName = name + " " + Data.lastName;
                                                        Data.emailId = emailId;
                                                        Data.hearAboutText = hearAboutUsText;
                                                        if(FeaturesConfig.cashOnly)
                                                        {
                                                            if(facebookLogin)
                                                            {
                                                                sendFacebookSignupValues(RegisterScreen.this, "");
                                                            }
                                                            else
                                                            {
                                                                sendSignupValues(RegisterScreen.this,"");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            startActivity(new Intent(
                                                                    RegisterScreen.this,
                                                                    FinalRegisterScreen.class));
                                                            overridePendingTransition(
                                                                    R.anim.right_in,
                                                                    R.anim.right_out);
                                                            finish();

                                                        }


                                                    } else {
                                                        passwordEt.requestFocus();
                                                        passwordEt.setError("Password must be of atleast six characters");
                                                    }
                                                } else {
                                                    passwordEt.requestFocus();
                                                    passwordEt.setError("Passwords does not match");
                                                }
                                            } else {
                                                phoneNoEt.requestFocus();
                                                phoneNoEt.setError("Please enter valid phone number");
                                            }
                                        } else {
                                            emailIdEt.requestFocus();
                                            emailIdEt.setError("Please enter valid email id");
                                        }
                                    }
                                }
                            }
//							}
                            else {
                                phoneNoEt.requestFocus();
                                phoneNoEt.setError("Please enter valid phone number");
                            }
                        }
                    }
                }

            }
        });


        hearAboutUsET.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        signUpBtn.performClick();
                        break;

                    case EditorInfo.IME_ACTION_NEXT:
                        break;

                    default:
                }
                return true;
            }
        });


        if (facebookLogin) {
            firstNameEt.setText(Data.fbFirstName);
            lastNameEt.setText(Data.fbLastName);
            firstNameEt.setEnabled(false);
            lastNameEt.setEnabled(false);
            if ("".equalsIgnoreCase(Data.fbUserEmail)) {
                emailIdEt.setText("");
                emailIdEt.setEnabled(true);
            } else {
                emailIdEt.setText(Data.fbUserEmail);
                emailIdEt.setEnabled(false);
            }
        }

        try {
            if (getIntent().hasExtra("back_from_otp")) {
                if (facebookLogin) {

                    passwordEt.setVisibility(View.GONE);
                    confirmPasswordEt.setVisibility(View.GONE);
                    hearAboutUsET.setText(OTPConfirmScreen.facebookRegisterData.referralCode);
//					phoneNoEt.setText(OTPConfirmScreen.facebookRegisterData.phoneNo.replace(Data.countryCode, ""));
                    //passwordEt.setText(OTPConfirmScreen.facebookRegisterData.password);
                   // confirmPasswordEt.setText(OTPConfirmScreen.facebookRegisterData.password);
                    tvCountryCode.setText(Data.countryCode);
                } else {
                    firstNameEt.setText(Data.firstName);
                    lastNameEt.setText(Data.lastName);
                    emailIdEt.setText(OTPConfirmScreen.emailRegisterData.emailId);
                    hearAboutUsET.setText(OTPConfirmScreen.emailRegisterData.referralCode);
//					phoneNoEt.setText(OTPConfirmScreen.emailRegisterData.phoneNo.replace(Data.countryCode, ""));
                    passwordEt.setText(OTPConfirmScreen.emailRegisterData.password);
                    confirmPasswordEt.setText(OTPConfirmScreen.emailRegisterData.password);
                    tvCountryCode.setText(Data.countryCode);
                }
                phoneNoEt.setText(Data.phoneNoWithoutCountryCode);
            } else {
                tvCountryCode.setText("+" + GetCountryZipCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        new DeviceTokenGenerator(this).generateDeviceToken(this, new IDeviceTokenReceiver() {

            @Override
            public void deviceTokenReceived(final String regId) {
                Data.deviceToken = regId;
                Log.e("deviceToken in IDeviceTokenReceiver", Data.deviceToken + "..");
            }
        });


//		nameEt.setText("Test");
//		lastNameEt.setText("Passenger84");
//		emailIdEt.setText("passenger84@click-labs.com");
//		phoneNoEt.setText("9999999999");
//		passwordEt.setText("passenger");
//		confirmPasswordEt.setText("passenger");


//		Toast.makeText(getApplicationContext(), ""+GetCountryZipCode(), Toast.LENGTH_LONG).show();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            Session.getActiveSession().onActivityResult(this, requestCode,
                    resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String GetCountryZipCode() {

        String CountryID = "";
        String CountryZipCode = "";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        // getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        Log.e("CountryID", "=" + CountryID);
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                return CountryZipCode;
            }
        }
        return "";
    }

    String GetCountryPhoneCode(String CountryID) {

        String CountryZipCode = "";

//		TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
//		// getNetworkCountryIso
//		CountryID = manager.getSimCountryIso().toUpperCase();
        Log.e("CountryID", "=" + CountryID);
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                return CountryZipCode;
            }
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Data.locationFetcher == null) {
            Data.locationFetcher = new LocationFetcher(RegisterScreen.this, 1000, 1);
        }

    }

    @Override
    protected void onPause() {
        try {
            if (Data.locationFetcher != null) {
                Data.locationFetcher.destroy();
                Data.locationFetcher = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();


    }


    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    boolean isPhoneValid(CharSequence phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }


    @Override
    public void onBackPressed() {
        performBackPressed();
        super.onBackPressed();
    }


    public void performBackPressed() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (Session.getActiveSession() != null) {
                        Session.getActiveSession().closeAndClearTokenInformation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Intent intent = new Intent(RegisterScreen.this, SplashNewActivity.class);
        intent.putExtra("no_anim", "yes");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }


    @Override
    public void onLocationChanged(Location location, int priority) {
        // TODO Auto-generated method stub
        Data.latitude = location.getLatitude();
        Data.longitude = location.getLongitude();

    }


    /**
     * ASync for register from server
     */
    public void sendSignupValues(final Activity activity, final String paymentNounce) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            resetFlags();
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

           Log.i("user_name", "=" + Data.userName);
            Log.i("ph_no", "=" + Data.phoneNo);
            Log.i("email", "=" + Data.emailId);
           Log.i("password", "=" + Data.password);
           Log.i("otp", "=" + "");
            Log.i("device_token", "=" + Data.deviceToken);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("country", "=" + Data.country);
           Log.i("device_name", "=" + Data.deviceName);
           Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

            if (Data.locationFetcher != null) {
                Data.latitude = Data.locationFetcher.getLatitude();
                Data.longitude = Data.locationFetcher.getLongitude();
            }

           Log.i("corporateEmail", "=" + Data.corporateEmail);
           Log.i("corporteCode", "=" + Data.corporteCode);
            Log.i("paymentTypeFlag", "=" + Data.paymentTypeFlag);
            Data.paymentTypeFlag=3;
            RestClient.getApiService().sendSignupValues(Data.userName, Data.phoneNo, Data.emailId, Data.password, "", Data.DEVICE_TYPE, Data.uniqueDeviceId, Data.deviceToken,
                    Data.latitude, Data.longitude, Data.country, Data.deviceName, Data.appVersion, Data.osVersion, Data.hearAboutText, "0", "0", paymentNounce, "",
                    Data.paymentTypeFlag, Data.corporateEmail, Data.corporteCode, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            android.util.Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);


                                boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);

                                if (!newUpdate) {

                                    if (!jObj.isNull("error")) {
                                        int flag = jObj.getInt("flag");
                                        String errorMessage = jObj.getString("error");

                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            HomeActivity.logoutUser(activity);
                                        } else if (0 == flag) {
                                            Data.phoneNo = jObj.getString("phone_no");

                                            otpFlag = 0;
                                            sendToOtpScreen = true;
                                            Data.nounce = paymentNounce;

                                        } else {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                        DialogPopup.dismissLoadingDialog();
                                    } else {
                                        new JSONParser().parseLoginData(activity, response);

                                        Database.getInstance(RegisterScreen.this).insertEmail(Data.emailId);
                                        Database.getInstance(RegisterScreen.this).close();

                                        loginDataFetched = true;

                                        DialogPopup.dismissLoadingDialog();

                                    }
                                } else {
                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            android.util.Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });


        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }

    /**
     * ASync for login from server
     */
    public void sendFacebookSignupValues(final Activity activity, final String paymentNounce) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            resetFlags();
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

           Log.i("user_fb_id", "=" + Data.fbId);
           Log.i("user_fb_name", "=" + Data.fbFirstName + " " + Data.fbLastName);
           Log.i("fb_access_token", "=" + Data.fbAccessToken);
           Log.i("username", "=" + Data.fbUserName);
           Log.i("fb_mail", "=" + Data.fbUserEmail);
           Log.i("latitude", "=" + Data.latitude);
           Log.i("longitude", "=" + Data.longitude);
           Log.i("device_token", "=" + Data.deviceToken);
           Log.i("country", "=" + Data.country);
           Log.i("app_version", "=" + Data.appVersion);
           Log.i("os_version", "=" + Data.osVersion);
           Log.i("device_name", "=" + Data.deviceName);
           Log.i("device_type", "=" + Data.DEVICE_TYPE);
           Log.i("otp", "=" + "");
           Log.i("ph_no", "=" + Data.phoneNo);
           Log.i("password", "=" + Data.password);
           Log.i("referral_code", "=" + Data.hearAboutText);
           Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

            Data.paymentTypeFlag = 3;
            Log.i("corporateEmail", "=" + Data.corporateEmail);
            Log.i("corporteCode", "=" + Data.corporteCode);
            Log.i("paymentTypeFlag", "=" + Data.paymentTypeFlag);

            RestClient.getApiService().sendFacebookLoginValues(Data.fbId, Data.fbFirstName + " " + Data.fbLastName, Data.fbAccessToken, "" + Data.fbUserName, Data.fbUserEmail,
                    Data.latitude, Data.longitude, Data.deviceToken, Data.country, Data.appVersion, Data.osVersion, Data.deviceName, Data.DEVICE_TYPE, Data.uniqueDeviceId, "",
                    Data.phoneNo, /*Data.password,*/ Data.hearAboutText, paymentNounce, Data.paymentTypeFlag, Data.corporateEmail, Data.corporteCode, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);


                                boolean newUpdate = SplashNewActivity.checkIfUpdate(jObj, activity);

                                if (!newUpdate) {
                                    if (!jObj.isNull("error")) {
                                        int flag = jObj.getInt("flag");
                                        String errorMessage = jObj.getString("error");
                                        if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                            HomeActivity.logoutUser(activity);
                                        } else if (2 == flag) {
                                            Data.phoneNo = jObj.getString("phone_no");
                                            otpFlag = 1;
                                            sendToOtpScreen = true;
                                            Data.nounce = paymentNounce;

                                        } else {
                                            new DialogPopup().alertPopup(activity, "", errorMessage);
                                        }
                                        DialogPopup.dismissLoadingDialog();
                                    } else {
                                        new JSONParser().parseLoginData(activity, response);
                                        loginDataFetched = true;
                                        Database.getInstance(RegisterScreen.this).insertEmail(Data.fbUserEmail);
                                        Database.getInstance(RegisterScreen.this).close();
                                        DialogPopup.dismissLoadingDialog();
                                    }
                                } else {
                                    DialogPopup.dismissLoadingDialog();
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                DialogPopup.dismissLoadingDialog();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            android.util.Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus && Data.isOpenGooglePlay) {
            Data.isOpenGooglePlay = false;
        } else if (hasFocus && loginDataFetched) {
            loginDataFetched = false;
            startActivity(new Intent(RegisterScreen.this, HomeActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        } else if (hasFocus && sendToOtpScreen) {
            sendIntentToOtpScreen();
        }

    }

    /**
     * Send intent to otp screen by making required data objects
     * flag 0 for email, 1 for Facebook
     */
    public void sendIntentToOtpScreen() {
        if (0 == otpFlag) {
            RegisterScreen.facebookLogin = false;
            OTPConfirmScreen.intentFromRegister = true;
            OTPConfirmScreen.emailRegisterData = new EmailRegisterData(Data.userName, Data.emailId, Data.phoneNo, Data.password, Data.hearAboutText);
            startActivity(new Intent(RegisterScreen.this, OTPConfirmScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        } else if (1 == otpFlag) {
            RegisterScreen.facebookLogin = true;
            OTPConfirmScreen.intentFromRegister = true;
            OTPConfirmScreen.facebookRegisterData = new FacebookRegisterData(Data.phoneNo, /*Data.password,*/ Data.hearAboutText);
            startActivity(new Intent(RegisterScreen.this, OTPConfirmScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
    }

}
