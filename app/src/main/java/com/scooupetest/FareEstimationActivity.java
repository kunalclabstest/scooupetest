package com.scooupetest;

import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.FareStructure;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;
import com.flurry.android.FlurryAgent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class FareEstimationActivity extends BaseActivity {

    RelativeLayout relative;

    Button backBtn, enterNewDestinationBtn;
    TextView title, tvEstimatedFare, tvErrorMessage, tvCarType, tvPickupLocationValue;
    ProgressBar progressBar;
   ArrayList<FareStructure> fareStructureArrayList = new ArrayList<FareStructure>();

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fare_estimate);
        relative = (RelativeLayout) findViewById(R.id.relative);
        new ASSL(FareEstimationActivity.this, relative, 1134, 720, false);
        backBtn = (Button) findViewById(R.id.backBtn);
        title = (TextView) findViewById(R.id.header);
        title.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvPickupLocation = (TextView) findViewById(R.id.tvPickUpLocationText);
        tvPickupLocation.setTypeface(Data.getFont(getApplicationContext()));
        tvPickupLocationValue = (TextView) findViewById(R.id.tvPickUpLocation);
        tvPickupLocationValue.setTypeface(Data.getFont(getApplicationContext()));
        tvPickupLocationValue.setText("" + getIntent().getExtras().get("PickupLocation"));
        TextView tvDestinationLocation = (TextView) findViewById(R.id.tvDestinationLocation);
        tvDestinationLocation.setTypeface(Data.getFont(getApplicationContext()));
        tvDestinationLocation.setText("" + getIntent().getExtras().get("DestinationLocation"));
        TextView tvDestinationLocationValue = (TextView) findViewById(R.id.tvDestinationLocationText);
        tvDestinationLocationValue.setTypeface(Data.getFont(getApplicationContext()));
        tvCarType = (TextView) findViewById(R.id.tvCarType);
        tvCarType.setTypeface(Data.getFont(getApplicationContext()));
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvEstimatedFare = (TextView) findViewById(R.id.tvEstimatedFareValue);
        tvEstimatedFare.setTypeface(Data.getFont(getApplicationContext()));
        tvErrorMessage = (TextView) findViewById(R.id.tvErrorMessage);
        tvErrorMessage.setTypeface(Data.getFont(getApplicationContext()));
        enterNewDestinationBtn = (Button) findViewById(R.id.newDestinationButton);
        enterNewDestinationBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        enterNewDestinationBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        if (tvPickupLocationValue.getText().toString().equalsIgnoreCase("")) {
            setPickupAddress();

        }
        getFareInRegion();
        tvErrorMessage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (tvErrorMessage.getText().toString().toUpperCase().contains(getString(R.string.tap_to_retry).toUpperCase())) {
                    getFareInRegion();
                }

            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        setCarType();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        super.onBackPressed();
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

    /**
     * API call for calculateAndSetEstimateFare
     */
    public void getFareInRegion() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            Log.i("pickupLatLng", "==" + Data.pickupLatLng);
            Log.i("destination", "==" + Data.destinationLatLng);
            tvErrorMessage.setVisibility(View.GONE);
            tvEstimatedFare.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            RestClient.getApiService().getFareByRegion("" + Data.pickupLatLng.latitude, "" + Data.pickupLatLng.longitude, new Callback<String>() {

                        @Override
                        public void failure(final RetrofitError error) {
                            Log.i("Server failure", "= " + error);
                            tvErrorMessage.setText(getString(R.string.error_occurred));
                            tvErrorMessage.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);

                        }

                        @Override
                        public void success(String response, Response arg1) {
                            Log.i("Server response", "response = " + response);
                            try {
                                parseFareStructure(new JSONObject(response));
//
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                tvErrorMessage.setText(getString(R.string.error_occurred));
                                tvErrorMessage.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);

                            }


                        }
                    }
            );
        } else {
            new DialogPopup().alertPopup(FareEstimationActivity.this, "", getString(R.string.check_internet_message));
        }
    }


    /**
     * API call for calculateAndSetEstimateFare
     */
    public void calculateAndSetEstimateFare() {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            Log.i("pickupLatLng", "==" + Data.pickupLatLng);
            Log.i("destination", "==" + Data.destinationLatLng);

            RestClient.getDistanceMatrixApiService().getDistanceMatrixData(Data.pickupLatLng.latitude + "," + Data.pickupLatLng.longitude, Data.destinationLatLng.latitude + "," + Data.destinationLatLng.longitude, "EN", new Callback<String>() {

                        @Override
                        public void failure(final RetrofitError error) {

                            tvErrorMessage.setText(getString(R.string.error_occurred));
                            tvErrorMessage.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);

                        }

                        @Override
                        public void success(String response, Response arg1) {
                            Log.i("Server response", "response = " + response);
                            try {


                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if ("OK".equalsIgnoreCase(status)) {
                                    JSONObject element0 = jsonObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0);
                                    if (element0.has("distance")) {

                                        Log.i("distance", "==" + element0.getJSONObject("distance").getString("text"));
                                        Log.i("duration", "==" + element0.getJSONObject("duration").getString("text"));
                                        Double distanceInRequiredUnit = element0.getJSONObject("distance").getDouble("value") * Double.parseDouble(getString(R.string.distance_conversion_factor));
                                        Double timeInMinute = element0.getJSONObject("duration").getDouble("value") / 60;
                                        tvEstimatedFare.setText(calculateFare(distanceInRequiredUnit, timeInMinute));
                                        tvEstimatedFare.setVisibility(View.VISIBLE);
                                        tvErrorMessage.setVisibility(View.GONE);

                                    } else {
                                        tvErrorMessage.setText("Unable to perform a fare estimate.");
                                        tvEstimatedFare.setVisibility(View.GONE);
                                        tvErrorMessage.setVisibility(View.VISIBLE);

                                    }
                                    progressBar.setVisibility(View.GONE);

                                }
                                else
                                {
                                    tvErrorMessage.setText(status+". "+getString(R.string.tap_to_retry));
                                    tvErrorMessage.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                tvErrorMessage.setText(getString(R.string.error_occurred));
                                tvErrorMessage.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);

                            }


                        }
                    }
            );
        } else {
            new DialogPopup().alertPopup(FareEstimationActivity.this, "", getString(R.string.check_internet_message));
        }
    }


    public String calculateFare(double distanceInRequiredUnit, double totalTimeInMin) {
        totalTimeInMin = totalTimeInMin - fareStructureArrayList.get(Data.defaultCarType).freeMinutes;
        if (totalTimeInMin < 0) {
            totalTimeInMin = 0;
        }
        double fareOfRideTime = totalTimeInMin * fareStructureArrayList.get(Data.defaultCarType).farePerMin;
        double fare = fareOfRideTime + fareStructureArrayList.get(Data.defaultCarType).fixedFare + ((distanceInRequiredUnit <= fareStructureArrayList.get(Data.defaultCarType).thresholdDistance) ? (0) : ((distanceInRequiredUnit - fareStructureArrayList.get(Data.defaultCarType).thresholdDistance) *  fareStructureArrayList.get(Data.defaultCarType).farePerDistanceUnit));
        Log.i("fare", "==" + fare);
        Log.i("estimated Fare", "+==" + "" + getString(R.string.currency_symbol) + " " + Math.ceil(fare - fare / 10) + "-" + Math.ceil(fare + fare / 10));
        return "" + getString(R.string.currency_symbol) + (int) (fare - fare / 10) + "-" + (int) (fare + fare / 10);
    }

    public void setCarType() {

        switch (Data.defaultCarType) {
            case 0:
                tvCarType.setText(getString(R.string.vehicle_1));
                break;
            case 1:
                tvCarType.setText(getString(R.string.vehicle_2));
                break;
            case 2:
                tvCarType.setText(getString(R.string.vehicle_3));
                break;
            case 3:
                tvCarType.setText(getString(R.string.vehicle_4));
                break;
            case 4:
                tvCarType.setText(getString(R.string.vehicle_5));
                break;
            default:
                tvCarType.setText(getString(R.string.vehicle_5));
                break;

        }


    }

    public void setPickupAddress() {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(Data.pickupLatLng.latitude, Data.pickupLatLng.longitude, 1);

            String address = addresses.get(0).getAddressLine(0);
//
            if (addresses.get(0).getAddressLine(1) != null) {
                tvPickupLocationValue.setText(address + ", " + addresses.get(0).getAddressLine(1));
            } else {
                tvPickupLocationValue.setText(address);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void parseFareStructure(JSONObject userData) {
        try {
            JSONArray fareDetailsArr = userData.getJSONArray("fare_details");
            fareStructureArrayList.clear();
            for (int i = 0; i < fareDetailsArr.length(); i++) {
                JSONObject fareDetail = fareDetailsArr.getJSONObject(i);
                double farePerMin = 0;
                double freeMinutes = 0;
                if (fareDetail.has("fare_per_min")) {
                    farePerMin = fareDetail.getDouble("fare_per_min");
                }
                if (fareDetail.has("fare_threshold_time")) {
                    freeMinutes = fareDetail.getDouble("fare_threshold_time");
                }
               fareStructureArrayList.add(new FareStructure(fareDetail.getDouble("fare_fixed"),
                        fareDetail.getDouble("fare_threshold_distance"),
                        fareDetail.getDouble("fare_per_km"),
                        farePerMin, freeMinutes, fareDetail.getInt("car_seats"),fareDetail.getInt("cancellation_time")));

            }
            calculateAndSetEstimateFare();
        } catch (Exception e) {
            e.printStackTrace();
            tvErrorMessage.setText(getString(R.string.error_occurred));
            tvErrorMessage.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

        }
    }
}
