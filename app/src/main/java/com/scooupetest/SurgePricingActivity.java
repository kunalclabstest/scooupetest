package com.scooupetest;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scooupetest.utils.BaseActivity;
import com.flurry.android.FlurryAgent;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import rmn.androidscreenlibrary.ASSL;

public class SurgePricingActivity extends BaseActivity {


    LinearLayout relative;

    Button backBtn;
    TextView title;


    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.surge_pricing_activity);

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(SurgePricingActivity.this, relative, 1134, 720, false);


        backBtn = (Button) findViewById(R.id.crossBtn);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvSurgePriceMessage = (TextView) findViewById(R.id.tvSurgePriceMessage);
        tvSurgePriceMessage.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvSurgeFactor = (TextView) findViewById(R.id.tvSurgeFactor);
        tvSurgeFactor.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        TextView tvNormalFareText = (TextView) findViewById(R.id.tvNormalFareText);
        tvNormalFareText.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvMinimumFare = (TextView) findViewById(R.id.tvMinimumFare);
        tvMinimumFare.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvFarePerMin = (TextView) findViewById(R.id.tvFarePerMin);
        tvFarePerMin.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvFarePerMile = (TextView) findViewById(R.id.tvFarePerMile);
        tvFarePerMile.setTypeface(Data.getFont(getApplicationContext()));
        TextView tvExpirationTime = (TextView) findViewById(R.id.tvSurgePriceExpireTime);
        tvExpirationTime.setTypeface(Data.getFont(getApplicationContext()));
        Button acceptButton = (Button) findViewById(R.id.acceptBtn);
        acceptButton.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        acceptButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Data.isAcceptedMaximumFare = true;
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Data.isAcceptedMaximumFare = false;
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        DecimalFormatSymbols custom=new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        decimalFormat.setDecimalFormatSymbols(custom);

        tvSurgeFactor.setText("" + String.format(Locale.US,"%.1f", Data.surgeFactor) + "x");

        tvExpirationTime.setText("THIS RATE EXPIRES IN " + Data.expiryMinutes + ((Data.expiryMinutes > 1) ? " MINS" : " MIN"));
        tvMinimumFare.setText(getString(R.string.currency_symbol) + Data.fareStructureArrayList.get(Data.defaultCarType).fixedFare + " MINIMUM FARE");
        tvFarePerMin.setText(getString(R.string.currency_symbol) + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).farePerMin) + "/ MIN");
        tvFarePerMile.setText(getString(R.string.currency_symbol) + decimalFormat.format(Data.fareStructureArrayList.get(Data.defaultCarType).farePerDistanceUnit) + "/ " + getString(R.string.distance_unit).toUpperCase());


    }


    @Override
    public void onBackPressed() {
        Data.isAcceptedMaximumFare = false;
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

}

