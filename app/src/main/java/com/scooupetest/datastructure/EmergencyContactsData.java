package com.scooupetest.datastructure;

public class EmergencyContactsData {
    public String userName, phoneNo, id;
    public boolean isContactSelected;


    public EmergencyContactsData(String userName, String phoneNo, String id,boolean isContactSelected) {
        this.userName = userName;
        this.phoneNo = phoneNo;
        this.id = id;
        this.isContactSelected = isContactSelected;
    }
}
