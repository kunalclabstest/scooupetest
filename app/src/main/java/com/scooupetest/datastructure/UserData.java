package com.scooupetest.datastructure;

public class UserData {
    public String accessToken, userName, userImage, referralCode, phoneNo, email, accountNumber, corporateEmail,sosPhoneNo,sosCountryCode;
    public int canSchedule, canChangeLocation, schedulingLimitMinutes, isAvailable, exceptionalDriver, gcmIntent, christmasIconEnable, nukkadEnable, isCorporateAccount, isPromoApplied;

    public UserData(String accessToken, String userName, String userImage, String referralCode, String phoneNo, String email,
                    int canSchedule, int canChangeLocation, int schedulingLimitMinutes, int isAvailable, int exceptionalDriver, int gcmIntent, int christmasIconEnable, int nukkadEnable, int isCorporateAccount, String accountNumber, String corporateEmail, int isPromoApplied,
                    String sosPhoneNo, String sosCountryCode) {
        this.accessToken = accessToken;
        this.userName = userName;
        this.userImage = userImage;
        this.referralCode = referralCode;
        this.phoneNo = phoneNo;
        this.email = email;
        this.canSchedule = canSchedule;
        this.canChangeLocation = canChangeLocation;
        this.schedulingLimitMinutes = schedulingLimitMinutes;
        this.isAvailable = isAvailable;
        this.exceptionalDriver = exceptionalDriver;
        this.gcmIntent = gcmIntent;
        this.christmasIconEnable = christmasIconEnable;
        this.nukkadEnable = nukkadEnable;
        this.isCorporateAccount = isCorporateAccount;
        this.accountNumber = accountNumber;
        this.corporateEmail = corporateEmail;
        this.isPromoApplied = isPromoApplied;
        this.sosPhoneNo = sosPhoneNo;
        this.sosCountryCode = sosCountryCode;
    }
}
