package com.scooupetest.datastructure;

public class RideInfo {

    public String id;
    public String fromLocation;
    public String toLocation;
    public String fare;
    public String tip;
    public String discount;
    public String distance;
    public String time;
    public int couponUsed, rideTime, waitTime;

    public RideInfo(String id, String fromLocation, String toLocation, String fare, String tip, String discount, String distance, String time,
                     int couponUsed, int rideTime, int waitTime) {
        this.id = id;
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.fare = fare;
        this.discount = discount;
        this.distance = distance;
        this.time = time;
        this.couponUsed = couponUsed;
        this.tip = tip;
        this.rideTime = rideTime;
        this.waitTime = waitTime;
    }

    @Override
    public String toString() {
        return fromLocation + " " + toLocation + " " + fare + " "+ tip + " " + distance + " " + time + " " + rideTime + " " + waitTime;
    }

}
