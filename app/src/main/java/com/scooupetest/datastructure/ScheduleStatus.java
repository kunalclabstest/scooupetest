package com.scooupetest.datastructure;

public enum ScheduleStatus {
    IN_QUEUE(0, "Request in queue"),
    IN_PROCESS(1, "Request in process"),
    PROCESSED(2, "Request processed"),
    NOT_PROCESSED(3, "Request not processed");

    private int ordinal;
    private String statusString;

    private ScheduleStatus(int ordinal, String statusString) {
        this.ordinal = ordinal;
        this.statusString = statusString;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public String getStatusString() {
        return statusString;
    }
}
