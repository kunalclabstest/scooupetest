package com.scooupetest.datastructure;

import com.google.android.gms.maps.model.LatLng;

public class FutureSchedule {

    public String pickupId, pickupAddress, pickupTime, destinationAddress;
    public LatLng pickupLatLng, destinationLatLng;
    public int modifiable, status;


    public FutureSchedule(String pickupId, String pickupAddress, String destinationAddress, String pickupTime, LatLng pickupLatLng, LatLng destinationLatLng, int modifiable, int status) {
        this.pickupId = pickupId;
        this.pickupAddress = pickupAddress;
        this.destinationAddress = destinationAddress;
        this.pickupTime = pickupTime;
        this.destinationLatLng = destinationLatLng;
        this.pickupLatLng = pickupLatLng;
        this.modifiable = modifiable;
        this.status = status;
    }

    @Override
    public String toString() {
        return pickupId + " " + pickupLatLng + " " + pickupTime + " " + status;
    }

}
