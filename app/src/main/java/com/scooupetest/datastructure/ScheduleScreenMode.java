package com.scooupetest.datastructure;

public enum ScheduleScreenMode {
    INITIAL, PICK_LOCATION, SEARCH
}
