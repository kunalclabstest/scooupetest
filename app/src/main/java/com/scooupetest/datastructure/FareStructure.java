package com.scooupetest.datastructure;

public class FareStructure {
    public double fixedFare;
    public double thresholdDistance;
    public double farePerDistanceUnit;
    public double farePerMin;
    public double freeMinutes;
    public int maxSize,cancelThresholdTime;

    public FareStructure(double fixedFare, double thresholdDistance, double farePerDistanceUnit, double farePerMin, double freeMinutes, int maxSize,int cancelThresholdTime) {
        this.fixedFare = fixedFare;
        this.thresholdDistance = thresholdDistance;
        this.farePerDistanceUnit = farePerDistanceUnit;
        this.farePerMin = farePerMin;
        this.freeMinutes = freeMinutes;
        this.maxSize = maxSize;
        this.cancelThresholdTime = cancelThresholdTime;
    }

    public double calculateFare(double totalDistanceInKm, double totalTimeInMin) {
        totalTimeInMin = totalTimeInMin - freeMinutes;
        if (totalTimeInMin < 0) {
            totalTimeInMin = 0;
        }
        double fareOfRideTime = totalTimeInMin * farePerMin;
        double fare = fareOfRideTime + fixedFare + ((totalDistanceInKm <= thresholdDistance) ? (0) : ((totalDistanceInKm - thresholdDistance) * farePerDistanceUnit));
        fare = Math.ceil(fare);
        return fare;
    }
}
