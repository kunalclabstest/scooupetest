package com.scooupetest.datastructure;

public class SplitFareData {
    public String userName, phoneNo, splitId;
    public int status;

    public SplitFareData(String userName, String phoneNo, int status, String splitId) {
        this.userName = userName;
        this.phoneNo = phoneNo;
        this.status = status;
        this.splitId = splitId;
    }
}
