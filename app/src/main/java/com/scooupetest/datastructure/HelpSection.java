package com.scooupetest.datastructure;

import android.content.res.Resources;

import com.scooupetest.R;

public enum HelpSection {
    MAIL_US(-2, R.string.send_us_email),
    CALL_US(-1, R.string.call_us),

    ABOUT(0, R.string.about_us),
    FAQ(1, R.string.faq),
    PRIVACY(2, R.string.privacy_policy),
    TERMS(3, R.string.terms_of_use),
    FARE_DETAILS(4, R.string.fare_details),
    SCHEDULES_TNC(5, R.string.terms_of_schedule);

    private int ordinal;
    private int name;

    private HelpSection(int ordinal, int name) {
        this.ordinal = ordinal;
        this.name = name;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public String getName(Resources r) {
        return r.getText(name).toString();
    }
}

