package com.scooupetest.datastructure;

public enum EngagementStatus {

    REQUESTED(0),
    // request has been sent
    ACCEPTED(1),
    // request has been accepted by the driver
    STARTED(2);
    // ride has started

    private int ordinal;

    private EngagementStatus(int ordinal) {
        this.ordinal = ordinal;
    }

    public int getOrdinal() {
        return ordinal;
    }
}
