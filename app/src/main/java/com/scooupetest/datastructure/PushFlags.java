package com.scooupetest.datastructure;

public enum PushFlags {
    REQUEST(0),
    RIDE_STARTED(3),
    RIDE_ENDED(4),
    RIDE_ACCEPTED(5),
    DRIVER_ARRIVED(1001),
    RIDE_REJECTED_BY_DRIVER(7),
    NO_DRIVERS_AVAILABLE(8),
    DEFAULTER(12),
    DRIVER_HIT_COLLECT_CASH(52),
    CHANGE_STATE(20),
    DISPLAY_MESSAGE(21),
    TOGGLE_LOCATION_UPDATES(22),
    HEARTBEAT(40),
    START_WAIT(9),
    END_WAIT(10),
    FORCE_STATE_CHANGE(400),
    SPLIT_FARE_REQUEST(11);

    private int ordinal;

    private PushFlags(int ordinal) {
        this.ordinal = ordinal;
    }

    public int getOrdinal() {
        return ordinal;
    }
}
