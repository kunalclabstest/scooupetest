package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.datastructure.UserData;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Prefs;
import com.scooupetest.utils.Utils;
import com.scooupetest.utils.Validation;
import com.flurry.android.FlurryAgent;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.squareup.picasso.CircleTransform;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import rmn.androidscreenlibrary.ASSL;

public class profile extends BaseActivity implements ImageChooserListener {
    public static final int REQUEST_CODE_CROP_IMAGE = 100;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 3;
    private static final int FETCH_FROM_FB = 4;
    RelativeLayout reviewUserImageRL;
    boolean isProfileUpdated = false;
    private ImageView reviewUserImage;
    private ProgressBar reviewUserImgProgress;
    private Button backBtn, doneButton;
    private EditText userFirstName, userLastName, bankAccountNumber, routingNumber;
    private AutoCompleteTextView country;
    private TextView userEmail, userPhone, headerText;
    private Uri mImageCaptureUri;
    private File mFileTemp;
    private ImageChooserManager imageChooserManager;
    private String isPicChange = "0";
    private LinearLayout relative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        relative = (LinearLayout) findViewById(R.id.relroot);
        new ASSL(profile.this, relative, 1134, 720, false);
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_android));

        reviewUserImage = (ImageView) findViewById(R.id.reviewUserImage);
        reviewUserImgProgress = (ProgressBar) findViewById(R.id.reviewUserImgProgress);
        reviewUserImageRL = (RelativeLayout) findViewById(R.id.reviewUserImageRL);
        userFirstName = (EditText) findViewById(R.id.etUserFirstName);
        userFirstName.setTypeface(Data.getFont(getApplicationContext()));
        userLastName = (EditText) findViewById(R.id.etUserLastName);
        userLastName.setTypeface(Data.getFont(getApplicationContext()));


        Validation validation = new Validation();
        validation.setValidationFilter(Validation.TextType.FirstName, userFirstName);
        validation.setValidationFilter(Validation.TextType.LastName, userLastName);
        if(Data.userData==null)
        {
            Data.userData= Prefs.with(getApplicationContext()).getObject("USER_DATA", UserData.class);
        }

        String[] splitUserName = Data.userData.userName.split("\\s+");
        userFirstName.setText(splitUserName[0]);
        String lastName = "";
        for (int i = 1; i < splitUserName.length; i++) {

            lastName = lastName + splitUserName[i] + " ";
        }
        userLastName.setText(lastName);
        userEmail = (TextView) findViewById(R.id.tvEmail);
        userEmail.setTypeface(Data.getFont(getApplicationContext()));
        userEmail.setText(Data.userData.email);

        userPhone = (TextView) findViewById(R.id.tvPhoneNumber);
        userPhone.setTypeface(Data.getFont(getApplicationContext()));
        userPhone.setText(Data.userData.phoneNo);

        TextView tvPersonalInfo = (TextView) findViewById(R.id.tvPersonalInfo);
        tvPersonalInfo.setTypeface(Data.getFont(getApplicationContext()));


        bankAccountNumber = (EditText) findViewById(R.id.etAccountNumber);
        bankAccountNumber.setTypeface(Data.getFont(getApplicationContext()));

        routingNumber = (EditText) findViewById(R.id.etRoutingNumber);
        routingNumber.setTypeface(Data.getFont(getApplicationContext()));

        country = (AutoCompleteTextView) findViewById(R.id.tvCountry);
        country.setTypeface(Data.getFont(getApplicationContext()));


        headerText = (TextView) findViewById(R.id.title);
        headerText.setTypeface(Data.getFont(getApplicationContext()));

        backBtn = (Button) findViewById(R.id.backBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        reviewUserImageRL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // finish();
                // overridePendingTransition(R.anim.left_in, R.anim.left_out);
                //
                Log.i("On image Click", "Image click");
                showchooseImagePopup(profile.this);
                // showSelection(1);
            }
        });
//		


        doneButton = (Button) findViewById(R.id.doneBtn);
        doneButton.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);


        doneButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(userFirstName.getWindowToken(), 0);
                if (userFirstName.getText().toString().length() > 0) {
//
                    String userName = userFirstName.getText().toString() + " " + userLastName.getText().toString();
                    Log.i("userName", "==" + userName);
                    Log.i("userName", "==" + Data.userData.userName);

                    if (isPicChange.equalsIgnoreCase("1")) {
                        updateProfile("");

                    } else if (!(userName.equalsIgnoreCase(Data.userData.userName))) {
                        updateProfile("");
                    } else {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }
                } else {
                    userFirstName.setError("Please fill name.");
                    userFirstName.requestFocus();
                }
            }
        });

        Button btnChangePassword = (Button) findViewById(R.id.changePasswordBtn);
        btnChangePassword.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        btnChangePassword.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(profile.this, ChangePassword.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        mFileTemp = Utils.getTempImageFile();
        if (mFileTemp.exists())
            mFileTemp.delete();
        mFileTemp = Utils.getTempImageFile();

        try {
            Picasso.with(profile.this).load(Data.userData.userImage).skipMemoryCache().transform(new CircleTransform()).into(reviewUserImage);
        } catch (Exception e) {
        }

    }

    // The Activity's onStart method
    @Override
    protected void onStart() {
        super.onStart();
        // Set up the Flurry session
        FlurryAgent.onStartSession(this,getString(R.string.flurry_key));
    }

    // The Activity's onStop method
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK &&
                (requestCode == ChooserType.REQUEST_PICK_PICTURE ||
                        requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            imageChooserManager.submit(requestCode, data);
        }
    }


    public void updateProfile(final String stripeToken) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(
                getApplicationContext())) {

            DialogPopup.showLoadingDialog(this, getString(R.string.loading));
            Log.i("access_token", Data.userData.accessToken);
            Log.i("new_username", userFirstName.getText().toString() + " " + userLastName.getText().toString());
            Log.i("user_pic", isPicChange);
            final String userName = userFirstName.getText().toString() + " " + userLastName.getText().toString();
            try {

                RestClient.getApiService().updateProfile(new TypedString(Data.userData.accessToken), new TypedString(userName), new TypedString("" + stripeToken), new TypedString(isPicChange), new TypedFile("image/jpeg", mFileTemp), new Callback<String>() {
                    private JSONObject jObj;

                    @Override
                    public void success(String response, Response r) {
                        Log.v("Server response", "response = " + response);

                        try {
                            jObj = new JSONObject(response);

                            if (!jObj.isNull("error")) {

                                int flag = jObj.getInt("flag");
                                String errorMessage = jObj
                                        .getString("error");

                                if (ApiResponseFlags.INVALID_ACCESS_TOKEN.getOrdinal() == flag) {
                                    HomeActivity.logoutUser(profile.this);

                                } else {
                                    new DialogPopup().alertPopup(
                                            profile.this, "", errorMessage);
                                }
                            } else {

                                new DialogPopup().alertPopup(profile.this,
                                        "", jObj.getString("log"));

                                isProfileUpdated = true;
                                if (isPicChange.equalsIgnoreCase("1")) {

                                    Data.userData.userName = userName;
                                    Data.userData.userImage = jObj.getString("user_image");

                                } else {
                                    Data.userData.userName = userName;
                                }
                                Prefs.with(getApplicationContext()).save("USER_DATA",Data.userData);
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                            new DialogPopup().alertPopup(profile.this, "",
                                    Data.SERVER_ERROR_MSG);
                        }

                        DialogPopup.dismissLoadingDialog();

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("request fail", error.toString());
                        DialogPopup.dismissLoadingDialog();
                        new DialogPopup().alertPopup(profile.this, "", Data.SERVER_NOT_RESOPNDING_MSG);
                    }
                });
                Log.i("image", "" + mFileTemp);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        } else {
            new DialogPopup().alertPopup(profile.this, "",
                    getString(R.string.check_internet_message));
        }
    }

    public void showchooseImagePopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.choose_image_popup);

            RelativeLayout frameLayout = (RelativeLayout) dialog
                    .findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);

            Button camera = (Button) dialog.findViewById(R.id.camera);
            camera.setTypeface(Data.getFont(activity));

            camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    chooseImageFromCamera();
                }
            });

            Button gallery = (Button) dialog.findViewById(R.id.gallery);
            gallery.setTypeface(Data.getFont(activity));

            gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    chooseImageFromGallery();
                }
            });

            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                Log.v("onImageChosen called", "onImageChosen called");
                if (chosenImage != null) {
                    // Use the image
                    // image.getFilePathOriginal();
                    // image.getFileThumbnail();
                    // image.getFileThumbnailSmall();
                    isPicChange = "1";
                    mFileTemp = new File(chosenImage.getFileThumbnailSmall());


                    Picasso.with(profile.this).load(mFileTemp).transform(new CircleTransform()).resize(300, 300).centerCrop()
                            .skipMemoryCache().into(reviewUserImage);
                }
            }
        });
    }

    @Override
    public void onError(String s) {

    }

//    private class LongOperation extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params) {
//
//            bitmap = adjustImageOrientation(bitmap, mFileTemp.getAbsolutePath());
//
//            CommonUtil.saveTempImage(bitmap);
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
////			doneBtn.setClickable(true);
//            reload();
//        }
//
//        @Override
//        protected void onPreExecute() {
////			doneBtn.setClickable(false);
//            Picasso.with(profile.this).load("www.google.com").skipMemoryCache()
//                    .transform(new CircleTransform()).resize(300, 300)
//                    .centerCrop().skipMemoryCache().into(reviewUserImage);
//
//            // Picasso.with(profile.this).load("www.google.com")
//            // .transform(new BlurTransform(profile.this))
//            // .resize(300, 300).centerCrop().skipMemoryCache()
//            // .into(reviewUserImgBlured);
//        }
//
//        @Override
//        protected void onProgressUpdate(Void... values) {
//        }
//    }
//
//    private Bitmap adjustImageOrientation(Bitmap image, String picturePath) {
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(picturePath);
//            int exifOrientation = exif.getAttributeInt(
//                    ExifInterface.TAG_ORIENTATION,
//                    ExifInterface.ORIENTATION_NORMAL);
//
//            int rotate = 0;
//            switch (exifOrientation) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotate = 90;
//                    break;
//
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotate = 180;
//                    break;
//
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotate = 270;
//                    break;
//            }
//
//            if (rotate != 0) {
//                int w = image.getWidth();
//                int h = image.getHeight();
//
//                // Setting pre rotate
//                Matrix mtx = new Matrix();
//                mtx.preRotate(rotate);
//
//                // Rotating Bitmap & convert to ARGB_8888, required by tess
//                image = Bitmap.createBitmap(image, 0, 0, 300, 300, mtx, false);
//
//            }
//            image = image.copy(Bitmap.Config.ARGB_8888, true);
//            image = Bitmap.createScaledBitmap(bitmap, 500,
//                    (int) (((float) bitmap.getHeight() / (float) bitmap
//                            .getWidth()) * 500), false);
//        } catch (Exception e) {
//            return null;
//        }
//
//        return image;
//    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && isProfileUpdated) {
            isProfileUpdated = false;
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }


    private void chooseImageFromGallery() {
        // int chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
        imageChooserManager.setImageChooserListener(this);
        try {
            imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void chooseImageFromCamera() {
        // int chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, "myfolder", true);
        imageChooserManager.setImageChooserListener(this);
        try {
            imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
