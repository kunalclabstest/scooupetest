package com.scooupetest;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.scooupetest.datastructure.ApiResponseFlags;
import com.scooupetest.datastructure.DriverInfo;
import com.scooupetest.datastructure.EndRideData;
import com.scooupetest.datastructure.EngagementStatus;
import com.scooupetest.datastructure.FareStructure;
import com.scooupetest.datastructure.PassengerScreenMode;
import com.scooupetest.datastructure.UserData;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.FeaturesConfig;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Prefs;
import com.scooupetest.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    public JSONParser() {

    }

    public void parseLoginData(Context context, String response) throws Exception {
        JSONObject jObj = new JSONObject(response);
        JSONObject userData = jObj.getJSONObject("user_data");

        Data.userData = parseUserData(context, userData);
        Prefs.with(context).save("USER_DATA",Data.userData);

            SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
            Editor editor = pref.edit();
            editor.putString(Data.SP_ACCESS_TOKEN_KEY, Data.userData.accessToken);
            editor.commit();


        try {
            int currentUserStatus = userData.getInt("current_user_status");

            if (currentUserStatus == 2) {
                HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
            }
        } catch (Exception e) {
            e.printStackTrace();

            HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
        }

        parseFareStructure(userData);

    }


    public void parseFareStructure(JSONObject userData) {
        try {
            JSONArray fareDetailsArr = userData.getJSONArray("fare_details");
            Data.fareStructureArrayList.clear();
            for (int i = 0; i < fareDetailsArr.length(); i++) {
                JSONObject fareDetail = fareDetailsArr.getJSONObject(i);
                double farePerMin = 0;
                double freeMinutes = 0;
                if (fareDetail.has("fare_per_min")) {
                    farePerMin = fareDetail.getDouble("fare_per_min");
                }
                if (fareDetail.has("fare_threshold_time")) {
                    freeMinutes = fareDetail.getDouble("fare_threshold_time");
                }
                if(fareDetail.getInt("default_flag")==1){
                    Data.defaultCarType=i;
                }
                Data.fareStructureArrayList.add(new FareStructure(fareDetail.getDouble("fare_fixed"),
                        fareDetail.getDouble("fare_threshold_distance"),
                        fareDetail.getDouble("fare_per_km"),
                        farePerMin, freeMinutes, fareDetail.getInt("car_seats"),fareDetail.getInt("cancellation_time")));

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public UserData parseUserData(Context context, JSONObject userData) throws Exception {

        int canSchedule = 0, canChangeLocation = 0, schedulingLimitMinutes = 0, isAvailable = 1, exceptionalDriver = 0, gcmIntent = 1, christmasIconEnable = 0, nukkadEnable = 0, isPromoCodeApplied = 0;
        String phoneNo = "", corporateEmail = "", accountNumber = "", sosPhone = "", sosCode = "";

        if (userData.has("can_schedule")) {
            canSchedule = userData.getInt("can_schedule");
        }

        if (userData.has("can_change_location")) {
            canChangeLocation = userData.getInt("can_change_location");
        }

        if (userData.has("scheduling_limit")) {
            schedulingLimitMinutes = userData.getInt("scheduling_limit");
        }

        if (userData.has("is_available")) {
            isAvailable = userData.getInt("is_available");
        }

        if (userData.has("exceptional_driver")) {
            exceptionalDriver = userData.getInt("exceptional_driver");
        }
        if (userData.has("last4_digits")) {
            accountNumber = "XXXX XXXX XXXX " + userData.getString("last4_digits");
        }
        if (userData.has("corp_email")) {
            corporateEmail = userData.getString("corp_email");
        }
        if (userData.has("having_promo")) {
            isPromoCodeApplied = userData.getInt("having_promo");
        }


        try {
            if (userData.has("gcm_intent")) {
                gcmIntent = userData.getInt("gcm_intent");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (userData.has("christmas_icon_enable")) {
                christmasIconEnable = userData.getInt("christmas_icon_enable");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (userData.has("phone_no")) {
            phoneNo = userData.getString("phone_no");
        }

        try {
            if (userData.has("nukkad_enable")) {
                nukkadEnable = userData.getInt("nukkad_enable");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (userData.has("sos_phone_no")) {
                sosPhone = userData.getString("sos_phone_no");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (userData.has("sos_country_code")) {
                sosCode = userData.getString("sos_country_code");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //"gcm_intent": 0, christmas_icon_enable

        return new UserData(userData.getString("access_token"), userData.getString("user_name"),
                userData.getString("user_image"), userData.getString("referral_code"), phoneNo, userData.getString("user_email"),
                canSchedule, canChangeLocation, schedulingLimitMinutes, isAvailable, exceptionalDriver, gcmIntent, christmasIconEnable, nukkadEnable, userData.getInt("is_corp"), accountNumber, corporateEmail, isPromoCodeApplied,sosPhone,sosCode);
    }

    public String parseAccessTokenLoginData(Context context, String response, String accessToken) throws Exception {

        JSONObject jObj = new JSONObject(response);

        //Fetching login data
        JSONObject jLoginObject = jObj.getJSONObject("login");
        JSONObject userData = jLoginObject.getJSONObject("user_data");

        Data.userData = parseUserData(context, userData);
        Prefs.with(context).save("USER_DATA", Data.userData);

        parseFareStructure(userData);

        //Fetching drivers info
        JSONObject jDriversObject = jObj.getJSONObject("drivers");
        parseDriversToShow(jDriversObject, "data");

        if (jObj.has("split_fare_popup")) {
            Data.isSplitPopupShow = 1;
            Data.splitFareData = jObj.getJSONObject("split_fare_popup");
            Log.i("split_fare_popup in access tokenn", "===" + jObj.getJSONObject("split_fare_popup"));
        } else {
            Data.isSplitPopupShow = 0;
            Data.splitFareData = null;
        }

        //Fetching user current status
        JSONObject jUserStatusObject = jObj.getJSONObject("status");
        String resp = parseCurrentUserStatus(context, jUserStatusObject);

        parseLastRideData(jObj);

        return resp;
    }


    //TODO
    public void parseLastRideData(JSONObject jObj) {

        try {
            JSONObject jLastRideData = jObj.getJSONObject("last_ride");
            Data.cSessionId = "";
            Data.cEngagementId = jLastRideData.getString("engagement_id");

            JSONObject jDriverInfo = jLastRideData.getJSONObject("driver_info");
            Data.cDriverId = jDriverInfo.getString("id");

            Data.pickupLatLng = new LatLng(0, 0);

            Data.assignedDriverInfo = new DriverInfo(Data.cDriverId, jDriverInfo.getString("name"), jDriverInfo.getString("user_image"), jDriverInfo.getInt("driver_rating"));
            Data.endRideData = new EndRideData(jLastRideData.getString("promo_code"));

            Data.totalDistance = jLastRideData.getDouble("distance_travelled");
            Data.totalFare = jLastRideData.getDouble("fare");
            Data.waitTime = jLastRideData.getString("wait_time");
            Data.rideTime = String.valueOf(Double.parseDouble(jLastRideData.getString("ride_time")) + Double.parseDouble(jLastRideData.getString("wait_time")));
            Data.CO2Saved = jLastRideData.getDouble("co2_saved");

            Data.isPaymentSuccessfull = jLastRideData.getString("is_payment_successful");
            FeaturesConfig.isTipEnable = jLastRideData.getInt("tip_enable")==1?true:false;
            FeaturesConfig.isAutomaticPaymentEnable = jLastRideData.getInt("automatic_payment")==1?true:false;
            Data.defaulterFlag = jLastRideData.getString("defaulter_flag");
            Log.i("Data.defaulterFlag", "==" + Data.defaulterFlag);
            Data.discount = jLastRideData.getDouble("discount");
            if (jLastRideData.has("car_type")) {
                Data.defaultCarType = jLastRideData.getInt("car_type");
            }if (jLastRideData.has("tip")) {
                Data.tip = jLastRideData.getDouble("tip");
            }


            HomeActivity.passengerScreenMode = PassengerScreenMode.P_RIDE_END;

            try {
                Data.couponJSON = jLastRideData;
                Log.i("Data.couponJSON", "=" + Data.couponJSON);
            } catch (Exception e) {
                e.printStackTrace();
                Data.couponJSON = new JSONObject();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getUserStatus(Context context, String accessToken) {
        String returnResponse = "";
        try {

            String result = RestClient.getApiService().getUserStatus(accessToken);
            Log.e("result of = user_status", "=" + result);
            if (result.contains("SERVER_TIMEOUT")) {
                returnResponse = "SERVER_TIMEOUT";
                return returnResponse;
            } else {
                Log.i("Data.pickupLatlng", "in getuserStatus" + Data.pickupLatLng);
                JSONObject jObject1 = new JSONObject(result);
                returnResponse = parseCurrentUserStatus(context, jObject1);
                Log.i("Data.pickupLatlng", "after parse" + Data.pickupLatLng);
                return returnResponse;
            }
        } catch (Exception e) {
            e.printStackTrace();
            returnResponse = "SERVER_TIMEOUT";
            return returnResponse;
        }
    }


    public void parseDriversToShow(JSONObject jObject, String jsonArrayKey) {
        try {
            Data.driverInfos.clear();
            JSONArray data = jObject.getJSONArray(jsonArrayKey);
            for (int i = 0; i < data.length(); i++) {
                JSONObject dataI = data.getJSONObject(i);
                String userId = dataI.getString("user_id");
                double latitude = dataI.getDouble("latitude");
                double longitude = dataI.getDouble("longitude");
                String userName = dataI.getString("user_name");
                String userImage = dataI.getString("user_image");
                String driverCarImage = dataI.getString("driver_car_image");
                String phoneNo = dataI.getString("phone_no");
                int rating = dataI.getInt("rating");
                String carNumber = "";
                if (dataI.has("driver_car_no")) {
                    carNumber = dataI.getString("driver_car_no");
                }
                Data.driverInfos.add(new DriverInfo(userId, latitude, longitude, userName, userImage, driverCarImage, phoneNo, rating, carNumber, 0,dataI.getDouble("driver_car_direction_angle")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String parseCurrentUserStatus(Context context, JSONObject jObject1) {

        String returnResponse = "";

        String screenMode = "";

        int engagementStatus = -1;
        String engagementId = "", sessionId = "", userId = "", latitude = "", longitude = "",
                driverName = "", driverImage = "", driverCarImage = "", driverPhone = "", driverCarNumber = "",
                pickupLatitude = "", pickupLongitude = "";
        int freeRide = 0, driverRating = 4;
        Double driverBearing=0.0;

        try {

            if (jObject1.has("error")) {
                returnResponse = "SERVER_TIMEOUT";
                return returnResponse;
            } else {

                int flag = jObject1.getInt("flag");

                if (ApiResponseFlags.ASSIGNING_DRIVERS.getOrdinal() == flag) {
                    Log.i("ASSIGNING_DRIVERS", "ASSIGNING_DRIVERS");
                    Log.i("defaultCarType", "==" + jObject1.getInt("car_type"));
                    sessionId = jObject1.getString("session_id");
                    Data.defaultCarType = jObject1.getInt("car_type");
                    engagementStatus = EngagementStatus.REQUESTED.getOrdinal();
                } else if (ApiResponseFlags.ENGAGEMENT_DATA.getOrdinal() == flag) {
                    JSONArray lastEngInfoArr = jObject1.getJSONArray("last_engagement_info");
                    JSONObject jObject = lastEngInfoArr.getJSONObject(0);

                    engagementStatus = jObject.getInt("status");

                    if ((1 == engagementStatus) || (2 == engagementStatus)) {
                        engagementId = jObject.getString("engagement_id");
                        sessionId = jObject.getString("session_id");
                        userId = jObject.getString("driver_id");
                        latitude = jObject.getString("current_location_latitude");
                        longitude = jObject.getString("current_location_longitude");
                        driverName = jObject.getString("user_name");
                        driverImage = jObject.getString("user_image");
                        driverCarImage = jObject.getString("driver_car_image");
                        driverPhone = jObject.getString("phone_no");
                        driverRating = jObject.getInt("rating");
                        pickupLatitude = jObject.getString("pickup_latitude");
                        pickupLongitude = jObject.getString("pickup_longitude");
//										Data.defaulterFlag=jObject.getString("defaulter_flag");
                        HomeActivity.customerWaitTime = jObject.getInt("wait_time") * 1000;
                        Data.isWaitStart = jObject.getInt("wait_time_started");
                        if (jObject.has("driver_car_no")) {
                            driverCarNumber = jObject.getString("driver_car_no");
                        }
                        if (jObject.has("free_ride")) {
                            freeRide = jObject.getInt("free_ride");
                        }
                        if (jObject.has("car_type")) {
                            Data.defaultCarType = jObject.getInt("car_type");
                        }

                        if (jObject.has("driver_car_direction_angle")) {
                            driverBearing = jObject.getDouble("driver_car_direction_angle");
                        }
                        if (jObject.has("accept_time")) {
                            Data.rideAcceptedTime = jObject.getString("accept_time").replace("T"," ");
                        }

                        if (FeaturesConfig.isArrivedFeatureEnable) {
                            Data.isArrived = jObject.getInt("is_arrived");
                            HomeActivity.timeTillArrived = jObject.getInt("arrival_time") * 1000;
//											HomeActivity.timeTillArrived=1000;
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            engagementStatus = -1;
            returnResponse = "SERVER_TIMEOUT";
            return returnResponse;
        }


        if (EngagementStatus.REQUESTED.getOrdinal() == engagementStatus) {
            screenMode = Data.P_ASSIGNING;
        } else if (EngagementStatus.ACCEPTED.getOrdinal() == engagementStatus) {
            screenMode = Data.P_REQUEST_FINAL;
        } else if (EngagementStatus.STARTED.getOrdinal() == engagementStatus) {
            screenMode = Data.P_IN_RIDE;
            Log.e("Screen mode mein mach gyi", "" + screenMode);
        } else {
            screenMode = "";
        }


        if ("".equalsIgnoreCase(screenMode)) {

            HomeActivity.passengerScreenMode = PassengerScreenMode.P_INITIAL;
            clearSPData(context);
        } else if (Data.P_ASSIGNING.equalsIgnoreCase(screenMode)) {
            HomeActivity.passengerScreenMode = PassengerScreenMode.P_ASSIGNING;
            Data.cSessionId = sessionId;
            clearSPData(context);
        } else {
            //TODO
            SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);

            Data.cSessionId = sessionId;
            Data.cEngagementId = engagementId;
            Data.cDriverId = userId;

            Data.pickupLatLng = new LatLng(Double.parseDouble(pickupLatitude), Double.parseDouble(pickupLongitude));

            double dLatitude = Double.parseDouble(latitude);
            double dLongitude = Double.parseDouble(longitude);

            String SP_C_DRIVER_DISTANCE = pref.getString(Data.SP_C_DRIVER_DISTANCE, "0");
            String SP_C_DRIVER_DURATION = pref.getString(Data.SP_C_DRIVER_DURATION, "");

            Data.assignedDriverInfo = new DriverInfo(userId, dLatitude, dLongitude, driverName,
                    driverImage, driverCarImage, driverPhone, driverRating, driverCarNumber, freeRide,driverBearing);
            Log.e("Data.assignedDriverInfo on login", "=" + Data.assignedDriverInfo.latLng);
            Data.assignedDriverInfo.distanceToReach = SP_C_DRIVER_DISTANCE;
            Data.assignedDriverInfo.durationToReach = SP_C_DRIVER_DURATION;

            Log.e("Data.assignedDriverInfo.durationToReach in get_current_user_status", "=" + Data.assignedDriverInfo.durationToReach);


            if (Data.P_REQUEST_FINAL.equalsIgnoreCase(screenMode)) {
                HomeActivity.passengerScreenMode = PassengerScreenMode.P_REQUEST_FINAL;
            } else if (Data.P_IN_RIDE.equalsIgnoreCase(screenMode)) {
                HomeActivity.passengerScreenMode = PassengerScreenMode.P_IN_RIDE;

                HomeActivity.totalDistance = Double.parseDouble(pref.getString(Data.SP_TOTAL_DISTANCE, "-1"));

                if (Utils.compareDouble(HomeActivity.totalDistance, -1.0) == 0) {
                    Data.startRidePreviousLatLng = Data.pickupLatLng;
                } else {
                    String lat1 = pref.getString(Data.SP_LAST_LATITUDE, "0");
                    String lng1 = pref.getString(Data.SP_LAST_LONGITUDE, "0");
                    Data.startRidePreviousLatLng = new LatLng(Double.parseDouble(lat1), Double.parseDouble(lng1));
                }

            } else if (Data.P_RIDE_END.equalsIgnoreCase(screenMode)) {
                String SP_C_TOTAL_DISTANCE = pref.getString(Data.SP_C_TOTAL_DISTANCE, "0");
                String SP_C_TOTAL_FARE = pref.getString(Data.SP_C_TOTAL_FARE, "0");
                String SP_C_WAIT_TIME = pref.getString(Data.SP_C_WAIT_TIME, "0");
                String SP_C_RIDE_TIME = pref.getString(Data.SP_C_RIDE_TIME, "0");

                Data.totalDistance = Double.parseDouble(SP_C_TOTAL_DISTANCE);
                Data.totalFare = Double.parseDouble(SP_C_TOTAL_FARE);
                Data.waitTime = SP_C_WAIT_TIME;
                Data.rideTime = SP_C_RIDE_TIME;

                HomeActivity.passengerScreenMode = PassengerScreenMode.P_RIDE_END;
            }

        }


        return returnResponse;
    }


    public void clearSPData(final Context context) {
        SharedPreferences pref = context.getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        Editor editor = pref.edit();

        editor.putString(Data.SP_TOTAL_DISTANCE, "-1");
        editor.putString(Data.SP_WAIT_TIME, "0");
        editor.putString(Data.SP_RIDE_TIME, "0");
        editor.putString(Data.SP_RIDE_START_TIME, "" + System.currentTimeMillis());
        editor.putString(Data.SP_LAST_LATITUDE, "0");
        editor.putString(Data.SP_LAST_LONGITUDE, "0");

        editor.putString(Data.SP_CUSTOMER_SCREEN_MODE, "");

        editor.putString(Data.SP_C_SESSION_ID, "");
        editor.putString(Data.SP_C_ENGAGEMENT_ID, "");
        editor.putString(Data.SP_C_DRIVER_ID, "");
        editor.putString(Data.SP_C_LATITUDE, "0");
        editor.putString(Data.SP_C_LONGITUDE, "0");
        editor.putString(Data.SP_C_DRIVER_NAME, "");
        editor.putString(Data.SP_C_DRIVER_IMAGE, "");
        editor.putString(Data.SP_C_DRIVER_CAR_IMAGE, "");
        editor.putString(Data.SP_C_DRIVER_PHONE, "");
        editor.putString(Data.SP_C_DRIVER_RATING, "");
        editor.putString(Data.SP_C_DRIVER_DISTANCE, "0");
        editor.putString(Data.SP_C_DRIVER_DURATION, "");

        editor.putString(Data.SP_C_TOTAL_DISTANCE, "0");
        editor.putString(Data.SP_C_TOTAL_FARE, "0");
        editor.putString(Data.SP_C_WAIT_TIME, "0");
        editor.putString(Data.SP_C_RIDE_TIME, "0");

        editor.commit();

        Database.getInstance(context).deleteSavedPath();
        Database.getInstance(context).close();

    }

}
