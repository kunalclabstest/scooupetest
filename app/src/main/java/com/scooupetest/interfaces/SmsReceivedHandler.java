package com.scooupetest.interfaces;

/**
 * Created by Nadeem Khan on 6/24/15.
 */
public interface SmsReceivedHandler {
    public void onReceiveOTP(String OTP);
}
