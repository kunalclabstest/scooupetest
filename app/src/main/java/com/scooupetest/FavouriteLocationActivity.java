package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.datastructure.SearchResult;
import com.scooupetest.locationfiles.AutoCompleteSearchResult;
import com.scooupetest.locationfiles.MapUtils;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Prefs;
import com.scooupetest.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import rmn.androidscreenlibrary.ASSL;


public class FavouriteLocationActivity extends BaseActivity {

    RelativeLayout rootLayout;
    ASSL assl;
    ListView listViewSearch;
    EditText etLocation;
    Thread autoCompleteThread;
    RelativeLayout relClearSearchLocation;
    Boolean refreshingAutoComplete = false;
    Button backBtn;
    ProgressBar searchLocationProgress;
    TextView tvLocation,title;
    Button removeFavouriteButton;
    ImageView imgLocationIcon;
    DialogPopup saveDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    ArrayList<AutoCompleteSearchResult> autoCompleteSearchResults = new ArrayList<AutoCompleteSearchResult>();
    SearchListAdapter searchListAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_location);
        rootLayout = (RelativeLayout) findViewById(R.id.rootRelative);
        assl = new ASSL(FavouriteLocationActivity.this, rootLayout, 1134, 720, false);
        listViewSearch = (ListView) findViewById(R.id.listViewSearch);
        relClearSearchLocation = (RelativeLayout) findViewById(R.id.relClearSearchLocation);
        etLocation = (EditText) findViewById(R.id.etLocation);
        etLocation.setTypeface(Data.getFont(getApplicationContext()));
        searchListAdapter = new SearchListAdapter();
        backBtn = (Button) findViewById(R.id.backBtn);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        tvLocation.setTypeface(Data.getFont(getApplicationContext()));
        searchLocationProgress = (ProgressBar) findViewById(R.id.progressBarSearch);
        searchLocationProgress.setVisibility(View.GONE);
        imgLocationIcon = (ImageView) findViewById(R.id.imgLocationIcon);
        imgLocationIcon.setVisibility(View.VISIBLE);
        pref = getApplicationContext().getSharedPreferences(Data.SETTINGS_SHARED_PREF_NAME, 0);
        editor = pref.edit();
        saveDialog = new DialogPopup();
        listViewSearch.setAdapter(searchListAdapter);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etLocation, InputMethodManager.SHOW_IMPLICIT);
        removeFavouriteButton = (Button) findViewById(R.id.removeFavouriteButton);
        removeFavouriteButton.setTypeface(Data.getFont(getApplicationContext()));
        if (Data.fromFavouriteHome) {
            removeFavouriteButton.setText(getString(R.string.remove_home));
            tvLocation.setText(getString(R.string.home_location));
            if (Data.favouriteHome.equalsIgnoreCase("")) {
                removeFavouriteButton.setVisibility(View.GONE);
                etLocation.append(Data.currentPickupText);
            } else {
                etLocation.append(Data.favouriteHome);
                removeFavouriteButton.setVisibility(View.VISIBLE);
            }
        } else {
            removeFavouriteButton.setText(getString(R.string.remove_work));
            tvLocation.setText(getString(R.string.work_location));
            if (Data.favouriteWork.equalsIgnoreCase("")) {
                removeFavouriteButton.setVisibility(View.GONE);
                etLocation.append(Data.currentPickupText);
            } else {
                etLocation.append(Data.favouriteWork);
                removeFavouriteButton.setVisibility(View.VISIBLE);
            }
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data.fromFavouriteHome = false;
                Data.fromFavouriteWork = false;
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        removeFavouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                removeFavouriteDialog(FavouriteLocationActivity.this,"Are you sure you want to remove this location?");
            }
        });

        etLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                autoCompleteSearchResults.clear();
                searchListAdapter.notifyDataSetChanged();
                Log.i("search text", "==" + s.toString().trim());
                if (s.length() > 0) {
                    getSearchResults(s.toString().trim(), Data.pickupLatLng);
                    relClearSearchLocation.setVisibility(View.VISIBLE);
                } else {
                    relClearSearchLocation.setVisibility(View.GONE);
                }
            }
        });

        etLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.requestFocus();
                Utils.showSoftKeyboard(FavouriteLocationActivity.this, etLocation);
            }
        });
        relClearSearchLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                etLocation.setText("");
            }
        });

    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        autoCompleteSearchResults.clear();
        getSearchResults(etLocation.getText().toString(),Data.pickupLatLng);
    }


    class ViewHolderSearchItem {
        TextView textViewSearchName, textViewSearchAddress;
        LinearLayout relative;
        int id;
    }

    class SearchListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ViewHolderSearchItem holder;

        public SearchListAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return autoCompleteSearchResults.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new ViewHolderSearchItem();
                convertView = mInflater.inflate(R.layout.list_item_search_item, null);
                holder.textViewSearchName = (TextView) convertView.findViewById(R.id.textViewSearchName);
                holder.textViewSearchName.setTypeface(Data.getFont(FavouriteLocationActivity.this));
                holder.textViewSearchAddress = (TextView) convertView.findViewById(R.id.textViewSearchAddress);
                holder.textViewSearchAddress.setTypeface(Data.getFont(FavouriteLocationActivity.this));
                holder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                holder.relative.setTag(holder);
                holder.relative.setLayoutParams(new ListView.LayoutParams(720, ViewGroup.LayoutParams.WRAP_CONTENT));
                ASSL.DoMagic(holder.relative);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolderSearchItem) convertView.getTag();
            }


            holder.id = position;

            try {
                holder.textViewSearchName.setText(autoCompleteSearchResults.get(position).name);
                holder.textViewSearchAddress.setText(autoCompleteSearchResults.get(position).address);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.relative.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder = (ViewHolderSearchItem) v.getTag();
                    Utils.hideSoftKeyboard(FavouriteLocationActivity.this, etLocation);
                    AutoCompleteSearchResult autoCompleteSearchResult = autoCompleteSearchResults.get(holder.id);
                    if (!"".equalsIgnoreCase(autoCompleteSearchResult.placeId)) {
                        if (Data.fromFavouriteHome) {
                            Data.favouriteHome = autoCompleteSearchResult.name;
                            getSearchResultFromPlaceId(autoCompleteSearchResult.placeId);
                            Prefs.with(getApplicationContext()).save("favourite_home", autoCompleteSearchResult.name);

                        }
                        if (Data.fromFavouriteWork) {
                            Data.favouriteWork = autoCompleteSearchResult.name;
                            getSearchResultFromPlaceId(autoCompleteSearchResult.placeId);
                            Prefs.with(getApplicationContext()).save("favourite_work", autoCompleteSearchResult.name);

                        }

                    }
                }
            });
            return convertView;
        }

        @Override
        public void notifyDataSetChanged() {
            try {

                if (autoCompleteSearchResults.size() > 1) {
                    if (autoCompleteSearchResults.contains(new AutoCompleteSearchResult("No results found", "", ""))) {
                        autoCompleteSearchResults.remove(autoCompleteSearchResults.indexOf(new AutoCompleteSearchResult("No results found", "", "")));
                    }
                }
                super.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public synchronized void getSearchResultFromPlaceId(final String placeId) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                SearchResult searchResult = MapUtils.getSearchResultsFromPlaceIdGooglePlaces(placeId, FavouriteLocationActivity.this);
                if(Data.fromFavouriteHome){
                    Prefs.with(getApplicationContext()).save("fav_home_latLng", searchResult.latLng);
                    finish();
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }else{
                    Prefs.with(getApplicationContext()).save("fav_work_latLng", searchResult.latLng);
                    finish();
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            }
        }).start();
    }
    public synchronized void getSearchResults(final String searchText, final LatLng latLng) {
        try {
            if (!refreshingAutoComplete) {

                searchLocationProgress.setVisibility(View.VISIBLE);
                imgLocationIcon.setVisibility(View.GONE);

                if (autoCompleteThread != null) {
                    autoCompleteThread.interrupt();
                }

                autoCompleteThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        refreshingAutoComplete = true;
                        autoCompleteSearchResults.addAll(MapUtils.getAutoCompleteSearchResultsFromGooglePlaces(searchText, latLng, FavouriteLocationActivity.this));
                        Log.i("setSearchResultsToList", "==");
                        refreshingAutoComplete = false;
                        recallLatestTextSearch(searchText, latLng);
                        setSearchResultsToList();
                        autoCompleteThread = null;


                    }
                });
                autoCompleteThread.start();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Data.fromFavouriteHome = false;
        Data.fromFavouriteWork = false;
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    public synchronized void recallLatestTextSearch(final String searchText, final LatLng latLng) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String currentText = etLocation.getText().toString().trim();
                    if (searchText.equalsIgnoreCase(currentText)) {

                    } else {

                        if (currentText.length() > 0) {
                            autoCompleteSearchResults.clear();
                            searchListAdapter.notifyDataSetChanged();
                            Log.i("currentText", "==" + currentText);
                            getSearchResults(currentText, latLng);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public synchronized void setSearchResultsToList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try {
                    if (etLocation.getText().toString().equalsIgnoreCase("")) {
                        autoCompleteSearchResults.clear();
                    }
                    searchListAdapter.notifyDataSetChanged();
                    searchLocationProgress.setVisibility(View.GONE);
                    imgLocationIcon.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Displays Remove Favourites alert
     */
    public void removeFavouriteDialog(final Activity activity, String message) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, false);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));

            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

            textHead.setVisibility(View.GONE);
            textMessage.setText(message);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);
            btnOk.setText(getString(R.string.yes));
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity), Typeface.BOLD);
            btnCancel.setText(getString(R.string.no));
            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setVisibility(View.GONE);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    if(removeFavouriteButton.getText().toString().equalsIgnoreCase("Remove Home")){
                        Data.favouriteHome = "";
                        HomeActivity.favouriteSelected= false;
                        HomeActivity.removedFavourite = true;
                        Prefs.with(getApplicationContext()).remove("favourite_home");
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }else if(removeFavouriteButton.getText().toString().equalsIgnoreCase("Remove Work")) {
                        Data.favouriteWork = "";
                        HomeActivity.favouriteSelected= false;
                        HomeActivity.removedFavourite = true;
                        Prefs.with(getApplicationContext()).remove("favourite_work");
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.findViewById(R.id.innerRl).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                }
            });

            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
