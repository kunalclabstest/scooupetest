package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.customlayouts.ProgressWheel;
import com.scooupetest.locationfiles.LocationFetcher;
import com.scooupetest.locationfiles.LocationUpdate;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.DeviceTokenGenerator;
import com.scooupetest.utils.FlurryEventLogger;
import com.scooupetest.utils.IDeviceTokenReceiver;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Prefs;
import com.scooupetest.utils.UniqueIMEIID;
import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONObject;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class SplashNewActivity extends BaseActivity implements LocationUpdate {

    public static Dialog restartPhoneDialog;
    LinearLayout relative, linearServerFail;
    RelativeLayout layoutNoInternet;
    TextView tvNoInternet;
    ProgressWheel progressBar1;
    public boolean loginDataFetched=false;


//	GoogleCloudMessaging gcm;
    Button buttonLogin, buttonRegister, btnCallUs;
    boolean noNetFirstTime = false, noNetSecondTime = false;
    Handler checkNetHandler = new Handler();
    Runnable checkNetRunnable = new Runnable() {

        @Override
        public void run() {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                        noNetSecondTime = false;
                        linearServerFail.setVisibility(View.GONE);

                        accessTokenLogin(SplashNewActivity.this);
                        FlurryEventLogger.appStarted(Data.deviceToken);
                    } else {
                        linearServerFail.setVisibility(View.VISIBLE);
                        tvNoInternet.setText(getString(R.string.no_internet_connection));
//                        new DialogPopup().alertPopup(SplashNewActivity.this, "", Data.CHECK_INTERNET_MSG);
                        progressBar1.setVisibility(View.GONE);
                        noNetSecondTime = true;

                    }

                }
            });

        }
    };

    public static  Boolean isNewUpdateAvailable=false;
    public static boolean checkIfUpdate(JSONObject jObj, Activity activity) throws Exception {
//		"popup": {
//	        "title": "Update Version",
//	        "text": "Update app with new version!",
//	        "cur_version": 116,			// could be used for local check
//	        "is_force": 1				// 1 for forced, 0 for not forced
//	}
        if (!jObj.isNull("popup")) {
            try {
                JSONObject jupdatePopupInfo = jObj.getJSONObject("popup");
                String title = jupdatePopupInfo.getString("Update");
                String text = jupdatePopupInfo.getString("text");
                int currentVersion = jupdatePopupInfo.getInt("cur_version");
                int isForce = jupdatePopupInfo.getInt("is_force");

                if (Data.appVersion >= currentVersion) {
                    return false;
                } else {
                    DialogPopup.dismissLoadingDialog();
                    isNewUpdateAvailable=true;
                    SplashNewActivity.appUpdatePopup(title, text, isForce, activity);
                    if (isForce == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Displays appUpdatePopup dialog
     */
    public static void appUpdatePopup(String title, String message, final int isForced, final Activity activity) {
        try {

            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_two_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, false);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            ImageView imgHorizontalLine = (ImageView) dialog.findViewById(R.id.imgHorizontalLine);

            imgHorizontalLine.setVisibility(View.VISIBLE);
            textMessage.setMovementMethod(new ScrollingMovementMethod());
            textMessage.setMaxHeight((int) (800.0f * ASSL.Yscale()));

			textHead.setText(title);
            textHead.setVisibility(View.VISIBLE);
            textMessage.setText(message);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity), Typeface.BOLD);

            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));
            if (isForced == 1) {
                btnCancel.setVisibility(View.GONE);
            } else {
                btnCancel.setVisibility(View.VISIBLE);
            }
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
//                    activity.finish();
                }
            });


            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Data.isOpenGooglePlay = true;
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + Data.packageName));
                    activity.startActivity(intent);
                    activity.finish();
                    activity.overridePendingTransition(R.anim.right_in, R.anim.right_out);
                }

            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,getString(R.string.flurry_key));
        FlurryAgent.onEvent("Splash started");
    }


    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        SharedPreferences preferences = getSharedPreferences(Data.SETTINGS_SHARED_PREF_NAME, 0);
        String link = preferences.getString(Data.SP_SERVER_LINK, Data.DEFAULT_SERVER_URL);
        Data.favouriteHome = Prefs.with(getApplicationContext()).getString("favourite_home", "");
        Data.favouriteWork = Prefs.with(getApplicationContext()).getString("favourite_work","");
        Data.SERVER_URL = Data.DEFAULT_SERVER_URL;

        if (link.equalsIgnoreCase(Data.TRIAL_SERVER_URL)) {
            Data.SERVER_URL = Data.TRIAL_SERVER_URL;
        } else if (link.equalsIgnoreCase(Data.LIVE_SERVER_URL)) {
            Data.SERVER_URL = Data.LIVE_SERVER_URL;
        } else if (link.equalsIgnoreCase(Data.DEV_SERVER_URL)) {
            Data.SERVER_URL = Data.DEV_SERVER_URL;
        }


		String languageSelected = Prefs.with(getApplicationContext()).getString(Data.SP_SELECTED_LANGUAGE,"default");
		if(!"default".equalsIgnoreCase(languageSelected)){
			Locale locale = new Locale(languageSelected);
		    Locale.setDefault(locale);
		    Configuration config = new Configuration();
		    config.locale = locale;
		    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		}


        setContentView(R.layout.splash_new);

        if (Data.locationFetcher == null) {
            Data.locationFetcher = new LocationFetcher(SplashNewActivity.this, 1000, 1);
        }


        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(SplashNewActivity.this, relative, 1134, 720, false);

        progressBar1 = ((ProgressWheel) findViewById(R.id.progress_wheel));
        progressBar1.spin();
        progressBar1.setVisibility(View.GONE);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        buttonRegister.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        buttonLogin.setVisibility(View.GONE);
        buttonRegister.setVisibility(View.GONE);


        buttonLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashNewActivity.this, SplashLogin.class));
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RegisterScreen.facebookLogin = false;
                startActivity(new Intent(SplashNewActivity.this, RegisterScreen.class));
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });

        linearServerFail = (LinearLayout) findViewById(R.id.linearServerFail);
        layoutNoInternet = (RelativeLayout) findViewById(R.id.layoutNoInternet);
        tvNoInternet = (TextView) findViewById(R.id.tvNoInternet);
        tvNoInternet.setTypeface(Data.getFont(getApplicationContext()));
        btnCallUs = (Button) findViewById(R.id.btnCall);
        btnCallUs.setTypeface(Data.getFont(getApplicationContext()));
        btnCallUs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + getString(R.string.customer_care_no)));
                startActivity(callIntent);
            }
        });

        TextView changeUrlTv = (TextView) findViewById(R.id.changeUrlTv);
        changeUrlTv.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                confirmDebugPasswordPopup(SplashNewActivity.this);
                FlurryEventLogger.debugPressed("no_token");
                return false;
            }
        });


        try {                                                                                        // to get AppVersion, OS version, country code and device name
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            Data.appVersion = pInfo.versionCode;
            Data.packageName = pInfo.packageName;
            Log.i("appVersion", Data.appVersion + "..");
            Data.osVersion = android.os.Build.VERSION.RELEASE;
            Log.i("osVersion", Data.osVersion + "..");
            Data.country = getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry(Locale.getDefault());
            Log.i("countryCode", Data.country + "..");
            Data.deviceName = (android.os.Build.MANUFACTURER + android.os.Build.MODEL).toString();
            Log.i("deviceName", Data.deviceName + "..");
            Data.uniqueDeviceId = UniqueIMEIID.getUniqueIMEIId(this);
            Log.e("Data.uniqueDeviceId = ", "=" + Data.uniqueDeviceId);

        } catch (Exception e) {
            Log.e("error in fetching appversion and gcm key", ".." + e.toString());
        }
        Log.generateKeyHash(getApplicationContext());

        noNetFirstTime = false;
        noNetSecondTime = false;

        noNetFirstTime = true;
        getDeviceToken();



        layoutNoInternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                noNetFirstTime = false;
                noNetSecondTime = false;
                getDeviceToken();
            }
        });


    }

    public void getDeviceToken() {
        progressBar1.setVisibility(View.VISIBLE);
        new DeviceTokenGenerator(SplashNewActivity.this).generateDeviceToken(SplashNewActivity.this, new IDeviceTokenReceiver() {

            @Override
            public void deviceTokenReceived(final String regId) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Data.deviceToken = regId;
                        Log.e("deviceToken in IDeviceTokenReceiver", Data.deviceToken + "..");
                        callFirstAttempt();

                    }
                });

            }
        });
    }

    @Override
    protected void onResume() {
        if (Data.locationFetcher == null) {
            Data.locationFetcher = new LocationFetcher(SplashNewActivity.this, 1000, 1);
        }

        int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resp != ConnectionResult.SUCCESS) {
            Log.e("Google Play Service Error ", "=" + resp);
            new DialogPopup().showGooglePlayErrorAlert(SplashNewActivity.this);
        } else {
            new DialogPopup().showLocationSettingsAlert(SplashNewActivity.this);
        }


        super.onResume();
    }

    @Override
    protected void onPause() {
        try {
            Data.locationFetcher.destroy();
            Data.locationFetcher = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    public void callFirstAttempt() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                    noNetFirstTime = false;
                    linearServerFail.setVisibility(View.GONE);
                    accessTokenLogin(SplashNewActivity.this);
                } else {
                    linearServerFail.setVisibility(View.VISIBLE);
                    tvNoInternet.setText(getString(R.string.no_internet_connection));
//                    new DialogPopup().alertPopup(SplashNewActivity.this, "", Data.CHECK_INTERNET_MSG);
                    progressBar1.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * ASync for access token login from server
     */
    public void accessTokenLogin(final Activity activity) {

        long startTime = System.currentTimeMillis();
        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        final String accessToken = pref.getString(Data.SP_ACCESS_TOKEN_KEY, "");
        if (!"".equalsIgnoreCase(accessToken)) {
            buttonLogin.setVisibility(View.GONE);
            buttonRegister.setVisibility(View.GONE);
            if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {

                progressBar1.setVisibility(View.VISIBLE);

                if (Data.locationFetcher != null) {
                    Data.latitude = Data.locationFetcher.getLatitude();
                    Data.longitude = Data.locationFetcher.getLongitude();
                }

                Double latitude, longitude;

                    latitude = Data.latitude;
                    longitude = Data.longitude;

                RestClient.getApiService().accessTokenLogin(accessToken, Data.deviceToken, latitude, longitude, Data.appVersion, Data.DEVICE_TYPE, Data.uniqueDeviceId, new Callback<String>() {
                    @Override
                    public void success(String response, Response res) {

                        Log.e("Server response of access_token", "response = " + response);

                        try {
                            JSONObject jObj = new JSONObject(response);

                            boolean newUpdate = false;
                            if (jObj.has("login")) {
                                newUpdate = SplashNewActivity.checkIfUpdate(jObj.getJSONObject("login"), activity);
                            }

                            if (!newUpdate) {
                                if (!jObj.isNull("error")) {
                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (0 == flag) {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (1 == flag) {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (8 == flag) {
                                        noNetFirstTime = false;
                                        noNetSecondTime = false;


                                        startActivity(new Intent(SplashNewActivity.this, SplashLogin.class));
                                        finish();
                                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                    progressBar1.setVisibility(View.GONE);
                                } else {

                                    new AccessTokenDataParseAsync(activity, response, accessToken).execute();

                                }
                            } else {
                                progressBar1.setVisibility(View.GONE);
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                            progressBar1.setVisibility(View.GONE);
                            linearServerFail.setVisibility(View.VISIBLE);
                            tvNoInternet.setText(getString(R.string.unable_to_reach_server));
//                            new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            DialogPopup.dismissLoadingDialog();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // Log.e("request fail", arg3.toString());
                        progressBar1.setVisibility(View.GONE);
                        linearServerFail.setVisibility(View.VISIBLE);
                        tvNoInternet.setText(getString(R.string.unable_to_reach_server));
                        DialogPopup.dismissLoadingDialog();
                    }
                });


            } else {
                linearServerFail.setVisibility(View.VISIBLE);
                tvNoInternet.setText(getString(R.string.no_internet_connection));
                progressBar1.setVisibility(View.GONE);
            }
        } else {
            linearServerFail.setVisibility(View.GONE);
            progressBar1.setVisibility(View.GONE);
            buttonLogin.setVisibility(View.VISIBLE);
            buttonRegister.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);


        if (hasFocus && Data.isOpenGooglePlay) {
            Data.isOpenGooglePlay = false;
        }
        else if(hasFocus &&loginDataFetched)
        {
            startActivity(new Intent(SplashNewActivity.this, HomeActivity.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            loginDataFetched=false;

        }else if (hasFocus && noNetFirstTime) {
            noNetFirstTime = false;
            checkNetHandler.postDelayed(checkNetRunnable, 4000);
        } else if (hasFocus && noNetSecondTime) {
            noNetSecondTime = false;
            //finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }

    //TODO debug code confirm popup
    public void confirmDebugPasswordPopup(final Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.edittext_confirm_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity));
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));
            final EditText etCode = (EditText) dialog.findViewById(R.id.etCode);
            etCode.setTypeface(Data.getFont(activity));

            textHead.setText("Confirm Debug Password");
            textMessage.setText("Please enter password to continue.");

            textHead.setVisibility(View.GONE);
            textMessage.setVisibility(View.GONE);


            final Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
            btnConfirm.setTypeface(Data.getFont(activity));
            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String code = etCode.getText().toString().trim();
                    if ("".equalsIgnoreCase(code)) {
                        etCode.requestFocus();
                        etCode.setError("Code can't be empty.");
                    } else {
                        if (Data.DEBUG_PASSWORD.equalsIgnoreCase(code)) {
                            dialog.dismiss();
                            changeServerLinkPopup(activity);
                        } else {
                            etCode.requestFocus();
                            etCode.setError("Code not matched.");
                        }
                    }
                }

            });


            etCode.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    int result = actionId & EditorInfo.IME_MASK_ACTION;
                    switch (result) {
                        case EditorInfo.IME_ACTION_DONE:
                            btnConfirm.performClick();
                            break;

                        case EditorInfo.IME_ACTION_NEXT:
                            break;

                        default:
                    }
                    return true;
                }
            });

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }

            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //TODO change server link popup
    void changeServerLinkPopup(final Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.setContentView(R.layout.custom_three_btn_dialog);

            FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.rv);
            new ASSL(activity, frameLayout, 1134, 720, true);

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = Data.dialogDimAmount;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            RelativeLayout innerRl = (RelativeLayout) dialog.findViewById(R.id.innerRl);
            innerRl.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                }
            });


            TextView textHead = (TextView) dialog.findViewById(R.id.textHead);
            textHead.setTypeface(Data.getFont(activity), Typeface.BOLD);
            TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
            textMessage.setTypeface(Data.getFont(activity));


            SharedPreferences preferences = activity.getSharedPreferences(Data.SETTINGS_SHARED_PREF_NAME, 0);
            String link = preferences.getString(Data.SP_SERVER_LINK, Data.DEFAULT_SERVER_URL);

            if (link.equalsIgnoreCase(Data.TRIAL_SERVER_URL)) {
                textMessage.setText("Current server is SALES.\nChange to:");
            } else if (link.equalsIgnoreCase(Data.LIVE_SERVER_URL)) {
                textMessage.setText("Current server is LIVE.\nChange to:");
            } else if (link.equalsIgnoreCase(Data.DEV_SERVER_URL)) {
                textMessage.setText("Current server is DEV.\nChange to:");
            }


            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setTypeface(Data.getFont(activity));
            btnOk.setText("LIVE");

            Button btnNeutral = (Button) dialog.findViewById(R.id.btnNeutral);
            btnNeutral.setTypeface(Data.getFont(activity));
            btnNeutral.setText("DEV");

            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setTypeface(Data.getFont(activity));
            btnCancel.setText("SALES");

            Button crossbtn = (Button) dialog.findViewById(R.id.crossbtn);
            crossbtn.setTypeface(Data.getFont(activity));
            crossbtn.setVisibility(View.VISIBLE);


            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences preferences = activity.getSharedPreferences(Data.SETTINGS_SHARED_PREF_NAME, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Data.SP_SERVER_LINK, Data.LIVE_SERVER_URL);
                    editor.commit();

                    Data.SERVER_URL = Data.LIVE_SERVER_URL;
                    dialog.dismiss();
                }
            });

            btnNeutral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences preferences = activity.getSharedPreferences(Data.SETTINGS_SHARED_PREF_NAME, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Data.SP_SERVER_LINK, Data.DEV_SERVER_URL);
                    editor.commit();

                    Data.SERVER_URL = Data.DEV_SERVER_URL;
                    dialog.dismiss();
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences preferences = activity.getSharedPreferences(Data.SETTINGS_SHARED_PREF_NAME, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Data.SP_SERVER_LINK, Data.TRIAL_SERVER_URL);
                    editor.commit();

                    Data.SERVER_URL = Data.TRIAL_SERVER_URL;
                    dialog.dismiss();
                }
            });


            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location, int priority) {
        Data.latitude = location.getLatitude();
        Data.longitude = location.getLongitude();

    }

    class AccessTokenDataParseAsync extends AsyncTask<String, Integer, String> {

        Activity activity;
        String response, accessToken;

        public AccessTokenDataParseAsync(Activity activity, String response, String accessToken) {
            this.activity = activity;
            this.response = response;
            this.accessToken = accessToken;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String resp = new JSONParser().parseAccessTokenLoginData(activity, response, accessToken);
                return resp;
            } catch (Exception e) {
                e.printStackTrace();
                return "SERVER_TIMEOUT";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.contains("SERVER_TIMEOUT")) {
                linearServerFail.setVisibility(View.VISIBLE);
                tvNoInternet.setText(getString(R.string.unable_to_reach_server));
            } else {

                noNetFirstTime = false;
                noNetSecondTime = false;
                loginDataFetched=true;
                if(!isNewUpdateAvailable)
                {
                    startActivity(new Intent(SplashNewActivity.this, HomeActivity.class));
                    finish();
                    overridePendingTransition(R.anim.right_in, R.anim.right_out);
                    loginDataFetched=false;
                    isNewUpdateAvailable=false;
                }

            }


            progressBar1.setVisibility(View.GONE);
        }

    }


}
