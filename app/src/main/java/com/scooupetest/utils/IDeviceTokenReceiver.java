package com.scooupetest.utils;

public interface IDeviceTokenReceiver {
    public void deviceTokenReceived(String regId);
}
