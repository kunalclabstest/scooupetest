package com.scooupetest.utils;

import android.support.v4.app.FragmentActivity;

import com.scooupetest.Data;

public class BaseActivity extends FragmentActivity {
    @Override
    protected void onResume() {
        Data.context = this;
        super.onResume();
    }

    @Override
    protected void onPause() {
        Data.context = null;
        super.onPause();
    }
}
