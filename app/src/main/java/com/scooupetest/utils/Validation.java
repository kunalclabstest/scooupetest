package com.scooupetest.utils;

/**
 * This class is use for all the basic validation which we use in project
 *
 * @author Eshant Mittal
 */

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.widget.EditText;

public class Validation {

    // This TextWatcher sub-class formats entered numbers as 1 (123) 456-7890
    private boolean mFormatting; // this is a flag which prevents the
    // stack(onTextChanged)
    private boolean clearFlag;
    private int mLastStartLocation;
    private String mLastBeforeText;
    private InputFilter[] filterNameValidation = new InputFilter[]{new InputFilter() {
        public CharSequence filter(CharSequence src, int start, int end,
                                   Spanned dst, int dstart, int dend) {
            if (src.equals("")) { // for backspace
                return src;
            }
            if (src.toString().matches("[a-zA-Z ]+")) {
                return src;
            }
            return "";
        }
    }};

    /**
     * This is a method for various validation like Password, FirstName,LastName,FullName,PhoneNumber
     *
     *
     * In this method we have to pass two
     *
     *
     * @param textType
     * 					Password for space validation means no space will be entered in password field.
     * 					FirstName for space validation and no any special character will be entered.
     * 					LastName for space validation and no any special character will be entered.
     * 					FullName no any special character will be entered.
     * 					PhoneNumber entered numbers as 1 (123) 456-7890 or (999) 999-9999 .
     *
     *
     * @param editTexts
     * 					 pass the edittext name on which the validation has to be check.
     * 					 You can also pass more than 1 edittext in one call.
     */

    public void setValidationFilter(TextType textType, EditText... editTexts) {
        switch (textType) {
            case Password:
                editTextNoSpace(editTexts);
                break;
            case FirstName:
            case LastName:
                editTextNoSpace(editTexts);
                editTextNameFilter(editTexts);
                break;
            case FullName:
                editTextNameFilter(editTexts);
                break;
            case PhoneNumber:
                for (final EditText editText : editTexts) {
                    usPhoneNumberFormatter(editText);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param editTexts
     */
    private void editTextNoSpace(EditText... editTexts) {

        for (final EditText editText : editTexts)

            editText.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    // TODO Auto-generated method stub

                    String result = s.toString().replaceAll(" ", "");

                    if (!s.toString().equals(result)) {

                        editText.setText(result);

                        editText.setSelection(result.length());

                    }
                }

            });
    }

    /**
     *
     * @param editTextName
     */
    private void editTextNameFilter(EditText... editTextName) {
        for (final EditText editTexts : editTextName) {
            editTexts.setFilters(filterNameValidation);

        }
    }

    /**
     *
     * @param mWeakEditText
     */
    private void usPhoneNumberFormatter(final EditText mWeakEditText) {
        mWeakEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (after == 0 && s.toString().equals("1 ")) {
                    clearFlag = true;
                }
                mLastStartLocation = start;
                mLastBeforeText = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().length() == 1) {
                    if (s.toString().equals("1")) {
                        mWeakEditText
                                .setFilters(new InputFilter[]{new InputFilter.LengthFilter(
                                        16)});
                    } else {
                        mWeakEditText
                                .setFilters(new InputFilter[]{new InputFilter.LengthFilter(
                                        14)});
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Make sure to ignore calls to afterTextChanged caused by the
                // work
                // done below
                if (!mFormatting) {
                    mFormatting = true;
                    int curPos = mLastStartLocation;
                    String beforeValue = mLastBeforeText;
                    String currentValue = s.toString();
                    String formattedValue = formatUsNumber(s);
                    if (currentValue.length() > beforeValue.length()) {
                        int setCusorPos = formattedValue.length()
                                - (beforeValue.length() - curPos);
                        mWeakEditText.setSelection(setCusorPos < 0 ? 0
                                : setCusorPos);
                    } else {
                        int setCusorPos = formattedValue.length()
                                - (currentValue.length() - curPos);
                        if (setCusorPos > 0
                                && !Character.isDigit(formattedValue
                                .charAt(setCusorPos - 1))) {
                            setCusorPos--;
                        }
                        mWeakEditText.setSelection(setCusorPos < 0 ? 0
                                : setCusorPos);
                    }
                    mFormatting = false;
                }
            }
        });
    }

    private String formatUsNumber(Editable text) {
        StringBuilder formattedString = new StringBuilder();
        // Remove everything except digits
        int p = 0;
        while (p < text.length()) {
            char ch = text.charAt(p);
            if (!Character.isDigit(ch)) {
                text.delete(p, p + 1);
            } else {
                p++;
            }
        }
        // Now only digits are remaining
        String allDigitString = text.toString();

        int totalDigitCount = allDigitString.length();

        if (totalDigitCount == 0
                || (totalDigitCount > 10 && !allDigitString.startsWith("1"))
                || totalDigitCount > 11) {
            // May be the total length of input length is greater than the
            // expected value so we'll remove all formatting
            text.clear();
            text.append(allDigitString);
            return allDigitString;
        }
        int alreadyPlacedDigitCount = 0;
        // Only '1' is remaining and user pressed backspace and so we clear
        // the edit text.
        if (allDigitString.equals("1") && clearFlag) {
            text.clear();
            clearFlag = false;
            return "";
        }
        if (allDigitString.startsWith("1")) {
            formattedString.append("1 ");
            alreadyPlacedDigitCount++;
        }
        // The first 3 numbers beyond '1' must be enclosed in brackets "()"
        if (totalDigitCount - alreadyPlacedDigitCount > 3) {
            formattedString.append("("
                    + allDigitString.substring(alreadyPlacedDigitCount,
                    alreadyPlacedDigitCount + 3) + ") ");
            alreadyPlacedDigitCount += 3;
        }
        // There must be a '-' inserted after the next 3 numbers
        if (totalDigitCount - alreadyPlacedDigitCount > 3) {
            formattedString
                    .append(allDigitString.substring(alreadyPlacedDigitCount,
                            alreadyPlacedDigitCount + 3) + "-");
            alreadyPlacedDigitCount += 3;
        }
        // All the required formatting is done so we'll just copy the
        // remaining digits.
        if (totalDigitCount > alreadyPlacedDigitCount) {
            formattedString.append(allDigitString
                    .substring(alreadyPlacedDigitCount));
        }

        text.clear();
        text.append(formattedString.toString());
        return formattedString.toString();
    }

    public enum TextType {
        Password, FirstName, LastName, FullName, PhoneNumber
    }

}
