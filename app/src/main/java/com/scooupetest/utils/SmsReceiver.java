package com.scooupetest.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.scooupetest.OTPConfirmScreen;
import com.scooupetest.R;

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //---get the SMS message passed in---
        Log.i("REceive bro", "==");
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;

        try {
            if (bundle != null) {

                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

                    if (msgs[i].getMessageBody().toString().toLowerCase().contains(context.getString(R.string.otp_sms_keyword).toLowerCase())) {
                        if (OTPConfirmScreen.smsReceivedHandler != null) {
                            try {
                                String message = msgs[i].getMessageBody().toString();
                                String otp = message.substring(message.substring(0, message.indexOf(".")).lastIndexOf(" ") + 1, message.indexOf("."));
                                OTPConfirmScreen.smsReceivedHandler.onReceiveOTP(otp);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
