package com.scooupetest.utils;


public class FeaturesConfig {
    public static boolean isArrivedFeatureEnable = true;
    public static boolean isSosEnable = true;

    //payment method
    public static boolean cashOnly = true;
    public static boolean cardOnly = false;
    public static boolean cashCardBoth = false;
    public static boolean isCorporateAccountEnable = false;
    public static boolean isAutomaticPaymentEnable = true;
    public static boolean isTipEnable = true;
    public static boolean isChangeLanguagesEnable = false;


}
