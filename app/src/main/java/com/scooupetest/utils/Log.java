package com.scooupetest.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Environment;
import android.util.Base64;

import com.scooupetest.Data;

import java.io.File;
import java.io.FileWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Custom log class overrides Android Log
 *
 * @author shankar
 */
public class Log {


    private static final boolean PRINT = true;                                            // true for printing and false for not
    private static final boolean WRITE_TO_FILE = false;                                    // true for writing log to file and false for not
    private static final boolean WRITE_TO_FILE_IN = false;                                    // true for writing log to file and false for not
    static String LOG_FILE = "LOGFILE";

    public Log() {
    }

    public static void i(String tag, String message) {
        if (PRINT) {
            android.util.Log.i(tag, message);
            if (WRITE_TO_FILE) {
                writeLogToFile(Data.APP_NAME, tag + "<>" + message);
            }
        }
    }

    public static void d(String tag, String message) {
        if (PRINT) {
            android.util.Log.d(tag, message);
            if (WRITE_TO_FILE) {
                writeLogToFile(Data.APP_NAME, tag + "<>" + message);
            }
        }
    }

    public static void e(String tag, String message) {
        if (PRINT) {
            android.util.Log.e(tag, message);
            if (WRITE_TO_FILE) {
                writeLogToFile(Data.APP_NAME, tag + "<>" + message);
            }
        }
    }

    public static void v(String tag, String message) {
        if (PRINT) {
            android.util.Log.v(tag, message);
            if (WRITE_TO_FILE) {
                writeLogToFile(Data.APP_NAME, tag + "<>" + message);
            }
        }
    }


    public static void writeLogToFile(final String filePrefix, final String response) {
        if (WRITE_TO_FILE_IN) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        String fileName = Environment.getExternalStorageDirectory() + "/" + filePrefix + "_" + LOG_FILE + ".txt";
                        File gpxfile = new File(fileName);

                        if (!gpxfile.exists()) {
                            gpxfile.createNewFile();
                        }

                        FileWriter writer = new FileWriter(gpxfile, true);
                        writer.append("\n" + response);
                        writer.flush();
                        writer.close();

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }).start();
        }

    }
    public static void generateKeyHash(Context context) {
        if (PRINT) {
            try { // single sign-on for fb application
                PackageInfo info = context.getPackageManager().getPackageInfo(
                        Data.packageName,
                        PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.e("KeyHash:", ","
                            + Base64.encodeToString(md.digest(),
                            Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("error:", "," + e.toString());
            } catch (NoSuchAlgorithmException e) {
                Log.e("error:", "," + e.toString());
            }
        }
    }
}

