package com.scooupetest;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;

import org.json.JSONObject;

import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class TippingDialog implements OnClickListener {

    private static final int iTAP_TO_PAY_BUTTON = R.id.btnTapToPay;

    private static final float TRANSLUCENCY = 0.7f;

    private static double TOTAL_PAYABLE_AMOUNT = 0;

    private Activity activity;

    private Dialog tippingDialog;

    private TextView tvFare;
    private TextView tvTotalFareSplit;
    private TextView tvTipPercentage;
    private SeekBar sbTippingSetter;
    private Button btnTapToPay;
    private String totalFare = "0.00";
    private String paymentMethod;
    private int TIPPING_AMOUNT_IN_CENTS = 0;
    private String currentTippingAmount = "0";

    public TippingDialog(Activity activity) {
        this.activity = activity;
    }

    /**
     * Initial setup of the Dialog
     *
     * @param totalFare
     */
    public void showDialog(String totalFare, String paymentMethod) {

        if (!totalFare.equalsIgnoreCase(""))
            this.totalFare = totalFare;

        this.paymentMethod = paymentMethod;
        tippingDialog = new Dialog(activity,
                android.R.style.Theme_Translucent_NoTitleBar);
        Window dialogWindow = tippingDialog.getWindow();
        tippingDialog.setContentView(R.layout.tipping_layout);

        new ASSL(activity,
                (RelativeLayout) tippingDialog
                        .findViewById(R.id.rlTippingLayout), 1134, 720, true);

        WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
        layoutParams.dimAmount = Data.dialogDimAmount;

        dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        tippingDialog.setCancelable(false);
        tippingDialog.setCanceledOnTouchOutside(false);

        initViews();

        tippingDialog.show();

    }

    /**
     * Initializes all the Views used inside the Layout of the Activity
     */
    private void initViews() {


        tvFare = (TextView) tippingDialog.findViewById(R.id.tvFare);
        tvFare.setTypeface(Data.getFont(activity), Typeface.BOLD);
        RelativeLayout rlcross = (RelativeLayout) tippingDialog.findViewById(R.id.RlCross);
        if (Data.discount > 0) {
            ImageView imgFreeRideIcon = (ImageView) tippingDialog
                    .findViewById(R.id.passengerFreeRideIcon);
            imgFreeRideIcon.setVisibility(View.VISIBLE);

        }
        tvFare.setText(activity.getResources().getString(R.string.currency_symbol) + String.format(Locale.US,"%.2f", (Double.parseDouble(totalFare) - Data.discount)));

        tvTotalFareSplit = (TextView) tippingDialog
                .findViewById(R.id.tvTotalFareSplit);

        tvTotalFareSplit.setTypeface(Data.getFont(activity));

        TOTAL_PAYABLE_AMOUNT = Double.valueOf(totalFare);

        tvTotalFareSplit.setText(activity.getResources().getString(R.string.currency_symbol) + totalFare + " + "
                + activity.getResources().getString(R.string.currency_symbol) + getTippingAmount(0) + " "+activity.getString(R.string.tip));

        tvTipPercentage = (TextView) tippingDialog
                .findViewById(R.id.tvTipPercentage);
        tvTipPercentage.setTypeface(Data.getFont(activity));
        tvTipPercentage.setText(""+activity.getString(R.string.tip)+" = 0%");

        btnTapToPay = (Button) tippingDialog.findViewById(iTAP_TO_PAY_BUTTON);
        btnTapToPay.setTypeface(Data.getFont(activity));
        btnTapToPay.setOnClickListener(this);

        sbTippingSetter = (SeekBar) tippingDialog
                .findViewById(R.id.sbTippingSetter);
        sbTippingSetter.setProgress(0);

        rlcross.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                HomeActivity.tipPaymentListener.setPaymentButtonLayout();
                tippingDialog.dismiss();
            }
        });
        sbTippingSetter
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {

                        Log.v("SEEKBAR_PROGRESS", "" + progress);
                        reflectValuesToUI(progress);
                    }
                });

        Log.v("TIPPING LAYOUT", "ALL VIEWS INITIALLIZED");

    }

    private String getTippingAmount(double percentage) {

        double tippingAmount = Double.valueOf(totalFare) * (percentage / 100);

        String tip = String.format(Locale.US,"%.2f", tippingAmount);

        TIPPING_AMOUNT_IN_CENTS = (int) (tippingAmount * 100);

        TOTAL_PAYABLE_AMOUNT = Double.valueOf(String.format(Locale.US,"%.2f",
                Double.valueOf(totalFare) + Double.valueOf(tip)));

        return tip;

    }

    @Override
    public void onClick(View button) {

        switch (button.getId()) {

            case iTAP_TO_PAY_BUTTON:
                payFare(activity);
                break;

        }

    }

    /**
     * Sets the Corresponding Values to the UI components
     *
     * @param progress The progress level in the {@link SeekBar}
     */
    private void reflectValuesToUI(int progress) {
        Log.v("SEEKBAR", "PROGRESS = " + progress);

        // int currentTippingPercentage = progress + INITIAL_TIPPING_AMOUNT;
        int currentTippingPercentage = progress;
        tvTipPercentage.setText(""+activity.getString(R.string.tip)+" = " + currentTippingPercentage + "%");
        Log.v("SEEKBAR", "TIP % = " + currentTippingPercentage);

        currentTippingAmount = getTippingAmount(currentTippingPercentage);
        Log.v("SEEKBAR", "TIPPING_AMOUNT = " + currentTippingAmount);

        tvTotalFareSplit.setText(activity.getResources().getString(R.string.currency_symbol) + totalFare + " + "
                + activity.getResources().getString(R.string.currency_symbol) + currentTippingAmount + " "+activity.getString(R.string.tip));
        tvFare.setText(activity.getResources().getString(R.string.currency_symbol)
                + String.format(Locale.US,"%.2f", Double.valueOf(TOTAL_PAYABLE_AMOUNT) - Data.discount));

    }


    /**
     * ASync for start ride in  driver mode from server
     */
    public void payFare(final Activity activity) {
        if (AppStatus.getInstance(activity.getApplicationContext()).isOnline(activity.getApplicationContext())) {


            DialogPopup.showLoadingDialog(activity, activity.getString(R.string.loading));

            Log.i("access_token", "=" + Data.userData.accessToken);
            Log.i("engagement_id", "=" + Data.cEngagementId);
            Log.i("tip", "=" + currentTippingAmount);
            Log.i("payment_method", "=" + paymentMethod);
            Log.i("to_pay", "=" + String.format(Locale.US,"%.2f", Double.valueOf(TOTAL_PAYABLE_AMOUNT) - Data.discount));


            RestClient.getApiService().payFare(Data.userData.accessToken, currentTippingAmount, "" + Data.cEngagementId, paymentMethod, "" + String.format(Locale.US,"%.2f", Double.valueOf(TOTAL_PAYABLE_AMOUNT) - Data.discount), new Callback<String>() {
                private JSONObject jObj;

                @Override
                public void success(String response, Response r) {
                    Log.e("Server response", "response = " + response);

                    Data.tip = Double.parseDouble(currentTippingAmount);
                    try {
                        jObj = new JSONObject(response);

                        if (!jObj.isNull("error")) {

                            String errorMessage = jObj.getString("error");

                            if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                HomeActivity.logoutUser(activity);
                            } else {
                                new DialogPopup().alertPopup(activity, "", errorMessage);
                            }
                        } else {

                            tippingDialog.dismiss();
                            Data.defaulterFlag = "0";
                            HomeActivity.tipPaymentListener.setPaymentButtonLayout();

                            HomeActivity.tipPaymentListener
                                    .setFareWithTip(
                                            currentTippingAmount,
                                            String.format(Locale.US,
                                                    "%.2f",
                                                    Double.valueOf(TOTAL_PAYABLE_AMOUNT)));
                            new DialogPopup().alertPopup(activity, "",
                                    jObj.getString("log"));
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                    }

                    DialogPopup.dismissLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("request fail", error.toString());
                    DialogPopup.dismissLoadingDialog();
                    new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                }
            });
        } else {

            new DialogPopup().alertPopup(activity, "", activity.getString(R.string.check_internet_message));

        }

    }


}
