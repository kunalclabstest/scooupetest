package com.scooupetest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class AppliedPromoCodes {

    @Expose
    private String log;
    @Expose
    private Integer flag;
    @SerializedName("promo_data")
    @Expose
    private List<PromoData> promoData = new ArrayList<PromoData>();

    /**
     * @return The log
     */
    public String getLog() {
        return log;
    }

    /**
     * @param log The log
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
     * @return The flag
     */
    public Integer getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * @return The promoData
     */
    public List<PromoData> getPromoData() {
        return promoData;
    }

    /**
     * @param promoData The promo_data
     */
    public void setPromoData(List<PromoData> promoData) {
        this.promoData = promoData;
    }

}