package com.scooupetest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PromoData {


    @SerializedName("promo_id")
    @Expose
    private String promoId;
    @SerializedName("is_default")
    @Expose
    private Integer isDefault;
    @SerializedName("promo_image")
    @Expose
    private String promoImage;
    @Expose
    private String description;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;


    public String getPromoId() {
        return promoId;
    }

    /**
     * @return The isDefault
     */
    public Integer getIsDefault() {
        return isDefault;
    }

    /**
     * @param isDefault The is_default
     */
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * @return The promoImage
     */
    public String getPromoImage() {
        return promoImage;
    }

    /**
     * @param promoImage The promo_image
     */
    public void setPromoImage(String promoImage) {
        this.promoImage = promoImage;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The expiryDate
     */
    public String getExpiryDate() {
        String formattedTime = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat output = new SimpleDateFormat("dd MMMM");
            Date d = sdf.parse(expiryDate);
            formattedTime = output.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "Valid till " + formattedTime;
    }


    /**
     * @param expiryDate The promo_image
     */
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

}