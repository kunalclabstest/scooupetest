package com.scooupetest.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ListCreditCard {

    @Expose
    private Integer id;
    @SerializedName("last4_digits")
    @Expose
    private String last4Digits;
    @SerializedName("payment_type_flag")
    @Expose
    private Integer paymentTypeFlag;
    @SerializedName("default_card")
    @Expose
    private Integer defaultCard;
    @SerializedName("card_added_on")
    @Expose
    private String cardAddedOn;
    @SerializedName("corp_email")
    @Expose
    private String corpEmail;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The last4Digits
     */
    public String getLast4Digits() {
        return last4Digits;
    }

    /**
     * @param last4Digits The last4_digits
     */
    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    /**
     * @return The paymentTypeFlag
     */
    public Integer getPaymentTypeFlag() {
        return paymentTypeFlag;
    }

    /**
     * @param paymentTypeFlag The payment_type_flag
     */
    public void setPaymentTypeFlag(Integer paymentTypeFlag) {
        this.paymentTypeFlag = paymentTypeFlag;
    }

    /**
     * @return The defaultCard
     */
    public Integer getDefaultCard() {
        return defaultCard;
    }

    /**
     * @param defaultCard The default_card
     */
    public void setDefaultCard(Integer defaultCard) {
        this.defaultCard = defaultCard;
    }

    /**
     * @return The cardAddedOn
     */
    public String getCardAddedOn() {
        return cardAddedOn;
    }

    /**
     * @param cardAddedOn The card_added_on
     */
    public void setCardAddedOn(String cardAddedOn) {
        this.cardAddedOn = cardAddedOn;
    }

    /**
     * @return The corpEmail
     */
    public String getCorpEmail() {
        return corpEmail;
    }

    /**
     * @param corpEmail The corp_email
     */
    public void setCorpEmail(String corpEmail) {
        this.corpEmail = corpEmail;
    }

}