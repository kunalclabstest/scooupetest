package com.scooupetest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AvailablePaymentMethods {

    @SerializedName("list_credit_cards")
    @Expose
    private List<ListCreditCard> listCreditCards = new ArrayList<ListCreditCard>();

    /**
     * @return The listCreditCards
     */
    public List<ListCreditCard> getListCreditCards() {
        return listCreditCards;
    }

    /**
     * @param listCreditCards The list_credit_cards
     */
    public void setListCreditCards(List<ListCreditCard> listCreditCards) {
        this.listCreditCards = listCreditCards;
    }

}