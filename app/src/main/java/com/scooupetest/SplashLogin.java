package com.scooupetest;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.scooupetest.customlayouts.DialogPopup;
import com.scooupetest.facebook.FacebookLoginCallback;
import com.scooupetest.facebook.FacebookLoginHelper;
import com.scooupetest.locationfiles.LocationFetcher;
import com.scooupetest.locationfiles.LocationUpdate;
import com.scooupetest.retrofit.RestClient;
import com.scooupetest.utils.AppStatus;
import com.scooupetest.utils.BaseActivity;
import com.scooupetest.utils.DeviceTokenGenerator;
import com.scooupetest.utils.FlurryEventLogger;
import com.scooupetest.utils.IDeviceTokenReceiver;
import com.scooupetest.utils.Log;
import com.scooupetest.utils.Utils;
import com.scooupetest.utils.Validation;
import com.scooupetest.utils.Validation.TextType;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rmn.androidscreenlibrary.ASSL;

public class SplashLogin extends BaseActivity implements LocationUpdate {

    static String emailFromForgotPassword = "";
    TextView title;
    Button backBtn;
    AutoCompleteTextView emailEt;
    EditText passwordEt;
    Button signInBtn, forgotPasswordBtn, facebookSignInBtn;
    TextView extraTextForScroll;
    TextView orText;
    LinearLayout relative;
    boolean loginDataFetched = false, facebookRegister = false, sendToOtpScreen = false;
    int otpFlag = 0;
    String phoneNoOfLoginAccount = "";
    String enteredEmail = "";
    FacebookLoginCallback facebookLoginCallback = new FacebookLoginCallback() {
        @Override
        public void facebookLoginDone() {
            sendFacebookLoginValues(SplashLogin.this);
            FlurryEventLogger.facebookLoginClicked(Data.fbId);
        }
    };

    public static boolean isSystemPackage(PackageInfo pkgInfo) {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
                : false;
    }

    public void resetFlags() {
        loginDataFetched = false;
        facebookRegister = false;
        sendToOtpScreen = false;
        otpFlag = 0;
    }

    // *****************************Used for flurry work***************//
    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
        FlurryAgent.onEvent("Login started");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_login);

        resetFlags();

        enteredEmail = "";

        relative = (LinearLayout) findViewById(R.id.relative);
        new ASSL(SplashLogin.this, relative, 1134, 720, false);

        title = (TextView) findViewById(R.id.title);
        title.setTypeface(Data.getFont(getApplicationContext()));
        backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setTypeface(Data.getFont(getApplicationContext()));

        emailEt = (AutoCompleteTextView) findViewById(R.id.emailEt);
        emailEt.setTypeface(Data.getFont(getApplicationContext()));
        passwordEt = (EditText) findViewById(R.id.passwordEt);
        passwordEt.setTypeface(Data.getFont(getApplicationContext()));

        signInBtn = (Button) findViewById(R.id.signInBtn);
        signInBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        forgotPasswordBtn = (Button) findViewById(R.id.forgotPasswordBtn);
        forgotPasswordBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);
        facebookSignInBtn = (Button) findViewById(R.id.facebookSignInBtn);
        facebookSignInBtn.setTypeface(Data.getFont(getApplicationContext()), Typeface.BOLD);

        extraTextForScroll = (TextView) findViewById(R.id.extraTextForScroll);

        orText = (TextView) findViewById(R.id.orText);
        orText.setTypeface(Data.getFont(getApplicationContext()));


        Validation validation = new Validation();
        validation.setValidationFilter(TextType.Password, passwordEt);


        String[] emails = Database.getInstance(this).getEmails();


        ArrayAdapter<String> adapter;

        if (emails == null) {                                                                            // if emails from database are not null
            emails = new String[]{};
            adapter = new ArrayAdapter<String>(this, R.layout.dropdown_textview, emails);
        } else {                                                                                        // else reinitializing emails to be empty
            adapter = new ArrayAdapter<String>(this, R.layout.dropdown_textview, emails);
        }

        adapter.setDropDownViewResource(R.layout.dropdown_textview);                    // setting email array to EditText DropDown list
        emailEt.setAdapter(adapter);
        emailEt.setText(emailFromForgotPassword);
        emailEt.clearFocus();
        emailFromForgotPassword = "";

        signInBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String email = emailEt.getText().toString().trim();
                String password = passwordEt.getText().toString().trim();
                if ("".equalsIgnoreCase(email)) {
                    emailEt.requestFocus();
                    emailEt.setError("Please enter email");
                } else {
                    if ("".equalsIgnoreCase(password)) {
                        passwordEt.requestFocus();
                        passwordEt.setError("Please enter password");
                    } else {
                        if (isEmailValid(email)) {
                            Utils.hideSoftKeyboard(SplashLogin.this, emailEt);
                            enteredEmail = email;
                            sendLoginValues(SplashLogin.this, email, password);
                            FlurryEventLogger.emailLoginClicked(email);
                        } else {
                            emailEt.requestFocus();
                            emailEt.setError("Please enter valid email");
                        }
                    }
                }
            }
        });


        forgotPasswordBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ForgotPasswordScreen.emailAlready = emailEt.getText().toString();
                startActivity(new Intent(SplashLogin.this, ForgotPasswordScreen.class));
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                performBackPressed();
            }
        });


        passwordEt.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        signInBtn.performClick();
                        break;

                    case EditorInfo.IME_ACTION_NEXT:
                        break;

                    default:
                }
                return true;
            }
        });


        facebookSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginDataFetched = false;
                new FacebookLoginHelper().openFacebookSession(SplashLogin.this, facebookLoginCallback, true);
//				new FacebookLoginCreator().openFacebookSession(SplashLogin.this, facebookLoginCallback, true);
            }
        });


        final View activityRootView = findViewById(R.id.mainLinear);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
                new OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        // r will be populated with the coordinates of your view
                        // that area still visible.
                        activityRootView.getWindowVisibleDisplayFrame(r);

                        int heightDiff = activityRootView.getRootView()
                                .getHeight() - (r.bottom - r.top);
                        if (heightDiff > 100) { // if more than 100 pixels, its
                            // probably a keyboard...

                            /************** Adapter for the parent List *************/

                            ViewGroup.LayoutParams params_12 = extraTextForScroll
                                    .getLayoutParams();

                            params_12.height = (int) (heightDiff);

                            extraTextForScroll.setLayoutParams(params_12);
                            extraTextForScroll.requestLayout();

                        } else {

                            ViewGroup.LayoutParams params = extraTextForScroll
                                    .getLayoutParams();
                            params.height = 0;
                            extraTextForScroll.setLayoutParams(params);
                            extraTextForScroll.requestLayout();

                        }
                    }
                });


        try {                                                                                        // to get AppVersion, OS version, country code and device name
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            Data.appVersion = pInfo.versionCode;
            Log.i("appVersion", Data.appVersion + "..");
            Data.osVersion = android.os.Build.VERSION.RELEASE;
            Log.i("osVersion", Data.osVersion + "..");
            Data.country = getApplicationContext().getResources().getConfiguration().locale.getCountry();
            Log.i("countryCode", Data.country + "..");
            Data.deviceName = (android.os.Build.MANUFACTURER + android.os.Build.MODEL).toString();
            Log.i("deviceName", Data.deviceName + "..");
        } catch (Exception e) {
            Log.e("error in fetching appversion and gcm key", ".." + e.toString());
        }


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        try {
            if (getIntent().hasExtra("back_from_otp") && !RegisterScreen.facebookLogin) {
                emailEt.setText(OTPConfirmScreen.emailRegisterData.emailId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        new DeviceTokenGenerator(this).generateDeviceToken(this, new IDeviceTokenReceiver() {

            @Override
            public void deviceTokenReceived(final String regId) {
                Data.deviceToken = regId;
                Log.e("deviceToken in IDeviceTokenReceiver", Data.deviceToken + "..");
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Data.locationFetcher == null) {
            Data.locationFetcher = new LocationFetcher(SplashLogin.this, 1000, 1);
        }


        int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resp != ConnectionResult.SUCCESS) {
            Log.e("Google Play Service Error ", "=" + resp);
            new DialogPopup().showGooglePlayErrorAlert(SplashLogin.this);
        } else {
            new DialogPopup().showLocationSettingsAlert(SplashLogin.this);
        }

    }

    @Override
    protected void onPause() {
        try {
            if (Data.locationFetcher != null) {
                Data.locationFetcher.destroy();
                Data.locationFetcher = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();

    }


    @Override
    public void onBackPressed() {
        performBackPressed();
        super.onBackPressed();
    }


    public void performBackPressed() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (Session.getActiveSession() != null) {
                        Session.getActiveSession().closeAndClearTokenInformation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Intent intent = new Intent(SplashLogin.this, SplashNewActivity.class);
        intent.putExtra("no_anim", "yes");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }


    boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    /**
     * ASync for login from server
     */
    public void sendLoginValues(final Activity activity, final String emailId, final String password) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            resetFlags();
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));

            Log.i("Server uRL", "=" + Data.SERVER_URL);
            Log.i("email", "=" + emailId);
            Log.i("password", "=" + password);
            Log.e("device_token", "=" + Data.deviceToken);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("country", "=" + Data.country);
            Log.i("device_name", "=" + Data.deviceName);
            Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);


            if (Data.locationFetcher != null) {
                Data.latitude = Data.locationFetcher.getLatitude();
                Data.longitude = Data.locationFetcher.getLongitude();
            }
            RestClient.getApiService().sendLoginValues(emailId, password, Data.DEVICE_TYPE, Data.uniqueDeviceId, Data.deviceToken, "" + Data.latitude, "" + Data.longitude, Data.country, Data.deviceName,
                    Data.appVersion, Data.osVersion, new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response r) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);
                                if (!jObj.isNull("error")) {
                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");

                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (0 == flag) { // {"error": 'some parameter missing',"flag":0}//error
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (1 == flag) { // {"error":"email not  registered","flag":1}//error
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (2 == flag) { // {"error":"incorrect password","flag":2}//error
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (3 == flag) { // {"error":"enter otp","flag":2}//error
                                        phoneNoOfLoginAccount = (jObj.has("phone_no")) ? (jObj.getString("phone_no")) : "";
                                        otpFlag = 0;
//                                        sendToOtpScreen = true;
                                        sendIntentToOtpScreen();
                                        Data.nounce = "";
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                    DialogPopup.dismissLoadingDialog();
                                } else {

                                    new JSONParser().parseLoginData(activity, response);

                                    Database.getInstance(SplashLogin.this).insertEmail(emailId);
                                    Database.getInstance(SplashLogin.this).close();
                                    accessTokenLogin(activity);

                                }

                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("request fail", error.toString());
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    /**
     * ASync for login from server
     */
    public void sendFacebookLoginValues(final Activity activity) {
        if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
            resetFlags();
            DialogPopup.showLoadingDialog(activity, getString(R.string.loading));
            Log.i("user_fb_id", "=" + Data.fbId);
            Log.i("user_fb_name", "=" + Data.fbFirstName + " " + Data.fbLastName);
            Log.i("fb_access_token", "=" + Data.fbAccessToken);
            Log.i("username", "=" + Data.fbUserName);
            Log.i("fb_mail", "=" + Data.fbUserEmail);
            Log.i("latitude", "=" + Data.latitude);
            Log.i("longitude", "=" + Data.longitude);
            Log.i("device_token in fb login", "=" + Data.deviceToken);
            Log.i("country", "=" + Data.country);
            Log.i("app_version", "=" + Data.appVersion);
            Log.i("os_version", "=" + Data.osVersion);
            Log.i("device_name", "=" + Data.deviceName);
            Log.i("device_type", "=" + Data.DEVICE_TYPE);
            Log.i("unique_device_id", "=" + Data.uniqueDeviceId);
            Log.i("Server link", "=" + Data.SERVER_URL + "/customer_fb_registeration_form");

            if (Data.locationFetcher != null) {
                Data.latitude = Data.locationFetcher.getLatitude();
                Data.longitude = Data.locationFetcher.getLongitude();
            }
            RestClient.getApiService().sendFacebookLoginValues(Data.fbId, Data.fbFirstName + " " + Data.fbLastName, Data.fbAccessToken, Data.fbUserName, Data.fbUserEmail,
                    Data.latitude, Data.longitude, Data.deviceToken, Data.country, Data.appVersion, Data.osVersion, Data.deviceName, Data.DEVICE_TYPE, Data.uniqueDeviceId, "",
                    "", "", "", 4, "", "", new Callback<String>() {
                        private JSONObject jObj;

                        @Override
                        public void success(String response, Response arg1) {
                            Log.i("Server response", "response = " + response);

                            try {
                                jObj = new JSONObject(response);
                                if (!jObj.isNull("error")) {
                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (2 == flag) { // {"error": 'Please enter otp',"flag":2}
                                        phoneNoOfLoginAccount = (jObj.has("phone_no")) ? (jObj.getString("phone_no")) : "";
                                        otpFlag = 1;
//                                        sendToOtpScreen = true;
                                        sendIntentToOtpScreen();
                                        Data.nounce = "";
                                    } else if (3 == flag) { // {"error": 'Please enter details',"flag":3}
                                        facebookRegister = true;
                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                    DialogPopup.dismissLoadingDialog();
                                } else {
                                    new JSONParser().parseLoginData(activity, response);
                                    Database.getInstance(SplashLogin.this).insertEmail(Data.fbUserEmail);
                                    Database.getInstance(SplashLogin.this).close();
                                    accessTokenLogin(activity);

                                }
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                                DialogPopup.dismissLoadingDialog();
                            }

                        }


                        @Override
                        public void failure(RetrofitError error) {
                            DialogPopup.dismissLoadingDialog();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        }
                    });
        } else {
            new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
        }

    }


    /**
     * Send intent to otp screen by making required data objects
     * flag 0 for email, 1 for Facebook
     */
    public void sendIntentToOtpScreen() {
        if (0 == otpFlag) {
            RegisterScreen.facebookLogin = false;
            OTPConfirmScreen.intentFromRegister = false;
            OTPConfirmScreen.emailRegisterData = new EmailRegisterData("", enteredEmail, phoneNoOfLoginAccount, "", "");
            startActivity(new Intent(SplashLogin.this, OTPConfirmScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        } else if (1 == otpFlag) {
            RegisterScreen.facebookLogin = true;
            OTPConfirmScreen.intentFromRegister = false;
            OTPConfirmScreen.facebookRegisterData = new FacebookRegisterData(phoneNoOfLoginAccount, "");
            startActivity(new Intent(SplashLogin.this, OTPConfirmScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            Session.getActiveSession().onActivityResult(this, requestCode,
                    resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);


        if (hasFocus && Data.isOpenGooglePlay) {
            Data.isOpenGooglePlay = false;
        } else if (hasFocus && loginDataFetched) {


            Map<String, String> articleParams = new HashMap<String, String>();
            articleParams.put("username", Data.userData.userName);
            FlurryAgent.logEvent("App Login", articleParams);

            loginDataFetched = false;
            startActivity(new Intent(SplashLogin.this, HomeActivity.class));

            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        } else if (hasFocus && facebookRegister) {
            facebookRegister = false;
            RegisterScreen.facebookLogin = true;
            startActivity(new Intent(SplashLogin.this, RegisterScreen.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        } else if (hasFocus && sendToOtpScreen) {
            sendIntentToOtpScreen();
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(relative);
        System.gc();
    }


    @Override
    public void onLocationChanged(Location location, int priority) {
        Data.latitude = location.getLatitude();
        Data.longitude = location.getLongitude();
    }


    /**
     * ASync for access token login from server
     */
    public void accessTokenLogin(final Activity activity) {

        SharedPreferences pref = getSharedPreferences(Data.SHARED_PREF_NAME, 0);
        final String accessToken = pref.getString(Data.SP_ACCESS_TOKEN_KEY, "");
        if (!"".equalsIgnoreCase(accessToken)) {
            if (AppStatus.getInstance(getApplicationContext()).isOnline(getApplicationContext())) {
                Double latitude, longitude;
                if (Data.locationFetcher != null) {
                    Data.latitude = Data.locationFetcher.getLatitude();
                    Data.longitude = Data.locationFetcher.getLongitude();
                }

                    latitude = Data.latitude;
                    longitude = Data.longitude;

                Log.i("accessToken", "=" + accessToken);
                Log.i("device_token", Data.deviceToken);
                Log.i("latitude", "" + Data.latitude);
                Log.i("longitude", "" + Data.longitude);
                Log.i("app_version", "" + Data.appVersion);
                Log.i("unique_device_id", "=" + Data.uniqueDeviceId);

                RestClient.getApiService().accessTokenLogin(accessToken, Data.deviceToken, latitude, longitude, Data.appVersion, Data.DEVICE_TYPE, Data.uniqueDeviceId, new Callback<String>() {
                    private JSONObject jObj;

                    @Override
                    public void success(String response, Response r) {
                        Log.e("Server response of access_token", "response = " + response);

                        try {
                            jObj = new JSONObject(response);
                            boolean newUpdate = false;
                            if (jObj.has("login")) {
                                newUpdate = SplashNewActivity.checkIfUpdate(jObj.getJSONObject("login"), activity);
                            }

                            if (!newUpdate) {
                                if (!jObj.isNull("error")) {


                                    //										{"error":"some parameter missing","flag":0}//error
                                    //										{"error":"invalid access token","flag":1}//error

                                    int flag = jObj.getInt("flag");
                                    String errorMessage = jObj.getString("error");
                                    if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
                                        HomeActivity.logoutUser(activity);
                                    } else if (0 == flag) { // {"error": 'some parameter missing',"flag":0}//error
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (1 == flag) { // {"error":"email not  registered","flag":1}//error
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    } else if (8 == flag) { // {"error":"email not  registered","flag":1}//error
                                        //											new DialogPopup().alertPopup(activity, "", errorMessage);


                                    } else {
                                        new DialogPopup().alertPopup(activity, "", errorMessage);
                                    }
                                    DialogPopup.dismissLoadingDialog();
                                } else {

                                    new AccessTokenDataParseAsync(activity, response, accessToken).execute();

                                }
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                            new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
                            DialogPopup.dismissLoadingDialog();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("request fail", error.toString());
                        DialogPopup.dismissLoadingDialog();
                        new DialogPopup().alertPopup(activity, "", Data.SERVER_NOT_RESOPNDING_MSG);
                        DialogPopup.dismissLoadingDialog();
                    }
                });
            } else {
                new DialogPopup().alertPopup(activity, "", getString(R.string.check_internet_message));
            }
        }


    }


    class AccessTokenDataParseAsync extends AsyncTask<String, Integer, String> {

        Activity activity;
        String response, accessToken;

        public AccessTokenDataParseAsync(Activity activity, String response, String accessToken) {
            this.activity = activity;
            this.response = response;
            this.accessToken = accessToken;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String resp = new JSONParser().parseAccessTokenLoginData(activity, response, accessToken);
                return resp;
            } catch (Exception e) {
                e.printStackTrace();
                return "SERVER_TIMEOUT";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.contains("SERVER_TIMEOUT")) {
                loginDataFetched = false;
                new DialogPopup().alertPopup(activity, "", Data.SERVER_ERROR_MSG);
            } else {

                loginDataFetched = true;

            }


            DialogPopup.dismissLoadingDialog();
        }

    }

}
